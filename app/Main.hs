{-# LANGUAGE TemplateHaskell #-}

-- TEST :! stack exec analyzer -- ./test/examples/Constraints.hs -c ./test/examples/constraints.yml

module Main where

import           AddTopTypeSig
import           Analysis.AstTraversal
import           Analysis.Halting
import           Analysis.Helper
import           Analysis.Constraints
import           Analysis.Recursion
import           Analysis.Queries
import           Analysis.Simplification
import           Analysis.Substitution
import           Analysis.UnfoldFold
import           Analysis.UnfoldFold2
import           Common.Applicative
import           Common.IO
import           Common.List
import           Common.Maybe
import           Common.String
import           Parsing.Parser

import           Control.Arrow
import           Control.Concatenative
import           Control.Lens hiding (both)
import           Control.Monad
import           Data.Either
import           Data.Function
import           Data.IntSet                  (IntSet)
import qualified Data.IntSet                  as IntSet
import           Data.List
import           Data.Maybe
import           Debug.Trace
import           System.Clock
import           System.Console.GetOpt
import           System.Directory
import           System.Environment
import           System.Exit
import           System.FilePath.Posix
import           System.IO
import           System.Process
import           Text.Parsec.Error
import           Text.Printf
import           Text.Regex.TDFA ((=~))

data SingleGlobal = SingleGlobal {
  _declGraph :: DeclGraph,
  _globOptions :: Options
  }

data Options = Options {
  _input :: IO String,
  _output :: String -> IO (),
  _outputFilePath :: Maybe FilePath,
  _jutgePath :: String,
  _amode :: AMode,
  _verbose :: Bool,
  _simplify :: Bool,
  _deepFuns :: Bool,
  _constraintsFile :: Maybe FilePath
  }

data JutgeFileReport = JutgeFileReport {
  _recursiveFunctions :: [String]         
  , _transformed :: [(String, Decl)]
  , _postDeclGraph :: DeclGraph
  , _preDeclGraph :: DeclGraph
  }

type JutgeFolderTestReport = [(FilePath, Bool)]

newtype JutgeFolderReport = JutgeFolderReport [JutgeFileReport]

data AMode = JutgeReport | JutgeTrans | JutgeTest | SingleFile | Interactive | JutgeCheck
           deriving (Eq, Show)


makeLenses ''SingleGlobal
makeLenses ''Options

outputln :: Options -> String -> IO ()
outputln opt = (>>_output opt "\n") . _output opt

outlv :: Options -> String -> IO ()
outlv opt = when (_verbose opt) . outputln opt

data InpCor = Inp Int | Cor Int deriving (Eq, Show,Ord)

readInpCor :: String -> Maybe (Int -> InpCor)
readInpCor "cor" = Just Cor
readInpCor "inp" = Just Inp
readInpCor _ = Nothing

testNum :: InpCor -> Int
testNum (Inp x) = x
testNum (Cor x) = x

main :: IO ()
main = do
  opts <- getArgs >>= compilerOpts
  clearOutputFile opts
  case _amode opts of
    Interactive -> do
       dg <- parseInput opts
       mainMenu SingleGlobal {_declGraph = dg, _globOptions = opts}
    SingleFile -> do
      dg <- parseInput opts
      doTransformAll SingleGlobal {_declGraph = dg, _globOptions = opts}
    JutgeReport -> do
      time <- timeAction_ (analyzeJutgeFolder opts >>= reportJutgeFolder opts)
      reportTime opts time
    JutgeTrans -> do
      time <- timeAction_ (analyzeJutgeFolder opts >>= exportJutgeFolder opts outFolder)
      reportTime opts time
    JutgeTest -> do
      time <- timeAction_ (testJutgeFolder opts >>= reportJutgeFolderTest opts)
      reportTime opts time
    JutgeCheck -> do
      dg <- parseInput opts
      mcstrs <- readConstraints . fromJust $ _constraintsFile opts
      case mcstrs of
        Just cstrs -> do
          let res = checkJutgeFile dg cstrs
          opts ^. output $ encodeToString YAML res
        Nothing -> error "There was an error reading the constraints file"
      --time <- timeAction_ (checkJutgeFile opts >>= reportJutgeFolderTest opts)
      --reportTime opts time
  

checkJutgeFile :: DeclGraph -> ModuleConstraints -> Infractions
checkJutgeFile dg cstrs = Infractions $ checkConstraint dg (cstrs ^. importsAllowed) ++
                          concatMap (checkConstraint dg) (cstrs ^. declConstraints)
    

-- returns transformed functions before transformation
transformedBefore :: JutgeFileReport -> [Decl]
transformedBefore JutgeFileReport {_preDeclGraph = g, _transformed = trans} =
  map (getDecl g) keys
  where
    keys = map (getName2node g.funBindName.snd) trans
    names = map (funBindName.snd) trans


defaultOptions :: Options
defaultOptions = Options {
  _input = getContents,
  _output = putStr,
  _outputFilePath = Nothing,
  _amode = SingleFile,
  _verbose = False,
  _simplify = True,
  _deepFuns = False,
  _jutgePath = "",
  _constraintsFile = Nothing
  }

ghciProc :: [FilePath] -> IO (Maybe Handle, Maybe Handle, Maybe Handle, ProcessHandle)
ghciProc load = createProcess (proc "ghci-7.6.3" (["-v0","-w", "-fobject-code", "-O"] ++ load)) 
   {std_in = CreatePipe, std_out = CreatePipe}


findTests :: Options -> IO [(FilePath, FilePath)]
findTests opts@Options{_jutgePath = jp} = do
  testsPath <- getCurrentDirectory <$>> (</> testsFolder)
  namesType <- getDirectoryContents testsPath <$>> mapMaybe parse
  -- contains pairs (inputfile, correctfile)
  let r = mapMaybe ispair $ groupBy ((==)`on`testNum.snd) $ sort namesType
  outlv opts ("Found " ++ show (length r) ++ " tests")
  return $ map (both (testsPath</>)) r
  where
    parse :: String -> Maybe (String, InpCor)
    parse x = case x =~ reg :: (String, String, String, [String]) of
      (_,name,_,[num, incor]) -> (readInpCor incor) <$>> \x -> (name, x (read num))
      _ -> Nothing
    ispair [(a,Inp _),(b,Cor _)] = Just (a,b)
    ispair [(b,Cor _),(a,Inp _)] = Just (a,b)
    ispair z = Nothing 
    reg = "test-([0-9])+.(inp|cor)\\>"

  
runTest :: FilePath -> FilePath -> FilePath -> IO Bool 
runTest hs inp corr = do
  putStr $ "testing " ++ hs ++ " | " ++ takeFileName inp
  (Just inh, Just outh,_,proch) <- ghciProc [hs]
  readFile inp >>= hPutStr inh
  hClose inh
  res <- hGetContents outh
  correct <- readFile corr
  -- hClose outh
  let r = res == correct
  waitForProcess proch
  unless r $ do
  --  putStrLn "correct:"
  --  putStrLn correct
    when collectErrors $ appendFile "errors" (hs ++ " " ++ takeFileName inp ++ "\n")
    putStrLn "\nout:"
    putStrLn res
  putStrLn $ " " ++ show r
  return r 


collectErrors :: Bool
collectErrors = True

testJutgeFolder :: Options -> IO JutgeFolderTestReport
testJutgeFolder opts@Options{_jutgePath = p} = do
  setCurrentDirectory p 
  acs <- acceptedFiles "."  -- <$>> take 1 . drop 21
  putStrLn $ "acs = " ++ show acs
  tests <- findTests opts -- >>= putStr. unlines . (map show)
  when collectErrors $ writeFile "errors" ""
  res <- sequence [runTest ac inp cor <$>> (,) ac | ac <- acs, (inp, cor) <- tests]
  return res

  

reportJutgeFolderTest :: Options -> JutgeFolderTestReport -> IO ()
reportJutgeFolderTest opts@Options{_output = output} rep = do
  outl $ "\nPassed Tests: " ++ numPerCent (length passed) (length rep)
  outl $ "Failed Tests: " ++ numPerCent (length rep  - length passed) (length rep)
  where
    outl = outputln opts
    passed = filter snd rep

optionsDescr :: [OptDescr (Options -> IO Options)]
optionsDescr =
  [ Option ['i']     ["interactive"]
    (NoArg (\ opts -> return opts { _amode = Interactive }))
    "Interactive session. Allows selecting single definitions and make queries about them"
  ,  Option ['r'] ["jutge-report"]
     (NoArg (\opts -> return opts {_amode = JutgeReport}))
    $ unlines ["Analyzes a folder with jutge.org files and reports statistics",
              "the folder must contain a file named functions.txt containing the names of the functions to analyze"]
  , Option ['j'] ["jutge-trans"]
    (NoArg (\a -> return a {_amode = JutgeTrans}))
    ("Analyzes a folder with jutge.org files and writes the transformed" ++
    " in the 'out' folder")
  , Option ['t'] ["jutge-test"]
    (NoArg (\a -> return a {_amode = JutgeTest}))
    "Uses testfiles in tests/ to test all problems in a folder"
  , Option ['o'] ["outpath"]
    (ReqArg (\file a -> return a {_output = appendFile file,
                               _outputFilePath = Just file}) "FILE")
    "Output path. If no output file is specified, stdout is used"
  , Option ['v'] ["verbose"]
    (NoArg (\opts -> return opts {_verbose = True}))
    "Enables verbose messages"
  , Option ['d'] ["_deepFuns"]
    (NoArg (\opts -> return opts {_deepFuns = True}))
    "Also transform inner declarations (only available in interactive mode)"
  , Option ['n'] ["nosimplify"]
    (NoArg (\opts -> return opts {_simplify = False}))
    "Disables simplifications"
  , Option ['c'] ["check-constraints"]
    (ReqArg (\file a -> return a {_amode = JutgeCheck, _constraintsFile = Just file}) "CSTR-FILE")
    "Checks if a module respects all the constraints"
  , Option "h" ["help"]
    (NoArg $
     const $ do
         let header = "Usage: analyzer [OPTION...] module.hs"
         hPutStrLn stderr (usageInfo header optionsDescr)
         exitSuccess)
    "Show this help"
    ]

compilerOpts :: [String] -> IO Options
compilerOpts argv =
  case getOpt Permute optionsDescr argv of
   (o,[],[]) -> foldM (flip id) defaultOptions o
   (o, [file], []) -> foldM (flip id) defaultOptions o
                      <$>> \op -> op{_input = readFile file, _jutgePath = file}
   (o,files,[]) -> putStrLn "WARNING: Giving multiple input files is not advised"
                   >> foldM (flip id) defaultOptions o
                   <$>> \op->op{_input = concat <$> mapM readFile files, _jutgePath = head files}
   (_,_,errs) -> ioError (userError (concat errs ++ usageInfo header optionsDescr))
     where header = "Usage: analyzer [OPTION...] module.hs"

outFolder :: FilePath
outFolder = "out"

testsFolder :: FilePath
testsFolder = "tests"


reportTime :: Options -> TimeSpec -> IO ()
reportTime opts t = 
  outl (box "Total Execution Time: " ++ showTimeSpec t)
  where outl = outputln opts
        showTimeSpec TimeSpec{sec = s, nsec = n} =
          show s ++ "." ++ take prec (show n) ++ "s" 
          where prec = 3
  

clearOutputFile :: Options -> IO ()
clearOutputFile Options{_outputFilePath = file}= do
    case file of
     Just f -> writeFile f ""
     _ -> return ()


parseInput :: Options -> IO DeclGraph
parseInput opts = do
  parseRes <- _input opts <$>> parseModule "input"
  case parseRes of
   Left err -> ioError (userError $ show err)
   Right decls -> do
     when (_verbose opts) $ putStrLn "parse Ok"
     return $ mkDeclGraph decls


mainMenu :: SingleGlobal -> IO ()
mainMenu glob = rmenu (mainMenu glob) options
  where options = [
          (selectDecl glob, "Select declaration"),
          (doTransformAll glob, "Transform all"),
          (printImports (glob ^. declGraph . imports), "Print imports")]


printImports :: [ImportDecl] -> IO ()
printImports = mapM_ (putStrLn . prettyPrint)

doTransformAll :: SingleGlobal -> IO ()
doTransformAll = mapM_ (putStrLn . (++"\n") . prettyDecl) . transformAll

transformAll :: SingleGlobal -> [Decl]
transformAll glob@SingleGlobal{
  _declGraph = g,
  _globOptions = Options{
    _output = output}} = do
  let funkeys = filter (isFunBind . getDecl g) $ IntSet.toList $ _nodesWithDecl g
  mapMaybe (transformOne glob) funkeys

-- |Attempts transformations in this order
-- 1 toFold
-- 2 tailRecursion (reverse trick)
-- 3 general tailRecursion
transformOne :: SingleGlobal -> Node -> Maybe Decl
transformOne g = fmap snd . transformOneWhich g

transformsNames = [toFoldTransName, tailTransName]

toFoldTransName = "toFold"
concatOptName = "ConcatOptimization"
                  
transformOneWhich :: SingleGlobal -> Node -> Maybe (String, Decl)
transformOneWhich SingleGlobal{
  _declGraph = g,
  _globOptions=Options{
    _output = output,
    _simplify = simplify}} node = do
  let
    transfs = [
      (concatOptName, transformConcat id),
      (toFoldTransName, foldTransform),
      (tailTransName, tailTransformWrapped)]
    res = listToMaybe $ mapMaybe (\(name, t) -> t g node <$>> (,) name) transfs
  case res of
   Just (tname, d) -> do
     let ud = d
     return $ (,) tname $ if simplify
       then last $ simplifyAll g _mapDecl ud
       else ud
   Nothing -> Nothing --putStrLn $ prettyPrint $ unLabelDecl (getDecl g node)

tailTransName :: String
tailTransName = "Tail"

selectDecl :: SingleGlobal -> IO ()
selectDecl glob@SingleGlobal{_declGraph = g} = do
  let decls = map (getDecl g) $ IntSet.toList $ candidateDecls glob
  node <- chooseOne False prettyDecl decls <$>> funBindName 
  processDecl glob node

candidateDecls :: SingleGlobal -> NodeSet
candidateDecls SingleGlobal{
  _declGraph = g,
  _globOptions = Options {
    _deepFuns = deep
    }}
  | deep = getFunNodes g
  | otherwise = getTopFunNodes g

processDecl :: SingleGlobal -> Name -> IO ()
processDecl glob@SingleGlobal{_declGraph = g} name = brmenu (selectDecl glob) options
  where
    node = getName2node g name
    options =
      [(askIsRecursive glob name, "Is recursive?"),
       (doTransform2 glob node, "Tail recursion transformation"),
       (doFoldTransform glob node, "To fold"),
       (doTransformConcat glob node, "Concat optimization (with reverse)"),
       (doQueryHalts glob node, "Find ranquing function (halts?)"), 
       (doReverseTransform glob node, "Reverse transformation"),
       (printDeclLabels glob node, "Print selected declaration with labels"),
       (printDecl glob node, "Print selected declaration")]


askIsRecursive :: SingleGlobal -> Name -> IO ()
askIsRecursive SingleGlobal{_declGraph = g} name = putStrLn (box $ boolToYesNo $ nameIsRecursive g name)

failMsg :: String
failMsg = "Could not apply the transformation"

doGenericTransform
  :: (DeclGraph -> t -> Maybe Decl) -> SingleGlobal -> t -> IO ()
doGenericTransform t SingleGlobal{_declGraph = g,
                                 _globOptions = Options{_verbose = verbose}} node = do
  let res = t g node
  case res of
   Just d -> do
     let d' = simplifyAll g _mapDecl $ unLabelDecl d
     putStrLn $ prettyPrint $ head d'
     when (lengthGT 1 d') $ do 
       separator1
       putStrLn "Simplified:"
       putStrLn $ if  verbose || True
         then unlines $ map prettyPrint $ tail d'
         else prettyPrint (last d')
   Nothing -> putStrLn $ box failMsg

doQueryHalts SingleGlobal{
  _declGraph = g,
  _globOptions = Options{_verbose=verb}} node = halts g node verb



doTransform1 :: SingleGlobal -> Node -> IO ()
doTransform1 = doGenericTransform $ transform1 id 

doTransformConcat :: SingleGlobal -> Node -> IO ()
doTransformConcat = doGenericTransform $ transformConcat id

doReverseTransform :: SingleGlobal -> Node -> IO ()
doReverseTransform g node = do
  target <- chooseOneAlias True alias
  doGenericTransform (reverseTransf target True) g node
  where
    alias = [(Nothing, "Always apply"),
             (Just True, "Target left recursion"),
             (Just False, "Target right recursion")]

doFoldTransform :: SingleGlobal -> Node -> IO ()
doFoldTransform = doGenericTransform foldTransform

doTransform2 :: SingleGlobal -> Node -> IO ()
doTransform2 = doGenericTransform tailTransformWrapped
  
printDecl :: SingleGlobal -> Node -> IO ()
printDecl glob@SingleGlobal{_declGraph = g} node = putStrLn $ prettyDecl $ getDecl g node

printDeclLabels :: SingleGlobal -> Node -> IO ()
printDeclLabels glob@SingleGlobal{_declGraph = g} node = putStrLn $ prettyPrint $ getDecl g node

analyzeJutgeFolder :: Options -> IO JutgeFolderReport
analyzeJutgeFolder
  opts@Options{_jutgePath = jutgePath, _verbose = verbose,
              _amode = amode} = do
  outl $ "Scanning " ++ jutgePath
  setCurrentDirectory jutgePath
  acs <- acceptedFiles "." >>= parse
  gs <- reportParse acs
  funs <- functions
  let fileReports = map (analyzeJutgeFile funs opts) gs
  return $ JutgeFolderReport fileReports
  where
    parse :: [FilePath] -> IO [Either ParseError DeclGraph]
    parse = mapM (mparseDeclGraphWithSigs' opts)-- mparseDeclGraph
    functions :: IO [String]
    functions = words <$> readFile "functions.txt"
    outl = outputln opts
    outlr = when (amode == JutgeReport) . outputln opts
    reportParse :: [Either ParseError DeclGraph] -> IO [DeclGraph]
    reportParse l = do
      outlr ("Total accepted submissions: " ++ show (length l))
      outlr ("Parse errors: " ++ show (length errors))
      when (not $ null errors)$ outl $ show $ head errors
      return success
      where
        (errors, success) = partitionEithers l

-- adds the missing signatures from the singatures.hs files
mparseDeclGraphWithSigs'
  :: Options -> FilePath -> IO (Either ParseError DeclGraph)
mparseDeclGraphWithSigs' Options{_amode = JutgeTrans} d = do
  withsigs <- addTopTypeSig d
  return $ mparseDeclGraphFromString d withsigs
mparseDeclGraphWithSigs' _ d = mparseDeclGraph d

reportJutgeFolder :: Options -> JutgeFolderReport -> IO ()
reportJutgeFolder opts@Options{_jutgePath = jutgePath}
  (JutgeFolderReport fileReports) = do
  reportRecursion fileReports
  reportTransforms fileReports
  where
    functions :: IO [String]
    functions = words <$> readFile "functions.txt"
    outl = outputln opts
    reportRecursion :: [JutgeFileReport] -> IO ()
    reportRecursion rs = do
      outl $ "\n" ++ box "Recursion Report"
      outl ("Submissions with recursive functions: "
            ++ numPerCent numFilesWithRec total ++ "\n")
      mapM_ reportFunRec countr
      outl ""
      where
        total = length rs
        countr = count $ concatMap _recursiveFunctions rs
        numFilesWithRec = length $ filter (not.null._recursiveFunctions) rs
        reportFunRec (name, k) = outl $ name ++
                                 " is implemented recursively in " ++
                                 numPerCent k total ++ " submissions"

    reportTransforms :: [JutgeFileReport] -> IO ()
    reportTransforms rs = do
      funs <- functions
      let numFuns = (total*) . length  $ funs
      outl $ "\n" ++ box "Transformations Report"
      outl $ "Transformations applied: " ++ numPerCent (length alltrans) numFuns ++ "\n"
      mapM_ (reportTrans numFuns funs) eachTransCount
      where
        alltrans = concatMap _transformed rs 
        total = length rs
        numtransforms = length alltrans
        eachTransCount = count (map fst alltrans)
        reportTrans :: Int -> [String] -> (String, Int) -> IO ()
        reportTrans numFuns funs (trans, k) = do
          outl ("Transformation " ++ trans ++ " applied " ++ numPerCent k numFuns ++ " times")
          mapM_ reportFun funs
          outl "\n+------------+\n"
          where
            -- how many times this transform was applied to each fun
            reportFun :: String -> IO ()
            reportFun fun = do
              --print $ map (nameStr.funBindName.snd) $ take 3 alltrans
              outl ("\t--> "++ fun ++": " ++ numPerCent numthis numFuns)
                where
                  numthis = length $ filter ((==fun).nameStr.funBindName.snd)
                            thistrans
                  thistrans = filter ((==trans).fst) alltrans
            

{-
:!./dist/build/analyzer/analyzer src/Tests/Data/P25054_ca -r
:!./dist/build/analyzer/analyzer src/Tests/Data/P25054_ca -j
:!./dist/build/analyzer/analyzer src/Tests/Data/P25054_ca/out -t
-}
f = analyzeJutgeFolder defaultOptions {
  _jutgePath = ps!!2
  }
  where ps = ["src/Tests/Data/P25054_ca", -- llistes
              "src/Tests/Data/P31745_ca", -- ordre superior 1
              "src/Tests/Data/P93632_ca"] -- ordre superior 2

t = runTest p inp cor
  where p = base </> "out/c61ab418dc2742840d9b6ddeab244123-AC.hs"
        base = "src/Tests/Data/P25054_ca"
        inp = base </> "tests/test-" ++ show k ++ ".inp"
        cor = base </> "tests/test-" ++ show k ++ ".cor"
        k = 2

acceptedFiles :: FilePath -> IO [FilePath]
acceptedFiles p = sort . filter isAC <$> getDirectoryContents p

analyzeJutgeFile :: [String] -> Options -> DeclGraph -> JutgeFileReport
analyzeJutgeFile snames opts g =
  JutgeFileReport {
    _recursiveFunctions = filter (nameIsRecursive g . Ident) snames    
    , _transformed = trans
    , _preDeclGraph = g
    , _postDeclGraph = g'
    }
  where keys = map (getName2node g . Ident) snames
        g' = mkDeclGraph (_sourcePath g, _imports g, newdecls)
        (notrans,trans) = first (map $ getDecl g) $ partitionEithers (map (maybeToEither transform) keys)
        newdecls = map snd trans ++ notrans ++ auxiliaryfuns ++ getTypeSigs g -- ++ transftypes ++ unchangedtypes
        transform = transformOneWhich SingleGlobal{_globOptions = opts,
                                                   _declGraph = g}
        -- functions not in functions.txt that are top level
        auxiliaryfuns = let topkeys = getTopBindedNodes g -- Set.fromList snames
                         in map (getDecl g)$ IntSet.toList $ topkeys IntSet.\\ IntSet.fromList keys
        -- nodes where the tail transf was applied
        transkeys :: [Node]
        transkeys = map (getName2node g . funBindName . snd) $ filter ((==tailTransName).fst) trans
        transnames = map (funBindName . snd) $ filter ((==tailTransName).fst) trans
        transftypes = mapMaybe (tailTransformType g) transkeys
          where
            fromJust' x = trace "sdfasdff" (fromJust x)
        unchangedtypes = filter ((`IntSet.notMember`set) . getName2node g . head . typeSigNames) (getTypeSigs g)  
          where
            set :: IntSet
            set = IntSet.fromList transkeys :: IntSet
                                               
exportJutgeFolder :: Options -> FilePath -> JutgeFolderReport -> IO ()
exportJutgeFolder opts folder (JutgeFolderReport fileReports) = do
  createDirectoryIfMissing True folder
  setCurrentDirectory folder
  mapM_ writeJutgeFile (filter (isJust . _sourcePath . _postDeclGraph) fileReports)
  putStr""
  where
    writeJutgeFile r@JutgeFileReport {_postDeclGraph = g} = do
      writeFile filename contents
      where filename = takeFileName (fromJust $ _sourcePath g)
            contents = unlines [before,replicate 50 '-', prettyModule g]
            before = comment $ unlines $ map prettyDecl $ transformedBefore r

prettyModule :: DeclGraph -> String
prettyModule DeclGraph{_node2sigMap = types, _topDecls = ds, _imports = imps} =
  unlines (map prettyPrint imps) ++
  --unlines (map prettyPrint $ nubOrd $ IntMap.elems types) ++
  unlines (intersperse "" $ map prettyDecl ds)


comment :: String -> String
comment = unlines . map ("-- "++) . lines 

isAC :: FilePath -> Bool
isAC = and . zipWith (==) (reverse "AC.hs") . reverse

perCent :: (Integral t) => t -> t -> Float
perCent a' t' = (a/t)*100
  where (a,t) = both fromIntegral (a', t')

numPerCent :: (Integral t, Show t) => t -> t -> String
numPerCent a t = show a ++ " " ++ perCentPar a t

perCentPar :: Integral t => t -> t -> String
perCentPar a t = "("  ++ printf "%0.3f" (perCent a t) ++ "%)"


moveErrors :: IO ()
moveErrors = do
  getCurrentDirectory >>= print
  setCurrentDirectory "./src/Tests/Data/P25054_ca/"
  files <- readFile "./out/errors" <$>> lines <$>> map (head.words)
  createDirectoryIfMissing False target
  mapM_ (\x -> copyFile x (target</>x)) files
  where target= "selection"
