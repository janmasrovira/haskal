module AddTopTypeSig (addTopTypeSig) where

import           Control.Concatenative
import           Data.Either.Unwrap
import           Data.List (deleteFirstsBy)
import           Language.Haskell.Exts.Pretty
import           Language.Haskell.Exts.Syntax
import           System.IO
import           System.Process

--Own
import           Parsing.Parser




-- |'addTopTypeSig' takes the name of a file which contains a correct Haskell module and returns its code with the missing top level type signatures in the original file prepended
addTopTypeSig :: FilePath -- ^name of the source file
                 -> IO String -- ^the code with all the top level type signatures
addTopTypeSig src = do
  (Just inh, Just outh, _, proch) <- createProcess (proc "ghci-7.6.3" ["-v0"]) 
   {std_in = CreatePipe, std_out = CreatePipe}
  putStrLn $ "adding signatures to " ++ src
  hPutStr inh $ unlines [":add " ++ src, ":browse"]
  hClose inh
  br <- hGetContents outh
  let allts = filter isTypeSignature $ parseDecls  br
  code <- readFile src
  let explts = filter isTypeSignature $ parseDecls code 
      extrats = biAp (concatMap unfoldTypeSig) (deleteFirstsBy eqTypeSig) allts explts
      strextrats = concatMap prettyPrint extrats
  waitForProcess proch
  return $ strextrats ++ separator ++ code 
   where separator = "\n\n{-\n " ++ replicate 30 '=' ++ "\n-}\n\n"


main :: IO ()
main = addTopTypeSig src >>= writeFile src_out
  where src = "tests/t1.hs"
        src_out = "tests/t1_.hs"


isTypeSignature :: Decl -> Bool
isTypeSignature TypeSig {} = True
isTypeSignature _ = False

eqTypeSig :: Decl -> Decl -> Bool
eqTypeSig (TypeSig _ a _) (TypeSig _ b _) = a == b

unfoldTypeSig :: Decl -> [Decl]
unfoldTypeSig ts@(TypeSig _ [_] _) = [ts]
unfoldTypeSig (TypeSig l ns t) = [TypeSig l [name] t | name <- ns]

