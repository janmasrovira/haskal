{-# LANGUAGE ScopedTypeVariables #-}

module Analysis.AstTraversal (mkAstMap, AstMap (..), MaybeMap,
                              emptyMapArgs, MaybeMapArgs (..),
                              mkAstMapAccum, AstMapAccum(..),
                              emptyMapAccumArgs, MaybeMapAccumArgs (..),
                              mkAstFold, AstFold(..),
                              emptyFoldArgs, MaybeFoldArgs (..),
                              MaybeFold,
                              accumToFold, foldToAccumId, combineMap,
                              combineFold, combineMapAccum) where

--Own
import           Analysis.Helper
import           Common.Applicative
import           Common.List
import           Common.Maybe
import           Parsing.Parser

-- Exts
import           Language.Haskell.Exts.Pretty
import           Language.Haskell.Exts.Syntax

-- Utilities
import           Control.Concatenative
import           Control.Monad
import           Data.Either.Unwrap
import           Data.List
import           Data.List.Split
import           Data.Map                     (Map)
import qualified Data.Map                     as Map
import           Data.Maybe
import qualified Data.Set                     as Set
import           Control.Lens                 hiding (List, both)


import           Debug.Trace

accumToFold_ :: (acc -> x -> (acc, y)) -> acc -> x -> acc
accumToFold_ =  (.) (fst .)

accumToFold :: (acc -> x -> (acc, y)) -> acc -> x -> acc
accumToFold f ac x =  fst $ f ac x


foldToAccumId :: (acc -> x -> acc) -> acc -> x -> (acc, x)
foldToAccumId f acc x = (f acc x, x)

foldToAccumIdM :: Monad m => (acc -> x -> m acc) -> acc -> x -> m (acc, x)
foldToAccumIdM f acc x = do
  fx <- f acc x
  return (fx, x)


type MaybeMap a = a -> Maybe a

data MaybeMapArgs = MaybeMapArgs
  {_maybeMapExp :: [MaybeMap Exp],
   _maybeMapPat :: [MaybeMap Pat],
   _maybeMapLName :: [MaybeMap Name],
   _maybeMapDecl :: [MaybeMap Decl],
   _maybeMapRhs :: [MaybeMap Rhs],
   _maybeMapMatch :: [MaybeMap Match],
   _maybeMapBinds :: [MaybeMap Binds],
   _maybeMapStmt :: [MaybeMap Stmt],
   _maybeMapAlt :: [MaybeMap Alt],
   _maybeMapQOp :: [MaybeMap QOp],
   _maybeMapGuardedRhs :: [MaybeMap GuardedRhs]}

data AstMap = AstMap {
  _mapArgs :: MaybeMapArgs, -- do NOT change this manually after creation
  _mapExp :: Exp -> Exp,
  _mapPat :: Pat -> Pat,
  _mapDecl :: Decl -> Decl,
  _mapLName :: Name -> Name, -- only for names on the left hand side (match name, typesig name, asPat)
  _mapRhs :: Rhs -> Rhs,
  _mapBinds :: Binds -> Binds,
  _mapMatch :: Match -> Match,
  _mapStmt :: Stmt -> Stmt,
  _mapAlt :: Alt -> Alt,
  _mapQOp :: QOp -> QOp,
  _mapGuardedRhs :: GuardedRhs -> GuardedRhs
  }

emptyMapArgs :: MaybeMapArgs
emptyMapArgs = MaybeMapArgs {
  _maybeMapExp = [], _maybeMapPat = [], _maybeMapLName = [],
  _maybeMapDecl = [], _maybeMapRhs = [], _maybeMapMatch = [],
  _maybeMapBinds = [], _maybeMapStmt = [], _maybeMapAlt = [],
  _maybeMapQOp = [], _maybeMapGuardedRhs = []
  }

combineMap :: [AstMap] -> AstMap
combineMap l = mkAstMap . combineMapArgs $ map _mapArgs l

combineMapArgs :: [MaybeMapArgs] -> MaybeMapArgs
combineMapArgs = foldr1 f
  where f MaybeMapArgs {
          _maybeMapExp = a1, _maybeMapPat = a2, _maybeMapLName = a3,
          _maybeMapDecl = a4, _maybeMapRhs = a5, _maybeMapMatch = a6,
          _maybeMapBinds = a7, _maybeMapStmt = a8, _maybeMapAlt = a9,
          _maybeMapQOp = a10, _maybeMapGuardedRhs = a11}
          MaybeMapArgs {
          _maybeMapExp = b1, _maybeMapPat = b2, _maybeMapLName = b3,
          _maybeMapDecl = b4, _maybeMapRhs = b5, _maybeMapMatch = b6,
          _maybeMapBinds = b7, _maybeMapStmt = b8, _maybeMapAlt = b9,
          _maybeMapQOp = b10, _maybeMapGuardedRhs = b11
          }
          = MaybeMapArgs {
          _maybeMapExp = a1 ++ b1, _maybeMapPat = a2 ++ b2, _maybeMapLName = a3 ++ b3,
          _maybeMapDecl = a4 ++ b4, _maybeMapRhs = a5 ++ b5, _maybeMapMatch = a6 ++ b6,
          _maybeMapBinds = a7 ++ b7, _maybeMapStmt = a8 ++ b8, _maybeMapAlt = a9 ++ b9,
          _maybeMapQOp = a10 ++ b10, _maybeMapGuardedRhs = a11 ++ b11
          }

mkAstMap :: MaybeMapArgs -> AstMap
mkAstMap args@MaybeMapArgs {
  _maybeMapExp = me, _maybeMapPat = mp, _maybeMapLName = mn,
  _maybeMapDecl = md, _maybeMapRhs = mr, _maybeMapMatch = mm,
  _maybeMapBinds = mb, _maybeMapStmt = ms, _maybeMapAlt = ma,
  _maybeMapQOp = mqo, _maybeMapGuardedRhs = mg} = astmap
  where
    headJust = head . catMaybes
    astmap = AstMap {
      _mapArgs = args,
      _mapExp = mape,
      _mapPat = mapp,
      _mapLName = mapn,
      _mapDecl = mapd,
      _mapStmt = maps,
      _mapBinds = mapb,
      _mapMatch = mapm,
      _mapRhs = mapr,
      _mapAlt = mapa,
      _mapQOp = mapqo,
      _mapGuardedRhs = mapgr
      }

    genericMap :: [MaybeMap a] -> (a -> a) -> a -> a
    genericMap mf defaultf e = headJust $ map ($ e) (mf ++ [Just . defaultf])

    mape = genericMap me de
    mapp = genericMap mp dp
    mapn = genericMap mn dn
    mapd = genericMap md dd
    mapr = genericMap mr dr
    mapb = genericMap mb db
    mapgr = genericMap mg dgr
    maps = genericMap ms ds
    mapm = genericMap mm dm
    mapa = genericMap ma da
    mapqs = genericMap [] dqs
    mapqo = genericMap mqo dqo

    dqo :: QOp -> QOp
    dqo = id

    dqs :: QualStmt -> QualStmt
    dqs (QualStmt s) = QualStmt (maps s)

    db :: Binds -> Binds
    db (BDecls decls) = BDecls $ map mapd decls

    de :: Exp -> Exp
    de (InfixApp a o b) = InfixApp (mape a) (mapqo o) (mape b)
    de (App a b) = biAp mape App a b
    de (NegApp a) = NegApp (mape a)
    de (Lambda s ps e) = Lambda s (map mapp ps) (mape e)
    de (Let b e) = Let (mapb b) (mape e)
    de (If a b c) = triAp mape If a b c
    de (Case e alts) = Case (mape e) (map mapa alts)
    de (Do sts) = Do (map maps sts)
    de (Tuple Boxed es) = Tuple Boxed (map mape es)
    de (List es) = List (map mape es)
    de (Paren e) = Paren (mape e)
    de (LeftSection e q) = LeftSection (mape e) (mapqo q)
    de (RightSection q e) = RightSection (mapqo q) (mape e)
    de (EnumFrom e) = EnumFrom (mape e)
    de (EnumFromTo a b) = biAp mape EnumFromTo a b
    de (EnumFromThen a b) = biAp mape EnumFromThen a b
    de (EnumFromThenTo a b c) = triAp mape EnumFromThenTo a b c
    de (ListComp e qss) = ListComp (mape e) (map mapqs qss)
    de (ExpTypeSig l e t) = ExpTypeSig l (mape e) t
    de e = e

    dp :: Pat -> Pat
    dp (PApp q ps) = PApp q $ map mapp ps
    dp (PTuple Boxed ps) = PTuple Boxed $ map mapp ps
    dp (PParen p) = PParen $ mapp p
    dp (PList ps) = PList $ map mapp ps
    dp (PAsPat n p) = PAsPat (mapn n) (mapp p)
    dp (PInfixApp a q b) = PInfixApp (mapp a) q (mapp b)
    dp (PVar n) = PVar $ mapn n
    dp p = p

    dn :: Name -> Name
    dn = id

    dd :: Decl -> Decl
    dd (TypeSig l ns t) = TypeSig l (map mapn ns) t
    dd (FunBind ms) = FunBind $ map mapm ms
    dd (PatBind l p r b) = PatBind l (mapp p) (mapr r) (fmap mapb b)
    dd d = d

    dm :: Match -> Match
    dm (Match l n ps t r b) = Match l (mapn n) (map mapp ps) t (mapr r) (fmap mapb b)

    dr :: Rhs -> Rhs
    dr (UnGuardedRhs e) = UnGuardedRhs $ mape e
    dr (GuardedRhss gs) = GuardedRhss $ map mapgr gs

    dgr :: GuardedRhs -> GuardedRhs
    dgr (GuardedRhs l sts e) = GuardedRhs l (map maps sts) (mape e)

    ds :: Stmt -> Stmt
    ds (Qualifier e) = Qualifier $ mape e
    ds (Generator l p e) = Generator l (mapp p) (mape e)
    ds (LetStmt b) = LetStmt (mapb b)

    da :: Alt -> Alt
    da (Alt l p r b) = Alt l (mapp p) (mapr r) (fmap mapb b)

---------------------------------------

type MaybeMapAccum a acc = acc -> a -> Maybe (acc, a)

data MaybeMapAccumArgs acc =
  MaybeMapAccumArgs
  {maybeMapAccumExp :: [MaybeMapAccum Exp acc],
   maybeMapAccumPat :: [MaybeMapAccum Pat acc],
   maybeMapAccumLName :: [MaybeMapAccum Name acc],
   maybeMapAccumDecl :: [MaybeMapAccum Decl acc],
   maybeMapAccumRhs :: [MaybeMapAccum Rhs acc],
   maybeMapAccumMatch :: [MaybeMapAccum Match acc],
   maybeMapAccumBinds :: [MaybeMapAccum Binds acc],
   maybeMapAccumStmt :: [MaybeMapAccum Stmt acc],
   maybeMapAccumAlt :: [MaybeMapAccum Alt acc],
   maybeMapAccumQOp :: [MaybeMapAccum QOp acc],
   maybeMapAccumGuardedRhs :: [MaybeMapAccum GuardedRhs acc]}


data AstMapAccum acc = AstMapAccum {
  mapAccumArgs :: MaybeMapAccumArgs acc,
  mapAccumExp :: acc -> Exp -> (acc, Exp),
  mapAccumPat :: acc -> Pat -> (acc, Pat),
  mapAccumDecl :: acc -> Decl -> (acc, Decl),
  mapAccumLName :: acc -> Name -> (acc, Name), -- only for names on the left hand side (match name, typesig name, asPat)
  mapAccumRhs :: acc -> Rhs -> (acc, Rhs),
  mapAccumBinds :: acc -> Binds -> (acc, Binds),
  mapAccumMatch :: acc -> Match -> (acc, Match),
  mapAccumStmt :: acc -> Stmt -> (acc, Stmt),
  mapAccumAlt :: acc -> Alt -> (acc, Alt),
  mapAccumQOp :: acc -> QOp -> (acc, QOp),
  mapAccumGuardedRhs :: acc -> GuardedRhs -> (acc, GuardedRhs)
  }

emptyMapAccumArgs :: MaybeMapAccumArgs acc
emptyMapAccumArgs = MaybeMapAccumArgs {
  maybeMapAccumExp = [], maybeMapAccumPat = [], maybeMapAccumLName = [],
  maybeMapAccumDecl = [], maybeMapAccumRhs = [], maybeMapAccumMatch = [],
  maybeMapAccumBinds = [], maybeMapAccumStmt = [], maybeMapAccumAlt = [],
  maybeMapAccumQOp = [], maybeMapAccumGuardedRhs = []
  }

combineMapAccum :: [AstMapAccum acc] -> AstMapAccum acc
combineMapAccum l = mkAstMapAccum . combineMapAccumArgs $ map mapAccumArgs l

combineMapAccumArgs :: [MaybeMapAccumArgs acc] -> MaybeMapAccumArgs acc
combineMapAccumArgs = foldr1 f
  where f MaybeMapAccumArgs {
          maybeMapAccumExp = a1, maybeMapAccumPat = a2, maybeMapAccumLName = a3,
          maybeMapAccumDecl = a4, maybeMapAccumRhs = a5, maybeMapAccumMatch = a6,
          maybeMapAccumBinds = a7, maybeMapAccumStmt = a8, maybeMapAccumAlt = a9,
          maybeMapAccumQOp = a10, maybeMapAccumGuardedRhs = a11}
          MaybeMapAccumArgs {
          maybeMapAccumExp = b1, maybeMapAccumPat = b2, maybeMapAccumLName = b3,
          maybeMapAccumDecl = b4, maybeMapAccumRhs = b5, maybeMapAccumMatch = b6,
          maybeMapAccumBinds = b7, maybeMapAccumStmt = b8, maybeMapAccumAlt = b9,
          maybeMapAccumQOp = b10, maybeMapAccumGuardedRhs = b11
          }
          = MaybeMapAccumArgs {
          maybeMapAccumExp = a1 ++ b1, maybeMapAccumPat = a2 ++ b2, maybeMapAccumLName = a3 ++ b3,
          maybeMapAccumDecl = a4 ++ b4, maybeMapAccumRhs = a5 ++ b5, maybeMapAccumMatch = a6 ++ b6,
          maybeMapAccumBinds = a7 ++ b7, maybeMapAccumStmt = a8 ++ b8, maybeMapAccumAlt = a9 ++ b9,
          maybeMapAccumQOp = a10 ++ b10, maybeMapAccumGuardedRhs = a11 ++ b11
          }


mkAstMapAccum :: forall acc . MaybeMapAccumArgs acc -> AstMapAccum acc
mkAstMapAccum args@MaybeMapAccumArgs {
  maybeMapAccumExp = me, maybeMapAccumPat = mp, maybeMapAccumLName = mn,
  maybeMapAccumDecl = md, maybeMapAccumRhs = mr, maybeMapAccumMatch = mm,
  maybeMapAccumBinds = mb, maybeMapAccumStmt = ms, maybeMapAccumAlt = ma,
  maybeMapAccumQOp = mqo, maybeMapAccumGuardedRhs = mg} = astmap
  where
    headJust = head . catMaybes
    astmap = AstMapAccum {
      mapAccumArgs = args,
      mapAccumExp = mape,
      mapAccumPat = mapp,
      mapAccumLName = mapn,
      mapAccumDecl = mapd,
      mapAccumStmt = maps,
      mapAccumBinds = mapb,
      mapAccumMatch = mapm,
      mapAccumRhs = mapr,
      mapAccumAlt = mapa,
      mapAccumQOp = mapqo,
      mapAccumGuardedRhs = mapgr
      }

    mapAccum = mapAccumL

    genericAccumMap :: [(acc -> a -> Maybe (acc, a))] ->
                       (acc -> a -> (acc, a)) ->
                       acc -> a -> (acc, a)
    genericAccumMap mf defaultf ac e = headJust $ map (\f -> f ac e)
                                       (mf ++ [\ ac x -> Just $ defaultf ac x])


    mape = genericAccumMap me de
    mapp = genericAccumMap mp dp
    mapn = genericAccumMap mn dn
    mapd = genericAccumMap md dd
    mapr = genericAccumMap mr dr
    mapb = genericAccumMap mb db
    mapgr = genericAccumMap mg dgr
    maps = genericAccumMap ms ds
    mapm = genericAccumMap mm dm
    mapa = genericAccumMap ma da
    mapqs = genericAccumMap [] dqs
    mapqo = genericAccumMap mqo dqo


    ae = mapAccum mape
    aa = mapAccum mapa
    as = mapAccum maps
    aqs = mapAccum mapqs
    ap = mapAccum mapp
    an = mapAccum mapn
    am = mapAccum mapm
    agr = mapAccum mapgr
    aqo = mapAccum mapqo


    dqs :: acc -> QualStmt -> (acc, QualStmt)
    dqs acc q@(QualStmt s) = second QualStmt $ maps acc s

    db :: acc -> Binds -> (acc, Binds)
    db acc (BDecls decls) = second BDecls $ mapAccum mapd acc decls


    de :: acc -> Exp -> (acc, Exp)
    de acc (InfixApp a o b) = (acc2, InfixApp x o2 y)
      where (acc1, [x,y]) = ae acc [a,b]
            (acc2, [o2]) = aqo acc1 [o]
    de acc (App a b) = second (argList2 App) $ ae acc [a,b]
    de acc (NegApp a) = second (NegApp . head) (ae acc [a])
    de acc (Lambda s ps e) = (acc2, Lambda s ps1 e1)
      where (acc1, ps1) = mapAccum mapp acc ps
            (acc2, e1) = mape acc1 e
    de acc (Let b e) = (acc2, Let b1 e1)
      where (acc1, b1) = mapb acc b
            (acc2, e1) = mape acc1 e
    de acc (If a b c) = second (argList3 If) $ ae acc [a,b,c]
    de acc (Case e alts) = second (Case e1) $ aa acc1 alts
      where (acc1, e1) = mape acc e
    de acc (Do sts) = second Do $ as acc sts
    de acc (Tuple Boxed es) = second (Tuple Boxed) (ae acc es)
    de acc (List es) = second List (ae acc es)
    de acc (Paren e) = second Paren (mape acc e)
    de acc (LeftSection e q) = (acc2, LeftSection e1 q1)
      where (acc1, [e1]) = ae acc [e]
            (acc2, [q1]) = aqo acc1 [q]
    de acc (RightSection q e) = (acc2, RightSection q1 e1)
      where (acc1, [e1]) = ae acc [e]
            (acc2, [q1]) = aqo acc1 [q]
    de acc (EnumFrom e) = second EnumFrom (mape acc e)
    de acc (EnumFromTo a b) = second (argList2 EnumFromTo) (ae acc [a, b])
    de acc (EnumFromThen a b) = second (argList2 EnumFromThen) (ae acc [a, b])
    de acc (EnumFromThenTo a b c) = second (argList3 EnumFromThenTo) (ae acc [a, b, c])
    de acc (ListComp e qss) = second (ListComp e1) (aqs acc1 qss)
      where (acc1, e1) = mape acc e
    de acc (ExpTypeSig l e t) = second (\e1 -> ExpTypeSig l e1 t) (mape acc e)
    de acc e = (acc, e)

    dp :: acc -> Pat -> (acc, Pat)
    dp acc (PApp q ps) = second (PApp q) $ ap acc ps
    dp acc (PTuple Boxed ps) = second (PTuple Boxed) $ ap acc ps
    dp acc (PParen p) = second PParen $ mapp acc p
    dp acc (PList ps) = second PList $ ap acc ps
    dp acc (PAsPat n p) = second (PAsPat n1) $ mapp acc1 p
      where (acc1, n1) = mapn acc n
    dp acc (PInfixApp a q b) = second (PInfixApp a1 q) $ mapp acc1 b
      where (acc1, a1) = mapp acc a
    dp acc p = (acc, p)

    dn :: acc -> Name -> (acc, Name)
    dn = (,)

    dd :: acc -> Decl -> (acc, Decl)
    dd acc (TypeSig l ns t) = (acc1, TypeSig l ns1 t)
      where (acc1, ns1) = an acc ns
    dd acc (FunBind ms) = second FunBind $ am acc ms
    dd acc (PatBind l p r b) = (acc3, PatBind l p1 r1 b1)
      where (acc1, p1) = mapp acc p
            (acc2, r1) = mapr acc1 r
            (acc3, b1) = case b of
              Nothing -> (acc2, Nothing)
              Just jb -> second Just $ mapb acc2 jb
    dd acc d  = (acc, d)

    dm :: acc -> Match -> (acc, Match)
    dm acc (Match l n ps t r b) = (acc4, Match l n1 ps1 t r1 b1)
      where (acc1, n1) = mapn acc n
            (acc2, ps1) = ap acc1 ps
            (acc3, r1) = mapr acc2 r
            (acc4, b1) = case b of
              Nothing -> (acc3, Nothing)
              Just jb -> second Just (mapb acc3 jb)

    dr :: acc -> Rhs -> (acc, Rhs)
    dr acc (UnGuardedRhs e) = second UnGuardedRhs $ mape acc e
    dr acc (GuardedRhss gs) = second GuardedRhss $ agr acc gs

    dgr :: acc -> GuardedRhs -> (acc, GuardedRhs)
    dgr acc (GuardedRhs l sts e) = second (GuardedRhs l sts1) $ mape acc1 e
      where (acc1, sts1) = as acc sts

    ds :: acc -> Stmt -> (acc, Stmt)
    ds acc (Qualifier e) = second Qualifier $ mape acc e
    ds acc (Generator l p e) = second (Generator l p1) (mape acc1 e)
      where (acc1, p1) = mapp acc p
    ds acc (LetStmt b) = second LetStmt (mapb acc b)

    da :: acc -> Alt -> (acc, Alt)
    da acc (Alt l p r b) = (acc3, Alt l p1 r1 b1)
      where (acc1, p1) = mapp acc p
            (acc2, r1) = mapr acc1 r
            (acc3, b1) = case b of
              Nothing -> (acc2, Nothing)
              Just jb -> second Just (mapb acc2 jb)


    dqo :: acc -> QOp -> (acc, QOp)
    dqo = (,)

-------------------------------------------------------------

type MaybeFold a acc = acc -> a -> Maybe acc

data MaybeFoldArgs acc = MaybeFoldArgs
  {maybeFoldExp :: [MaybeFold Exp acc],
   maybeFoldPat :: [MaybeFold Pat acc],
   maybeFoldLName :: [MaybeFold Name acc],
   maybeFoldDecl :: [MaybeFold Decl acc],
   maybeFoldRhs :: [MaybeFold Rhs acc],
   maybeFoldMatch :: [MaybeFold Match acc],
   maybeFoldBinds :: [MaybeFold Binds acc],
   maybeFoldStmt :: [MaybeFold Stmt acc],
   maybeFoldAlt :: [MaybeFold Alt acc],
   maybeFoldQOp :: [MaybeFold QOp acc],
   maybeFoldGuardedRhs :: [MaybeFold GuardedRhs acc]
  }

data AstFold acc = AstFold {
  foldArgs :: MaybeFoldArgs acc,
  foldExp :: acc -> Exp -> acc,
  foldPat :: acc -> Pat -> acc,
  foldDecl :: acc -> Decl -> acc,
  foldLName :: acc -> Name -> acc, -- only for names on the left hand side (match name, typesig name, asPat)
  foldRhs :: acc -> Rhs -> acc,
  foldBinds :: acc -> Binds -> acc,
  foldMatch :: acc -> Match -> acc,
  foldStmt :: acc -> Stmt -> acc,
  foldAlt :: acc -> Alt -> acc,
  foldQOp :: acc -> QOp -> acc,
  foldGuardedRhs :: acc -> GuardedRhs -> acc
  }


emptyFoldArgs :: MaybeFoldArgs acc
emptyFoldArgs = MaybeFoldArgs {
  maybeFoldExp = [], maybeFoldPat = [], maybeFoldLName = [],
  maybeFoldDecl = [], maybeFoldRhs = [], maybeFoldMatch = [],
  maybeFoldBinds = [], maybeFoldStmt = [], maybeFoldAlt = [],
  maybeFoldQOp = [], maybeFoldGuardedRhs = []
  }


combineFold :: [AstFold acc] -> AstFold acc
combineFold l = mkAstFold . combineFoldArgs $ map foldArgs l

combineFoldArgs :: [MaybeFoldArgs acc] -> MaybeFoldArgs acc
combineFoldArgs = foldr1 f
  where f MaybeFoldArgs {
          maybeFoldExp = a1, maybeFoldPat = a2, maybeFoldLName = a3,
          maybeFoldDecl = a4, maybeFoldRhs = a5, maybeFoldMatch = a6,
          maybeFoldBinds = a7, maybeFoldStmt = a8, maybeFoldAlt = a9,
          maybeFoldQOp = a10, maybeFoldGuardedRhs = a11}
          MaybeFoldArgs {
          maybeFoldExp = b1, maybeFoldPat = b2, maybeFoldLName = b3,
          maybeFoldDecl = b4, maybeFoldRhs = b5, maybeFoldMatch = b6,
          maybeFoldBinds = b7, maybeFoldStmt = b8, maybeFoldAlt = b9,
          maybeFoldQOp = b10, maybeFoldGuardedRhs = b11
          }
          = MaybeFoldArgs {
          maybeFoldExp = a1 ++ b1, maybeFoldPat = a2 ++ b2, maybeFoldLName = a3 ++ b3,
          maybeFoldDecl = a4 ++ b4, maybeFoldRhs = a5 ++ b5, maybeFoldMatch = a6 ++ b6,
          maybeFoldBinds = a7 ++ b7, maybeFoldStmt = a8 ++ b8, maybeFoldAlt = a9 ++ b9,
          maybeFoldQOp = a10 ++ b10, maybeFoldGuardedRhs = a11 ++ b11
          }


mkAstFold :: forall acc . MaybeFoldArgs acc -> AstFold acc
mkAstFold args@MaybeFoldArgs {
  maybeFoldExp = me, maybeFoldPat = mp, maybeFoldLName = mn,
  maybeFoldDecl = md, maybeFoldRhs = mr, maybeFoldMatch = mm,
  maybeFoldBinds = mb, maybeFoldStmt = ms, maybeFoldAlt = ma,
  maybeFoldQOp = mqo, maybeFoldGuardedRhs = mgr} = astmap
  where
    headJust = head . catMaybes
    astmap = AstFold {
      foldArgs = args,
      foldExp = mape,
      foldPat = mapp,
      foldLName = mapn,
      foldDecl = mapd,
      foldStmt = maps,
      foldBinds = mapb,
      foldMatch = mapm,
      foldRhs = mapr,
      foldAlt = mapa,
      foldQOp = mapqo,
      foldGuardedRhs = mapgr
      }


    genericFold :: [acc -> a -> Maybe acc] ->
                   (acc -> a -> acc) ->
                   acc -> a -> acc
    genericFold mf defaultf ac e =
      headJust $ map (\f -> f ac e)
      (mf ++ [\ ac x -> Just $ defaultf ac x])

    defaultFold f = accumToFold $ f ama
      where
        ama = mkAstMapAccum emptyMapAccumArgs {
              maybeMapAccumExp = map foldToAccumIdM me,
              maybeMapAccumPat = map foldToAccumIdM mp,
              maybeMapAccumLName = map foldToAccumIdM mn,
              maybeMapAccumDecl = map foldToAccumIdM md,
              maybeMapAccumRhs = map foldToAccumIdM mr,
              maybeMapAccumMatch = map foldToAccumIdM mm,
              maybeMapAccumBinds = map foldToAccumIdM mb,
              maybeMapAccumStmt = map foldToAccumIdM ms,
              maybeMapAccumAlt = map foldToAccumIdM ma,
              maybeMapAccumQOp = map foldToAccumIdM mqo
              }

    mape = genericFold me $ defaultFold mapAccumExp
    mapp = genericFold mp $ defaultFold mapAccumPat
    mapn = genericFold mn $ defaultFold mapAccumLName
    mapd = genericFold md $ defaultFold mapAccumDecl
    mapr = genericFold mr $ defaultFold mapAccumRhs
    mapb = genericFold mb $ defaultFold mapAccumBinds
    maps = genericFold ms $ defaultFold mapAccumStmt
    mapm = genericFold mm $ defaultFold mapAccumMatch
    mapa = genericFold ma $ defaultFold mapAccumAlt
    mapqo = genericFold mqo $ defaultFold mapAccumQOp
    mapgr = genericFold mgr $ defaultFold mapAccumGuardedRhs
