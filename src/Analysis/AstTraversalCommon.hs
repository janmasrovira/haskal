-- | all exported functions should start with ast
module Analysis.AstTraversalCommon (
  astNamesRhsR, astNamesBindsRhs, astNamesBindsArgsRhs,
  astNamesRhsRS, astNamesBindsRhsS, astNamesBindsArgsRhsS,
  astUnGuardedIfToGuarded, astNamesRhs,
  astRemoveParens, astRemoveAppQOp, astFunctionCalls, astNamesRhsS,
  astFunctionCallsConds, astAllNames, astAllNamesS,
  astAllExprs
  ) where

--Own
import           Analysis.AstTraversal
import           Analysis.Helper
import           Parsing.Parser

-- Exts
import           Language.Haskell.Exts.Pretty
import           Language.Haskell.Exts.Syntax

-- Utilities
import           Data.List
import           Data.Map.Strict              (Map)
import qualified Data.Map.Strict              as Map
import           Data.Maybe
import           Data.Set                     (Set)
import qualified Data.Set                     as Set
import           Debug.Trace



-- |Removes all parenthesis (they are useless after parsing)
astRemoveParens :: AstMap
astRemoveParens = ast
  where ast = mkAstMap emptyMapArgs {
          _maybeMapExp = [unparenE],
          _maybeMapPat = [unparenP]
          }
          where unparenE (Paren e) = Just $ _mapExp ast e
                unparenE _ = Nothing
                unparenP (PParen p) = Just $ _mapPat ast p
                unparenP _ = Nothing

-- |Removes $ (they are useless after parsing)
-- this may imply the need of extra parethesis when printing (but no need to add them explicitly)
astRemoveAppQOp :: AstMap
astRemoveAppQOp = mkAstMap emptyMapArgs {
  _maybeMapExp = [s]              
  }
  where
    s (InfixApp a (QVarOp (UnQual (Symbol "$"))) b) =
      Just $ App (_mapExp astRemoveAppQOp a) (_mapExp astRemoveAppQOp b)
    s _ = Nothing


-- |Names in the right hand side (recursive)
astNamesRhsR :: AstFold [Name]
astNamesRhsR = mkAstFold $ emptyFoldArgs {maybeFoldExp = [mEN (:)]}

-- |Names in the right hand side (non recursive through binds (ie where))
astNamesRhs :: AstFold [Name]
astNamesRhs = mkAstFold $ emptyFoldArgs {
  maybeFoldExp = [mEN (:)],
  maybeFoldBinds = [\acc _ -> Just acc]}

-- |Names in the right hand side (non recursive through binds (ie where)), no repetitions
astNamesRhsS :: AstFold (Set Name)
astNamesRhsS = mkAstFold $ emptyFoldArgs {
  maybeFoldExp = [mEN Set.insert],
  maybeFoldBinds = [const (const $ Just Set.empty)]}

-- |Names in the right hand side (recursive, no repetitions)
astNamesRhsRS :: AstFold (Set Name)
astNamesRhsRS = mkAstFold $ emptyFoldArgs {maybeFoldExp = [mEN Set.insert]}

-- accums names in expressions
mEN :: (Name -> t -> s) -> t -> Exp -> Maybe s
mEN f ac (Var (UnQual n)) = Just $ f n ac
mEN _ _ _  = Nothing

-- names in patterns
mPN :: (Name -> acc -> acc) -> acc -> Pat -> Maybe acc
mPN f ac (PVar n) = Just $ f n ac
-- mPN foldPat f ac (PAsPat n p) = do -- not sure if this match should be here
--   let ac1 =  foldPat ac p
--   return $ f n ac1
mPN _ _ _ = Nothing


-- |Names in the right hand side and binded by function names and pattern bindings (no arguments)
astNamesBindsRhsS :: AstFold (Set Name)
astNamesBindsRhsS = ast
  where fPat = mPN (Set.insert)
        ast = mkAstFold $ emptyFoldArgs {
          maybeFoldExp = [mEN (Set.insert)], maybeFoldPat = [fPat] }

astAllNamesS :: AstFold (Set Name)
astAllNamesS = astNamesBindsArgsRhsS
                                        
astAllNames :: AstFold [Name]
astAllNames = astNamesBindsArgsRhs

astNamesBindsRhs :: AstFold [Name]
astNamesBindsRhs = ast
  where ast = mkAstFold $ emptyFoldArgs {
          maybeFoldExp = [mEN (:)], maybeFoldPat = [mPN (:)]}



-- | Names in the right hand side and binded by function names, pattern bindings and arguments (all names i think)
astNamesBindsArgsRhs :: AstFold [Name]
astNamesBindsArgsRhs = ast where
  ast = mkAstFold $ emptyFoldArgs {
    maybeFoldExp = [mEN (:)],
    maybeFoldLName = [\ac -> Just . (:ac)],
    maybeFoldPat = [mPN (:)]}
  

astNamesBindsArgsRhsS :: AstFold (Set Name)
astNamesBindsArgsRhsS = ast
  where ast = mkAstFold $ emptyFoldArgs {
          maybeFoldExp = [mEN Set.insert],
          maybeFoldLName = [\ac -> Just . (`Set.insert`ac)],
          maybeFoldPat = [mPN Set.insert]}


astUnGuardedIfToGuarded :: AstMap
astUnGuardedIfToGuarded = mkAstMap emptyMapArgs {_maybeMapRhs = [s]}
  where
    s :: Rhs -> Maybe Rhs
    s (UnGuardedRhs (If cond a b)) =
      Just $ GuardedRhss [GuardedRhs unknownSrcLoc [Qualifier cond]
                          $ _mapExp astUnGuardedIfToGuarded a,
                          GuardedRhs unknownSrcLoc [Qualifier otherwiseExp]
                          $ _mapExp astUnGuardedIfToGuarded b]
    s _ = Nothing


  
-- | returns each call's arguments
astFunctionCalls :: QName -> AstFold [[Exp]]
astFunctionCalls funName = ast
  where
    ast = mkAstFold emptyFoldArgs{maybeFoldExp = [fe]}
    fe ac ap@App{} =
      case appChain ap of
       (Just fun, args)
         | fun == funName -> Just $ args : (concatMap (foldExp ast []) args ++ ac)
         | otherwise -> Nothing
       _ -> Nothing
    fe _ _ = Nothing


-- | returns each call's arguments
-- Use like this: snd $ foldExp astFunctionCallsConds ([],[]) exp
-- returns (a, [(b,c)])
-- a must be ignored since undefined is returned, it's only used to pass the conditionals accumulated up to a point
-- b is a list of conditionals
-- c are the arguments of the funciton call
astFunctionCallsConds :: QName -> AstFold ([(Bool, Exp)], [([(Bool, Exp)], [Exp])])
astFunctionCallsConds funName = ast
  where
    ast = mkAstFold emptyFoldArgs{maybeFoldExp = [fe], maybeFoldRhs = [fr]}
   -- fe :: ([(Bool, Exp)], [([(Bool, Exp)], [Exp])]) -> Exp -> Maybe ([(Bool, Exp)], [([(Bool, Exp)], [Exp])])
    fe (conds, ac) ap@App{} = 
      case appChain ap of
       (Just fun, args)
         | fun == funName ->
           Just (conds, (conds, args) : condsNcalls ++ ac)
         | otherwise -> Nothing
         where
           condsNcalls :: [([(Bool, Exp)], [Exp])]
           condsNcalls = concatMap (snd . foldExp ast (conds, [])) args
       _ -> Nothing
    fe (conds, _) (If cond a b) =  Just (conds, ra ++ rb)
      where
        ra = snd $ foldExp ast ((True, cond):conds, []) a
        rb = snd $ foldExp ast ((False, cond):conds, []) b
    fe _ e = Nothing

    --fr _ _ = undefined
    fr (conds, ac) (GuardedRhss gs) = Just (conds, newAc) -- zipWith 
      where
        newAc :: [([(Bool, Exp)], [Exp])]
        newAc = concat $ zipWith (\guar prevconds -> snd $ fg guar (prevconds ++ conds)) gs prevConds
        prevConds = inits $ concatMap (map ((,) False)) allConds
        allConds = map guardedRhsConds gs
        fg :: GuardedRhs -> [(Bool, Exp)] -> ([(Bool, Exp)], [([(Bool, Exp)], [Exp])])
        fg g@(GuardedRhs _ _ e) prev = foldExp ast (prev ++ newconds ++ conds, []) e
          where newconds = [(True, qualif) | qualif <- guardedRhsConds g]
    fr _ _ = Nothing
        --fg _ _ = []


-- | collects all expressions, non recursive through binds
astAllExprs :: AstFold (Set Exp)
astAllExprs = mkAstFold $ emptyFoldArgs {
  maybeFoldExp = [facum]
  , maybeFoldBinds = [\acc _ -> Just acc]
  }
  where
    -- TODO: LAZY (i am lazy, not the language) IMPLEMENTATION, SUPER INEFFICIENT!!!
    facum acc e
      | e`Set.member`acc = Nothing
      | otherwise = Just $ foldExp astAllExprs acc1 e
      where acc1 = Set.insert e acc
