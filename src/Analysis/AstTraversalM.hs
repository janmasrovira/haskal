-- {-# LANGUAGE ScopedTypeVariables #-}

module Analysis.AstTraversalM (mkAstMap, AstMap (..), MaybeMap,
                              emptyMapArgs, MaybeMapArgs (..),
                              mkAstMapAccum, AstMapAccum(..),
                              emptyMapAccumArgs, MaybeMapAccumArgs (..),
                              mkAstFold, AstFold(..),
                              emptyFoldArgs, MaybeFoldArgs (..),
                              MaybeFold,
                              accumToFold, foldToAccumId, combineMap,
                              combineFold, combineMapAccum) where

--Own
import           Analysis.Helper
import           Common.Applicative
import           Common.List
import           Common.Maybe
import           Parsing.Parser

-- Exts
import           Language.Haskell.Exts.Pretty
import           Language.Haskell.Exts.Syntax

-- Utilities
import           Control.Concatenative
import           Control.Monad
import           Data.Either.Unwrap
import           Data.List
import           Data.List.Split
import           Data.Map (Map)
import qualified Data.Map as Map
import           Data.Maybe
import qualified Data.Set as Set

  
import Debug.Trace


accumToFoldM :: Monad m => (acc -> x -> m (acc, y)) -> acc -> x -> m acc
accumToFoldM f ac x = fmap fst $ f ac x

--accumToFold :: Monad m => (acc -> x -> m (acc, y)) -> acc -> x -> m acc
accumToFold :: (t -> t1 -> (a, b)) -> t -> t1 -> a
accumToFold f ac x = fst $ f ac x


foldToAccumId :: (acc -> x -> acc) -> acc -> x -> (acc, x)
foldToAccumId f acc x = (f acc x, x)

foldToAccumIdM :: Monad m => (acc -> x -> m acc) -> acc -> x -> m (acc, x)
foldToAccumIdM f acc x = do
  fx <- f acc x
  return (fx, x)


type MaybeMap m a = a -> m (Maybe a)

data MaybeMapArgs m = MaybeMapArgs 
  {_maybeMapExp :: [MaybeMap m Exp],
   _maybeMapPat :: [MaybeMap m Pat],
   _maybeMapLName :: [MaybeMap m Name],
   _maybeMapDecl :: [MaybeMap m Decl],
   _maybeMapRhs :: [MaybeMap m Rhs],
   _maybeMapMatch :: [MaybeMap m Match],
   _maybeMapBinds :: [MaybeMap m Binds],
   _maybeMapStmt :: [MaybeMap m Stmt],
   _maybeMapAlt :: [MaybeMap m Alt],
   _maybeMapQOp :: [MaybeMap m QOp]}

data AstMap m = AstMap {
  mapArgs :: MaybeMapArgs m, -- do NOT change this manually after creation
  _mapExp :: Exp -> m Exp,
  _mapPat :: Pat -> m Pat,
  _mapDecl :: Decl -> m Decl,
  _mapLName :: Name -> m Name, -- only for names on the left hand side (match name, typesig name, asPat)
  _mapRhs :: Rhs -> m Rhs,
  _mapBinds :: Binds -> m Binds,
  _mapMatch :: Match -> m Match,
  _mapStmt :: Stmt -> m Stmt,
  _mapAlt :: Alt -> m Alt,
  _mapQOp :: QOp -> m QOp
  }

emptyMapArgs :: Monad m => MaybeMapArgs m
emptyMapArgs = MaybeMapArgs {
  _maybeMapExp = [], _maybeMapPat = [], _maybeMapLName = [],
  _maybeMapDecl = [], _maybeMapRhs = [], _maybeMapMatch = [],
  _maybeMapBinds = [], _maybeMapStmt = [], _maybeMapAlt = [],
  _maybeMapQOp = []
  }

combineMap :: Monad m => [AstMap m] -> AstMap m
combineMap l = mkAstMap . combineMapArgs $ map mapArgs l

combineMapArgs :: Monad m => [MaybeMapArgs m] -> MaybeMapArgs m
combineMapArgs = foldr1 f
  where f MaybeMapArgs {
          _maybeMapExp = a1, _maybeMapPat = a2, _maybeMapLName = a3,
          _maybeMapDecl = a4, _maybeMapRhs = a5, _maybeMapMatch = a6,
          _maybeMapBinds = a7, _maybeMapStmt = a8, _maybeMapAlt = a9,
          _maybeMapQOp = a10}
          MaybeMapArgs {
          _maybeMapExp = b1, _maybeMapPat = b2, _maybeMapLName = b3,
          _maybeMapDecl = b4, _maybeMapRhs = b5, _maybeMapMatch = b6,
          _maybeMapBinds = b7, _maybeMapStmt = b8, _maybeMapAlt = b9,
          _maybeMapQOp = b10
          }
          = MaybeMapArgs {
          _maybeMapExp = a1 ++ b1, _maybeMapPat = a2 ++ b2, _maybeMapLName = a3 ++ b3,
          _maybeMapDecl = a4 ++ b4, _maybeMapRhs = a5 ++ b5, _maybeMapMatch = a6 ++ b6,
          _maybeMapBinds = a7 ++ b7, _maybeMapStmt = a8 ++ b8, _maybeMapAlt = a9 ++ b9,
          _maybeMapQOp = a10 ++ b10
          }

mkAstMap :: Monad m => MaybeMapArgs m -> AstMap m
mkAstMap args@MaybeMapArgs {
  _maybeMapExp = me, _maybeMapPat = mp, _maybeMapLName = mn,
  _maybeMapDecl = md, _maybeMapRhs = mr, _maybeMapMatch = mm,
  _maybeMapBinds = mb, _maybeMapStmt = ms, _maybeMapAlt = ma,
  _maybeMapQOp = mqo} = astmap
  where
    headJust = head . catMaybes
    astmap = AstMap {
      mapArgs = args, 
      _mapExp = mape,
      _mapPat = mapp,
      _mapLName = mapn,
      _mapDecl = mapd,
      _mapStmt = maps,
      _mapBinds = mapb,
      _mapMatch = mapm,
      _mapRhs = mapr,
      _mapAlt = mapa,
      _mapQOp = mapqo
      }

    genericMap mf defaultf e = liftM headJust $ mapM ($ e) (mf ++ [fmap Just . defaultf])

    mape = genericMap me de
    mapp = genericMap mp dp
    mapn = genericMap mn dn
    mapd = genericMap md dd
    mapr = genericMap mr dr
    mapb = genericMap mb db
    mapgr = genericMap [] dgr
    maps = genericMap ms ds
    mapm = genericMap mm dm
    mapa = genericMap ma da
    mapqs = genericMap [] dqs
    mapqo = genericMap mqo dqo

--    dqo :: QOp -> QOp
    dqo = return

    --dqs :: QualStmt -> QualStmt
    dqs (QualStmt s) = maps s <$>> QualStmt

    --db :: Binds -> Binds
    db (BDecls decls) = mapM mapd decls <$>> BDecls
    
   -- de :: Exp -> Exp
    de (InfixApp a o b) = do
      a' <- mape a
      o' <- mapqo o
      b' <- mape b
      return $ InfixApp a' o' b'
    de (App a b) = biApM mape ((.) (return .) App) a b
    de (NegApp a) = mape a <$>> NegApp
    de (Lambda s ps e) = do
      ps' <- mapM mapp ps
      e' <- mape e
      return $ Lambda s ps' e'
    de (Let b e) = do
      b' <- mapb b
      e' <- mape e
      return $ Let b' e'
    de (If a b c) = triApM mape ((.) ((.) (return .)) If) a b c
    de (Case e alts) = do
      e' <- mape e
      alts' <- mapM mapa alts
      return $ Case e' alts'
    de (Do sts) = fmap Do (mapM maps sts)
    de (Tuple Boxed es) = fmap (Tuple Boxed) (mapM mape es)
    de (List es) = fmap List (mapM mape es)
    de (Paren e) = fmap Paren (mape e)
    de (LeftSection e q) = do
      e' <- mape e
      q' <- mapqo q
      return $ LeftSection e' q'
    de (RightSection q e) = do
      q' <- mapqo q
      e' <- mape e
      return $ RightSection q' e'
    de (EnumFrom e) = fmap EnumFrom (mape e)
    de (EnumFromTo a b) = biApM mape ((.) (return .) EnumFromTo) a b
    de (EnumFromThen a b) = biApM mape ((.) (return .) EnumFromThen) a b
    de (EnumFromThenTo a b c) = triApM mape ((.) ((.) (return .)) EnumFromThenTo) a b c
    de (ListComp e qss) = do
      e' <- mape e
      qss' <- mapM mapqs qss
      return $ ListComp e' qss'
    de (ExpTypeSig l e t) = do
      e' <- mape e
      return $ ExpTypeSig l e' t
    de e = return e

    --dp :: Pat -> Pat 
    dp (PApp q ps) = fmap (PApp q) $ mapM mapp ps
    dp (PTuple Boxed ps) = fmap (PTuple Boxed) $ mapM mapp ps
    dp (PParen p) = fmap PParen $ mapp p
    dp (PList ps) = fmap PList $ mapM mapp ps
    dp (PAsPat n p) = do
      n' <- mapn n
      p' <- mapp p
      return $ PAsPat n' p'
    dp (PInfixApp a q b) = do
      a' <- mapp a
      b' <- mapp b
      return $ PInfixApp a' q b'
    dp (PVar n) = fmap PVar $ mapn n
    dp p = return p

    --dn :: Name -> Name
    dn = return

    --dd :: Decl -> Decl
    dd (TypeSig l ns t) = do
      ns' <- mapM mapn ns
      return $ TypeSig l ns' t
    dd (FunBind ms) = fmap FunBind $ mapM mapm ms
    dd (PatBind l p r b) = do
      p' <- mapp p
      r' <- mapr r
      b' <- mapb b
      return $ PatBind l p' r' b'

    --dm :: Match -> Match
    dm (Match l n ps t r b) = do
      n' <- mapn n
      ps' <- mapM mapp ps
      r' <- mapr r
      b' <- mapb b
      return $ Match l n' ps' t r' b'

   -- dr :: Rhs -> Rhs
    dr (UnGuardedRhs e) = fmap UnGuardedRhs $ mape e
    dr (GuardedRhss gs) = fmap GuardedRhss $ mapM mapgr gs

    --dgr :: GuardedRhs -> GuardedRhs
    dgr (GuardedRhs l sts e) = do
      sts' <- mapM maps sts
      e' <- mape e
      return $ GuardedRhs l sts' e'

    --ds :: Stmt -> Stmt
    ds (Qualifier e) = fmap Qualifier $ mape e
    ds (Generator l p e) = do
      p' <- mapp p
      e' <- mape e
      return $ Generator l p' e'
    ds (LetStmt b) = fmap LetStmt (mapb b)

    --da :: Alt -> Alt
    da (Alt l p r b) = do
      p' <- mapp p
      r' <- mapr r
      b' <- mapb b
      return $ Alt l p' r' b'

---------------------------------------

type MaybeMapAccum m a acc = acc -> a -> m (Maybe (acc, a))

data MaybeMapAccumArgs m acc =
  MaybeMapAccumArgs
  {maybeMapAccumExp :: [MaybeMapAccum m Exp acc],
   maybeMapAccumPat :: [MaybeMapAccum m Pat acc],
   maybeMapAccumLName :: [MaybeMapAccum m Name acc],
   maybeMapAccumDecl :: [MaybeMapAccum m Decl acc],
   maybeMapAccumRhs :: [MaybeMapAccum m Rhs acc],
   maybeMapAccumMatch :: [MaybeMapAccum m Match acc],
   maybeMapAccumBinds :: [MaybeMapAccum m Binds acc],
   maybeMapAccumStmt :: [MaybeMapAccum m Stmt acc],
   maybeMapAccumAlt :: [MaybeMapAccum m Alt acc],
   maybeMapAccumQOp :: [MaybeMapAccum m QOp acc] }
  

data AstMapAccum m acc = AstMapAccum {
  mapAccumArgs :: MaybeMapAccumArgs m acc,
  mapAccumExp :: acc -> Exp -> m (acc, Exp),
  mapAccumPat :: acc -> Pat -> m (acc, Pat),
  mapAccumDecl :: acc -> Decl -> m (acc, Decl),
  mapAccumLName :: acc -> Name -> m (acc, Name), -- only for names on the left hand side (match name, typesig name, asPat)
  mapAccumRhs :: acc -> Rhs -> m (acc, Rhs),
  mapAccumBinds :: acc -> Binds -> m (acc, Binds),
  mapAccumMatch :: acc -> Match -> m (acc, Match),
  mapAccumStmt :: acc -> Stmt -> m (acc, Stmt),
  mapAccumAlt :: acc -> Alt -> m (acc, Alt), 
  mapAccumQOp :: acc -> QOp -> m (acc, QOp)
  }
                       
emptyMapAccumArgs :: Monad m => MaybeMapAccumArgs m acc
emptyMapAccumArgs = MaybeMapAccumArgs {
  maybeMapAccumExp = [], maybeMapAccumPat = [], maybeMapAccumLName = [],
  maybeMapAccumDecl = [], maybeMapAccumRhs = [], maybeMapAccumMatch = [],
  maybeMapAccumBinds = [], maybeMapAccumStmt = [], maybeMapAccumAlt = [],
  maybeMapAccumQOp = []
  }

combineMapAccum :: Monad m => [AstMapAccum m acc] -> AstMapAccum m acc
combineMapAccum l = mkAstMapAccum . combineMapAccumArgs $ map mapAccumArgs l

combineMapAccumArgs :: Monad m => [MaybeMapAccumArgs m acc] -> MaybeMapAccumArgs m acc
combineMapAccumArgs = foldr1 f
  where f MaybeMapAccumArgs {
          maybeMapAccumExp = a1, maybeMapAccumPat = a2, maybeMapAccumLName = a3,
          maybeMapAccumDecl = a4, maybeMapAccumRhs = a5, maybeMapAccumMatch = a6,
          maybeMapAccumBinds = a7, maybeMapAccumStmt = a8, maybeMapAccumAlt = a9,
          maybeMapAccumQOp = a10}
          MaybeMapAccumArgs {
          maybeMapAccumExp = b1, maybeMapAccumPat = b2, maybeMapAccumLName = b3,
          maybeMapAccumDecl = b4, maybeMapAccumRhs = b5, maybeMapAccumMatch = b6,
          maybeMapAccumBinds = b7, maybeMapAccumStmt = b8, maybeMapAccumAlt = b9,
          maybeMapAccumQOp = b10
          }
          = MaybeMapAccumArgs {
          maybeMapAccumExp = a1 ++ b1, maybeMapAccumPat = a2 ++ b2, maybeMapAccumLName = a3 ++ b3,
          maybeMapAccumDecl = a4 ++ b4, maybeMapAccumRhs = a5 ++ b5, maybeMapAccumMatch = a6 ++ b6,
          maybeMapAccumBinds = a7 ++ b7, maybeMapAccumStmt = a8 ++ b8, maybeMapAccumAlt = a9 ++ b9,
          maybeMapAccumQOp = a10 ++ b10
          }


mapAccumLM :: Monad m => (a -> b -> m (a, c)) -> a -> [b] -> m (a, [c])
mapAccumLM _ a [] = return (a, [])
mapAccumLM f a (x:xs) = do
  (a', c) <- f a x
  (a'', cs) <- mapAccumLM f a' xs
  return (a'', c : cs)


mkAstMapAccum :: Monad m => MaybeMapAccumArgs m acc -> AstMapAccum m acc
mkAstMapAccum args@MaybeMapAccumArgs {
  maybeMapAccumExp = me, maybeMapAccumPat = mp, maybeMapAccumLName = mn,
  maybeMapAccumDecl = md, maybeMapAccumRhs = mr, maybeMapAccumMatch = mm,
  maybeMapAccumBinds = mb, maybeMapAccumStmt = ms, maybeMapAccumAlt = ma,
  maybeMapAccumQOp = mqo} = astmap
  where
    headJust = head . catMaybes
    astmap = AstMapAccum {
      mapAccumArgs = args,
      mapAccumExp = mape,
      mapAccumPat = mapp,
      mapAccumLName = mapn,
      mapAccumDecl = mapd,
      mapAccumStmt = maps,
      mapAccumBinds = mapb,
      mapAccumMatch = mapm,
      mapAccumRhs = mapr,
      mapAccumAlt = mapa,
      mapAccumQOp = mapqo
      }

    mapAccum = mapAccumLM
             
    -- genericAccumMap :: [(acc -> a -> Maybe (acc, a))] ->
    --                    (acc -> a -> (acc, a)) ->
    --                    acc -> a -> (acc, a)
    genericAccumMap mf defaultf ac e = liftM headJust $ mapM (\f -> f ac e)
                                       (mf ++ [(.) (fmap Just .) defaultf])
                                                    

    mape = genericAccumMap me de
    mapp = genericAccumMap mp dp
    mapn = genericAccumMap mn dn
    mapd = genericAccumMap md dd
    mapr = genericAccumMap mr dr
    mapb = genericAccumMap mb db
    mapgr = genericAccumMap [] dgr
    maps = genericAccumMap ms ds
    mapm = genericAccumMap mm dm
    mapa = genericAccumMap ma da
    mapqs = genericAccumMap [] dqs
    mapqo = genericAccumMap mqo dqo


    ae = mapAccum mape
    aa = mapAccum mapa
    as = mapAccum maps
    aqs = mapAccum mapqs
    ap = mapAccum mapp
    an = mapAccum mapn
    am = mapAccum mapm
    agr = mapAccum mapgr
    aqo = mapAccum mapqo
    ad = mapAccum mapd
  
    -- dqs :: acc -> QualStmt -> (acc, QualStmt)
    dqs acc q@(QualStmt s) = do
      s' <- maps acc s
      return $ second QualStmt s'

   -- db :: acc -> Binds -> m (acc, Binds)
    db acc (BDecls decls) = fmap (second BDecls) $ ad acc decls
    
    --  de :: acc -> Exp -> (acc, Exp)
    de acc (InfixApp a o b) = do
      (acc1, [x,y]) <- ae acc [a,b]
      (acc2, [o2]) <- aqo acc1 [o]
      return (acc2, InfixApp x o2 y)
    de acc (App a b) = fmap (second (argList2 App)) $ ae acc [a,b]
    de acc (NegApp a) = fmap (second (NegApp . head)) (ae acc [a])
    de acc (Lambda s ps e) = do
      (acc1, ps1) <- ap acc ps
      (acc2, e1) <- mape acc1 e
      return (acc2, Lambda s ps1 e1)
    de acc (Let b e) = do
      (acc1, b1) <- mapb acc b
      (acc2, e1) <- mape acc1 e
      return (acc2, Let b1 e1)
    de acc (If a b c) = fmap (second (argList3 If)) $ ae acc [a,b,c]
    de acc (Case e alts) = do
      (acc1, e1) <- mape acc e
      (acc2, alts1) <- aa acc1 alts
      return (acc2, Case e1 alts1)
    de acc (Do sts) = fmap (second Do) $ as acc sts
    de acc (Tuple Boxed es) = fmap (second (Tuple Boxed)) (ae acc es)
    de acc (List es) = fmap (second List) (ae acc es)
    de acc (Paren e) = fmap (second Paren) (mape acc e)
    de acc (LeftSection e q) = do
      (acc1, [e1]) <- ae acc [e]
      (acc2, [q1]) <- aqo acc1 [q] 
      return (acc2, LeftSection e1 q1)
    de acc (RightSection q e) = do
      (acc1, [e1]) <- ae acc [e]
      (acc2, [q1]) <- aqo acc1 [q] 
      return (acc2, RightSection q1 e1)
    de acc (EnumFrom e) = fmap (second EnumFrom) (mape acc e)
    de acc (EnumFromTo a b) = fmap (second (argList2 EnumFromTo)) (ae acc [a, b])
    de acc (EnumFromThen a b) = fmap (second (argList2 EnumFromThen)) (ae acc [a, b]) 
    de acc (EnumFromThenTo a b c) = fmap (second (argList3 EnumFromThenTo)) (ae acc [a, b, c])
    de acc (ListComp e qss) = do
      (acc1, e1) <- mape acc e
      fmap (second (ListComp e1)) (aqs acc1 qss)
    de acc (ExpTypeSig l e t) = do
      (acc1, e1) <- mape acc e
      return $ (acc1, ExpTypeSig l e1 t)
    de acc e = return (acc, e)

    --dp :: acc -> Pat -> (acc, Pat)
    dp acc (PApp q ps) = fmap (second (PApp q)) $ ap acc ps
    dp acc (PTuple Boxed ps) = fmap (second (PTuple Boxed)) $ ap acc ps
    dp acc (PParen p) = fmap (second PParen) $ mapp acc p
    dp acc (PList ps) = fmap (second PList) $ ap acc ps
    dp acc (PAsPat n p) = do
      (acc1, n1) <- mapn acc n
      (acc2, p1) <- mapp acc p
      return (acc2, PAsPat n1 p1)
    dp acc (PInfixApp a q b) = do
      (acc1, a1) <- mapp acc a
      (acc2, b1) <- mapp acc b
      return (acc2, PInfixApp a1 q b1)
    dp acc p = return (acc, p)
    
    --dn :: acc -> Name -> (acc, Name)
    dn = (.) (return .) (,)

    --dd :: acc -> Decl -> (acc, Decl)
    dd acc (TypeSig l ns t) = do
      (acc1, ns1) <- an acc ns
      return (acc1, TypeSig l ns1 t)
    dd acc (FunBind ms) = fmap (second FunBind) $ am acc ms
    dd acc (PatBind l p r b) = do
      (acc1, p1) <- mapp acc p
      (acc2, r1) <- mapr acc1 r
      fmap (second (PatBind l p1 r1)) $ mapb acc2 b
      
    dm acc (Match l n ps t r b) = do
      (acc1, n1) <- mapn acc n
      (acc2, ps1) <- ap acc1 ps
      (acc3, r1) <- mapr acc2 r
      (acc4, b1) <- mapb acc3 b
      return (acc4, Match l n1 ps1 t r1 b1)
            
--  dr :: acc -> Rhs -> (acc, Rhs)
    dr acc (UnGuardedRhs e) = fmap (second UnGuardedRhs) $ mape acc e
    dr acc (GuardedRhss gs) = fmap (second GuardedRhss) $ agr acc gs

 --   dgr :: acc -> GuardedRhs -> (acc, GuardedRhs)
    dgr acc (GuardedRhs l sts e) = do
      (acc1, sts1) <- as acc sts
      fmap (second (GuardedRhs l sts1)) $ mape acc1 e

    --ds :: acc -> Stmt -> (acc, Stmt)
    ds acc (Qualifier e) = fmap (second Qualifier) $ mape acc e
    ds acc (Generator l p e) = do
      (acc1, p1) <- mapp acc p
      fmap (second (Generator l p1)) (mape acc1 e)
    ds acc (LetStmt b) = fmap (second LetStmt) (mapb acc b)

    --da :: acc -> Alt -> (acc, Alt)
    da acc (Alt l p r b) = do
      (acc1, p1) <- mapp acc p
      (acc2, r1) <- mapr acc1 r
      (acc3, b1) <- mapb acc2 b
      return (acc3, Alt l p1 r1 b1)
    

    --dqo :: acc -> QOp -> (acc, QOp)
    dqo = (.) (return .) (,)

-------------------------------------------------------------

type MaybeFold m a acc = acc -> a -> m (Maybe acc)

data MaybeFoldArgs m acc = MaybeFoldArgs
  {maybeFoldExp :: [MaybeFold m Exp acc],
   maybeFoldPat :: [MaybeFold m Pat acc],
   maybeFoldLName :: [MaybeFold m Name acc],
   maybeFoldDecl :: [MaybeFold m Decl acc],
   maybeFoldRhs :: [MaybeFold m Rhs acc],
   maybeFoldMatch :: [MaybeFold m Match acc],
   maybeFoldBinds :: [MaybeFold m Binds acc],
   maybeFoldStmt :: [MaybeFold m Stmt acc],
   maybeFoldAlt :: [MaybeFold m Alt acc],
   maybeFoldQOp :: [MaybeFold m QOp acc]
   }

data AstFold m acc = AstFold {
  foldArgs :: MaybeFoldArgs m acc,
  foldExp :: acc -> Exp -> m acc,
  foldPat :: acc -> Pat -> m acc,
  foldDecl :: acc -> Decl -> m acc,
  foldLName :: acc -> Name -> m acc, -- only for names on the left hand side (match name, typesig name, asPat)
  foldRhs :: acc -> Rhs -> m acc,
  foldBinds :: acc -> Binds -> m acc,
  foldMatch :: acc -> Match -> m acc,
  foldStmt :: acc -> Stmt -> m acc,
  foldAlt :: acc -> Alt -> m acc,
  foldQOp :: acc -> QOp -> m acc
  }


emptyFoldArgs :: Monad m => MaybeFoldArgs m acc
emptyFoldArgs = MaybeFoldArgs {
  maybeFoldExp = [], maybeFoldPat = [], maybeFoldLName = [],
  maybeFoldDecl = [], maybeFoldRhs = [], maybeFoldMatch = [],
  maybeFoldBinds = [], maybeFoldStmt = [], maybeFoldAlt = [],
  maybeFoldQOp = []
  }


combineFold :: Monad m => [AstFold m acc] -> AstFold m acc
combineFold l = mkAstFold . combineFoldArgs $ map foldArgs l

combineFoldArgs :: Monad m => [MaybeFoldArgs m acc] -> MaybeFoldArgs m acc
combineFoldArgs = foldr1 f
  where f MaybeFoldArgs {
          maybeFoldExp = a1, maybeFoldPat = a2, maybeFoldLName = a3,
          maybeFoldDecl = a4, maybeFoldRhs = a5, maybeFoldMatch = a6,
          maybeFoldBinds = a7, maybeFoldStmt = a8, maybeFoldAlt = a9,
          maybeFoldQOp = a10}
          MaybeFoldArgs {
          maybeFoldExp = b1, maybeFoldPat = b2, maybeFoldLName = b3,
          maybeFoldDecl = b4, maybeFoldRhs = b5, maybeFoldMatch = b6,
          maybeFoldBinds = b7, maybeFoldStmt = b8, maybeFoldAlt = b9,
          maybeFoldQOp = b10
          }
          = MaybeFoldArgs {
          maybeFoldExp = a1 ++ b1, maybeFoldPat = a2 ++ b2, maybeFoldLName = a3 ++ b3,
          maybeFoldDecl = a4 ++ b4, maybeFoldRhs = a5 ++ b5, maybeFoldMatch = a6 ++ b6,
          maybeFoldBinds = a7 ++ b7, maybeFoldStmt = a8 ++ b8, maybeFoldAlt = a9 ++ b9,
          maybeFoldQOp = a10 ++ b10
          }


mkAstFold :: Monad m => MaybeFoldArgs m acc -> AstFold m acc
mkAstFold args@MaybeFoldArgs {
  maybeFoldExp = me, maybeFoldPat = mp, maybeFoldLName = mn,
  maybeFoldDecl = md, maybeFoldRhs = mr, maybeFoldMatch = mm,
  maybeFoldBinds = mb, maybeFoldStmt = ms, maybeFoldAlt = ma,
  maybeFoldQOp = mqo} = astmap
  where
    headJust = head . catMaybes
    astmap = AstFold {
      foldArgs = args,
      foldExp = mape,
      foldPat = mapp,
      foldLName = mapn,
      foldDecl = mapd,
      foldStmt = maps,
      foldBinds = mapb,
      foldMatch = mapm,
      foldRhs = mapr,
      foldAlt = mapa,
      foldQOp = mapqo
      }

    genericFold mf defaultf ac e = 
      liftM headJust $ mapM (\f -> f ac e)
      (mf ++ [(.) (fmap Just .)  defaultf])

    defaultFold f = accumToFoldM $ f ama
      where
        foldMToAccumMaybeM fo ac l = do
          x <- fo ac l
          return $ case x of
           Just a -> Just (a, l)
           Nothing -> Nothing
        ama = mkAstMapAccum MaybeMapAccumArgs {
          maybeMapAccumExp = map foldMToAccumMaybeM me
          , maybeMapAccumPat = map foldMToAccumMaybeM mp
          , maybeMapAccumLName = map foldMToAccumMaybeM mn
          , maybeMapAccumDecl = map foldMToAccumMaybeM md
          , maybeMapAccumRhs = map foldMToAccumMaybeM mr
          , maybeMapAccumMatch = map foldMToAccumMaybeM mm
          , maybeMapAccumBinds = map foldMToAccumMaybeM mb
          , maybeMapAccumStmt = map foldMToAccumMaybeM ms
          , maybeMapAccumAlt = map foldMToAccumMaybeM ma
          , maybeMapAccumQOp = map foldMToAccumMaybeM mqo                        
          }
              
    mape = genericFold me $ defaultFold mapAccumExp
    mapp = genericFold mp $ defaultFold mapAccumPat
    mapn = genericFold mn $ defaultFold mapAccumLName
    mapd = genericFold md $ defaultFold mapAccumDecl
    mapr = genericFold mr $ defaultFold mapAccumRhs
    mapb = genericFold mb $ defaultFold mapAccumBinds
    maps = genericFold ms $ defaultFold mapAccumStmt
    mapm = genericFold mm $ defaultFold mapAccumMatch
    mapa = genericFold ma $ defaultFold mapAccumAlt
    mapqo = genericFold mqo $ defaultFold mapAccumQOp


-- f = fold [] $ parseExp "a  + b"
--   where eNames ns (Var (UnQual n)) = Just $ n : ns
--         eNames ns e = trace ("express: " ++ show (prettyPrint e)) Nothing
--         fold = foldExp $ mkAstFold $ emptyFoldArgs {maybeFoldExp = [eNames]
   
