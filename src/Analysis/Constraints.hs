{-# LANGUAGE TemplateHaskell #-}

-- TEST :! stack exec analyzer -- ./test/examples/Constraints.hs -c ./test/examples/constraints.yml

module Analysis.Constraints
       (ConstraintFormat (..),
        ConstraintScope(..), _ForFunctions, _ForMatchesOf, concreteScope,
        ImportsConstraint(..), _None, _All, _Only, _NoneOf,
        ModuleConstraints(..), importsAllowed, declConstraints,
        DeclConstraint(..), scope, description,
        DeclConstraintDescription(..),
        Infraction(..), -- message, function,
        Constraint(..), encodeToString,
        Infractions(..), _Infractions,
        readConstraints, writeConstraints)
       where

import           Language.Haskell.Exts.Syntax

import           Analysis.Helper
import           Analysis.Queries
import           Analysis.Recursion
import           Common.List

import           Control.Concatenative
import           Control.Lens                 hiding (both)
import           Data.Aeson
import           Data.Aeson.TH
import qualified Data.ByteString.Internal     as IBS
import qualified Data.ByteString.Lazy.Char8   as BS
import           Data.List
import qualified Data.Map                     as M
import qualified Data.Yaml                    as Y
import           System.FilePath.Posix
import           Text.Regex.TDFA              ((=~))

data ConstraintFormat = JSON | YAML

-----------------
-- CONSTRAINTS --
-----------------


data ConstraintScope =
  ForFunctions [String] -- for the functions specified in the list
  | ForMatchesOf String
  | Global
  deriving (Show)
makePrisms ''ConstraintScope

data ImportsConstraint = None -- imports banned
                       | All -- imports allowed
                       | Only [String] -- only some modules allowed (e.g. Data.List)
                       | NoneOf [String] -- only some modules banned
                       | OnlyMatching String
                       deriving (Show)

data EnforceBan = Enforced
                | Banned
                deriving (Show)


-- TODO: temporary implementation
data Infraction = Infraction {
  _message :: String
  } deriving (Show, Eq)

-- makeLenses ''Infraction

data Infractions = Infractions [Infraction]
makePrisms ''Infractions

-- this class makes no sense, TODO: think about removing it or finding a good use.
class Constraint a where
  checkConstraint :: DeclGraph -> a -> [Infraction]

-- | Module constraints
data ModuleConstraints = ModuleConstraints {
  _importsAllowed :: ImportsConstraint
  , _declConstraints :: [DeclConstraint]
  } deriving (Show)


data DeclConstraint =
  DeclConstraint {
    _description :: DeclConstraintDescription
    , _scope :: ConstraintScope
    } deriving (Show)


data DeclConstraintDescription =
  Recursion EnforceBan
  | SelfRecursion EnforceBan
  | BannedFunctions [String]
  | ListCompr EnforceBan
  | BanInfiniteEnum
  deriving (Show)



makePrisms ''ImportsConstraint

makeLenses ''DeclConstraint
makeLenses ''ModuleConstraints
makeLenses ''DeclConstraintDescription


concreteScope :: [String] -> ConstraintScope -> [String]
concreteScope omega (ForMatchesOf regex) = filter (=~regex) omega
concreteScope omega Global = omega
concreteScope _ (ForFunctions x) = x


-- (nodes within scope, not found names)
nodesWithinScope :: [String] -> DeclGraph -> ConstraintScope -> ([Node], [String])
nodesWithinScope allFuns dg scope = (toNodes dg known, map nameStr unkown)
  where allnames = map Ident omega
        (known, unkown) = partition (nameHasDecl dg) allnames
        omega = concreteScope allFuns scope


nodesWithinScope' :: DeclGraph -> ConstraintScope -> ([Node], [String])
nodesWithinScope' dg = nodesWithinScope (map nameStr $ getTopBindedNames dg) dg


instance Constraint DeclConstraint where
  -- TODO: this functions has too much code, it should be split.
  checkConstraint dg cstr =
    missingInf ++ case cstr of
    DeclConstraint {
      _description = Recursion r
      } -> case r of
             Enforced -> map infDeclNotRecursive nRecNames
             Banned -> map infDeclRecursive recNames
    DeclConstraint {
      _description = SelfRecursion r
      } -> case r of
      Enforced -> map infDeclNotSelfRecursive nSelfRecNames
      Banned -> map infDeclSelfRecursive selfRecNames
    DeclConstraint {
      _description = BannedFunctions banned} ->
      concatMap checkOne nodes
      where
        banList = map Ident banned
      -- names which have a definition are unbanned
        bannedKnownNames = filter (nameIsTop dg) banList
        -- final list
        bannedNames = filter (`notElem`bannedKnownNames) banList
        checkOne :: Node -> [Infraction]
        checkOne start = [ infDeclUses (nameStr startname) (nameStr b) |
                         b <- filter (reachesName dg startname) bannedNames]
          where startname = getNode2name dg start
    DeclConstraint {
      _description = ListCompr forceBan} ->
      let (usingListComp, notUsingListComp) = both (map (nameStr . getNode2name dg)) $ partition nodeUsesListComp nodes
          nodeUsesListComp :: Node -> Bool
          nodeUsesListComp node = not . null $ filter isListComp (reachableExprsFromNode dg node)
          isListComp (ListComp _ _) = True
          isListComp _ = False
      in
        case forceBan of
        Banned -> map infListComprBanned usingListComp
        Enforced -> map infListComprEnforced notUsingListComp
    DeclConstraint {
      _description = BanInfiniteEnum
      } -> let
      isInfiniteEnum (EnumFrom _) = True
      isInfiniteEnum (EnumFromThen _ _) = True
      isInfiniteEnum _ = False
      nodeUsesInfiniteEnum :: Node -> Bool
      nodeUsesInfiniteEnum node = not . null $ filter isInfiniteEnum (reachableExprsFromNode dg node)
      usingInfiniteEnums = map (nameStr . getNode2name dg) (filter nodeUsesInfiniteEnum nodes)
      in map infBanInfiniteEnum usingInfiniteEnums
    where
      (nodes, missing) = nodesWithinScope' dg (cstr^.scope)
      missingInf = map infMissingDecl missing
      (recNames, nRecNames) = both (map (nameStr . getNode2name dg)) $
        partition (nodeIsRecursive dg) nodes
      (selfRecNames, nSelfRecNames) = both (map (nameStr. getNode2name dg)) $
        partition (nodeIsSelfRecursive dg) nodes

instance Constraint ImportsConstraint where
  checkConstraint dg c =
    map infImportBanned
    (case c of
       None -> imps
       All -> []
       Only x -> filter (`notElem`x) imps
       NoneOf x -> filter (`elem`x) imps
       OnlyMatching regex -> filter (not.(=~regex)) imps
    )
    where
      imps = [ i ^. limportModule ^?! _ModuleName | i <- dg ^. imports]

infListComprBanned :: String -> Infraction
infListComprBanned fun =
  Infraction $ "Function " ++ fun ++ " uses a comprehension list, which is not allowed"

infListComprEnforced :: String -> Infraction
infListComprEnforced fun =
  Infraction $ "Function " ++ fun ++ " does not use a comprehension list, which is compulsory"


infBanInfiniteEnum :: String -> Infraction
infBanInfiniteEnum fun = Infraction $ "Function " ++ fun ++
                         " uses infinite enumeration, which is not allowed"

infImportBanned :: String -> Infraction
infImportBanned imp = Infraction $ "Your submission imports " ++ imp ++ ", which is not allowed"

infMissingDecl :: String -> Infraction
infMissingDecl fun = Infraction $ "Function " ++ fun ++ " is not defined"

infDeclNotSelfRecursive :: String -> Infraction
infDeclNotSelfRecursive fun = Infraction $ "Function " ++ fun ++ " ++ is not self recursive"

infDeclSelfRecursive :: String -> Infraction
infDeclSelfRecursive fun = Infraction $ "Function " ++ fun ++ " is self recursive"

infDeclNotRecursive :: String -> Infraction
infDeclNotRecursive fun = Infraction $ "Function " ++ fun ++ " is not recursive"

infDeclRecursive :: String -> Infraction
infDeclRecursive fun = Infraction $ "Function " ++ fun ++ " is recursive"

infDeclUses :: String -> String -> Infraction
infDeclUses fun ban = Infraction $ "Function " ++ fun ++ " uses " ++ ban ++", which is banned"

-------------
-- METHODS --
-------------

unconstrainedModule :: ModuleConstraints
unconstrainedModule = ModuleConstraints {
  _importsAllowed = All
  , _declConstraints = []
  }


-- testing
constrainedModule1 :: ModuleConstraints
constrainedModule1 = ModuleConstraints {
  _importsAllowed = None
  , _declConstraints = [
    DeclConstraint {_scope = ForFunctions ["myLength"],
     _description = BannedFunctions ["length"]}
    , DeclConstraint {_scope = ForFunctions ["myLength"],
                      _description = Recursion Enforced }
    , DeclConstraint {_scope = ForFunctions ["listComp"],
                      _description = ListCompr Enforced}
    ]
  }

constrainedModule2 :: ModuleConstraints
constrainedModule2 = ModuleConstraints {
  _importsAllowed = All
  , _declConstraints = [
      DeclConstraint {_scope = ForFunctions ["fun1", "fun2"],
                      _description = BanInfiniteEnum}
      , DeclConstraint {_scope = ForFunctions ["fun1", "fun2"],
                      _description = BannedFunctions ["enumFrom", "enumFromThen"]}
    ]
  }



t = putStr $ encodeToString YAML constrainedModule2

tf = t >> writeConstraints f constrainedModule1
  where f = "test/examples/constraints.yml"

readConstraints :: FilePath -> IO (Maybe ModuleConstraints)
readConstraints file =
  case fileFormatMap file of
    Just format -> readConstraints' format file
    Nothing -> return Nothing
  where
    readConstraints' :: ConstraintFormat -> FilePath -> IO (Maybe ModuleConstraints)
    readConstraints' JSON = (fmap decode) . BS.readFile
    readConstraints' YAML = Y.decodeFile

defaultFormat :: ConstraintFormat
defaultFormat = YAML

writeConstraints :: FilePath -> ModuleConstraints -> IO ()
writeConstraints file =
  case fileFormatMap file of
    Just format -> writeConstraints' format file
    Nothing -> writeConstraints' defaultFormat file -- defaults to defaultFormat
  where
    writeConstraints' :: ConstraintFormat -> FilePath -> ModuleConstraints -> IO ()
    writeConstraints' JSON file = BS.writeFile file . encode
    writeConstraints' YAML file = Y.encodeFile file


encodeToString :: ToJSON a => ConstraintFormat -> a -> String
encodeToString JSON = BS.unpack . encode
encodeToString YAML = IBS.unpackChars . Y.encode

-- | file.yml -> YAML, file.json -> JSON
fileFormatMap :: FilePath -> Maybe ConstraintFormat
fileFormatMap = flip M.lookup (M.fromList [(".json", JSON),
                                           (".yml", YAML), (".yaml", YAML)]) . takeExtension



main = do
  x <- readConstraints "./src/Tests/Data/constraints.yml"
  print x

-----------------------
-- TO JSON INSTANCES --
-----------------------

-- makes FromJSON and ToJSON instances automatically using TH
-- consider setting allNullaryToStringTag = False
$(deriveJSON defaultOptions {fieldLabelModifier = drop 1} ''ConstraintScope)
$(deriveJSON defaultOptions {fieldLabelModifier = drop 1} ''DeclConstraintDescription)
$(deriveJSON defaultOptions {fieldLabelModifier = drop 1} ''EnforceBan)
$(deriveJSON defaultOptions {fieldLabelModifier = drop 1} ''DeclConstraint)
$(deriveJSON defaultOptions {fieldLabelModifier = drop 1} ''ImportsConstraint)
$(deriveJSON defaultOptions {fieldLabelModifier = drop 1} ''ModuleConstraints)
$(deriveJSON defaultOptions {fieldLabelModifier = drop 1} ''Infraction)
$(deriveJSON defaultOptions {fieldLabelModifier = drop 1} ''Infractions)
