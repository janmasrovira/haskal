module Analysis.Halting (haltsScript, halts) where

--Own
import           Analysis.AstTraversal
import           Analysis.AstTraversalCommon
import           Analysis.Helper
import           Analysis.Operators
import           Analysis.Recursion
import           Analysis.SMTProcess
import           Analysis.SMTUtils
import           Common.Applicative
import           Common.List
import           Common.Pretty
import           Parsing.Parser

-- Exts
import           Language.Haskell.Exts.Pretty
import           Language.Haskell.Exts.Syntax

-- Utilities
import           Control.Arrow
import           Control.Concatenative
import           Control.Monad
import           Control.Monad.ST
import           Data.Array.ST
import           Data.Array.Unboxed           as A
import           Data.IntMap                  (IntMap)
import qualified Data.IntMap                  as IntMap
import           Data.Ix
import           Data.List
import           Data.Map.Strict              (Map)
import qualified Data.Map.Strict              as Map
import           Data.Maybe
import           Data.Set                     (Set)
import qualified Data.Set                     as Set
import qualified          Debug.Trace                  as T
import           GHC.Base                     (Alternative)
import qualified SMTLib2                      as SMT
import qualified SMTLib2.Core                 as SMTC
import qualified SMTLib2.Int                  as SMTI
import           System.IO.Unsafe


f = (argsN args, prettyExprM args <$> asLinearCombination args exp) --argsN args
  where
    exp = parseExp "(a*1)*b+1" --"1 + 3 * (a + b) * (c + 3* d)"
    names = map Ident ["a"]-- foldExp astNamesRhs [] exp
    args = mkArgs $ nubOrd names

r = do
  g <- parseDeclGraph path
  let names = [Ident "f", Ident "g"]
  --print $ SMT.pp sc
  mapM (\name -> halts g (getName2node g name) True) names
  where
    path = "src/Tests/Data/Euclides.hs"

traceU :: Applicative f => IO t -> f ()
traceU f = unsafePerformIO f `seq` pure ()

halts :: DeclGraph -> Node -> Bool -> IO ()
halts g key verbose = case ms of
  Nothing -> putStrLn "Error" --return ()
  Just script -> do
    when verbose $ putStrLn "" >> print (SMT.pp script) >> putStrLn ""
    runSMTScript' ([procCVC4, procZ3, procYices]!!1) script >>= putStr . filterSmtOutput
  where ms = haltsScript verbose g key

filterSmtOutput :: [Char] -> [Char]
filterSmtOutput e@('u':_) = "unsat"
filterSmtOutput x = x

--debugPrintCalls :: [([Exp], [Exp])] -> Maybe ()
debugPrintCalls :: [([Exp], [Exp])] -> Maybe ()
debugPrintCalls = fmap mconcat . mapM f
  where f (conds, args) = do
          traceM $ "function call found"
          traceM $ "conditions: " ++ commaSep (map prettyPrint conds)
          traceM $ "args: " ++ commaSep (map prettyPrint args)
          traceM ""
        commaSep = intercalate ", "

haltsScript :: Bool -> DeclGraph -> Node -> Maybe SMT.Script
haltsScript verbose g key = do
  -- traceM "\nhaltsScript"
  matches <- funBindMatches decl
  haltsMatch $ head matches  
   where
     decl = getDecl g key
     funName = funBindName decl
     patName (PVar n) = Just n
     patName _ = Nothing
     haltsMatch :: Match -> Maybe SMT.Script
     haltsMatch match@(Match _ _ ps _ _ _) = do
       guard (all isJust mnames)
       let
         condCalls :: [([Exp], [Exp])]
         condCalls = map (first $ normalizeConditionals) $ snd $
                     foldMatch (astFunctionCallsConds $ UnQual funName) ([],[]) match
         varsDecls = map smtDeclareIntVar (vars args)
       when verbose $ debugPrintCalls condCalls
       callsSmt <- concat <$> zipWithM callToSMT [0,2..] condCalls
       let script = SMT.Script $
                    smtScriptHeader ++
                    varsDecls ++
                    callsSmt ++
                    [SMT.CmdCheckSat,
                     smtGetValues $ map nameStr $ vars args,
                     SMT.CmdExit]
       return script
         where
           mnames = map patName ps
           args = mkArgs names
           names = catMaybes mnames
           -- | statements associated with a call
           callToSMT :: Int -> ([Exp], [Exp]) -> Maybe [SMT.Command]
           callToSMT id (conds, params) = do
             paramsL <- mapM (asLinearCombination args) params
             table <- mkCondsTable args condsIneqs
             --traceU (putStr $ prettyTable2 table)
             let ineqDecrease = callIneq args (vars args) paramsL
             when verbose $ T.traceM ("\nDecr: " ++ prettyIneq args ineqDecrease
                       ++ "\nBound: " ++ prettyIneq args ineqBound)
             return $
               farkas id args table [ineqDecrease]
               ++ farkas (id+1) args table [ineqBound]
             where
               condsIneqs :: [IneqLE0]
               condsIneqs = --ineqBound :
                 mapMaybe (asInequationLE0 args) conds
               ineqBound = lowerBoundIneq args (vars args)

vars :: Args -> [Name]
vars args = [Ident $ "c" ++ show i | i <- range (argsAllBounds args)]

mbInvExp :: (Bool, Exp) -> Maybe Exp
mbInvExp (True, e) = Just e
mbInvExp (False, InfixApp a (QVarOp op) b) = do
  iop <- boolInverse op
  return $ InfixApp a (QVarOp iop) b

normalizeConditionals :: [(Bool, Exp)] -> [Exp]
normalizeConditionals = mapMaybe mbInvExp

traceM :: Monad m => String -> m ()
traceM s = if debug then T.traceM s else pure ()
  where debug = 1 == 1

-- for each call
mkCondsTable :: Args -> [IneqLE0] -> Maybe Table
mkCondsTable args conds' = do
  --traceM $ "conds = " ++ show conds
  let lconds = map fromIneqLE0 conds
  guard $ all (Set.null . exprNames) lconds
  let rows = map (exprMToArrayNoNames args) lconds
  -- traceM $ "rows created" ++ show rows
  -- traceM $ "bounds = " ++ show bnds
  let
    assos = concat [ map (first $ (,) i) $ assocs row | (i, row) <- zip [0..] rows]
--  traceM $ "assocs = " ++ show assos
  let r = array bnds assos
  return r
  where numCols = argsN args + 1
        numRows = length conds
        conds = minusOneLE0 : conds'
        bnds = ((0,0), (numRows-1,numCols-1))

-- -1 <= 0
minusOneLE0 :: IneqLE0
minusOneLE0 = ineqToLE0 (IneqLE minusOne exprZero)
  where minusOne = subExpr exprZero exprOne 


farkas :: Int -> Args -> Table -> [IneqLE0] -> [SMT.Command]
farkas id args t ineq = declLambdas ++ lambdasGE0 ++ equalities
  where
   -- e = fromIneqLE0 ineq
    ((i0,j0), (i1,j1)) = bounds t
    jr = range (j0, j1)
    ir = range (i0, i1)
    lambdai i = Ident $ "lam" ++ show id ++ "_" ++ show i
    lambdas = map lambdai ir
    lambArgs = mkArgs lambdas
    lambCol j = sumExprs [lambColRow i | i <- ir]
      where lambColRow i = let num = t!(i,j)
                           in IntMap.singleton argsIndepIx (Map.singleton (Just $ lambdai i) num)
    lambdasLHS :: [SMT.Expr]
    lambdasLHS =  [mkSMTExpr lambArgs $ lambCol j | j <- jr]
    -- 0 <= l0,l1,...
    lambdasGE0 :: [SMT.Command]
    lambdasGE0 = [ SMT.CmdAssert $ ineqToSMT args $
                   IneqGE (exprSinglName argsIndepIx l) exprZero |
                   l <- elems (argsNames lambArgs)]
   -- callineq = callIneq args (vars args id) params
    exprsRHS :: [[SMT.Expr]]
    exprsRHS = --T.trace ("List = " ++ show l) $
               map (map (mkSMTExpr args)) l -- emptyArgs
      where l = map (exprMAsListOfIndepTerms args . fromIneqLE0) ineq
    equalities :: [SMT.Command]
    equalities = concatMap (map SMT.CmdAssert . zipWith (SMTC.===) lambdasLHS) exprsRHS
    declLambdas :: [SMT.Command]
    declLambdas = map smtDeclareIntVar lambdas

-- | f (n - 1) (m + 1) => c1*(n-1) + c2*(m+1) < c1*n + c2*m
-- exps should be [(n-1), (m+1)] (ie the arguments to the call expressed as linear combinations)
-- args should be [n,m]
-- names should be [c0,c1,c2]
-- no independent term since it is always neutralized
callIneq :: Args -> [Name] -> [ExprM] -> IneqLE0
callIneq args (_:names) exps = --trace ("\nCallDec: " ++ prettyIneq args leq )
                               leq
  where leq = ineqToLE0 $ IneqGT greater lesser -- Eps
        greater = sumExprs $ zipWith (\name x -> mulExprName (Just name, 1) x)
                  names [exprSinglArg i | i <- range $ argsArgsBounds args ]
        lesser = sumExprs $ zipWith (\name x -> mulExprName (Just name, 1) x) names exps
        lesserEps = sumExprs [lesser, epsilonExprM]

-- | c0 + c*n + c2*m  >= 0
-- -c0 -c*n -c2*m <= 0
lowerBoundIneq :: Args -> [Name] -> IneqLE0
lowerBoundIneq args names = --  trace ("\nLEQ = " ++ prettyIneq args leq ++ "\n") $
  leq
  where
    leq = ineqToLE0 $ IneqGE greater exprZero
    greater = IntMap.fromList $ zipWith cell
              (range $ argsAllBounds args) names
    cell ix name = (ix, Map.singleton (Just name) 1)
