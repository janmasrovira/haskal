{-# LANGUAGE TemplateHaskell #-}

module Analysis.Helper (
  isPatBind, isFunBind, funBindName, patNames, patNamesS, bindedNames, bindedNamesS,
  patsNames, patsNamesS,
  matchRhs, altRhs,  representant,
  matchName, splitType, isTypeSig, typeSigNames, typeSigType,
  bindFirstName, patBindPat, rhsIsGuarded, unGuardedRhsExp,
  matchExplArgs, matchAddExplArgs, matchSetRhs, typeSigSetNames,
  reverseExp, funBindMatches, nameToVar, strToVar, nameStr,
  renameFunBind, flipInfixApp, appArg, renameMatch, isList, qopi,
  funBindAddArgs, matchAddExplPats, matchSetRhsExp, unwrap, unwrap_, reWrap, unParenP,
  funBindNumArgs, matchNumArgs, declAddBinds, opName, idExp, concatMapExp,
  flipExp, appExp, otherwiseExp, LetExp (..), declRhss, guardedRhsExp, rhsExps,
  infixAppChain, appChain, unwrapLetExp, wrapLetExp,
  matchAddBinds, fmapLetExp, isQualifier, fromQualifier, guardedRhsConds, isOtherwise,
  foldNameParts, matchBinds,renameMatch',renameFunBind',leftMostApp,constVar, limportModule, _ModuleName
  ) where

--Own
import           Common.Applicative
import           Parsing.Parser

-- Exts
import           Language.Haskell.Exts.Pretty
import           Language.Haskell.Exts.Syntax

-- Utilities
import           Control.Lens hiding (List)
import           Control.Concatenative
import           Data.List
import           Data.Map.Strict              (Map)
import qualified Data.Map.Strict              as Map
import           Data.Set                     (Set)
import qualified Data.Set                     as Set
import           Data.Maybe

import           Debug.Trace

makeLensesFor [("importModule", "limportModule")] ''ImportDecl
makePrisms ''ModuleName

matchBinds :: Match -> Binds
matchBinds (Match _ _ _ _ _ b) = fromMaybe (BDecls []) b

typeSigSetNames :: [Name] -> Decl -> Decl
typeSigSetNames ns (TypeSig s _ t) = TypeSig s ns t

matchAddBinds :: Binds -> Match -> Match
matchAddBinds b (Match s n p t r Nothing) = Match s n p t r (Just b)
matchAddBinds (BDecls b) (Match s n p t r (Just (BDecls bs))) = Match s n p t r $ Just (BDecls (b ++ bs))


declRhss :: Decl -> [Rhs]
declRhss (PatBind _ _ r _) = [r]
declRhss (FunBind ms) = map matchRhs ms
declRhss _ = trace "warning: only PatBind and FunBind have Rhs" []

qopi :: String -> QOp
qopi = QVarOp . UnQual . Ident

opName :: QOp -> QName
opName (QVarOp n) = n
opName (QConOp n) = n

-- char = {l,r}, int = {0,1}, bool = strict
foldNameParts :: Exp -> Maybe (Char, Int, Bool)
foldNameParts (Var (UnQual (Ident s))) =
  case s of
  "foldl" -> Just ('l', 0, False)
  "foldl1" -> Just ('l', 1, False)
  "foldl1'" -> Just ('l', 1, True)
  "foldl'" -> Just ('l', 0, False)
  "foldr1" -> Just ('r', 1, False)
  "foldr" -> Just ('r', 0, False)
  _ -> Nothing


unParenP :: Pat -> Pat
unParenP (PParen e) = unParenP e
unParenP e = e

-- easier treatment for expressions wrapped in Let's
data LetExp = LetExp {
  unwrapped :: Exp,
  binds :: [Binds]
  } deriving (Show)

wrapLetExp :: LetExp -> Exp
wrapLetExp x = reWrap (binds x) (unwrapped x)

unwrapLetExp :: Exp -> LetExp
unwrapLetExp x = let (b, w) = unwrap x
                 in LetExp {unwrapped=w, binds = b}

fmapLetExp :: (Exp -> Exp) -> LetExp -> LetExp
fmapLetExp f le@LetExp{unwrapped = e} = le{unwrapped = f e}

unwrap :: Exp -> ([Binds], Exp)
unwrap (Paren e) = unwrap e
unwrap (Let b e) = first (b:) $ unwrap e
unwrap e = ([], e)

reWrap :: [Binds] -> Exp -> Exp
reWrap = flip (foldr Let)

matchNumArgs :: Match -> Int
matchNumArgs = length . matchExplArgs

funBindNumArgs :: Decl -> Maybe Int
funBindNumArgs (FunBind ms) = Just $ matchNumArgs (head ms)

declAddBinds :: [Decl] -> Decl -> Decl
declAddBinds ds (PatBind s p r Nothing) = PatBind s p r (Just $ BDecls ds)
declAddBinds ds (PatBind s p r (Just (BDecls bs))) = PatBind s p r (Just $ BDecls (ds ++ bs))
declAddBinds ds (FunBind [Match s n ps t r Nothing]) =
  FunBind [Match s n ps t r (Just $ BDecls ds)]
declAddBinds ds (FunBind [Match s n ps t r (Just (BDecls bs))]) =
                 FunBind [Match s n ps t r (Just $ BDecls (ds ++ bs))]


unwrap_ :: Exp -> Exp
unwrap_ = snd . unwrap

isList :: Exp -> Bool
isList (List _) = True
isList _ = False

appArg :: Exp -> Exp
appArg (App _ a) = a

matchSetRhsExp :: Exp -> Match -> Match
matchSetRhsExp e = matchSetRhs(UnGuardedRhs e)

funBindAddArgs :: [Name] -> Decl -> Decl
funBindAddArgs ns (FunBind ms) = FunBind $ map (matchAddExplArgs ns) ms

-- |Renames a match. The change is not propagated
renameMatch :: Name -> Match -> Match
renameMatch y (Match l _ ps t r b) = Match l y ps t r b

renameMatch' :: (String -> String) -> Match -> Match
renameMatch' f (Match l (Ident n) ps t r b) = Match l (Ident (f n)) ps t r b

renameFunBind :: Name -> Decl -> Decl
renameFunBind n (FunBind ms) = FunBind (map (renameMatch n) ms)

renameFunBind' :: (String -> String) -> Decl -> Decl
renameFunBind' n (FunBind ms) = FunBind (map (renameMatch' n) ms)


nameStr :: Name -> String
nameStr (Ident s) = s
nameStr (Symbol s) = s


rhsIsGuarded :: Rhs -> Bool
rhsIsGuarded (UnGuardedRhs _) = False
rhsIsGuarded _ = True

isPatBind :: Decl -> Bool
isPatBind PatBind{} = True
isPatBind _ = False

isFunBind :: Decl -> Bool
isFunBind FunBind{} = True
isFunBind _ = False

isTypeSig :: Decl -> Bool
isTypeSig TypeSig{} = True
isTypeSig _ = False

isQualifier :: Stmt -> Bool
isQualifier Qualifier{} = True
isQualifier _  = False

fromQualifier :: Stmt -> Maybe Exp
fromQualifier (Qualifier e) = Just e
fromQualifier _ = Nothing

guardedRhsConds :: GuardedRhs -> [Exp]
guardedRhsConds (GuardedRhs _ s _) = filter (not.isOtherwise) $ mapMaybe fromQualifier s

funBindMatches :: Decl -> Maybe [Match]
funBindMatches (FunBind m) = Just m
funBindMatches _ = Nothing

typeSigNames :: Decl -> [Name]
typeSigNames (TypeSig _ ns _) = ns

matchExplArgs :: Match -> [Pat]
matchExplArgs (Match _ _ args _ _ _) = args

matchSetRhs :: Rhs -> Match -> Match
matchSetRhs rhs (Match l n ps t _ b) = Match l n ps t rhs b

matchAddExplArgs :: [Name] -> Match -> Match
matchAddExplArgs newArgs = matchAddExplPats (map PVar newArgs)

matchAddExplPats :: [Pat] -> Match -> Match
matchAddExplPats newArgs (Match a b args c d e) = Match a b (newArgs ++ args) c d e


patBindPat :: Decl -> Pat
patBindPat (PatBind _ p _ _) = p

bindFirstName :: Decl -> Name
bindFirstName d
  | isFunBind d = funBindName d
  | isPatBind d = representant (patBindPat d)
  | isTypeSig d = head (typeSigNames d)

typeSigType :: Decl -> Type
typeSigType (TypeSig _ _ t) = t

funBindName :: Decl -> Name
funBindName (FunBind (m:_)) = matchName m

matchName :: Match -> Name
matchName (Match _ name _ _ _ _) = name

-- |Returns a list with all the variable 'Name's in a 'Pat'
patNames :: Pat -> [Name]
patNames (PVar name) = [name]
patNames (PInfixApp a _ b) = patNames a ++ patNames b
patNames (PList l) = concatMap patNames l
patNames (PTuple Boxed l) = concatMap patNames l
patNames (PApp _ l) = concatMap patNames l
patNames (PAsPat a p) = a : patNames p
patNames (PParen p) = patNames p
patNames _ = []

patNamesS :: Pat -> Set.Set Name
patNamesS = Set.fromList . patNames

patsNames :: [Pat] -> [Name]
patsNames = concatMap patNames

patsNamesS :: [Pat] -> Set.Set Name
patsNamesS = Set.fromList . patsNames

unGuardedRhsExp :: Rhs -> Exp
unGuardedRhsExp (UnGuardedRhs e) = e

altRhs :: Alt -> Rhs
altRhs (Alt _ _ r _) = r

matchRhs :: Match -> Rhs
matchRhs (Match _ _ _ _ r _) = r

rhsExps :: Rhs -> [Exp]
rhsExps (UnGuardedRhs e) = [e]
rhsExps (GuardedRhss gs) = map guardedRhsExp gs


guardedRhsExp :: GuardedRhs -> Exp
guardedRhsExp (GuardedRhs s st e) = e

representant :: Pat -> Name
representant = head . patNames


-- |Returns a list with all the 'Name's binded by a declarations block. Notice that the functions arguments are not included
bindedNames :: Decl -> [Name]
bindedNames (PatBind _ p _ _) = patNames p
bindedNames (FunBind (Match _ n _ _ _ _:_)) = [n]
bindedNames _ = []

bindedNamesS :: Decl -> Set.Set Name
bindedNamesS = Set.fromList . bindedNames


t = readFile d <$>> parseModule d
  where d = "Tests/Data/UniqueNames.hs"


splitType :: Type -> ([Type], Type)
splitType t = let x = r t in (init x, last x)
  where r (TyFun a b) = a : r b
        r t = [t]


reverseExp :: Exp
reverseExp = Var (UnQual (Ident "reverse"))

nameToVar :: Name -> Exp
nameToVar = Var . UnQual

strToVar :: String -> Exp
strToVar = nameToVar . Ident

flipInfixApp :: Exp -> Exp
flipInfixApp (InfixApp a o b) = InfixApp b o a

idExp :: Exp
idExp = Var (UnQual (Ident "id"))

concatMapExp :: Exp
concatMapExp = Var (UnQual (Ident "concatMap"))

appExp :: Exp
appExp = Var (UnQual (Symbol "$"))

flipExp :: Exp
flipExp = Var (UnQual (Ident "flip"))

otherwiseExp :: Exp
otherwiseExp = Var (UnQual (Ident "otherwise"))

infixAppChain :: Exp -> [Exp]
infixAppChain (InfixApp a op b) = c a ++ c b
  where c i@(InfixApp a op' b)
          | op == op' = c a ++ c b
          | otherwise = [i]
        c a = [a]
infixAppChain _ = []

appChain' :: QName -> Exp -> [Exp]
appChain' name i@(App _ _) = c i
  where
    f = Var name
    c i@(App a@App{} b) =
      case c a of
       [] -> [i]
       l -> l ++ c b
    c i@(App a b)
      | a == f = c b
      | otherwise = [i]
    c i = [i]

leftMostApp :: Exp -> Exp
leftMostApp (App l@App{} _) = leftMostApp l
leftMostApp (App l _) = l
leftMostApp e = e

-- |returns the function name and its arguments
appChain :: Exp -> (Maybe QName, [Exp])
appChain e = case leftMostApp e of
  (Var f) -> (Just f, appChain' f e)
  _ -> (Nothing, [e])


isOtherwise :: Exp -> Bool
isOtherwise (Var (UnQual (Ident "otherwise"))) = True
isOtherwise _ = False

constVar :: Exp
constVar = Var (UnQual $ Ident "const")
