module Analysis.Inlining (inlineAll) where

--Own
import           Analysis.AstTraversal
import           Analysis.Helper
import           Analysis.Recursion
import           Analysis.Substitution
import           Common.Applicative
import           Common.List
import           Common.Maybe
import           Common.Set
import           Parsing.Parser

-- Exts
import           Language.Haskell.Exts.Pretty
import           Language.Haskell.Exts.Syntax

-- Utilities
import           Control.Arrow

import           Control.Concatenative
import qualified Data.IntSet as IntSet
import           Data.List
import           Data.Maybe

import           Debug.Trace


f = do
  g <- parseDeclGraph d
  let g0 = inlineAll g
  print (_node2nameMap g)
  --print (getTopFunNodes g)
  --print (getKnownNames g)
  --print (_nodeReachabilityMap g)
  mapM_ (putStrLn . prettyPrint) (map unLabelDecl $ _topDecls g0)-- (_topDecls g)
  where
    d = "src/Tests/Data/Inlining.hs"

inlineAll :: DeclGraph -> DeclGraph
inlineAll g = mkDeclGraph' ds'
  where ds = _topDecls g
        --ds' = map (iterateWhileDiffLast (inlineStep g) . (:[])) ds 
        ds' = (iterateWhileDiffLast $ inlineStep g) ds

-- | inlines recursively all simple PatBind (see inlinable below)
-- implementation is not optimal
-- DeclGraph and given declarations must be consistent in terms of reachability and names<->nodes
-- why pass a declaration list instead of using top decls of DeclGraph? because of efficiency, so we avoid
-- remaking a whole DeclGraph after each step. Not sure if worth though (lazyness??)
inlineStep :: DeclGraph -> [Decl] -> [Decl]
inlineStep g ds = ds''
  where
    --TODO: PatBinds of the kind (a,b) = (exp1, exp2) sometimes could also be inlined
    inlinable :: Decl -> [(String, Exp)]
    inlinable pb@(PatBind _ pat _ _)
      | nodeIsTop g key || patBindIsSelfRecursive g pb = []
      | otherwise =
          case pb of                     
           (PatBind _ (PVar name@(Ident n)) (UnGuardedRhs e) (BDecls [])) -> [(n, e)]
           -- tuples assumed of the same size
           -- (PatBind _ (PTuple Boxed ps) (UnGuardedRhs (Tuple Boxed es)) (BDecls [])) -> []
           _ -> []
      where key = getName2node g $ representant pat
    inlinable _ = []

    inlinables :: [(String, Exp)]
    inlinables = concatMap inlinable (floatDecls ds)

    -- without the inlinables
    ds' :: [Decl]
    ds' = removeDecls (getName2node g) ds $ map (getName2node g . Ident . fst) inlinables 

    -- substitution
    -- WARNING: iterateWhileDiff is inefficient... 
    ds'' = iterateWhileDiffLast (map (_mapDecl (subsIdents inlinables))) ds'


-- | removes declarations of the specified Node's
-- the specified declarations MUST be consistent with the given DeclGraph (in terms of names <-> nodes)
-- a common case of use would be to pass the top declarations
-- if a patter bind binds two or more names, it is removed iff at least one name is in the remove list
-- if a typesignature contains more than one name, only the specified names are removed (if no names are left, the whole decl is removed)
-- careful! child declarations are also removed
removeDecls :: (Name -> Node) -> [Decl] -> [Node] -> [Decl]
removeDecls name2node ds rkeys = removeR  ds
  where astm = mkAstMap emptyMapArgs {_maybeMapBinds = [rb]}
        rkeysS = IntSet.fromList rkeys

        removeR :: [Decl] -> [Decl]
        removeR = map (_mapDecl astm) . mapMaybe filterDecl

        rb :: Binds -> Maybe Binds
        rb (BDecls ds) = Just $ BDecls $ removeR ds 

        -- returns Nothing if the declaration must be removed 
        filterDecl :: Decl -> Maybe Decl
        filterDecl self@(PatBind _ p _ _) = toMaybe (const $ not remove) self
          where keys =  setToIntSet name2node $ patNamesS p
                remove = not $ IntSet.null $ IntSet.intersection keys rkeysS
        filterDecl self@(FunBind (Match _ n _ _ _ _:_)) = toMaybe (const $ not remove) self
          where remove = IntSet.member (name2node n) rkeysS
        filterDecl (TypeSig s ns t)
          | null ns' = Nothing
          | otherwise = Just (TypeSig s ns' t)
          where ns' = filter (flip IntSet.notMember rkeysS . name2node) ns
        filterDecl d = Just d

