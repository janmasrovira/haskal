module Analysis.Operators (
  isAssociative_, isConcat, isCommutative_, neutralExp,
  concatQOp, consQOp, composeQOp, associativity_, isComplementary_, isUnaryIdempotent_, isBinaryIdempotent_,
  appQOp, foldingFunction, OpProperties (..), getOpProperties, boolInverse, consCon,
  isAbsoluteNull
  ) where

--Own
import           Analysis.Helper
import           Common.Applicative
import           Parsing.Parser

-- Exts
import           Language.Haskell.Exts.Fixity
import           Language.Haskell.Exts.Pretty
import           Language.Haskell.Exts.Syntax

-- Utilities
import           Control.Arrow
import           Control.Category (Category)
import           Control.Concatenative
import           Data.List
import           Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import           Data.Maybe
import           Data.Set (Set)
import qualified Data.Set as Set

data OpProperties = OpProperties {
  isAssociative :: Bool,
  isCommutative :: Bool,
  associativity :: Assoc,
  isBinaryIdempotent :: Bool,
  isUnaryIdempotent :: Bool,
  isComplementary :: Bool,
  operatorName :: QName
  }
  

getOpProperties :: QName -> OpProperties
getOpProperties op = OpProperties {
  operatorName = op,
  isAssociative = isAssociative_ op,
  isCommutative = isCommutative_ op,
  isComplementary = isComplementary_ op,
  isUnaryIdempotent = isUnaryIdempotent_ op,
  isBinaryIdempotent = isBinaryIdempotent_ op,
  associativity = associativity_ op
  }

associativeOps :: Set QName
associativeOps = Set.fromList $
                 map (UnQual . Symbol) syms
                 ++ map (UnQual . Ident) idents
  where syms = ["+","*","++", "&&", "||"]
        idents = ["lcm","gcd","max","min"]

isAssociative_ :: QName -> Bool
isAssociative_ = (`Set.member`associativeOps)

isConcat :: QOp -> Bool
isConcat (QVarOp (UnQual (Symbol "++"))) = True
isConcat _ = False
           
commutativeOps :: Set QName
commutativeOps = Set.fromList $
                 map (UnQual . Symbol) syms
                 ++ map (UnQual . Ident) idents
  where syms = ["+","*", "||", "&&"]
        idents = ["lcm","gcd","max","min"]
       
isCommutative_ :: QName -> Bool
isCommutative_ = (`Set.member`commutativeOps)

neutralExp :: QName -> Maybe Exp
neutralExp = (`Map.lookup`neutralExps)
  where neutralExps = Map.fromList $
                      map (first $ UnQual . Symbol) syms ++
                      map (first $ UnQual . Ident) idents
        syms = [("+", Lit (Int 0)),
                ("*", Lit (Int 1)),
                ("++", List []),
                (".", idExp),
                ("$", idExp),
                ("&&", Con (UnQual (Ident "True"))),
                ("||", Con (UnQual (Ident "False")))]
        idents = [
          ]

concatQOp :: QOp
concatQOp = QVarOp (UnQual (Symbol "++"))

appQOp :: QOp
appQOp = QVarOp (UnQual (Symbol "$"))

consQOp :: QOp
consQOp = QConOp (Special Cons)

consCon :: Exp
consCon = Con (Special Cons)

composeQOp :: QOp
composeQOp = QVarOp (UnQual (Symbol "."))

assocMap :: Map QName Assoc
assocMap = Map.fromList [ (qn, assoc) | (Fixity assoc _ qn) <- preludeFixities ]

associativity_ :: QName -> Assoc
associativity_ = fromMaybe AssocLeft . (`Map.lookup`assocMap)


foldingFunction :: QName -> Maybe QName
foldingFunction = (`Map.lookup`foldingFunctionMap)

foldingFunctionMap :: Map QName QName
foldingFunctionMap = Map.fromList $ idents ++ symbols
  where
    symbols = map ((Symbol***Ident) >>> both UnQual)
             [("++", "concat"),
             ("+", "sum"),
             ("*", "product"),
             ("&&", "and"),
             ("||", "or")]
    idents = map (both $  UnQual . Ident)
             [("min", "minimum"),
             ("max", "maximum")]

absoluteNull :: QName -> Maybe Exp
absoluteNull = (`Map.lookup`absoluteNullMap) 

isAbsoluteNull :: Exp -> QName -> Bool
isAbsoluteNull e q
  | maybe False (==e) (absoluteNull q) = True
  | otherwise = case (e, q) of
    --(App (Var (UnQual (Ident "const"))) _ ,UnQual (Symbol ".")) -> True
    _ -> False
                
absoluteNullMap :: Map QName Exp
absoluteNullMap = Map.fromList $ symbols
  where
    symbols = map (first $ UnQual . Symbol)
             [("*", Lit (Int 0)),
              ("&&", Con (UnQual (Ident "False"))),
              ("||", Con (UnQual (Ident "True")))
             ]

-- f (f x) = f x
isUnaryIdempotent_ :: QName -> Bool
isUnaryIdempotent_ = (`Set.member`unaryIdempotentQOps)

unaryIdempotentQOps :: Set QName
unaryIdempotentQOps  = Set.fromList $ map (UnQual . Ident) ["abs"]

-- f x x = x
isBinaryIdempotent_ :: QName -> Bool
isBinaryIdempotent_ = (`Set.member`binaryIdempotentQOps)

binaryIdempotentQOps :: Set QName
binaryIdempotentQOps = Set.fromList $
                       map (UnQual . Ident) ["max", "min", "gcd", "lcm"] ++
                       map (UnQual . Symbol) ["&&", "||"]

-- c (c x) = x
isComplementary_ :: QName -> Bool
isComplementary_ = (`Set.member`complementaryOps)


complementaryOps :: Set QName
complementaryOps = Set.fromList $
                   map (UnQual . Ident) ["reverse", "negate"]

boolInverse :: QName -> Maybe QName
boolInverse = (`Map.lookup`m)
  where m = Map.fromList $ map (both $ UnQual . Symbol)
            [ ("<", ">="),
              ("<=", ">"),
              (">", "<="),
              (">=", "<")]

