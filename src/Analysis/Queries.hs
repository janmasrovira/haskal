module Analysis.Queries (
  matchIsSelfRecursive
  , matchIsBase
  , nameIsRecursive
  , filterNodesWithDecl
  , filterNamesWithDecl
  , getAllNodes
  , getAllNames
  , getReachableNodes
  , getSig
  , reachableNodesFromRhsX
  , reachableNamesFromRhsX
  , nodeIsReachableFromRhsX
  , nameIsReachableFromRhsX
  , getTopFunNodes
  , getFunNodes
  , getAllDecls
  , getAllPatBinds
  , nameIsSelfRecursive
  , nodeIsSelfRecursive
  , nodeIsRecursive
  , patBindIsSelfRecursive
  , getUnLabeledTopDecls
  , nameHasDecl
  , nameIsTop
  , nodeIsTop
  , getTopBindedNodes
  , reachesName
  , reachesNode
  , nameDeclaration
  , getTopBindedNames
  , reachableExprsFromNode
  ) where

--Own
import           Analysis.AstTraversal
import           Analysis.AstTraversalCommon
import           Analysis.Helper
import           Analysis.Recursion
import           Analysis.Substitution
import           Common.Applicative
import           Common.Set
import           Common.Trace
import           Parsing.Parser

-- Exts
import           Language.Haskell.Exts.Pretty
import           Language.Haskell.Exts.Syntax

-- Utilities
import           Control.Arrow
import           Control.Concatenative
import           Control.Lens                      hiding (both)
import           Data.Either.Unwrap
import           Data.IntMap                       (IntMap)
import qualified Data.IntMap                       as IntMap
import           Data.IntSet                       (IntSet)
import qualified Data.IntSet                       as IntSet
import           Data.List                         hiding ((\\))
import           Data.Map                          (Map)
import qualified Data.Map                          as Map
import           Data.Maybe
import           Data.Set                          ((\\), Set)
import qualified Data.Set                          as Set

-- Debug


getSig :: DeclGraph -> Node -> Decl
getSig g = (_node2sigMap g IntMap.!)

getTopFunNodes :: DeclGraph -> NodeSet
getTopFunNodes g = IntSet.fromList $ [getName2node g $ funBindName d | d <- _topDecls g,
                                         isFunBind d]

getTopBindedNodes :: DeclGraph -> NodeSet
getTopBindedNodes g = IntSet.fromList $ toNodes g $ concatMap bindedNames $ _topDecls g

getTopBindedNames :: DeclGraph -> [Name]
getTopBindedNames g = concatMap bindedNames $ _topDecls g

nodeIsTop :: DeclGraph -> IntSet.Key -> Bool
nodeIsTop g = (`IntSet.member` getTopBindedNodes g)


-- | True if a top level declaration is provided
nameIsTop :: DeclGraph -> Name -> Bool
nameIsTop g n
  | nameHasDecl g n = nodeIsTop g $ getName2node g n
  | otherwise = False


getFunNodes :: DeclGraph -> NodeSet
getFunNodes g = IntSet.filter (isFunBind . getDecl g) $ _nodesWithDecl g


getReachableNodes :: DeclGraph -> Node -> NodeSet
getReachableNodes g = (view nodeReachabilityMap g IntMap.!)


-- |Set of ALL (known and unkown) name keys found
getAllNodes :: DeclGraph -> NodeSet
getAllNodes g = IntMap.keysSet (_node2nameMap g)


getAllNames :: DeclGraph -> Set Name
getAllNames g = toNamesS g $ getAllNodes g


getAllDecls :: DeclGraph -> [Decl]
getAllDecls = floatDecls . _topDecls


getAllPatBinds :: DeclGraph -> [Decl]
getAllPatBinds = filter isPatBind . getAllDecls


nameHasDecl :: DeclGraph -> Name -> Bool
nameHasDecl dg = maybe False (nodeHasDecl dg) . flip Map.lookup m
  where m = _name2nodeMap dg


getUnLabeledTopDecls :: DeclGraph -> [Decl]
getUnLabeledTopDecls = map unLabelDecl . view topDecls

testRecursiveMatch :: IO ()
testRecursiveMatch = do
  g <- parseDeclGraph d
  --printPretties ds
  let
      names = map Ident ["f"]
      nodes = map (getName2node g) names
      fdecl = getDecl g (head nodes)
      (bs, rs) = partition (matchIsBase g) (fromJust $ funBindMatches fdecl)
  --print (getAllNames g)
  --print (toNames g $ getKnownNames g)
  putStrLn $ both (unlines . map (prettyPrint)) >>>
    uncurry (\a b -> unlines ["Base matches:",a,"Recursive matches:", b]) $ (bs, rs)
    where d = "src/Tests/Data/Recursion.hs"

reachableNamesFromRhsX :: DeclGraph -> (AstFold (Set Name) -> Set a1 -> a -> Set Name) -> a -> Set Name
reachableNamesFromRhsX g foldX = toNamesS g . reachableNodesFromRhsX g foldX

nameIsReachableFromRhsX
  :: DeclGraph
     -> (AstFold (Set Name) -> Set a -> t -> Set Name)
     -> t
     -> Name
     -> Bool
nameIsReachableFromRhsX g foldX x name =
  case name `Map.lookup` _name2nodeMap g of
  Just node -> nodeIsReachableFromRhsX g foldX x (getName2node g name)
  _ -> False

nodeIsReachableFromRhsX :: DeclGraph
     -> (AstFold (Set Name) -> Set a -> t -> Set Name)
     -> t
     -> Node
     -> Bool
nodeIsReachableFromRhsX g foldX x = (`IntSet.member`reachableNodesFromRhsX g foldX x)

-- | if the starting name declaration depends on the second name
reachesName :: DeclGraph -> Name -> Name -> Bool
reachesName dg start end = maybe False fnodes
  (do
       sn <- mbName2node dg start
       se <- mbName2node dg end
       return (sn, se))
  where fnodes = uncurry (reachesNode dg)

reachesNode :: DeclGraph -> Node -> Node -> Bool
reachesNode dg start end = IntSet.member end $ getReachableNodes dg start


r :: IO ()
r = do
  g <- parseDeclGraph d
  print (view node2nameMap g)
  print (view nodeReachabilityMap g)
  printDeclGraph g
  --mapM_ (putStrLn . prettyPrint) ( g0)
  where
    d = "src/Tests/Data/Constraints.hs"


ta = do
  g <- parseDeclGraph d
  print $ take 5 $ Set.toList $ mconcat $ map (foldDecl astAllExprs mempty) (g ^. topDecls)
    where
      d = "src/Tests/Data/Constraints.hs"

patBindIsSelfRecursive :: DeclGraph -> Decl -> Bool
patBindIsSelfRecursive g (PatBind _ p r b) = any (nameIsSelfRecursive g) names
  where names = patNames p

-- | the name must be known
nameIsSelfRecursive :: DeclGraph -> Name -> Bool
nameIsSelfRecursive g = nodeIsSelfRecursive g . (getName2node g)

-- | true if it can reach itself or if exists another reachable node x from
-- this one such that (nodeIsRecursive x)
nodeIsSelfRecursive :: DeclGraph -> Node -> Bool
nodeIsSelfRecursive g n = reachesNode g n n

-- | TODO:
matchIsSelfRecursive :: DeclGraph -> Match -> Bool
matchIsSelfRecursive g m = nameIsReachableFromRhsX g foldMatch m (matchName m)

matchIsBase :: DeclGraph -> Match -> Bool
matchIsBase g = not . matchIsSelfRecursive g

f :: IO ()
f = do
  (a,b,c) <- readFile d <$>> parseModule d <$>> fromRight
  --printPretties ds
  let g = mkDeclGraph (a,b,c)
      names = map Ident ["f"]
      nodes = map (getName2node g) names
      fdecl = getDecl g (head nodes)
      (bs, rs) = partition (matchIsBase g) (fromJust $ funBindMatches fdecl)
  --print (getAllNames g)
  --print (toNames g $ getKnownNames g)
  putStrLn $ both (unlines . map prettyPrint) >>>
    uncurry (\a b -> unlines ["Base matches:",a,"Recursive matches:", b]) $ (bs, rs)
    where d = "src/Tests/Data/Recursion.hs"


-- | name must have an associated node
nameIsRecursive :: DeclGraph -> Name -> Bool
nameIsRecursive d = maybe False (nodeIsRecursive d) . mbName2node d

nodeIsRecursive :: DeclGraph -> Node -> Bool
nodeIsRecursive d = genericIsRecursive IntSet.empty d . IntSet.singleton

-- | same as the reachability map in the DeclGraph but the search starts at the Rhs of a certain declaration instead (useful for knowing if a match is self recursive)
-- note: for PatBind is the same as the reachability map
reachableNodesFromRhsX  :: DeclGraph
                           -> (AstFold (Set Name) -> Set a -> t -> Set Name)
                           -> t
                           -> NodeSet
reachableNodesFromRhsX g foldX x  =
  let nodesInExp = setToIntSet (getName2node g) $ foldX astNamesRhsS Set.empty x
      knownNodes = filterNodesWithDecl g nodesInExp
      recurNodes = IntSet.unions $ mapMaybe (`IntMap.lookup`_nodeReachabilityMap g) $
                   IntSet.toList knownNodes
  in IntSet.union nodesInExp recurNodes



genericIsRecursive :: NodeSet -- ^Already seen Nodes
                      -> DeclGraph -- ^Declaration graph
                      -> NodeSet -- ^Nodes to search recursively
                      -> Bool -- ^is recursive
genericIsRecursive startSet
  g@DeclGraph {_nodeMap = nameMap, _nodesWithDecl = known} =
    any (recur startSet) . IntSet.toList . filterNodesWithDecl g
  where
    na2no = getName2node g
    suc i = IntMap.keys $ (nameMap IntMap.! i)
    recur :: NodeSet -> Node -> Bool
    recur x node
      | node`IntSet.member`x = True
      | node`IntSet.notMember`known = False -- Note that unknown names are assumed nonrecursive
      | otherwise = any (recur x') sucs
      where sucs = suc node
            x' = IntSet.insert node x

filterNodesWithDecl :: DeclGraph -> NodeSet -> NodeSet
filterNodesWithDecl DeclGraph {_nodesWithDecl = known} = IntSet.filter (`IntSet.member`known)

filterNamesWithDecl :: DeclGraph -> [Name] -> [Name]
filterNamesWithDecl g@DeclGraph {_nodesWithDecl = known} = filter ((`IntSet.member`known) . getName2node g)


nameDeclaration :: DeclGraph -> Name -> Maybe Decl
nameDeclaration dg name = maybe Nothing (`IntMap.lookup` (dg^.node2declMap))
                          (name `Map.lookup` (dg^.name2nodeMap))



-- | collects all the expressions reachable from a node definition
-- If the node does not have a declaration, an empty list is returned
-- Non recursive through inner binds
reachableExprsFromNode :: DeclGraph -> Node -> [Exp]
reachableExprsFromNode dg node
  | nodeHasDecl dg node = Set.toList $ mconcat $ map (foldDecl astAllExprs mempty) starts
  | otherwise = []
  where
    starts :: [Decl]
    starts =  mapMaybe (mbGetDecl dg) $ IntSet.toList $ IntSet.insert node $
              IntMap.keysSet ((dg ^. nodeMap) IntMap.! node)
