{-# LANGUAGE TemplateHaskell #-}

module Analysis.Recursion (
  mkDeclGraph
  , Node
  , NodeSet
  , DeclGraph (..)
    --lenses:
  ,  nodeMap
  , node2nameMap
  , name2nodeMap
  , topDecls
  , knownSigs
  , nodesWithDecl
  , node2declMap
  , node2sigMap
  , imports
  , sourcePath
  , nodeReachabilityMap
    -- end lenses
  , printDeclGraph
  , addDecls
  , getName2node
  , getNode2name
  , getDecl
  , parseDeclGraph
  , floatDecls
  , mparseDeclGraph
  , mkDeclGraph'
  , getTypeSigs
  , getTypeSig
  , mparseDeclGraphWithSigs
  , mparseDeclGraphFromString
  , subsDecl
  , nodeHasDecl
  , mbGetDecl
  , getLabeledDecls
  , toNames
  , toNodes
  , toNodesS
  , toNamesS
  , mbName2node
  , mbNode2name
  , mbGetNameDecl
  ) where

-- All functions assume unique names !!!


--Own
import           Analysis.AstTraversal
import           Analysis.AstTraversalCommon
import           Analysis.Helper
import           Analysis.Substitution
import           Common.Applicative
import           Common.List
import           Common.Map
import           Common.Set
import           Common.Trace
import           Parsing.Parser


-- Exts
import           Language.Haskell.Exts.Pretty
import           Language.Haskell.Exts.Syntax

-- Utilities
import           Control.Concatenative
import           Data.Either.Unwrap
import           Data.List                         hiding ((\\))
import qualified Data.Map                          as Map
import           Data.Map                          (Map)
import qualified Data.Set                          as Set
import           Data.Set                          ((\\), Set)
import qualified Data.IntMap                       as IntMap
import qualified Data.IntSet                       as IntSet
import           Data.IntMap                       (IntMap)
import           Data.IntSet                       (IntSet)
import           Data.Maybe
import           Control.Arrow
import           Text.Parsec.Error
import           Control.Lens                      hiding (both)

-- Graph
import qualified Data.Graph.Inductive.Graph        as Graph
import           Data.Graph.Inductive.NodeMap
import qualified Data.Graph.Inductive.NodeMap      as NodeMap
import           Data.Graph.Inductive.PatriciaTree

-- Debug
import           Debug.Trace

type Node = Int
type NodeSet = IntSet

data DeclGraph = DeclGraph {
  _nodeMap :: IntMap (IntMap Int), -- the count is unreliable
  _node2nameMap :: IntMap Name,
  _name2nodeMap :: Map Name Node,
  _topDecls :: [Decl], -- top declarations
  _nodesWithDecl :: NodeSet, -- set of namekeys which definitions is known (arguments excluded)
  _knownSigs :: NodeSet,
  _nodeReachabilityMap :: IntMap NodeSet, -- reachability of names through definitions
  _node2declMap :: IntMap Decl,
  _node2sigMap :: IntMap Decl,
  _imports :: [ImportDecl],
  _sourcePath :: Maybe String
  } deriving (Show, Eq)

makeLenses ''DeclGraph


parseDeclGraph :: FilePath -> IO DeclGraph
parseDeclGraph d = readFile d <$>> parseModule d <$>> fromRight <$>> mkDeclGraph

--mparseDeclGraph :: FilePath -> IO $ Maybe DeclGraph
mparseDeclGraph :: FilePath -> IO (Either ParseError DeclGraph)
mparseDeclGraph d = readFile d <$>> parseModule d  <$>>
                    right mkDeclGraph


mparseDeclGraphFromString :: String -> String -> Either ParseError DeclGraph
mparseDeclGraphFromString src d = right (mkDeclGraph)
                                  (parseModule src d)

-- | function names (but not arguments) and pattern bindings to their decl
getDecl :: DeclGraph -> Node -> Decl
getDecl g = (_node2declMap g IntMap.!)

mbGetDecl :: DeclGraph -> Node -> Maybe Decl
mbGetDecl g = (`IntMap.lookup` _node2declMap g)

mbGetNameDecl :: DeclGraph -> Name -> Maybe Decl
mbGetNameDecl g name = mbName2node g name >>= mbGetDecl g

mbName2node :: DeclGraph -> Name -> Maybe Node
mbName2node dg = flip Map.lookup (dg^.name2nodeMap)

mbNode2name :: DeclGraph -> Node -> Maybe Name
mbNode2name dg = flip IntMap.lookup (dg^.node2nameMap)

toNames :: DeclGraph -> [Node] -> [Name]
toNames g = map (getNode2name g)

toNodes :: DeclGraph -> [Name] -> [Node]
toNodes g = map (getName2node g)

toNodesS :: DeclGraph -> Set Name -> NodeSet
toNodesS g = setToIntSet (getName2node g)

toNamesS :: DeclGraph -> NodeSet -> Set Name
toNamesS g = intSetToSet (getNode2name g)

mparseDeclGraphWithSigs :: FilePath -> [Decl] -> IO (Either ParseError DeclGraph)
mparseDeclGraphWithSigs d sigs =
  readFile d <$>> parseModule d <$>>
  right (mkDeclGraph . (\(a,b,c) -> (a,b,subsTopSigs sigs c)))


subsTopSigs :: [Decl] -> [Decl] -> [Decl]
subsTopSigs sigs = (++sigs) . filter (not . isTypeSig)

getTypeSigs :: DeclGraph -> [Decl]
getTypeSigs = nubOrd . IntMap.elems . _node2sigMap

getTypeSig :: DeclGraph -> IntSet.Key -> Maybe Decl
getTypeSig = flip IntMap.lookup . _node2sigMap


-- |The added declarations are assumed non-conflicting
-- Previous Node's are invalid
addDecls :: [Decl] -> DeclGraph -> DeclGraph
addDecls ds = mkDeclGraph' . withUniqueNames . map unLabelDecl . (ds ++ ) . view topDecls


-- UNTESTED
subsDecl :: Decl -> DeclGraph -> DeclGraph
subsDecl d = mkDeclGraph' . filter (\x -> not (isFunBind x) || (dname/=funBindName x)). view topDecls
  where dname = funBindName d

preprocess :: [Decl] -> [Decl]
preprocess = map (-- _mapDecl astUnGuardedIfToGuarded .
                  _mapDecl astRemoveParens . _mapDecl astRemoveAppQOp)
             . withUniqueNames . map unLabelDecl


mkDeclGraph' :: [Decl] -> DeclGraph
mkDeclGraph' ds = mkDeclGraph (Nothing,[],ds)

-- note: Declarations are not assumed unlabeled (they are unlabeled, see function preprocess)
-- | what is nodeReachabilityMap: indicates the dependencies between name definitions,
-- for example, if we have "f x = length x where g = ..." then f can reach (i.e. depends on)
-- only "length" but NOT "g"
mkDeclGraph :: SimpleModule -> DeclGraph
mkDeclGraph (path, imps,raw) = g
  where g = DeclGraph { --getNameGraph = ng,
          _nodeMap = nodeMap,
          _node2nameMap = no2na,
          _name2nodeMap = na2no,
          _topDecls = ds,
          _node2declMap = globaldeclmap,
          _nodesWithDecl = known, -- redundant
          _knownSigs = knownsigs,
          _node2sigMap = sigMap,
          _imports = imps,
          _nodeReachabilityMap = nameMapR,
          _sourcePath = path
          }
        ds = preprocess raw
        --ng = globalNameGraph nodeMap no2na na2no
        sigMap = IntMap.fromList [(key,d) | key <- IntSet.toList knownsigs,
                              let name = no2na IntMap.! key
                                  d = head . filter ((name`elem`) . typeSigNames) $ filter isTypeSig ds]
        knownsigs = IntSet.fromList $ concat [ map (na2no Map.!) $ typeSigNames d | d <- floatDecls ds, isTypeSig d]
        globaldeclmap = globalDeclMap ds no2na na2no
        (no2na, na2no) = assignNodes ds
        known = IntMap.keysSet globaldeclmap
        nameMapR = -- reachabilityMap
          IntMap.fromList nameMapRAssocs
          where
            nameMapRAssocs = [(namek, deepNames namek IntSet.empty) | namek <- IntSet.toList known]
            deepNames :: Node -> NodeSet -> NodeSet
            deepNames name accset
              | name`IntSet.member`accset = set'
              | otherwise = deep
                where sucs
                        | nodeHasDecl g name = nodeMap IntMap.! name
                        | otherwise = IntMap.empty
                      set' = IntSet.union accset (IntMap.keysSet sucs)
                      deep = foldr deepNames set' (IntMap.keys sucs)
        declRhsNames = foldDecl astNamesRhs []
        nodeMap :: IntMap (IntMap Int)
        nodeMap = IntMap.map (IntMap.fromList . count .
                              mapMaybe (`Map.lookup`na2no) . declRhsNames) globaldeclmap

nodeHasDecl :: DeclGraph -> Node -> Bool
nodeHasDecl = flip IntSet.member . _nodesWithDecl


getName2node :: DeclGraph -> Name -> Node
getName2node g = (_name2nodeMap g Map.!)

getNode2name :: DeclGraph -> Node -> Name
getNode2name g = (_node2nameMap g IntMap.!)

getLabeledDecls :: DeclGraph -> String
getLabeledDecls = unlines . map ((++"\n"). prettyPrint) . (^. topDecls)

printDeclGraph :: DeclGraph -> IO ()
printDeclGraph r = do
  let no2na = getNode2name r
      na2no = getName2node r
      na2noMap = _name2nodeMap r
      no2naMap = _node2nameMap r
      known = _nodesWithDecl r
      nodeMap = _nodeMap r
      knownsigs = _knownSigs r
      sigMap = _node2sigMap r
      declMap = _node2declMap r
  putStr "Labeled decls:\n" >> mapM_ (putStr . prettyPrint) (r ^. topDecls) >> putStrLn "\n"
  putStr "known -> " >> print known
  putStr "knownSigs -> " >> print knownsigs
  putStr "name2node -> " >> print (Map.assocs na2noMap)
  putStr "node2name -> " >> print (IntMap.assocs no2naMap)
  putStr "nodeMap -> " >> print nodeMap
  putStr "nodeReachabilityMap -> " >> print (_nodeReachabilityMap r)
  putStr "sigMap -> " >> print (IntMap.assocs sigMap)
  --putStr "declMap -> " >> print (IntMap.assocs declMap)

r :: IO ()
r = do
  g <- parseDeclGraph d
  print (view node2nameMap g)
  print (view nodeReachabilityMap g)
  printDeclGraph g
  --mapM_ (putStrLn . prettyPrint) ( g0)
  where
    d = "src/Tests/Data/Constraints.hs"


-- | Map of function names and pattern bindings (arguments ignored) to its declaration.
-- | Lambdas are ignored.
globalDeclMap  :: [Decl] -> IntMap Name -> Map Name Node -> IntMap Decl
globalDeclMap ds no2na na2no = m
  where
    fds = floatDecls ds
    m = IntMap.fromList $ glob fds
    glob :: [Decl] -> [(Node, Decl)]
    glob = concatMap f
    f :: Decl -> [(Node, Decl)]
    f d@(PatBind _ p _ _) = [(na2no Map.! name, d) | name <- patNames p]
    f d@(FunBind _) = [(na2no Map.! funBindName d, d)]
    f _ = []

assignNodes :: [Decl] -> (IntMap Name, Map Name Node)
assignNodes ds = mkBiIndex astAllNames
  where
    astAllNames :: [Name]
    astAllNames = Set.toList $ foldr (flip $ foldDecl astAllNamesS) Set.empty ds


floatDecls :: [Decl] -> [Decl]
floatDecls = concatMap (foldDecl ast [])
  where ast@AstFold {
          foldBinds = fb, foldRhs = fr, foldMatch = fm
                    } = mkAstFold emptyFoldArgs {
          maybeFoldDecl = [fdecl]
          }
        fdecl ac d@(FunBind ms) = Just $ d : (foldl fm ac ms)
        fdecl ac d@(PatBind _ _ r b) = Just $ d : fr (fromMaybe [] $ fmap (fb ac) b) r
        fdecl ac d = Just $ d : ac
