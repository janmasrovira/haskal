module Analysis.SMTProcess (
  runSMTScript,runSMTScript',
  procYices, procZ3, procCVC4 ) where

import SMTLib2
import SMTLib2.Int
import SMTLib2.Core

import Analysis.SMTUtils

import System.IO
import System.Process

-- all processes should read from a stream
procZ3 :: CreateProcess
procZ3 = proc "z3" ["-smt2", "-in"]

procCVC4 :: CreateProcess
procCVC4 = proc "cvc4" ["--lang" , "smt2"]

procYices :: CreateProcess
procYices = proc "yices-smt2" []

runSMTScript :: Script -> IO String
runSMTScript = runSMTScript' procYices
                 
runSMTScript' :: CreateProcess -> Script -> IO String
runSMTScript' process scpt = do
    (Just inh, Just outh, _, proch) <- createProcess process 
                                   {std_in = CreatePipe, std_out = CreatePipe}
    hPutStr inh $ show $ pp scpt
    hClose inh
    waitForProcess proch
    hGetContents outh
    
f = runSMTScript' procCVC4 s1
p = pp s1



s1 = Script [
  CmdSetLogic (N "QF_LIA")
  , CmdSetInfo Attr {attrName = N "smt-lib-version", attrVal = Just $ Lit (LitFrac 2.0)}
  , CmdDeclareFun (N x1) [] tInt
  , CmdAssert $ num 3 === var x1 
  , CmdCheckSat
  , CmdExit
  ]
  where x1 = "x1~~@!"
    

