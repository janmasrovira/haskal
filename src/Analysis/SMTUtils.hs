module Analysis.SMTUtils (
  Inequation(..), IneqLE0, exprOne, exprZero, ExprM,
  complInequation, symbolToInequation, fromIneqLE0,
  var, mMul, mSum, argsIndepIx, Args(..), mkArgs,
  argsN, argsArgsBounds, argsAllBounds, isArgIx, isIndepIx,
  asInequationLE0, asLinearCombination, Table,
  sumExprs, mulExprName, exprSinglArg, exprNames,
  exprMToArrayOnlyArgs, ineqToLE0, mkSMTExpr,
  subExpr, exprSinglArgNum, ineqLE0ToSMT, exprMToArrayNoNames,
  exprMAsList, smtScriptHeader, smtDeclareRealVar, smtDeclareIntVar,
  smtNum, exprSinglTerm, exprMAsListOfIndepTerms,
  prettyIneq, prettyExprM, emptyArgs, smtle0, exprSinglName,
  ineqToSMT, termHasNames, epsilonExprM, epsilonName, epsilonValue,
                                                      smtGetValues
  ) where

import           Analysis.AstTraversal
import           Analysis.AstTraversalCommon
import           Analysis.Recursion
import           Common.Applicative
import           Common.List
import           Parsing.Parser

-- Exts
import           Language.Haskell.Exts.Pretty
import           Language.Haskell.Exts.Syntax

-- Utilities
import           Control.Arrow
import           Control.Concatenative
import           Control.Monad
import           Control.Monad.ST
import           Data.Array.ST
import           Data.Array.Unboxed           as A
import           Data.IntMap                  (IntMap)
import qualified Data.IntMap                  as IntMap
import           Data.Ix
import           Data.List
import           Data.Map.Strict              (Map)
import qualified Data.Map.Strict              as Map
import           Data.Maybe
import           Data.Set                     (Set)
import qualified Data.Set                     as Set
import           Debug.Trace
import qualified SMTLib2                      as SMT
import qualified SMTLib2.Core                 as SMTC
import qualified SMTLib2.Int                  as SMTI
import Numeric (showFFloat)

f = do
  let a = asLinearCombination args exp
      a1 = exprMToArray args <$> a
      b = fmap (mkSMTExpr' args) a1
      c = fmap SMT.pp b
  print sexp
  print $ fmap assocs a1
  print c
    where
      sexp = "c*n + d - (c*(n-1) + d)"
      exp = _mapExp astRemoveParens $ parseExp sexp  --"1 + 3 * (a + b) * (c + 3* d)"
      names = map Ident ["n", "l"] -- foldExp astNamesRhs [] exp
      args = mkArgs $ nubOrd names

type RealNum = Int
type Term = Map (Maybe Name) RealNum
type ExprM = IntMap Term


data Inequation = IneqGT ExprM ExprM
                | IneqGE ExprM ExprM
                | IneqLE ExprM ExprM
                | IneqLT ExprM ExprM
                deriving (Show)

newtype IneqLE0 = IneqLE0 ExprM deriving (Show)

-- expression to denote a very small positive value
epsilonExprM :: ExprM
epsilonExprM = exprSinglName argsIndepIx epsilonName

epsilonName :: Name
epsilonName = Ident epsilonStr

epsilonStr = "_epsilon"

--epsilonValue :: RealNum
epsilonValue = 0.0001

-- so it prints "0.0001" and not 10e-4
epsilonValueStr :: String
epsilonValueStr = showFFloat Nothing epsilonValue ""


smtGetValues :: [String] -> SMT.Command
smtGetValues = SMT.CmdGetValue . map var

smtScriptHeader :: [SMT.Command]
smtScriptHeader = [
  SMT.CmdSetLogic (SMT.N "QF_LIA") ,
  -- SMT.CmdSetLogic (SMT.N "QF_LRA"),
  SMT.CmdSetInfo SMT.Attr {SMT.attrName = SMT.N "smt-lib-version",
                           SMT.attrVal = Just $ SMT.Lit (SMT.LitFrac 2.0)} ,
  SMT.CmdSetOption $ SMT.OptProduceModels True]
                  -- ++ smtEpsDefinition

smtEpsDefinition = [
  smtDeclareRealVar epsilonName ,
  SMT.CmdAssert (var epsilonStr SMTC.=== var epsilonValueStr)
  ]

smtDeclareRealVar :: Name -> SMT.Command
smtDeclareRealVar (Ident i) = SMT.CmdDeclareFun (SMT.N i) [] tRealNum
smtDeclareIntVar :: Name -> SMT.Command
smtDeclareIntVar (Ident i) = SMT.CmdDeclareFun (SMT.N i) [] SMTI.tInt

tRealNum :: SMT.Type
tRealNum = SMT.TApp (SMT.I (SMT.N "Real") []) []

ineqToLE0 :: Inequation -> IneqLE0
-- a > b ==> b - a + 1 <= 0
ineqToLE0 (IneqGT a b) = IneqLE0 $ sumExpr exprOne (subExpr b a)

-- a >= b ==> b - a <= 0
ineqToLE0 (IneqGE a b) = IneqLE0 $ subExpr b a

-- a <= b ==> a - b <= 0
ineqToLE0 (IneqLE a b) = IneqLE0 $ subExpr a b

-- a < b ==> a - b + 1 <= 0
ineqToLE0 (IneqLT a b) = IneqLE0 $ sumExpr exprOne (subExpr a b)

-- | complementary of an inequation
complInequation :: Inequation -> Inequation
complInequation (IneqGT a b) = IneqLE a b
complInequation (IneqGE a b) = IneqLT a b
complInequation (IneqLT a b) = IneqGE a b
complInequation (IneqLE a b) = IneqGT a b

symbolToInequation :: String -> Maybe (ExprM -> ExprM -> Inequation)
symbolToInequation "<" = Just IneqLT
symbolToInequation ">=" = Just IneqGE
symbolToInequation ">" = Just IneqGT
symbolToInequation "<=" = Just IneqLE
symbolToInequation _ = Nothing


ineqLE0ToSMT :: Args -> IneqLE0 -> SMT.Expr
ineqLE0ToSMT args (IneqLE0 e) = smtle0 (mkSMTExpr args e) 

-- | creates an inequation from an expression of
-- the kind "linExp1 < linExp2", infix operators supported: <=, >=, <, >
expToIneq :: Args -> Exp -> Maybe Inequation
expToIneq args (InfixApp a (QVarOp (UnQual (Symbol op))) b) = do
  ineq <- symbolToInequation op
  [la,lb] <- mapM (asLinearCombination args) [a,b]
  return $ ineq la lb
expToIneq _ _ = Nothing

fromIneqLE0 :: IneqLE0 -> ExprM
fromIneqLE0 (IneqLE0 e) = e

data Args = Args {
  -- indexs from 1 to n
  argsNames :: Array Int Name
  , argsNameIx :: Name -> Maybe Int
  }

emptyArgs :: Args
emptyArgs = mkArgs []

type Table = UArray (Int,Int) RealNum

-- returns either the index of the argument or else the independent index
argsNameIndepIx :: Args -> Name -> Int
argsNameIndepIx args n = head $ catMaybes [argsNameIx args n, Just argsIndepIx]

isArgIx :: Args -> Int -> Bool
isArgIx args = inRange (argsArgsBounds args)

isIndepIx :: Int -> Bool
isIndepIx = (== argsIndepIx)

argsArgsBounds :: Args -> (Int, Int)
argsArgsBounds args = (1, argsN args)

argsAllBounds :: Args -> (Int, Int)
argsAllBounds args = (0, argsN args)

argsN :: Args -> Int
argsN = snd . bounds . argsNames

-- | index of the independent term
argsIndepIx :: Int
argsIndepIx = 0

-- | precond: No repeated names
mkArgs :: [Name] -> Args
mkArgs names = Args {argsNames = argsNames_, argsNameIx = argsNameIx_}
  where
    argsNames_ = listArray (1, length names) names
    argsNameIx_ = flip Map.lookup dict
      where dict = Map.fromList $ map swap (A.assocs argsNames_) 


-- | not sure if this is the simplest method
var :: String -> SMT.Expr
var x = SMT.App (SMT.I (SMT.N x) []) Nothing []

smtNum :: RealNum -> SMT.Expr
--smtNum = SMT.Lit . SMT.LitFrac . toRational  -- for Reals
smtNum = SMT.Lit . SMT.LitNum . toInteger -- for Integrals

mApp :: String -> Maybe SMT.Expr -> [SMT.Expr] -> Maybe SMT.Expr
mApp fun neut xs = case neut of
  Just neutral -> mApp' $ filter (/=neutral) xs
  Nothing -> mApp' xs
  where
    mApp' [] = Nothing -- undefined -- testing, this line can be removed
    mApp' [x] = Just x
    mApp' xs = Just $ SMT.App (SMT.I (SMT.N fun) []) Nothing xs

mMul :: [SMT.Expr] -> Maybe SMT.Expr
mMul = mApp "*" (Just 1)

mSum :: [SMT.Expr] -> Maybe SMT.Expr
mSum = mApp "+" (Just 0)

smtNegate :: SMT.Expr -> SMT.Expr
smtNegate e = SMT.App (SMT.I (SMT.N "-") []) Nothing [e]

ineqToSMT :: Args -> Inequation -> SMT.Expr
ineqToSMT args = smtle0 . mkSMTExpr args . fromIneqLE0 . ineqToLE0

smtle0 :: SMT.Expr -> SMT.Expr
smtle0 a = fromJust $ mApp "<=" Nothing [a,smtNum 0]

mkSMTExpr :: Args -> ExprM -> SMT.Expr
mkSMTExpr args = mkSMTExpr' args . exprMToArray args 

prettyIneq :: Args -> IneqLE0 -> String
prettyIneq args (IneqLE0 e) = prettyExprM args e

mkSMTExpr' :: Args -> Array Int Term -> SMT.Expr
mkSMTExpr' args = fromMaybe (smtNum 0) . mSum .  mapMaybe f . assocs
  where
    f :: (Int, Term) -> Maybe SMT.Expr 
    f (ix, m)
      | isIndepIx ix = case mapMaybe (uncurry gmul) (Map.assocs m) of
        [] -> Just $ smtNum 0
        ss -> mSum ss
      | otherwise = let Ident name = argsNames args ! ix
                    in case sums of
                      Just (SMT.Lit (SMT.LitFrac 0)) -> Nothing
                      Just (SMT.Lit (SMT.LitNum 0)) -> Nothing
                      Just jsums -> mMul [var name, jsums]
                      _ -> Nothing
      where
        sums
          | Map.null m = Just $ smtNum 0
          | otherwise = mSum $ mapMaybe (uncurry gmul) (Map.assocs m)
        gmul :: Maybe Name -> RealNum -> Maybe SMT.Expr
        gmul (Just (Ident name)) 0 = Nothing
        gmul (Just (Ident name)) 1 = Just $ var name 
        gmul (Just (Ident name)) (-1) = Just $ smtNegate (var name)
        gmul (Just (Ident name)) k = mMul [var name, smtNum k]
        gmul Nothing k = Just $ smtNum k

exprMToArray :: Args -> ExprM -> Array Int Term
exprMToArray args dict = array (0, argsN args)
                         [(i, fromMaybe Map.empty $ IntMap.lookup i dict)  | i <- range bnds]
  where bnds = (0, argsN args)

exprMToArrayOnlyArgs :: Args -> ExprM -> UArray Int RealNum
exprMToArrayOnlyArgs args dict =
  array bnds [(i, fromMaybe 0 (IntMap.lookup i dict >>= Map.lookup Nothing ))
                        | i <- range bnds]
  where bnds = argsArgsBounds args

exprMToArrayNoNames :: Args -> ExprM -> UArray Int RealNum
exprMToArrayNoNames args dict =
  array bnds [(i, fromMaybe 0 (IntMap.lookup i dict >>= Map.lookup Nothing ))
             | i <- range bnds]
  where bnds = argsAllBounds args

-- | let a1,a2,.. be the args
-- the expression 3*a1*x + 2*a2 + 9 returns [9, 3*a1*x, 2*a2]
exprMAsList :: Args -> ExprM -> [ExprM]
exprMAsList args e = [ termAsExpr ix e | ix <- range $ argsAllBounds args] 

exprMAsListOfIndepTerms :: Args -> ExprM -> [ExprM]
exprMAsListOfIndepTerms args e = [ termAsIndepExpr ix e | ix <- range $ argsAllBounds args]

termAsIndepExpr :: IntMap.Key -> ExprM -> ExprM
termAsIndepExpr ix e = fromMaybe exprZero (IntMap.singleton argsIndepIx <$> IntMap.lookup ix e)


asInequationLE0 :: Args -> Exp -> Maybe IneqLE0
asInequationLE0 args e = ineqToLE0 <$> expToIneq args e


asLinearCombination :: Args -> Exp -> Maybe ExprM
asLinearCombination args exp' = lc exp
  where
    exp = _mapExp astRemoveParens exp'
    bnds = (0, argsN args)

    lc :: Exp -> Maybe ExprM
    lc (Var (UnQual name)) =
      case argsNameIx args name of
        Just ix -> Just $ exprSinglArg ix
        _ ->  Just $ exprSinglName (argsNameIndepIx args name) name
    lc (NegApp e) = negateExpr <$> lc e
    lc (Lit (Int n)) = Just $ exprSinglNum argsIndepIx 
                       (fromIntegral n)
    lc (InfixApp a (QVarOp (UnQual (Symbol "+"))) b) = do
      [la,lb] <- mapM lc [a,b]
      return $ sumExpr la lb
    lc (InfixApp a (QVarOp (UnQual (Symbol "-"))) b) = do
      [la,lb] <- mapM lc [a,b]
      return $ subExpr la lb
    lc (InfixApp a (QVarOp (UnQual (Symbol "*"))) b) = do
      [la, lb] <- mapM lc [a,b]
      maybeMulExpr args la lb
    lc _ = Nothing

termIsZero :: Term -> Bool
termIsZero = all (== 0)

termHasNames :: Term -> Bool
termHasNames = not . Set.null . termNames

termNames :: Term -> Set Name
termNames m = Set.fromList $ map (fromMaybe undefined . fst)
              $ filter ((/=0) . snd) $ filter (isJust . fst) (Map.assocs m)

sumTerm :: (Num a, Ord k) => Map k a -> Map k a -> Map k a
sumTerm = Map.unionWith (+)

exprIsZero :: ExprM -> Bool
exprIsZero = all termIsZero

mulTerm :: (Maybe Name, RealNum) -> Term -> Term
mulTerm (Nothing, k) = Map.map (*k)
mulTerm (mname, k) = Map.singleton mname . fromMaybe 0  . Map.lookup Nothing

exprHasArgsOrNames :: Args -> ExprM -> Bool
exprHasArgsOrNames args = not . Set.null . exprNamesAndArgs args

mulExprName :: (Maybe Name, RealNum) -> ExprM -> ExprM
mulExprName namek = IntMap.map (mulTerm namek)

exprNames :: ExprM -> Set Name
exprNames = Set.unions . map termNames . IntMap.elems

exprArgs :: Args -> ExprM -> Set Name
exprArgs args m = Set.fromList [argsNames args ! ix | (ix, t) <- IntMap.assocs m,
                                isArgIx args ix, not $ termIsZero t]

exprNamesAndArgs :: Args -> ExprM -> Set Name
exprNamesAndArgs args m = exprArgs args m `Set.union` exprNames m

sumExpr :: ExprM -> ExprM -> ExprM
sumExpr = IntMap.unionWith sumTerm 

sumExprs :: [ExprM] -> ExprM
sumExprs = foldr sumExpr exprZero

subExpr :: ExprM -> ExprM -> ExprM
subExpr la = sumExpr la . negateExpr

-- the expression should not contain any name
mulExpr :: ExprM -> Term -> 
           ExprM
mulExpr m = sumExprs . map (`mulExprName` m) . filter ((/=0) . snd) . Map.assocs


exprIndepTerm :: ExprM
                 -> Term
exprIndepTerm = fromMaybe Map.empty . IntMap.lookup argsIndepIx

negateExpr :: ExprM -> ExprM
negateExpr = IntMap.map (Map.map negate)

exprSinglNum :: IntMap.Key -> RealNum -> ExprM
exprSinglNum ix num = IntMap.singleton ix $ Map.singleton Nothing num

exprSinglName :: IntMap.Key -> Name -> ExprM
exprSinglName ix name = IntMap.singleton ix $ Map.singleton (Just name) 1

prettyExprM :: Args -> ExprM -> String
prettyExprM args e = show $ SMT.pp (mkSMTExpr args e)

exprSinglArg :: IntMap.Key -> ExprM
exprSinglArg = exprSinglArgNum 1

exprOne :: ExprM
exprOne = IntMap.singleton argsIndepIx (Map.singleton Nothing 1)

exprZero :: ExprM
exprZero = IntMap.empty

-- maybeMulName :: Args -> ExprM -> Name -> Maybe ExprM
-- maybeMulName args e = maybeMulExpr args e . exprSinglName argsIndepIx  

-- exprArgsAllOne :: Args -> IntMap Int
-- exprArgsAllOne args = IntMap.fromList [(i, 1) | i <- range (argsArgsBounds args)]

exprSinglArgNum :: RealNum -> IntMap.Key -> ExprM
exprSinglArgNum num ix = exprSinglArgNumName num ix Nothing

exprSinglTerm :: IntMap.Key -> Term -> IntMap Term
exprSinglTerm = IntMap.singleton

exprSinglArgNumName :: RealNum -> IntMap.Key -> (Maybe Name) -> ExprM
exprSinglArgNumName num ix name = IntMap.singleton ix $ Map.singleton name num

termAsExpr :: IntMap.Key -> ExprM -> ExprM
termAsExpr ix e = fromMaybe exprZero (IntMap.singleton ix <$> IntMap.lookup ix e)



maybeMulExpr ::
  Args
  -> ExprM
  -> ExprM
  -> Maybe ExprM
maybeMulExpr args a b =
  case partition (not . exprHasArgsOrNames args) [a,b] of
    (a@(_:_), b) -> let [litE, x] = a++b
                        litT = exprIndepTerm litE
                    in Just $ mulExpr x litT
    -- both have args or names
    _ -> 
      case partition (Set.null . exprArgs args) [a,b] of
      ([noargs], [nonames])
        | Set.null $ exprNames nonames -> let litT = exprIndepTerm noargs
                                          in Just $ mulExpr nonames litT
        | otherwise -> Nothing
      z -> Nothing
      
