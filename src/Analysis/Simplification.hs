module Analysis.Simplification (simplifyAll, simplifyEasy) where

--Own
import           Analysis.AstTraversal
import           Analysis.AstTraversalCommon
import           Analysis.Helper
import           Analysis.Operators
import           Analysis.Queries
import           Analysis.Recursion
import           Analysis.Substitution
import           Common.List
import           Parsing.Parser

-- Exts
import           Language.Haskell.Exts.Pretty
import           Language.Haskell.Exts.Syntax

-- Utilities
import           Control.Arrow
import           Control.Concatenative
import           Control.Monad
import           Data.Bits
import           Data.Either.Unwrap
import           Data.IntMap.Strict           (IntMap,(!))
import qualified Data.IntMap.Strict           as IntMap
import           Data.IntSet                  (IntSet)
import qualified Data.IntSet                  as IntSet
import           Data.List                    hiding ((\\))
import           Data.Map.Strict              (Map)
import qualified Data.Map.Strict              as Map
import           Data.Maybe
import           Data.Set                     ((\\), Set)
import qualified Data.Set                     as Set

import           Debug.Trace

f = mapM_ (putStrLn.prettyPrint) $ 
    simplifyEasy _mapExp $
    parseExp e16
    where e0 = "[] ++ ([] ++ []) ++ [1 + 0 + 5 *1 *1 ] + 0"
          e1 = "reverse $ (reverse.reverse) $ reverse x"
          e3 = "map (*1) $ [1,2,3]"
          e4 = "max (abs (abs x)) (abs x)"
          e5 = "concat $ (map (1:) [[0],[2]])"
          e51 = "(concat . map reverse, concat (map (++[1])), concat (map (++[1])) ll)"
          e6 = "[\\a b -> a:b, \\a b -> b * a, \\a b -> b ++ a]"
          e7 = "map (\\x -> x) [1,2]"
          e8 = "\\f x y -> f (g x) y"
          e9 = "1:2:3:5:[] ++ [1,2,3]"
          e10 = "foldl (\\a b -> a + b) 0 [1,2,3,4]"
          e11 = "(\\f x -> f x)"
          e12 = "foldr1 (*) []"
          e13 = "reverse [1,2,3]"
          e14 = "const 0 . f x"
          e15 = "let f x y = r x y where r h j = a++[] in f"
          e16 = "a++[]"
          
r = do
  g <- parseDeclGraph "src/Tests/Data/Simplification.hs"
  let ds = _topDecls g
  mapM_ (putStrLn . prettyDecl . last . simplifyEasy _mapDecl) ds

  
simplifyEasy :: Eq a => (AstMap -> a -> a) -> a -> [a]
simplifyEasy mapX = iterateWhileDiff (mapX easySimpls) .
                   mapX astRemoveParens .
                   mapX astRemoveAppQOp
  

easySimpls :: AstMap
easySimpls = combineMap easySimplsList

easySimplsList :: [AstMap]
easySimplsList =
            [ simplNeutral,
              simplConcatLists,
              simplComplement,
              simplBinaryIdempotent,
              simplUnaryIdempotent,
              simplMap,
              simplId,
              simplConcatMap,
              simplLambdas,
              simplFold,
              simplReverseLit,
              simplEmptyLet,
              simplAbsNull,
              simplConst
            ]

simplifyAll :: Eq a => DeclGraph -> (AstMap -> a -> a) -> a -> [a]
simplifyAll g mapx = iterateWhileDiff (mapx $ allSimpls g) .
                     mapx astRemoveParens .
                     mapx astRemoveAppQOp

allSimpls :: DeclGraph -> AstMap
allSimpls = combineMap . allSimplsList

allSimplsList :: DeclGraph -> [AstMap]
allSimplsList g = easySimplsList ++
  [simplFoldHO g]

-- | Simplification using neutral element properties
simplNeutral :: AstMap
simplNeutral = ast
  where ast = mkAstMap emptyMapArgs {
          _maybeMapExp = [s]
          }
        -- n is the neutral operand of the operator `o`
        -- n `o` x -> x
        -- (n`o`) -> id
        s :: Exp -> Maybe Exp
        s i@(InfixApp a o b) = do
          neutral <- neutralExp (opName o)
          f neutral
            where f neutral
                    | na = Just b1
                    | nb = Just a1
                    | otherwise = Nothing
                    where [na,nb] = map (==neutral) [a,b]
                          [a1,b1] = map (_mapExp simplNeutral) [a,b]
        s (RightSection o a) = do
          neutral <- neutralExp (opName o)
          if neutral == a
            then Just idExp
            else Nothing
        s (LeftSection a o) = do
          neutral <- neutralExp (opName o)
          if neutral == a
            then Just idExp
            else Nothing
        s _ = Nothing

-- | Simplification using complementary properties
simplComplement :: AstMap
simplComplement = ast
  where ast = mkAstMap emptyMapArgs {
          _maybeMapExp = [s]
          }
        s :: Exp -> Maybe Exp
        -- c (c x) -> x
        s (App (Var a) (App (Var b) e))
          | a == b && isComplementary_ a = Just $ _mapExp ast e
          | otherwise = Nothing
        -- c . c -> id
        s (InfixApp (Var a) (QVarOp (UnQual (Symbol "."))) (Var b))
          | a == b && isComplementary_ a = Just idExp
          | otherwise = Nothing
        s _ = Nothing

-- |Simplification using unary idempotency properties
simplUnaryIdempotent :: AstMap
simplUnaryIdempotent = ast
  where ast = mkAstMap emptyMapArgs {
          _maybeMapExp = [s]          
          }
        s :: Exp  -> Maybe Exp
        -- u (u x) -> u x
        s (App (Var a) be@(App (Var b) e))
          | a == b && isUnaryIdempotent_ a = Just $ _mapExp ast be
          | otherwise = Nothing
        -- u . u -> u
        s (InfixApp va@(Var a) (QVarOp (UnQual (Symbol "."))) (Var b))
          | a == b && isUnaryIdempotent_ a = Just va
          | otherwise = Nothing
        s _ = Nothing

-- |Simplification using binary idempotency properties
simplBinaryIdempotent :: AstMap
simplBinaryIdempotent = mkAstMap emptyMapArgs {
  _maybeMapExp = [s]
  }
  where
    s :: Exp -> Maybe Exp
    -- f x x -> x
    s (App (App (Var f) a) b)
      | isBinaryIdempotent_ f && a == b = Just $ _mapExp simplBinaryIdempotent a
      | otherwise = Nothing
    -- x`f`x -> x
    s (InfixApp a (QVarOp f) b)
      | isBinaryIdempotent_ f && a == b = Just $ _mapExp simplBinaryIdempotent a
      | otherwise = Nothing
    s _ = Nothing

-- |Simplifications for map
simplMap :: AstMap
simplMap = mkAstMap emptyMapArgs {
  _maybeMapExp = [s]
  }
  where -- map id = id
    s :: Exp -> Maybe Exp
    s (App (Var (UnQual (Ident "map"))) (Var (UnQual (Ident "id")))) = Just idExp
    s _ = Nothing

-- |Removes id applications
simplId :: AstMap
simplId = mkAstMap emptyMapArgs {
  _maybeMapExp = [s]
  }
  where s :: Exp -> Maybe Exp
        -- id x -> x
        s (App (Var (UnQual (Ident "id"))) x) = Just x
        s _ = Nothing

-- |concatMap related simplifications
simplConcatMap :: AstMap
simplConcatMap = mkAstMap emptyMapArgs {
  _maybeMapExp = [s]
  }
  where
    -- concat map -> concatMap 
    s (App (Var (UnQual (Ident "concat"))) (Var (UnQual (Ident "map")))) = Just concatMapExp
    -- concat (map f) -> concatMap f
    s (App (Var (UnQual (Ident "concat"))) (App (Var (UnQual (Ident "map"))) fun)) =
      Just $ App concatMapExp fun
    -- concat (map f) arg -> concatMap f arg
    s (App (Var (UnQual (Ident "concat"))) (App (App (Var (UnQual (Ident "map"))) fun) arg)) =
      Just $ App (App concatMapExp fun) arg
    -- concat .  map f -> concatMap f
    s (InfixApp (Var (UnQual (Ident "concat"))) (QVarOp (UnQual (Symbol ".")))
       (App (Var (UnQual (Ident "map"))) f)) = Just $ App concatMapExp f
    s (App (Var (UnQual (Ident "concatMap"))) (Var (UnQual (Ident "id")))) = Just idExp
    s _ = Nothing


-- |Explicit folds to implicit folding functions
-- i.e. foldl (+) 0 -> sum
simplFold :: AstMap
simplFold = mkAstMap emptyMapArgs {_maybeMapExp = [s]}
  where
    -- fold op base (for ops with neutral element)
    s (App (App e@(Var (UnQual fold)) (Var op)) base) = do
      guard $ isFold0 fold
      fun <- foldingFunction op
      -- neutral <- neutralExp op
      -- guard $ base == neutral -------------- here
      --Just $ Var fun
      return $ InfixApp (section base qop) composeQOp (Var fun)
      where qop = QVarOp op
            section = case foldNameParts e of
              Just ('l',_,_) -> LeftSection
              Just ('r',_,_) -> flip RightSection
    --fold1 op (for ops without neutral element)
    s (App (Var (UnQual fold)) (Var op)) = do
      guard $ isFold1 fold
      fun <- foldingFunction op
      guard $ isNothing $ neutralExp op
      Just $ Var fun
    s _ = Nothing
    folds1 = Set.fromList ["foldl1", "foldr1", "foldl1'"]
    folds0 = Set.fromList ["foldl", "foldr", "foldl'"]
    isFold0 = (`Set.member`folds0) . nameStr
    isFold1 = (`Set.member`folds1) . nameStr

simplAbsNull :: AstMap
simplAbsNull = mkAstMap emptyMapArgs {_maybeMapExp = [s]}
  where
    -- s (App (App fold (Var qname@(UnQual _))) e) = do
    --   foldNameParts fold
    --   absnull <- absoluteNull qname
    --   if e == absnull
    --     then return e
    --     else Nothing
    s (RightSection (QVarOp qname@(UnQual _)) e) = do
      if e `isAbsoluteNull` qname then Just $ App constVar e
        else Nothing
    s (LeftSection e (QVarOp qname@(UnQual _))) = do
      if e `isAbsoluteNull` qname then Just $ App constVar e
        else Nothing 
    s _  = Nothing

simplConst :: AstMap
simplConst = mkAstMap emptyMapArgs {_maybeMapExp = [s]}
  where
    s (App (App (Var (UnQual (Ident "const"))) f) _) = Just f
    s (InfixApp (App (Var (UnQual (Ident "const"))) x) (QVarOp (UnQual (Symbol "."))) _) = Just (App constVar x)
    s _ = Nothing

simplLambdas :: AstMap
simplLambdas = mkAstMap emptyMapArgs {_maybeMapExp = [s]}
  where
    -- (\a b -> a + b) -> (+)
    -- (\a b -> b ++ a) -> flip (++)
    s l@(Lambda _ [PVar a, PVar b] (InfixApp (Var x) op (Var y)))
      | axby = Just opOnly
      | aybx = if commut
               then Just opOnly
               else Just $ App flipExp opOnly
      | otherwise = Just l 
      where commut = isCommutative_ (opName op)
            opOnly = Var (opName op)
            axby = qa == x && qb == y
            aybx = qa == y && qb == x
            [qa,qb] = map UnQual [a,b]
    -- (\a -> a`op`) -> (a`op`)
    s (Lambda _ [PVar a] l@(LeftSection (Var x) _))
      | qa == x = Just l
      | otherwise = Nothing
        where qa = UnQual a
    s (Lambda _ [PVar a] l@(RightSection _ (Var x) ))
      | qa == x = Just l
      | otherwise = Nothing
        where qa = UnQual a
    -- (\x -> x) -> id
    s (Lambda _ [PVar a] (Var x))
      | qa == x = Just idExp
      | otherwise = Nothing
      where qa = UnQual a
    -- (\f x -> f x) -> $
    s (Lambda _ [PVar pf, PVar px] app@(App (Var f) (Var x)))
      | qpf == f && qpx == x = Just appExp
      | otherwise = Nothing
      where [qpf, qpx] = map UnQual [pf, px]
    -- (\f x y -> f y x) -> flip f
    s (Lambda _ [PVar pf, PVar px, PVar py] (App (App ef@(Var f) (Var y)) (Var x)))
      | qf == f && qx == x && qy == y = Just $ App flipExp ef 
      | otherwise = Nothing
      where [qf, qx, qy] = map UnQual [pf, px, py]
    -- eta reduction
    -- (\f x y -> f (g x) y) -> (\f x -> f (g x))
    s (Lambda loc args (App left (Var (UnQual right)))) =
      case (reverse args) of
       (PVar lastArg:rArgs)
         | lastArg == right && (lastArg`Set.notMember`foldExp astNamesRhsRS Set.empty left) ->
           Just $ Lambda loc (reverse rArgs) (_mapExp simplLambdas left)
         | otherwise -> Nothing
    s _ = Nothing

simplConcatLists :: AstMap
simplConcatLists = mkAstMap (emptyMapArgs {_maybeMapExp = [mape]})
  where 
    mape (InfixApp a o b) = do
      guard $ isList a && o == concatQOp || o == consQOp
      case concatList (InfixApp (_mapExp simplConcatLists a) o (_mapExp simplConcatLists b)) of
       (l, Just rest) -> Just $ InfixApp (List l) concatQOp rest'
         where rest' = _mapExp simplConcatLists rest
       (l, Nothing) -> Just $ List l
    mape _ = Nothing

-- a:b -> [a] ++ b
-- is this needed? i think concatList does the job
simplConsToConcat :: AstMap
simplConsToConcat = mkAstMap (emptyMapArgs {_maybeMapExp = [mape]})
  where
    mape (InfixApp a o b)
      | o == consQOp = Just $ InfixApp (List [a]) o b   -- recursive call optional
      | otherwise = Nothing
    _mape _ = Nothing

simplEmptyLet :: AstMap
simplEmptyLet = mkAstMap (emptyMapArgs {_maybeMapExp = [s]})
  where
    s (Let (BDecls []) e) = Just e
    s _ = Nothing

simplFoldHO :: DeclGraph -> AstMap
simplFoldHO g = mkAstMap (emptyMapArgs {_maybeMapExp = [s]})
  where
    -- foldl (\ac _ -> x + ac) b --> (+b) . (*x). length
    s (App (App fo (Lambda s [x,y] (InfixApp a (QVarOp (UnQual (Symbol "+"))) b))) base) = do
      traceD "lengthHo"
      traceD ("y = " ++ show y)
      (lr, zo, _) <- foldNameParts fo
      traceD "lengthHo2"
      case (lr,zo,x,y) of
        ('l',0,PVar accName, PWildCard) -> do
          traceD "lengthHo3"
          let isAcc (Var (UnQual x)) = x == accName
              isAcc _  = False
          case partition isAcc [a,b] of
            ([_], [other]) -> do
              traceD $ "other " ++ show other
              guard (not $ nameIsReachableFromRhsX g foldExp other accName)
              let template = parseExp "(+b).(*x).length"
              Just $ _mapExp (subsNamesExps [(Ident "b", base),
                                            (Ident "x", other)]) template
            _ -> Nothing
        shit -> traceD (show shit) >> Nothing
    s _ = Nothing

debug = 0 == 1

traceD :: String -> Maybe ()
traceD = when debug . traceM  

-- |[1] ++ [2] ++ [3] ++ d -> ([1,2,3], Just d)
-- [1] ++ [2] -> ([1,2], Nothing)
concatList :: Exp -> ([Exp], Maybe Exp)
concatList i@(InfixApp (List a) o b)
  | o == concatQOp = (a ++ l', e)
  | otherwise = ([], Just i)
  where (l', e) = concatList b
concatList i@(InfixApp a (QConOp (Special Cons)) b) = (a : l', e)
  where (l', e) = concatList b
concatList (List a) = (a, Nothing)
concatList (InfixApp a o (List []))
  | o == concatQOp = ([], Nothing)
concatList e = ([], Just e)

simplReverseLit :: AstMap
simplReverseLit = mkAstMap (emptyMapArgs {_maybeMapExp = [mape]})
  where
    mape (App (Var (UnQual (Ident "reverse"))) (List l)) = Just $ List $ reverse l
    mape _ = Nothing
