module Analysis.Substitution (
  withUniqueNames,
  prop, renameVarPat, renameVarPats, renameArgument, renamePatBind,
  unLabelDecl, patMapNames, expMap, subsExps,
  subsQOps, subsNames, subsNamesExps, subsIdents, unLabelX, prettyDecl
  ) where

--Own
import           Analysis.AstTraversal
import           Analysis.Helper
import           Analysis.Operators
import           Common.Applicative
import           Common.List
import           Common.Maybe
import           Parsing.Parser

-- Exts
import           Language.Haskell.Exts.Pretty
import           Language.Haskell.Exts.Syntax

-- Utilities
import           Control.Concatenative
import           Control.Monad
import           Data.Either.Unwrap
import           Data.List
import           Data.List.Split
import           Data.Map (Map)
import qualified Data.Map as Map
import           Data.Maybe
import qualified Data.Set as Set

import           Debug.Trace

f = do
  ds <- readFile d <$>> parseDecls
  mapM_ (putStrLn . prettyPrint) ds
  putStrLn "\nwithUniqueNames:"
  let uds = withUniqueNames ds
  mapM_ (putStrLn . prettyPrint) uds
  putStr "unLabel == original? " >> print (map unLabelDecl uds == ds)
    where d = "src/Tests/Data/Constraints.hs"


type NameSet = Set.Set Name
type NameStack = [([String], NameSet)]

prettyDecl :: Decl -> String
prettyDecl = prettyPrint . unLabelDecl


pushNames :: String -> NameStack -> [Name] -> NameStack
pushNames label [] n = [([label], foldl' (flip Set.insert) Set.empty n)]
pushNames label t@((labels, _):_) n = (label:labels, foldl' (flip Set.insert) Set.empty n):t

addNames :: NameStack -> [Name] -> NameStack
addNames ((labels, set):ts) n = (labels, foldl' (flip Set.insert) set n):ts

pushLabel :: String -> NameStack -> NameStack
pushLabel label st = pushNames label st []

withEmptySet :: NameStack -> NameStack
withEmptySet [] = []
withEmptySet ((labels,_):_) = [(labels,Set.empty)]

-- |Lookups a name in the stack and returns it with the corresponding label
lookupName :: NameStack -> Name -> Name
lookupName s a = case filter (Set.member a . snd) s of
                  [] -> a
                  ((labels,_):_) -> putLabel labels a


putLabel :: [String] -> Name -> Name
putLabel [] name = name
putLabel labels (Ident n) = Ident $ intercalate labelSep (filter (not. null) $ reverse labels) ++ lastsep ++ n
  where lastsep = labelSep

labelSep :: String
labelSep = "~"

labelPat :: NameStack -> Pat -> Pat
labelPat st p = foldl' (\pat name -> snd $ renameVarPat name (lookupName st name) pat) p (patNames p)

simpleMark :: Pat -> String
simpleMark p = let (Ident mark) = head $ patNames p in "$" ++ mark

fullMark :: Pat -> String
fullMark p = let idents = map (\(Ident x)->x) $ patNames p
             --in '$' : intercalate "$" idents (i think that the first '$' is not necessary)
             in intercalate "$" idents


emptyNameStack :: NameStack
emptyNameStack = [([], Set.empty)]


unLabelStr :: String -> String
unLabelStr = last . splitOn labelSep

unLabel :: Name -> Name
unLabel (Ident i) = Ident $ unLabelStr i
unLabel (Symbol s) = Symbol $ unLabelStr s

-- |Returns an equivalent declaration block but every pair of 'Name's will be equal iff they are binded to the same expression
withUniqueNames :: [Decl] -> [Decl]
withUniqueNames = wun emptyNameStack
  where

    letMark = "letIn"
    lambdaMark i = "lam" ++ show i
    whereMark = ""
    altMark = "alt"
    argsMark = "@"
    cardinal = "!"
    caseMark = "case"

    wun :: NameStack -> [Decl] -> [Decl]
    wun st = ublock True st . ublock False st

    ublock :: Bool -> NameStack -> [Decl] -> [Decl]
    ublock left st ds = map (ud left st') ds
      where st' = ublockheads st ds

    ub :: Bool -> NameStack -> Binds -> Binds
    ub left st (BDecls ds) = BDecls $ ublock left st ds

    ubheads :: String -> NameStack -> Binds -> NameStack
    ubheads mark st (BDecls ds) = st''
      where
        st' = pushLabel mark st
        st'' = ublockheads st' ds

    ublockheads :: NameStack -> [Decl] -> NameStack
    ublockheads st = addNames st . concatMap bindedNames


    ud :: Bool -- ^True: change header (name + args), False: change rhs and binds
          -> NameStack -- ^name stack
          -> Decl -- ^a declaration
          -> Decl -- ^result
    ud True st (TypeSig l ns t) = TypeSig l ns' t
      where ns' = map (lookupName st) ns
    ud False st (PatBind l p r maybeB) = PatBind l p rparents (Just bparents)
      where
        b :: Binds
        b = fromMaybe (BDecls []) maybeB

        --Rhs and binds: two sweeps
        stlabel = pushLabel (fullMark p) st
        stempty = withEmptySet stlabel
        --two sweeps
        --1 with where (labels names binded by the where block)
        stwhere = ubheads whereMark stempty b
        rwhere = ur False stwhere r
        bwhere = ub False stwhere b
        --2 with parents (labels names binded by its block and its parents)
        rparents = ur False stlabel rwhere --rargs
        bparents = ub False stlabel bwhere --bargs

    ud True st (PatBind l p r mbb) = PatBind l p' r' (Just b') -- > change headers
      where
        b = fromMaybe (BDecls []) mbb
        stlabel = pushLabel (fullMark p) st
        r' = ur True stlabel r
        b' = ub True stlabel b
        p' = labelPat st p

    ud left st (FunBind matches) = FunBind (map (sm left) $ enumerateStr1 matches)
      where
        sm :: Bool -> (String, Match) -> Match
        sm False (label, Match l n@(Ident i) ps t r mbb) = Match l n ps t rparents (Just bparents)
           where
             b = fromMaybe (BDecls []) mbb

             stlabel = pushLabel mark st
             stempty = withEmptySet stlabel
             --three sweeps
             --1 with where (labels names binded by the where block)
             stwhere = ubheads whereMark stempty b
             rwhere = ur False stwhere r
             bwhere = ub False stwhere b

             --2 with args (labels names binded by the args)
             stargs = pushNames argsMark stempty (patsNames ps)
             rargs = ur False stargs rwhere
             bargs = ub False stargs bwhere

             --3 with parents (labels names binded by its block and its parents)
             rparents = ur False stlabel rargs
             bparents = ub False stlabel bargs

             mark = i ++ cardinal ++ label

        sm True (label, Match l n@(Ident i) ps t r mbb) = Match l n' ps' t r' (Just b')
          where
            b = fromMaybe (BDecls []) mbb
            stempty = withEmptySet stlabel
            stlabel = pushLabel mark st
            mark = i ++ cardinal ++ label
            ps' = map (labelPat stargs) ps
            stargs = pushNames argsMark stempty (patsNames ps)
            n' = lookupName stlabel n
            r' = ur True stlabel r
            b' = ub True stlabel b
    ud _ _ d = d

    ur :: Bool -> NameStack -> Rhs -> Rhs
    ur left st (UnGuardedRhs e) = UnGuardedRhs (ue left st e)
    ur left st (GuardedRhss rs) = GuardedRhss $ map urs (enumerateStr1 rs)
      where
        urs :: (String, GuardedRhs) -> GuardedRhs
        urs (label, GuardedRhs l ss e) = GuardedRhs l (uss left st' ss) (ue left st' e)
          where st' = pushLabel mark st
                mark = altMark ++ label


    us :: Bool -> NameStack -> Stmt -> Stmt
    us left st (Qualifier e) = Qualifier $ ue left st e
    us _ _ s = s

    uss :: Bool -> NameStack -> [Stmt] -> [Stmt]
    uss left st = map (us left st)

    --ussheads :: NameStack -> [Stmt] -> NameStack
    --ussheads = undefined

    ue :: Bool -> NameStack -> Exp -> Exp
    ue left st = snd . s 0
      where
        s :: Int -> Exp -> (Int, Exp)
        s l a@(Var (UnQual name))
          | left = (l, a)
          | otherwise = (l, Var . UnQual $ lookupName st name)
        s l (InfixApp a o b) = saux l [a,b] (\ [a,b] -> InfixApp a o b)
        s l (App a b) = saux l [a,b] (\ [a,b] -> App a b)
        s l (NegApp a) = saux l [a] (NegApp . head)
        s l (If a b c) = saux l [a,b,c] (\ [a,b,c] -> If a b c)
        s l (Paren a) = saux l [a] (Paren . head)
        s l (List as) = saux l as List
        s l (Tuple Boxed as) = saux l as (Tuple Boxed)
        s l (EnumFrom a) = saux l [a] (EnumFrom . head)
        s l (EnumFromTo a b) = saux l [a,b] (\ [a, b] -> EnumFromTo a b)
        s l (EnumFromThen a b) = saux l [a,b] (\ [a,b] -> EnumFromThen a b)
        s l (EnumFromThenTo a b c) = saux l [a,b,c] (\ [a,b,c] -> EnumFromThenTo a b c)
        s l (LeftSection a o) = saux l [a] (\ [a] -> LeftSection a o)
        s l (RightSection o a) = saux l [a] (\ [a] -> RightSection o a)
        s l (Case e alts) = (numlamb, Case e' alts')
          where
            (numlamb, e') = s l e
            alts' = map (ua left st) (enumerateStr alts)
        s l a@(Lambda loc ps e) = (l + 1, Lambda loc ps' (ue left st' e))
          where st' = pushNames (lambdaMark l) st (patsNames ps)
                ps' = (left ?$ map (labelPat st')) ps
        s l (Let b e) = (l, Let b' e')
          where
            st' = ubheads letMark st b
            b' = ub left st' b
            e' = ue left st' e
        s l e = (l, e)
        saux :: Int -> [Exp] -> ([Exp] -> Exp) -> (Int, Exp)
        saux l es con = let (l', es') = mapAccumL s l es in (l', con es')

    ua :: Bool -> NameStack -> (String, Alt) -> Alt
    ua left st (label, Alt s p r mbb) = Alt s p' r' (Just b')
      where
        b = fromMaybe (BDecls []) mbb
        st' = pushNames (altMark ++ label) st (patNames p)
        st'' = ubheads whereMark st' b
        r' = ur left st'' r
        b' = ub left st' b
        p' = (left ?$ labelPat st') p




-- |Renames a variable in a pattern. Assumes it appears at most once
renameVarPat :: Name -> Name -> Pat -> (Bool, Pat)
renameVarPat x y = s
   where
     s a@(PVar n)
        | n == x = (True, PVar y)
        | otherwise = (False, a)
     s (PInfixApp a o b)
       | ta = (True, PInfixApp sa o b)
       | tb = (True, PInfixApp a o sb)
       | otherwise = (False, PInfixApp a o b)
       where (ta, sa) = s a
             (tb, sb) = s b
     s (PApp q ps) = let (tm, sps) = rvps ps in (tm, PApp q sps)
     s (PList ps) = let (tm, sps) = rvps ps in (tm, PList sps)
     s (PTuple Boxed ps) = let (tm, sps) = mapWhile1 s ps in (tm, PTuple Boxed sps)
     s (PParen p) = let (tm, sp) = s p in (tm, PParen sp)
     s (PAsPat n b)
       | n == x = (True, PAsPat y b)
       | otherwise = let (t, sb) = s b in (t, PAsPat n sb)
     s p = (False, p)
     rvps = renameVarPats x y

patMapNames :: (Name -> Name) -> Pat -> Pat
patMapNames f = s
   where
     s (PVar n) = PVar $ f n
     s (PInfixApp a o b) = PInfixApp (s a) o (s b)
     s (PApp q ps) = PApp q (map s ps)
     s (PList ps) = PList (map s ps)
     s (PTuple Boxed ps) = PTuple Boxed (map s ps)
     s (PParen p) = PParen (s p)
     s (PAsPat n b) = PAsPat (f n) (s b)
     s p = p


-- |Renames a variable in a list of patterns. Assumes it appears at most once throughout the list
renameVarPats :: Name -> Name -> [Pat] -> (Bool, [Pat])
renameVarPats x y = mapWhile1 (renameVarPat x y)

-- |Renames a variable in a pattern binding and propagates the change
renamePatBind :: Name -> Name -> Decl -> Decl
renamePatBind x y a@(PatBind l p r mbb)
  | appears = PatBind l p' (propRhs qx qy r) (Just $ propBinds qx qy b)
  | otherwise = a
  where (appears, p') = renameVarPat x y p
        qx = UnQual x
        qy = Var $ UnQual y
        b = fromMaybe (BDecls []) mbb


-- |Renames an argument. The change is not propagated
renameArgument :: Name -> Name -> Match -> Match
renameArgument x y (Match l n ps t r b) = Match l n ps' t r b
          where (_, ps') = renameVarPats x y ps


-- |Propagates a variable substitution from an upper scope
propBinds :: QName -> Exp -> Binds -> Binds
propBinds x y (BDecls ds) = BDecls $ propBDecl x y ds

-- |Propagates a variable substitution from an upper scope
propRhs :: QName -> Exp -> Rhs -> Rhs
propRhs x y (UnGuardedRhs a) = UnGuardedRhs (prop x y a)

-- |Propagates a variable substitution from an upper scope
propBDecl :: QName -> Exp -> [Decl] -> [Decl]
propBDecl x@(UnQual uqx) y ds = map sd' ds
      where
        sd' a@(PatBind l p r mbb)
          | uqx`elem`concatMap bindedNames ds = a
          | otherwise = PatBind l p (propRhs x y r) (Just $ propBinds x y b)
            where
              b = fromMaybe (BDecls []) mbb
        sd' (FunBind ms) = FunBind (map sm' ms)
          where sm' a@(Match l n args t r mbb)
                  | uqx`elem`concatMap bindedNames ds ++ patsNames args = a
                  | otherwise = Match l n args t (propRhs x y r) (Just $ propBinds x y b)
                  where b = fromMaybe (BDecls []) mbb


-- |Propagates a variable substitution from an upper scope
propAlt :: QName -> Exp -> Alt -> Alt
propAlt x y (Alt l p r mbb) = let (r', b') = propRhsBinds x y [p] r b in Alt l p r' (Just b')
  where
    b = fromMaybe (BDecls []) mbb

-- |Propagates a variable substitution from an upper scope
propRhsBinds :: QName -> Exp -> [Pat] -> Rhs -> Binds -> (Rhs, Binds)
propRhsBinds qn y _ r b = (propRhs qn y r, propBinds qn y b)


-- |Maps a function over an expression tree. Recursion is only done automatically when
-- 'Nothing' is returned
expMap :: (Exp -> Maybe Exp) -> Exp -> Exp
expMap f = s
  where
    s :: Exp -> Exp
    s x@(Var _) = g x x
    s x@(InfixApp a o b) = g (InfixApp (s a) o (s b)) x
    s x@(App a b) = g (biAp s App a b) x
    s x@(NegApp a) = g (NegApp $ s a) x
    s x@(If a b c) = g (triAp s If a b c) x
    s x@(Paren a) = g (Paren $ s a) x
    s x@(List as) = g (List $ map s as) x
    s x@(Tuple Boxed as) = g (Tuple Boxed $ map s as) x
    s x@(EnumFrom a) = g (EnumFrom $ s a) x
    s x@(EnumFromTo a b) = g (biAp s EnumFromTo a b) x
    s x@(EnumFromThen a b) = g (biAp s EnumFromThen a b) x
    s x@(EnumFromThenTo a b c) = g (triAp s EnumFromThenTo a b c) x
    s x@(LeftSection a o) = g (LeftSection (s a) o) x
    s x@(RightSection o a) = g (RightSection o (s a)) x
    s x@(Lambda l ps e) = g (Lambda l ps (s e)) x
    s x@(Case a alts) = g (Case (s a) (map sa alts)) x
    s x@(Let b e) = g (Let (sb b) (s e)) x
    s x@(Lit _) = g x x
    s x@(Do sts) = g (Do $ map ss sts) x
    s _ = error "not implemented"

    g = maybeApp f

    sr :: Rhs -> Rhs
    sr (UnGuardedRhs e) = UnGuardedRhs (s e)
    sr (GuardedRhss gs) = GuardedRhss (map gr gs)
     where
      gr :: GuardedRhs -> GuardedRhs
      gr (GuardedRhs l sts e) = GuardedRhs l (map ss sts) (s e)
    sb :: Binds -> Binds
    sb (BDecls ds) = BDecls (map sd ds)
    sd :: Decl -> Decl
    sd (PatBind l p r b) = PatBind l p (sr r) (fmap sb b)
    sd (FunBind ms) = FunBind (map sm ms)
     where
      sm (Match l n p t r b) = Match l n p t (sr r) (fmap sb b)
    sd d = d
    sa :: Alt -> Alt
    sa (Alt l p r b) = Alt l p (sr r) (fmap sb b)
    ss :: Stmt -> Stmt
    ss (Generator l p e) = Generator l p (s e)
    ss (Qualifier e) = Qualifier (s e)
    ss (LetStmt b) = LetStmt (sb b)


-- |Replaces every instance of a variable in its scope with an expression
prop :: QName -- ^Variable to replace
     -> Exp -- ^Expression to insert
     -> Exp -- ^Expression where to apply the substitution
     -> Exp -- ^Resulting expression
prop x@(UnQual uqx) y = s
  where
    s :: Exp -> Exp
    s a@(Var name) = if x == name then y else a
    s (InfixApp a o b) = InfixApp (s a) o (s b)
    s (App a b) = biAp s App a b
    s (NegApp a) = NegApp $ s a
    s (If a b c) = triAp s If a b c
    s (Paren a) = Paren $ s a
    s (List as) = List $ map s as
    s (Tuple Boxed as) = Tuple Boxed $ map s as
    s (EnumFrom a) = EnumFrom $ s a
    s (EnumFromTo a b) = biAp s EnumFromTo a b
    s (EnumFromThen a b) = biAp s EnumFromThen a b
    s (EnumFromThenTo a b c) = triAp s EnumFromThenTo a b c
    s (LeftSection a o) = LeftSection (s a) o
    s (RightSection o a) = RightSection o (s a)
    s (Lambda l ps e) = Lambda l ps $ (uqx`notElem`patsNames ps ?$ s) e
    s (Case a alts) = Case (s a) (map sa alts)
    s (Let b@(BDecls ds) e) = Let (sb b) $ (uqx`notElem`concatMap bindedNames ds ?$ s) e
    s e = e

    sr :: Rhs -> Rhs
    sr = propRhs x y
    sb :: Binds -> Binds
    sb = propBinds x y
    sds :: [Decl] -> [Decl]
    sds = propBDecl x y
    sa :: Alt -> Alt
    sa = propAlt x y
    srb :: [Pat] -> Rhs -> Binds -> (Rhs, Binds)
    srb = propRhsBinds x y

unLabelDecl :: Decl -> Decl
unLabelDecl = unLabelX _mapDecl

--unLabelX :: Decl -> Decl
unLabelX :: (AstMap -> t) -> t
unLabelX mapX = mapX astmap
  where args = emptyMapArgs {_maybeMapExp = [me], _maybeMapLName = [mn],
                             _maybeMapPat = [mp]}
        astmap = mkAstMap args

me (Var (UnQual name)) = Just (Var $ UnQual $ unLabel name)
me _ = Nothing
mn = Just . unLabel
mp (PVar name) = Just . PVar $ unLabel name
mp _ = Nothing
mqo (QVarOp (UnQual name)) = Just $ QVarOp $ UnQual $ unLabel name
mqo _ = Nothing


subsExps :: [(Exp, Exp)] -> AstMap
subsExps l = mkAstMap (emptyMapArgs {_maybeMapExp = [mape]})
  where mape = (`Map.lookup`m)
        m = Map.fromList l

subsNamesExps :: [(Name, Exp)] -> AstMap
subsNamesExps = subsExps . map (first $ Var . UnQual)

subsLNames :: [(Name, Name)] -> AstMap
subsLNames l =
  mkAstMap emptyMapArgs {_maybeMapLName = [mapn]}
  where
    mn = Map.fromList l
    mapn = (`Map.lookup`mn)


-- |(oldName, newName), substitution is applied both sides of a declaration
subsNames :: [(Name, Name)] -> AstMap
subsNames l = combineMap [aste, astn]
  where aste = mkAstMap (emptyMapArgs {_maybeMapExp = [mape]})
        mape = (`Map.lookup`Map.fromList le)
        le = map (both nameToVar) l
        astn = subsLNames l

subsIdents :: [(String, Exp)] -> AstMap
subsIdents = subsExps . map (first strToVar)


subsQOps :: [(QOp, QOp)] -> AstMap
subsQOps l = mkAstMap (emptyMapArgs {_maybeMapQOp = [mapq]})
  where mapq = (`Map.lookup`m)
        m = Map.fromList l
