module Analysis.UnfoldFold (
  transform1, reverseTransf, transformConcat,
  foldTransform
  ) where

--Own
import           Analysis.AstTraversal
import           Analysis.AstTraversalCommon
import           Analysis.Helper
import           Analysis.Operators
import           Analysis.Queries
import           Analysis.Recursion
import           Analysis.Simplification
import           Analysis.Substitution
import           Analysis.UnfoldFold2
import           Common.Applicative
import           Common.List
import           Common.String
import           Parsing.Parser

-- Exts
import           Language.Haskell.Exts.Pretty
import           Language.Haskell.Exts.Syntax

-- Utilities
import           Control.Arrow
import           Control.Concatenative
import           Control.Monad
import           Data.Bits
import           Data.Either.Unwrap
import           Data.IntMap.Strict                (IntMap,(!))
import qualified Data.IntMap.Strict                as IntMap
import           Data.IntSet                       (IntSet)
import qualified Data.IntSet                       as IntSet
import           Data.List                         hiding ((\\))
import           Data.Map.Strict                   (Map)
import qualified Data.Map.Strict                   as Map
import           Data.Maybe
import           Data.Set                          ((\\), Set)
import qualified Data.Set                          as Set

--import           Data.Tuple

-- Graph
import qualified Data.Graph.Inductive.Graph        as Graph
import           Data.Graph.Inductive.NodeMap
import qualified Data.Graph.Inductive.NodeMap      as NodeMap
import           Data.Graph.Inductive.PatriciaTree

-- Debug
import           Debug.Trace


debug = 1 == 0

preprocess :: [Decl] -> [Decl]
preprocess = map (_mapDecl $ combineMap [astRemoveParens, astRemoveAppQOp]) . withUniqueNames

f = do
  g <- parseDeclGraph d --readFile d <$>> parseModule d -- <$>> fromRight <$>> preprocess
  --printPretties ds
  let names = map Ident ["listZero"]
  --print (getAllNames g)
  --print (toNames g $ getKnownNames g)
  --mapM_ (testToFold g) names
  --mapM_ (testSing (transform1R (++"'")) g) names
  putStrLn "tail:" >> mapM_ (testSing (transform1 (++"'")) g) names
 -- putStrLn "\ntail with reverse:" >> mapM_ (testSing (transformConcat (++"'")) g) names
  putStrLn "\nfoldtrans:" >> mapM_ (testSing foldTransform g) names
  where d = "src/Tests/Data/Examples.hs"

--printPretties = mapM (putStrLn . prettyPrint)

tf = testsToFold
testsToFold = do
  g <- parseDeclGraph d
  mapM_ (testToFold g) names
  where
    d =  "src/Tests/Data/ToFold.hs"
    names = [Ident $ "f" ++ show n | n <- [1..2]]

data TailParts = TailParts {
  baseExp :: Exp,
  recOperand :: Exp, -- of the form "[let in]* fun exp"
  nonRecOperand :: Exp, -- any form
  operator :: QOp,
  isLeftRec :: Bool,
  recBinds :: [Binds], -- top let bindings in the recursive match expression (don't confuse it with recursive operand bindings!)
  baseMatch :: Match,
  recMatch :: Match
  }

tailPartsRecMatchBinds :: TailParts -> Binds
tailPartsRecMatchBinds = matchBinds . recMatch


data FoldParts = FoldParts {
  headName :: Maybe Name, -- head's name in the recursive pattern,  (h : t) => Just h, (_:t) -> Nothing
  basePat :: Pat,
  tailParts :: TailParts
  }



instance Show TailParts where
  show TailParts {
  baseExp = baseExp, recOperand = recOperand,
  nonRecOperand = nonRecOperand, operator = operator,
  isLeftRec = isLeftRec, recBinds = recBinds
  } = let line = (++ "\n")
          pline :: Pretty p => p -> String
          pline = line . prettyPrint
          s a b = line a ++ pline b
      in s "Base Expression: " baseExp ++
         s "Recursive Operand:" recOperand ++
         s "Non Recursive Operand:" nonRecOperand ++
         s "Operator:" operator ++
         line "Is Left Recursive:" ++ show isLeftRec ++
         line "Recursive Operand Binds" ++
            (unlines . map (\(BDecls ds) -> concatMap prettyPrint ds)) recBinds



testSing transform r name@(Ident sname) = do
  case transform r (getName2node r name) of
   Just d -> --putStrLn "\nTransform:" >>
     mapM_ (putStrLn . prettyPrint) [unLabelDecl d]
   Nothing -> putStr $ "\n" ++ sname ++ ": Cannot apply transformation\n"

testToFold g name@(Ident sname) = do
  putStrLn $ prettyPrint $ unLabelDecl (getDecl g node)
  sep
  case transform1 (++"T") g node of
    Just x -> (putStrLn . prettyPrint) (unLabelX _mapDecl x) >> sep
    _ -> return ()
  case findFoldParts g node of
   Just fp -> do
     putStrLn $ sname ++ ":"
     let foldexp = toFold fp
     (putStrLn . prettyPrint) (unLabelX _mapExp foldexp)
     let simpl = simplifyAll g _mapExp foldexp
     sep >> putStrLn "simplified:"
     let steps = map (unLabelX _mapExp) simpl
     mapM_ (putStrLn . prettyPrint) steps
     sep >> sep
   _ -> putStr $ "\n" ++ sname ++ ": Cannot transform to fold\n"
  where sep = putStrLn "---------------"
        node = getName2node g name

transformConcat :: (String -> String) -> DeclGraph -> Node -> Maybe Decl
transformConcat tag g key = do
  let mrevf = reverseTransf (Just True) False g key
  if isNothing mrevf
    then Nothing -- transform1 tag g key
    else do
    revf <- mrevf
    --traceM $ (prettyPrint . unLabelDecl) $ revf
    let
      revName = funBindName revf
      newName = Ident $  nameStr revName ++ "'"--Ident "temporalllll___"--revName
      revf2 = _mapDecl (subsNames [(revName, newName)]) revf
      g' = addDecls [revf2] g
      key' = getName2node g' newName
    -- traceM (show key')
   -- traceM  $ "AAAA" ++ ((prettyPrint . unLabelDecl) $ (getDecl g' key'))
    transR <- tailTransform g' key' -- transform1 (++"'") g' key'
    return $ declAddBinds [transR] $ revCallDeclArgs (nameStr revName) transR ["a"] ["[]","a"]


findTailParts :: DeclGraph -> Node -> Maybe TailParts
findTailParts g key = do
  let debug = 1 == 0
  when debug $ traceM "findTailParts ini"
  (b, r,_) <- splitBaseRec1 g key
  when debug $ traceM "post SplitBaseRec"
  [be, re] <- mapM rhsUnGuardedExp [b, r] -- TODO: check <$>> map (_mapExp concatLists)
  let (bindsRe, unWre) = unwrap re
  (nonRecurOperand, o, recurOperand, isLeftRecur) <- splitRecExp unWre
  guard $ isAssociative_ (opName o)
  return TailParts {
    operator = o,
    baseExp = be,
    recOperand = recurOperand,
    nonRecOperand = nonRecurOperand,
    isLeftRec = isLeftRecur,
    recBinds = bindsRe,
    baseMatch = b,
    recMatch = r
   }
  where
    funName = nameStr $ funBindName (getDecl g key)
    splitRecExp :: Exp -> Maybe (Exp, QOp, Exp, Bool)
    splitRecExp (InfixApp l op r) -- (nonrecur, operator, recur, isleftrecur)
      | nonRecCond l && recCond r = Just (l, op, r, False)
      | nonRecCond r && recCond l = Just (r, op, l, True)
      | otherwise = Nothing
    splitRecExp (App(App (Var qfun) l) r)
      | nonRecCond l && recCond r = Just (l, QVarOp qfun, r,False)
      | nonRecCond r && recCond l = Just (r, QVarOp qfun, l, True)
    splitRecExp _ = Nothing
    nonRecCond exp = not $ nodeIsReachableFromRhsX g foldExp exp key
    recCond e = case unwrap_ e of
      (App (Var (UnQual fun)) exp) -> nameStr fun == funName &&
                                      not (nodeIsReachableFromRhsX g foldExp exp key)
      _ -> False

foldTransform :: DeclGraph -> Node -> Maybe Decl
foldTransform g key = do
  e <- foldTransformExp g key
  let (FunBind (Match s n _ t _ b:_)) = getDecl g key
  return $ FunBind [Match s n [] t (UnGuardedRhs e) b]

foldTransformExp :: DeclGraph -> Node -> Maybe Exp
foldTransformExp g key = findFoldParts g key <$>> toFold


findFoldParts :: DeclGraph -> Node -> Maybe FoldParts
findFoldParts g key = do
  when debug $ traceM "findFoldParts init"
  tp <- findTailParts g key
  when debug $ traceM "TailParts found"
  let bMatch = baseMatch tp
      rMatch = recMatch tp
  basePat <- getBasePat bMatch
  (headName, tailName) <- getHeadTail rMatch
  -- TODO: guard that recursive call is applied to tail
  guard $ headCond (nonRecOperand tp) tailName
  guard $ tailCond tp tailName
  return FoldParts {tailParts = tp, basePat = basePat,
                    headName = headName}
    where
      -- only accepts patterns like
      -- base [] = anyExp
      -- base [x] = x
      getBasePat m = do
        let rhs = matchRhs m
            exp = unGuardedRhsExp rhs
        guard $ not $ rhsIsGuarded rhs
        case matchExplArgs m of
         [p@(PList [])] -> Just p
         [p@(PList [PVar x])] ->
           case exp of
                Var (UnQual y) -> guard (x == y) >> return p
                _ -> Nothing
         _ -> Nothing
      getHeadTail m =
        case unParenP $ head $ matchExplArgs m of
         PInfixApp (PVar headName) (Special Cons) (PVar tailName) -> Just (Just headName, tailName)
         PInfixApp PWildCard (Special Cons) (PVar tailName) -> Just (Nothing, tailName)
         _ -> Nothing
      headCond nonRecOperand tailName =
        not $ nameIsReachableFromRhsX g foldExp nonRecOperand tailName
      tailCond tp tailname = case recOperand tp of
        (App fun (Var (UnQual t))) -> t == tailname
        _ ->False


toFold :: FoldParts -> Exp
toFold FoldParts {
  tailParts = tailparts@TailParts {
     baseExp = baseExp, recOperand = recOperand,
     nonRecOperand = nonRecOperand, operator = operator,
     isLeftRec = isLeftRec, recBinds = recBinds,
     recMatch = recMatch, baseMatch = baseMatch
     },
  basePat = basePat, headName = headName
  } = Let (tailPartsRecMatchBinds tailparts) $
      subs template
  where template = parseExp $ fold ++ "(\\" ++ lamArgs ++
                   " -> " ++ lamExp ++ ") " ++ base
        subsN = case headName of
          Just x -> [subsNames [(Ident "_head", x)]]
          _ -> []
        subsE = subsIdents [("_nonRec", nonRecOperand),
                                     ("_base", baseExp')]
          where baseExp' = whereToLet baseMatch baseExp
        subsO = subsQOps [(parseQOp "`_op`", operator)]
        subs = _mapExp $ combineMap $ subsN ++ [subsE, subsO]
        assoc = associativity_ $ opName operator
        rl = if assoc == AssocRight then "r" else "l"
        oz = if basePat == (PList []) then "" else "1"
        lamArgs = case headName of
          Nothing -> unwords $ (assoc == AssocLeft ?$ reverse) ["_", "acc"]
          Just _ -> unwords $ (assoc == AssocLeft ?$ reverse) ["_head", "acc"]
        lamExp = l ++ " `_op` " ++ r
          where [l, r] = (isLeftRec ?$ reverse) ["_nonRec", "acc"]
        fold = "fold" ++ rl ++ oz
        base = if basePat == (PList []) then "_base" else ""


whereToLet :: Match -> Exp -> Exp
whereToLet (Match _ _ _ _ _ (Just b@(BDecls (_:_)))) = Let b
whereToLet _ = id

transform1 :: (String -> String) -> DeclGraph -> Node -> Maybe Decl
transform1 tag g key = do
  when debug $ traceM "transform1 "
  (b, r,_) <- splitBaseRec1 g key -- <$>> both (matchAddExplArgs [Ident accumName])
  when debug $ traceM "guardPoint A"
  [be, re] <- mapM rhsUnGuardedExp [b, r] -- <$>> TODO: -- map (_mapExp concatLists)
  when debug $ traceM "guardPoint B"
  let (bindsRe, unWre) = unwrap re
  (leftExp, o, (rightBinds, rightExp), swapCond) <- splitRecExp unWre
  guard $ isAssociative_ (opName o)
  let templateR = "_newf (_accum `_assocOp` _leftExp) _rightExp"
      (acc_left, left_acc) = (swapCond ?$ swap) (strToVar accumName, leftExp)
      subsExpRec = subsExps [(strToVar "_newf", strToVar newName),
                                   (strToVar "_accum", acc_left),
                                   (strToVar "_leftExp", left_acc),
                                   (strToVar "_rightExp", reWrap rightBinds $ appArg rightExp)]
      subsExpBase = subsExps [(strToVar "_b", be), (strToVar "_accum", strToVar accumName)]
      so = subsQOps [(qopi "_assocOp", o)]
      mapER = _mapExp $ combineMap [subsExpRec, so]
      mapEB = _mapExp $ combineMap [subsExpBase, so]
      recExp = mapER (reWrap bindsRe $ parseExp templateR)
      templateB = "_accum `_assocOp` _b"
      baseExp = mapEB $ (swapCond ?$ flipInfixApp) $ parseExp templateB
      d = renameFunBind (Ident newName)
          $ funBindAddArgs [Ident accumName] $
          FunBind [matchSetRhsExp baseExp b, matchSetRhsExp recExp r]
  Just d
  where
    funName = nameStr $ funBindName (getDecl g key)
    newName = tag funName
    accumName = "acc"
    splitRecExp :: Exp -> Maybe (Exp, QOp, ([Binds], Exp), Bool)
    splitRecExp (InfixApp l' op r') --left -> nonrec,  right -> rec
      | leftCond l && rightCond (snd ur) = Just (l, op, ur, False)
      | leftCond r && rightCond (snd ul) = Just (r, op, ul, True)
      | otherwise = Nothing
      where [l, r] = map (_mapExp astRemoveParens) [l', r']
            [ul, ur] = map unwrap [l', r']
    splitRecExp _ = Nothing
    leftCond exp = not $ nodeIsReachableFromRhsX g foldExp exp key
    rightCond (App (Var (UnQual fun)) exp) = nameStr fun == funName
                                             && not (nodeIsReachableFromRhsX g foldExp exp key)
    rightCond _ = False


reverseTransf :: Maybe Bool -- ^Nothing: always apply, True/False: target left/right recursive
              -> Bool -- ^ True: wrap in a reverse call so it is equivalent to the original
              -- False: returns the reversed result of the original
              -> DeclGraph -> Node -> Maybe Decl
reverseTransf mleftR wrap g key = do
  (baseMatch, recMatch, flipMatch) <- splitBaseRec1 g key
  [be, re] <- mapM rhsUnGuardedExp [baseMatch, recMatch] -- TODO: <$>> map (_mapExp concatLists)
  let (recBinds, unWre) = unwrap re
  (leftExp, op, (rightBinds, rightExp), flipCond) <- splitRecExp unWre
  guard $ maybe True (xor flipCond) mleftR
  guard $ opCond op
  let tBaseMatch = matchSetRhs (UnGuardedRhs (App reverseExp be)) baseMatch
      recExp = reWrap recBinds $
               _mapExp (subsExps
                       [(strToVar "_rf", nameToVar revName),
                        (strToVar "_x'", reWrap rightBinds rightExp),
                        (strToVar "_y", leftExp)])
               $ (flipCond ?$ flipInfixApp) $ parseExp "_rf _x' ++ reverse _y"
      tRecMatch = matchSetRhs (UnGuardedRhs recExp) recMatch
      newDecl = revCallDecl_ newName (nameStr revName) (matchNumArgs recMatch)
      revDecl = renameFunBind revName $ FunBind $ (flipMatch?$reverse) [tBaseMatch, tRecMatch]
  return $ if wrap
    then let (FunBind (m:_)) = newDecl in FunBind [matchAddBinds (BDecls [revDecl]) m]
    else revDecl
  --return $ (newDecl, revDecl)
  where
    decl = getDecl g key
    funName = funBindName decl
    revName = Ident (nameStr funName) -- ++"R"
    newName = nameStr funName ++ "'"
    opCond = isConcat
    leftCond exp = not $ nodeIsReachableFromRhsX g foldExp exp key
    rightCond (App (Var (UnQual fun)) exp) = fun == funName
                                             && not (nodeIsReachableFromRhsX g foldExp exp key)
    rightCond _ = False
    splitRecExp :: Exp -> Maybe (Exp, QOp, ([Binds], Exp), Bool)
    splitRecExp recRhsExp =
      case -- _mapExp concatLists TODO: guard
           recRhsExp of
       (InfixApp l' o r')
         | leftCond l && rightCond (snd ur) -> Just (l, o, second appArg ur, False)
         | leftCond r && rightCond (snd ul) -> Just (r, o, second appArg ul, True)
         | otherwise -> Nothing
         where [l, r] = map (_mapExp astRemoveParens) [l', r']
               [ul, ur] = map unwrap [l', r']
       _ -> Nothing


-- | (Base, Recursive)
splitBaseRec :: DeclGraph -> Node -> Maybe ([Match], [Match])
splitBaseRec g key = funBindMatches (getDecl g key) <$>> partition (matchIsBase g)

splitBaseRec1 :: DeclGraph -> Node -> Maybe (Match, Match, Bool)
splitBaseRec1 g key = do
  (a, b) <- splitBaseRec g key
  guard $ lengthEQ 1 a && lengthEQ 1 b
  return (head a, head b, head b == head (fromJust $ funBindMatches (getDecl g key)))

rhsUnGuardedExp :: Match -> Maybe Exp
rhsUnGuardedExp = fmap (unGuardedRhsExp . matchRhs) . mfilter (not . rhsIsGuarded . matchRhs) . return


revCallDecl_ :: String -> String -> Int -> Decl
revCallDecl_ newName callName numArgs = _mapDecl (subsExps [(strToVar "_rf", strToVar callName)])
                              $ parseDecl $ newName ++ " " ++ argsStr
                              ++ " = reverse (_rf " ++ argsStr ++ ")"
  where argsStr = unwords (take numArgs allStrings)



revCallDeclArgs :: String -> Decl -> [String] -> [String] -> Decl
revCallDeclArgs newName call argsStr callArgs =
  _mapDecl (subsExps [(strToVar "_rf", strToVar callName)])
  $ parseDecl $ newName ++ " " ++ unwords argsStr
  ++ " = reverse (_rf " ++ callargs ++ ")"
  where callName = nameStr $ funBindName call
        callargs = unwords callArgs

revCallDecl :: String -> Decl -> Decl
revCallDecl resFunName d = revCallDecl_ resFunName (nameStr (funBindName d)) (fromJust $ funBindNumArgs d)
