module Analysis.UnfoldFold2 (
  tailTransform, tailTransformType,tailTransformWrapped
  ) where

--Own
import           Analysis.AstTraversal
import           Analysis.AstTraversalCommon
import           Analysis.Helper
import           Analysis.Operators                hiding (operatorName)
import           Analysis.Queries
import           Analysis.Recursion
import           Analysis.Simplification
import           Analysis.Substitution
import           Common.Applicative
import           Common.List
import           Common.Map
import           Common.Maybe
import           Common.String
import           Parsing.Parser

-- Exts
import           Language.Haskell.Exts.Pretty
import           Language.Haskell.Exts.Syntax

-- Utilities
import           Control.Arrow
import           Control.Concatenative
import           Control.Monad
import           Data.Either
import           Data.Either.Unwrap
import           Data.IntMap.Strict                (IntMap,(!))
import qualified Data.IntMap.Strict                as IntMap
import           Data.IntSet                       (IntSet)
import qualified Data.IntSet                       as IntSet
import           Data.List                         hiding ((\\))
import           Data.Map.Strict                   (Map)
import qualified Data.Map.Strict                   as Map
import           Data.Maybe
import           Data.Set                          ((\\), Set)
import qualified Data.Set                          as Set

--import           Data.Tuple

-- Graph
import qualified Data.Graph.Inductive.Graph        as Graph
import           Data.Graph.Inductive.NodeMap
import qualified Data.Graph.Inductive.NodeMap      as NodeMap
import           Data.Graph.Inductive.PatriciaTree

-- Debug
import           Debug.Trace


f = do
  g <- parseDeclGraph d
  let ds = _topDecls g
      names = map Ident ["listZero"]
      nodes = map (getName2node g) names
      d = getDecl g (head nodes)
      parts = findFunParts g (head nodes)
  -- mapM_ (putStrLn . prettyPrint) ds
  case parts of
   Just p -> case recomposeFunParts True p of
     Just p -> putStrLn $ prettyPrint $ unLabelDecl p
     _ -> putStrLn $ "recompose fail"
   Nothing -> putStrLn "Could not apply the transformation"
  where d = "src/Tests/Data/Examples.hs"

t = do
  g <- parseDeclGraph d
  let ds = _topDecls g
      names = map Ident ["rev2"]
      nodes = map (getName2node g) names
      d = fromJust $ getTypeSig g (head nodes)
      ts = mapMaybe (tailTransformType g) nodes
  putStrLn $ prettyPrint d
  mapM_ (putStrLn . prettyPrint . unLabelDecl) ts
    where d = "src/Tests/Data/UnfoldFold_1.hs"


-- basically adds the accumulator type to the signature
tailTransformType :: DeclGraph -> Node -> Maybe Decl
tailTransformType g key = getTypeSig g key >>= f
  where
    f (TypeSig s [name] (TyFun a b)) = Just $ TypeSig  s [name] $ TyFun b (TyFun a b)
    f x = trace ("typex  " ++ show x) Nothing


tailTransform :: DeclGraph -> Node -> Maybe Decl
tailTransform g node = findFunParts g node >>= recomposeFunParts False

tailTransformWrapped :: DeclGraph -> Node -> Maybe Decl
tailTransformWrapped g node = findFunParts g node >>= recomposeFunParts True


data FunParts = FunParts {
  matchPartss :: [MatchParts],
  leftRec :: Maybe Bool,
  accumOp :: Either QOp Exp,
  argType :: Maybe Type
  }


type MatchParts = (Match, RhsParts) -- ^Original match and its rhsParts
type RhsParts = Either UnGuardedParts [GuardedParts] -- ^Original rhs and its parts (one per guard)
type UnGuardedParts = (Rhs, Maybe RhsExpParts)
--type UnGuardedParts = (Rhs, Maybe IfParts)
type GuardedParts = (GuardedRhs, Maybe RhsExpParts)
--type GuardedParts = (GuardedRhs, Maybe IfParts)
type RhsExpParts = Either BaseRhsParts RecursiveRhsParts
type BaseRhsParts = Exp
data RecursiveRhsParts = RecursiveRhsParts {
  topBinds :: [Binds], -- ^ bindings at the top (match x = let in topExp)
  baseOperands :: [Exp], -- ^ operands that don't call the function
  recOperand :: LetExp, -- ^ of the form "[let in]* fun exp"
  isLeftRec :: Maybe Bool, -- ^Nothing means either the operator is (commutative and associative) or it has no base operand
  operator :: Maybe (Either QOp Exp) -- ^Nothing if there is no operator
              -- Left for infix operator; Right for Var application
  } deriving (Show)

operatorName :: RecursiveRhsParts -> Maybe QName
operatorName = fmap (either opName (\(Var v) -> v)) . operator

operatorAssoc :: RecursiveRhsParts -> Maybe Assoc
operatorAssoc r = operatorQName r <$>> associativity_ -- either (Just . associativity_) (const Nothing)

operatorQName :: RecursiveRhsParts -> Maybe QName
operatorQName = fmap (either opName (\(Var v) -> v)) . operator

funPartsOperatorQName :: FunParts -> QName
funPartsOperatorQName = either opName (\(Var v) -> v) . accumOp

funPartsNeutralExp :: FunParts -> Maybe Exp
funPartsNeutralExp = neutralExp . funPartsOperatorQName

operatorIsAssoc :: RecursiveRhsParts -> Maybe Bool
operatorIsAssoc = fmap isAssociative_ . operatorName

functionQName :: RecursiveRhsParts -> QName
functionQName r = fromJust $ fst $ appChain $ unwrapped $ recOperand r


operatorIsCommut :: RecursiveRhsParts -> Maybe Bool
operatorIsCommut = fmap isCommutative_ . operatorName


-- ^ get the argument of the function in the recursive operand
-- does not work, test case:    a (b c)
-- recOperandArgs :: RecursiveRhsParts -> [Exp]
-- recOperandArgs r = trace ("\n\n" ++ prettyPrint (unwrapped (recOperand r)) ++ "\n") $ snd $ appChain $ unwrapped (recOperand r)

recOperandArgs r = [(\(App _ arg) -> arg) $ unwrapped (recOperand r)]

debug :: Bool
debug = 1 == 0

traceD :: String -> Maybe ()
traceD = when debug . traceM

traceE :: (a -> String) -> a -> a
traceE f x
  | debug = trace (f x) x
  | otherwise = x




findFunParts :: DeclGraph -> Node -> Maybe FunParts
findFunParts g node = do
  let d = getDecl g node
  guard $ isFunBind d
  let (FunBind ms) = d
  parts <- mapM (findMatchParts g) ms
  let allExps = allRhsExpParts parts
  guard $ all isJust allExps
  traceD "everything is Just fine"
  let recs :: [RecursiveRhsParts]
      recs = rights $ catMaybes allExps
      justOps = mapMaybe operator recs
      justRecs = mapMaybe isLeftRec recs
      argType_ = do
        t <- getTypeSig g node
        case typeSigType t of
          (TyFun a _) -> return a
          (TyForall _ _ (TyFun a _)) -> return a
          _ -> trace "Type no matcherino" Nothing

  traceD $ "will there be an operator? " ++ show (justOps)
  op <- listToMaybe justOps
  traceD "found an operator"
  guard $ all (==op) $ justOps
  let leftrec = listToMaybe justRecs
  -- all with the same recursion type
  maybe (return ()) (\l -> guard $ all (==l) justRecs) leftrec
  return FunParts {
    matchPartss = parts,
    leftRec = leftrec,
    accumOp = op,
    argType = argType_
    }

-- |Default recursion (when assoc and commut)
defaultIsLeftRec :: Bool
defaultIsLeftRec = False


-- allowed names
isLiteralExp :: Exp -> Bool
isLiteralExp = Set.null . foldExp astNamesRhsS Set.empty

allBaseExps :: FunParts -> [Exp]
allBaseExps FunParts{matchPartss = ms} = concatMap amp ms
  where
    amp :: MatchParts -> [Exp]
    amp (_, rs) = allrhsp rs
    allrhsp::RhsParts -> [Exp]
    allrhsp (Left ugp) = allunguarded ugp
    allrhsp (Right gp) = concatMap allguarded gp
    allunguarded :: UnGuardedParts -> [Exp]
    allunguarded (_, Just rexpp) = allrexpsp rexpp
    allunguarded _ = []
    allrexpsp :: RhsExpParts -> [Exp]
    allrexpsp (Left e) = [e]
    allrexpsp _ = []
    allguarded (_, Just rhsp) = allrexpsp rhsp
    allguarded _ = []

onlyLiteralExpr :: FunParts -> Maybe Exp
onlyLiteralExpr fp = case allBaseExps fp of
  [x] -> if isLiteralExp x then Just x else Nothing
  _ -> Nothing

findMatchParts :: DeclGraph -> Match -> Maybe MatchParts
findMatchParts g m@(Match _ _ [_] _ _ _)= Just $ ((,) m) $ (findRhsParts g $ matchName m) (matchRhs m)
findMatchParts _ _ = Nothing

findRhsParts :: DeclGraph -> Name -> Rhs -> RhsParts
findRhsParts g funName rhs@(UnGuardedRhs e) =
  left ((,) rhs) $ Left $ findRhsExpParts g funName e
findRhsParts g funName rhs@(GuardedRhss gs) =
  Right $ map (second (findRhsExpPartsG g funName) . dup) gs
  where
    findRhsExpPartsG :: DeclGraph -> Name -> GuardedRhs -> Maybe RhsExpParts
    findRhsExpPartsG g n (GuardedRhs s st e) = findRhsExpParts g n e


findRhsExpParts :: DeclGraph -> Name -> Exp -> Maybe RhsExpParts
findRhsExpParts g funName e =
  if nameIsReachableFromRhsX g foldExp e funName -- isRecursive
    then findRecursiveRhsParts g funName e <$>> Right
    else Just (Left e)

findRecursiveRhsParts :: DeclGraph -> Name -> Exp -> Maybe RecursiveRhsParts
findRecursiveRhsParts g funName e = do
  let (topBinds, ue) = unwrap e
      moperator = getOperator (UnQual funName) ue
  traceD $ "maybe operator = " ++ show moperator
  case moperator of
   Just operator -> do
     let opProp@OpProperties {isAssociative = isassoc, isCommutative = iscommut} =
           getOpProperties (either opName (\(Var v) -> v) operator)
     traceD $ "operator = " ++ show operator
     traceD $ "assoc = " ++ show isassoc
     traceD $ "commut = " ++ show iscommut
     (isLeftRec, baseOperands, recOperand) <- splitBR opProp ue
     return RecursiveRhsParts {
       topBinds = topBinds,
       baseOperands = baseOperands,
       recOperand = recOperand,
       isLeftRec = isLeftRec,
       operator = Just operator -- TODO: there might not be an operator
       }
   -- no operator found
   Nothing -> do
     traceD "No operand"
     recOperand <- recOperandCond ue
     return RecursiveRhsParts {
       topBinds = topBinds,
       baseOperands = [],
       isLeftRec = Nothing,
       operator = Nothing,
       recOperand = recOperand
       }
  where

    getOperator :: QName -> Exp -> Maybe (Either QOp Exp)
    getOperator f a@(App _ _) = mfilter (/=f) (fst (appChain a)) <$>> (Right . Var)
    getOperator _ (InfixApp _ op _) = Just $ Left op
    getOperator _ e = --trace ("oper=" ++ show e)
                      Nothing

    -- | Associative -> Commutative ->  ..
    splitBR :: OpProperties -> Exp -> Maybe (Maybe Bool, [Exp], LetExp)

    -- operator is both associative and commutative
    splitBR OpProperties{isAssociative = True, isCommutative = True} e = do
      chain <- getAppChain e
      case partition baseOperandCond chain of
       (baseOps, [x]) -> recOperandCond x <$>> (,,) Nothing baseOps
       _ -> Nothing

    -- operator is only associative
    splitBR OpProperties{isAssociative = True, isCommutative = False} e = do
      traceD $ "only assoc case"
      chain <- getAppChain e
      traceD $ "chain = " ++ show chain
      case span baseOperandCond chain of
       (baseOps, [x]) -> recOperandCond x <$>> (,,) (Just False) baseOps
       _ -> case span baseOperandCond (reverse chain) of
         (baseOps, [x]) -> recOperandCond x <$>> (,,) (Just True) baseOps
         _ -> Nothing


    splitBR _ _ = --trace "not assoc"
                  Nothing

    getAppChain :: Exp -> Maybe [Exp]
    getAppChain e = case e of
            a@InfixApp {} -> Just $ infixAppChain a
            b@App{} -> Just $ snd $ appChain b
            x -> --- trace ("shit " ++ show x)
              Nothing

    recOperandCond :: Exp -> Maybe LetExp
    recOperandCond e = do
      let le = unwrapLetExp e
      case unwrapped le of
       a@(App _ _) -> do
         case appChain a of
          (Just (UnQual f), args) -> do
            guard $ f == funName && all funNameNotReachableFromExp args
            return le
          _ -> Nothing
       _ -> Nothing

    baseOperandCond :: Exp -> Bool
    baseOperandCond = funNameNotReachableFromExp

    funNameNotReachableFromExp :: Exp -> Bool
    funNameNotReachableFromExp e = not $ nameIsReachableFromRhsX g foldExp e funName


allRhsExpParts :: [MatchParts] -> [Maybe RhsExpParts]
allRhsExpParts = concatMap matchRRhss
  where matchRRhss :: MatchParts -> [Maybe RhsExpParts]
        matchRRhss = rhsRRhss . snd
        rhsRRhss :: RhsParts -> [Maybe RhsExpParts]
        rhsRRhss (Left r) = [snd r]
        rhsRRhss (Right gs) = map snd gs


--------------------
-- RECONSTRUCTION --
--------------------

recomposeFunParts :: Bool -> FunParts -> Maybe Decl
recomposeFunParts wrap fp = ret
  where
   ret
      | wrap = liftM wrapped neutral
      | otherwise = Just tailRec

   --f x = tail neut x where -- when wrapped

   wrapped neu = FunBind [m]
     where m = Match unknownSrcLoc origName [PVar $ Ident "x"] Nothing (UnGuardedRhs call) binds
           call = App (App (Var (UnQual $ funBindName tailRec')) neu) (Var (UnQual (Ident "x")))
           binds = Just $ BDecls [tailRec']
   tailRec' = _mapDecl (subsNames [(origName,Ident$(nameStr origName ++ "TailRec"))])  tailRec

   origName = funBindName tailRec
   opname = funPartsOperatorQName fp
   neutral :: Maybe Exp
   neutral = case neutralExp opname of
     Just n -> Just n
     Nothing -> if isAssociative_ opname && isCommutative_ opname then onlyLiteralExpr fp else Nothing     -- undefined

   tailRec = funBindAddArgs [Ident "acc"] $
             FunBind (map recomposeMatchParts $ matchPartss fp)

   recomposeMatchParts :: MatchParts -> Match
   recomposeMatchParts (m, rhsparts) = matchSetRhs (recomposeRhsParts rhsparts) m

   recomposeRhsParts :: RhsParts -> Rhs
   recomposeRhsParts (Left (rhs, Nothing)) = rhs
   recomposeRhsParts (Left (_ ,Just gp)) = UnGuardedRhs $ recomposeRhsExpParts gp
   recomposeRhsParts (Right l) = GuardedRhss $ map recomposeGuardedParts l

   recomposeGuardedParts :: GuardedParts -> GuardedRhs
   recomposeGuardedParts (g, Nothing) = g
   recomposeGuardedParts (GuardedRhs s st _, Just r) = GuardedRhs s st (recomposeRhsExpParts r)


   recomposeRhsExpParts :: RhsExpParts -> Exp
   recomposeRhsExpParts = either recomposeBaseRhsParts  recomposeRecursiveRhsParts

   recomposeBaseRhsParts :: BaseRhsParts -> Exp
   recomposeBaseRhsParts r = case accumOp fp of
                Left qop -> (InfixApp x qop y)
                Right vop -> App (App vop x) y
     where leftrec = fromMaybe defaultIsLeftRec (leftRec fp)
           (x, y) = (not leftrec?$swap) (r, accumVar)
           maybeflip = case leftRec fp of Just False -> App flipExp
                                          _ -> id

   accumVar :: Exp
   accumVar = Var $ UnQual $ Ident "acc"

   recomposeRecursiveRhsParts :: RecursiveRhsParts -> Exp
   recomposeRecursiveRhsParts r = reWrap (topBinds r) newExp
     where
       leftRec = --trace ("show Left Rec = " ++ show (isLeftRec r))
                 fromMaybe defaultIsLeftRec $ isLeftRec r
       newExp =
         let vop = Var $ functionQName r
             args = recOperandArgs r
         in foldl App vop (accumArg:args)
       accumArg = case operator r of
                   Just (Left op) ->
                     fold (flip InfixApp op) accumVar (baseOperands r)
                   Just (Right f) -> fold (\ac x -> (App (App (maybeflip f) ac ) x)) accumVar (baseOperands r)
                   Nothing -> accumVar
       -- wrapped recursive operand
       --wr = wrapLetExp $ recOperand r --
       fold = if leftRec then foldr else foldl
       maybeflip = case isLeftRec r of Just False -> id --App flipExp
                                       _ -> App flipExp-- id
