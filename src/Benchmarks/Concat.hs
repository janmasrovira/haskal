module Concat (concatBenchmark) where

import Data.List
import Criterion.Main


input :: [[Int]]
input = [[1..50] | n <- [1..500]]



concatBenchmark :: Benchmark
concatBenchmark =
  bgroup "concat" [ bench "Augmentative"  $ nf concatAug input
                  , bench "TailRec" $ nf concatTail input      
                  , bench "ReverseTailRec" $ nf concatRev input
                  ]

concatAug [] = []
concatAug (x:xs) = x ++ concat xs

concatTail = f []
  where
    f a [] = a
    f a (x:xs) = f (a ++ x) xs


concatRev l = reverse (concatr' [] l)
  where concatr' acc [] = acc
        concatr' acc (x:xs) = concatr' ([x] ++ acc) xs
