{-# LANGUAGE OverloadedStrings #-}

module Main where

-- on ghci start: build target = benchmarks

import Sum
import Reverse
import Concat
import Criterion.Main
import Criterion.Types
import Text.LaTeX
import Text.LaTeX.Base.Pretty

import Data.List
import Text.Printf

import Debug.Trace

-- important!! Compile with -O2 to obtain reliable results

main :: IO ()
main = do
  writeFile reportcsv ""
  defaultMainWith defaultConfig {
    confInterval = 0.99
    , timeLimit = 20
    , csvFile = Just reportcsv
                --- , rawDataFile = Just "data.txt" --binary data, useless
    , reportFile = Just "report.html"
    }
    [
      sumBenchmark
       -- concatBenchmark
      --  reverseBenchmark
    ]
  writelatex 

-- parses csv and write the tex code in latextable file
writelatex :: IO ()
writelatex = do
  readFile reportcsv >>= writeFile latextable . csvToLaTeX
  readFile latextable >>= putStrLn

t = do
  d <- readFile reportcsv
  print $ map words . lines . map commatospace $ d

  
reportcsv :: String
reportcsv = "csvtable.csv"

latextable :: String
latextable = "latextable.tex"



mkTable :: [[String]] -> String
mkTable (colnameCSVs:xs) = prettyLaTeX $ tabular pos specs (header<>hline<>hline<>rows)
  where
    colnames = colnamesCat
    colnamesCat = ["Nom", "Mitjana", "MitjanaInf", "MitjanaSup",
                   "Desviació", "DesviacióInf", "DesviacióSup"]
    m = length colnames
    n = length xs
    pos = Just Center
    formatrow (name:nums) = name : map formatDouble nums
    formatDouble = printf "%.3f" . readMicro
    rows = foldl1 (\a b -> a <> lnbk <> b) [mkrow $ formatrow r | r <- xs]
    specs = intersperse VerticalLine $ replicate m LeftColumn
    mkrow = foldl1 (&) . (map fromString)
    header = foldl1 (&) (map fromString colnames) <> lnbk

-- ignores stdev bounds
mkTable2 :: [[String]] -> String
mkTable2 (colnameCSVs:xs) = prettyLaTeX $ tabular pos specs (header<>hline<>hline<>rows)
  where
    colnames = colnamesCat
    colnamesCat = ["Nom", "Mitjana", "MitjanaInf", "MitjanaSup",
                   "Desviació"]
    m = length colnames
    n = length xs
    pos = Just Center
    formatrow (name:nums) = name : map formatDouble nums
    formatDouble = printf "%.3f" . readMicro
    rows = foldl1 (\a b -> a <> lnbk <> b) [mkrow $ formatrow r | r <- xs]
    specs = intersperse VerticalLine $ replicate m LeftColumn
    mkrow = foldl1 (&) . (map fromString) . take m
    header = foldl1 (&) (map fromString colnames) <> lnbk


--readDouble x = (read x :: Double)

readMicro  = (*10^^6) . (read  ::String-> Double)

csvToLaTeX :: String -> String
csvToLaTeX = mkTable2 . map words . lines . map commatospace


commatospace :: Char -> Char
commatospace ',' = ' '
commatospace x = x
    
               











    
