module Reverse (reverseBenchmark) where


import Criterion.Main
import Data.Foldable

input :: [Int]
input = [1..999]


reverseBenchmark :: Benchmark
reverseBenchmark =
  bgroup "reverse" [ bench "augmentative"  $ nf reverseAugmentative input
                   , bench "tail (right appending)" $ nf reverseTail input
                   , bench "tail (left appending)" $ nf reverseTailR input
                   , bench "tail strict (left appending)" $ nf reverseTailRStrict input
               --    , bench "foldl'" $ nf reverseFoldlStrict input
                   , bench "ghc.list" $ nf reverse input
                   ]


reverseAugmentative :: [t] -> [t]
reverseAugmentative [] = []
reverseAugmentative (a:t) = reverseAugmentative t ++ [a]

reverseTail :: [a] -> [a]
reverseTail = r []
  where r ac [] = ac
        r ac (a:t) = r (ac ++ [a]) t

reverseTailR :: [a] -> [a]
reverseTailR l = reverse (r [] l)
  where r ac [] = reverse ac
        r ac (a:t) = (r ([a] ++ ac)) t

reverseFoldlStrict :: [a] -> [a]
reverseFoldlStrict = foldl' (flip (:)) []

reverseTailRStrict :: [a] -> [a]
reverseTailRStrict l = reverse (r [] l)
  where r ac [] = reverse ac
        r ac (a:t) = (r ([a] ++ ac)) t
