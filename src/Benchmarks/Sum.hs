module Sum (sumBenchmark) where

import Data.List
import Criterion.Main


input :: [Int]
input = replicate (9999) 3

sumBenchmark :: Benchmark
sumBenchmark =
  bgroup "sum" [ bench "Augmentative"  $ whnf sumAugmentative input
              ,   bench "TailRec"  $ whnf sumTail input
             -- , bench "tail strict" $ whnf sumTailStrict input
              , bench "Foldl'"  $ whnf sumFoldStrict input
              , bench "Foldr" $ whnf sumFoldr input
                 --              , bench "data.foldable" $ whnf sumDefault input
               ]

sumAugmentative :: [Int] -> Int
sumAugmentative [] = 0
sumAugmentative (a:b) = a + sumAugmentative b

sumTail :: [Int] -> Int
sumTail = sum' 0
  where
    sum' ac [] = ac
    sum' ac (a:t) = sum' (ac + a) t

sumTailStrict :: [Int] -> Int 
sumTailStrict = sum_ 0
  where
    sum_ ac [] = ac
    sum_ ac (a:t) = (sum_ $! (ac + a)) t


sumFoldStrict :: [Int] -> Int
sumFoldStrict = foldl' (+) 0

sumDefault :: [Int] -> Int
sumDefault = sum


sumFoldr :: [Int] -> Int
sumFoldr = foldr (+) 0
