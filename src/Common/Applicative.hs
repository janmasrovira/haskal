module Common.Applicative where

import Control.Applicative ((<$>))

(<$>>) :: Functor f => f a -> (a -> b) -> f b
(<$>>) = flip (<$>)
infixl 4 <$>>

($?) :: (a -> a) -> Bool -> a -> a
($?) f c = if c then f else id
infixr 2 $?

(?$) :: Bool -> (a -> a) -> a -> a
(?$) = flip ($?)
infixr 2 ?$


argList2 :: (a -> a -> a) -> [a] -> a
argList2 f (a:b:_) = f a b

argList3 :: (a -> a -> a -> a) -> [a] -> a
argList3 f (a:b:c:_) = f a b c

pure1 :: Applicative f => (a -> b) -> a -> f b
pure1 = (pure .)

pure2 :: Applicative f => (a -> b -> c) -> a -> b -> f c
pure2 = (.) (pure .)

pure3 :: Applicative f => (a -> b -> c -> d) -> a -> b -> c -> f d
pure3 = (.) ((pure .) .)
