module Common.Bounded (minB, maxB, minimumB, maximumB, maxWithB, minWithB,
                       maximumWithB, minimumWithB) where


maxWithB :: Ord a => a -> a -> a -> a
maxWithB m a b
  | a >= m = a
  | otherwise = max a b

minWithB :: Ord a => a -> a -> a -> a
minWithB m a b
  | a <= m = a
  | otherwise = min a b

maxB :: (Ord a, Bounded a) => a -> a -> a
maxB = maxWithB maxBound

minB :: (Ord a, Bounded a) => a -> a -> a
minB = maxWithB minBound

minimumB :: (Ord a, Bounded a) => [a] -> a
minimumB = foldr1 minB

maximumB :: (Ord a, Bounded a) => [a] -> a
maximumB = foldr1 maxB

minimumWithB :: Ord a => a -> [a] -> a
minimumWithB m = foldr1 (minWithB m)

maximumWithB :: Ord a => a -> [a] -> a
maximumWithB m = foldr1 (maxWithB m)
