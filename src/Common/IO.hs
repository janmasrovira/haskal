module Common.IO (
  chooseOne, chooseOneShow,
  separator1, separator, endl,
  menu, separatorThick, rmenu,
  bmenu, brmenu, chooseOneAlias,
  timeAction, timeAction_
  ) where


import Common.List
import Common.Applicative

import Control.Arrow
import Control.Monad
import Control.Applicative
import Data.Maybe
import System.Clock


timeAction_ :: IO a -> IO TimeSpec
timeAction_= fmap snd . timeAction

timeAction :: IO t -> IO (t, TimeSpec)
timeAction a = do
  t0 <- getTime clock
  r <- a
  t1 <- getTime clock
  return (r, diffTimeSpec t0 t1)
  where clock = Monotonic

readInt :: String -> Int
readInt = read

getIntInRange :: Int -> Int -> IO Int
getIntInRange a b = do
  n <- getLine <$>> words <$>> head <$>> readInt
  if a <= n && n <= b
    then return n
    else (putStrLn "not in range, try again" >> getIntInRange a b)


chooseOneShow :: Show a => Bool -> [a] -> IO a
chooseOneShow short = chooseOne short show

endl :: IO ()
endl = putStrLn ""

width :: Int
width = 50

separator :: IO ()
separator = putStrLn $ "+" ++ replicate (width - 2) '-' ++ "+" 

separator1 :: IO ()
--separator1 = endl >> separator >> endl
separator1 = separator

separatorThick :: IO ()
separatorThick = endl >> putStrLn (replicate width '=') >> endl

chooseOne :: Bool -> (b -> String) -> [b] -> IO b
chooseOne short showf l = do
  pleaseOne
  let sep = if short then return() else separator1
      jump = if short then "" else "\n\n"
  sep >> mapM_ (\(n, s) -> putStr ("[" ++ show n ++ "] " ++ jump) >> putStrLn s >> sep) c
  n <- getIntInRange 1 len
  separatorThick
  return $ l!!(n-1)
  where c = enumerate1 $ map showf l
        len = length l

chooseOneAlias :: Eq e => Bool -> [(e, String)] -> IO e
chooseOneAlias short alias = chooseOne short (fromJust . flip lookup alias) (map fst alias)

pleaseOne :: IO ()
pleaseOne = putStrLn "Please, choose one:"

menu :: [(IO a, String)] -> IO a
menu d = chooseOne True snd d >>= (\x -> putStrLn ("--> " ++ snd x ++ "\n") >> fst x <* separatorThick)

-- | menu with back
bmenu :: IO b -> [(IO b, String)] -> IO b
bmenu up d = menu (d ++ [(up, "Back")])

-- | menu with back and loop
brmenu :: IO a -> [(IO a, String)] -> IO b
brmenu up d = bmenu up d >> brmenu up d


-- | menu with loop
rmenu :: IO a -> [(IO b, String)] -> IO a
rmenu a d = menu d >> rmenu a d
