module Common.List (
  mapWhile, mapWhileExtra,
  mapWhile1, lengthGT,
  lengthGE, lengthEQ,
  spanExtra, span1,
  count, iterateWhileDiff,
  enumerate, enumerate1,
  enumerateStr, enumerateStr1, iterateWhileDiffLast,
  nubOrd, splits) where

import qualified Data.Map as Map
import Control.Applicative
import Control.Arrow
import qualified Data.Set as Set

enumerate :: [b] -> [(Int, b)]
enumerate = zip [0..]

enumerate1 :: [b] -> [(Int, b)]
enumerate1 = zip [1..]

enumerateStr :: [b] -> [(String, b)]
enumerateStr = map (first show) . zip [0..]

enumerateStr1 :: [b] -> [(String, b)]
enumerateStr1 = map (first show) . zip [1..]

splits :: [a] -> [([a], [a])]
splits l = takeWhile1 (not.null.snd) $ map (`splitAt`l) [0..]


-- |Maps a function over a list, stops before a 'False' is returned. Returns 'True' iff all applications of the function have returned 'True'
mapWhile :: (a -> (Bool, b)) -> [a] -> (Bool, [b])
mapWhile = mapWhileExtra 0

-- |Maps a function over a list, stops x elements after a 'False' is returned. Returns 'True' iff all applications of the function have returned 'True'
mapWhileExtra :: Int -> (a -> (Bool, b)) -> [a] -> (Bool, [b])
mapWhileExtra x f l = let (a, b) = spanExtra x fst (map f l) in (null b, map snd (a ++ b))

-- |Maps a function over a list, stops after a 'False' is returned. Returns 'True' iff all applications of the function have returned 'True'
mapWhile1 :: (a -> (Bool, b)) -> [a] -> (Bool, [b])
mapWhile1 f l = let (a, b) = spanExtra 1 fst (map f l) in (null b, map snd (a ++ b))

-- | Equivalent to span but takes 1 additional element
span1 :: (a -> Bool) -> [a] -> ([a], [a])
span1 = spanExtra 1

takeWhile1 :: (a -> Bool) -> [a] -> [a]
takeWhile1 f = fst . span1 f

-- | Equivalent to span but takes x additional elements
spanExtra :: Int -> (a -> Bool) -> [a] -> ([a], [a])
spanExtra x f l = let (a, b) = span f l
              in (a ++ take x b, drop 1 b) --dont use tail or be aware of empty b!

-- | Returns 'True' iff the length of the list is greater than the argument
lengthGT :: Int -> [a] -> Bool
lengthGT i = not . null . drop i

-- | Returns 'True' iff the length of the list is equal or greater than the argument
lengthGE :: Int -> [a] -> Bool
lengthGE 0 = const True
lengthGE i = not . null . drop (i - 1)

-- | Returns 'True' iff the length of the list is equal to the argument
lengthEQ :: Int -> [a] -> Bool
lengthEQ 0 l = null l
lengthEQ i l = case drop (i - 1) l of
                [_] -> True
                _ -> False

-- | Returns [(elem, number of occurences of elem)], order may vary
count :: Ord k => [k] -> [(k, Int)]
count = Map.assocs . foldr ins Map.empty
  where ins k = Map.insertWith' (+) k 1

iterateWhileDiff :: Eq a => (a -> a) -> a -> [a]
iterateWhileDiff f i = map fst $ takeWhile1 (uncurry (/=)) consec
  where l = iterate f i
        consec = zip l (tail l)

iterateWhileDiffLast :: Eq c => (c -> c) -> c -> c
iterateWhileDiffLast f = last . iterateWhileDiff f

nubOrd :: Ord a => [a] -> [a]
nubOrd = Set.toList . Set.fromList
