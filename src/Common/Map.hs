module Common.Map (mkBiIndex, mkBiIndexNoRep,
                  mkIndex, mkIndexLookup) where

import           Data.IntMap.Strict (IntMap)
import qualified Data.IntMap.Strict as IntMap

import qualified Data.Map.Strict as Map
import           Data.Map.Strict (Map)

import qualified Data.Set as Set

import Common.List

-- | Removes repeated elements before constructing index
mkBiIndex :: (Ord a) => [a] -> (IntMap a, Map a IntMap.Key)
mkBiIndex = mkBiIndexNoRep . Set.toList . Set.fromList


-- | Precond: all elements in the list are different
mkBiIndexNoRep :: (Ord a) => [a] -> (IntMap a, Map a IntMap.Key)
mkBiIndexNoRep l = (intMap, revMap)
  where intMap = mkIndex l'
        revMap = Map.fromList $ zip l' [0..]
        l' = Set.toList $ Set.fromList l

mkIndex :: [a] -> IntMap a
mkIndex = IntMap.fromList . enumerate

mkIndexLookup :: [a] -> (Int -> Maybe a)
mkIndexLookup = flip IntMap.lookup . IntMap.fromList . enumerate



