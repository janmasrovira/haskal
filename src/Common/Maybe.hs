module Common.Maybe (maybeApp, maybeAppId, maybeFApp,
                     maybeApp2, maybeAppId2, maybeFApp2,
                     filterMaybe, toMaybe, maybeToEither) where


import Data.Maybe
import Common.Applicative

maybeFApp :: (a -> Maybe b) -> (a -> b) -> a -> b
maybeFApp f fdefault x = fromMaybe (fdefault x) (f x)

maybeApp :: (a -> Maybe b) -> b -> a -> b
maybeApp f d = maybeFApp f (const d)

maybeAppId :: (a -> Maybe a) -> a -> a
maybeAppId f = maybeFApp f id

maybeFApp2 :: (a -> c -> Maybe b) -> (a -> c -> b) -> a -> c -> b
maybeFApp2 f fdefault x y = fromMaybe (fdefault x y) (f x y)

maybeApp2 :: (a -> c -> Maybe b) -> b -> a -> c -> b
maybeApp2 f d = maybeFApp2 f (const $ const d)

maybeAppId2 :: (a -> c -> Maybe a) -> a -> c -> a
maybeAppId2 f = maybeFApp2 f (flip $ const id)

filterMaybe :: (b -> Bool) -> Maybe b -> Maybe b
filterMaybe f m = do
  v <- m
  if f v then m else Nothing

maybeToEither :: (a -> Maybe b) -> a -> Either a b
maybeToEither f a = case f a of
  Just x -> Right x
  _ -> Left a

toMaybe :: (a -> Bool) -> a -> Maybe a
toMaybe cond a
  | cond a = Just a
  | otherwise = Nothing

