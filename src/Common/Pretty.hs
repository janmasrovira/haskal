module Common.Pretty
       ( prettyTable, prettyTable2)
       where

import Data.Array.IArray as Array
import Data.Array.Unboxed as UArray
import Text.PrettyPrint.Boxes
import Data.Ix
import Control.Arrow (second)

--prettyTable :: Show x => Array (Int,Int) x -> String
prettyTable :: (Show x, IArray a x) => a (Int,Int) x -> String
prettyTable a' = render $ hsep 1 center1 boxCols 
  where
    b@((i0, j0), (i1, j1)) = bounds a'
    ib = (i0, i1)
    jb = (j0, j1)
    a :: Array (Int,Int) String
    a = array b (map (second show) $ assocs a')--Array.amap show a'
    boxCols = [boxCol j | j <- range jb]
    boxCol j = punctuateV center1 vsep (map text $ col j)
      where maxl = maximum $ map length (col j) 
            vsep = text $ replicate maxl '-'
    col j = [ a!(i,j)| i <- range ib]




--prettyTable2 :: Show x => Array (Int,Int) x -> String
prettyTable2 :: (Show x, IArray a x) => a (Int,Int) x -> String
prettyTable2 a' = render $ hsep 1 center1 boxCols
  where
    b@((i0, j0), (i1, j1)) = bounds a'
    ib = (i0, i1)
    jb = (j0, j1)
    a :: Array (Int,Int) String
    a = array b (map (second show) $ assocs a')
    boxCols = col0 : [boxCol j | j <- range jb]
      where col0 = vsep 1 left $ emptyBox  1 1: [text $ show i ++ ":" | i <- range ib]
    boxCol j = punctuateV center1 sep (header : (map text $ col j))
      where maxl = maximum $ map length (col j) 
            sep = text $ replicate maxl '-'
            header = text $ show j ++ ":"
    col j = [ a!(i,j)| i <- range ib]


aa :: UArray (Int,Int) Int
aa = listArray ((1,1), (12,12)) [1,1000..]

f = putStrLn $ prettyTable2 aa
