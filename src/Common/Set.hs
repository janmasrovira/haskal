module Common.Set (intSetToSet, setToIntSet, disjoint) where

import qualified Data.Set as Set
import Data.Set (Set)
import qualified Data.IntSet as IntSet

intSetToSet :: Ord a => (IntSet.Key -> a) -> IntSet.IntSet -> Set.Set a
intSetToSet f = Set.fromList . map f . IntSet.toList

setToIntSet :: (a -> IntSet.Key) -> Set.Set a -> IntSet.IntSet
setToIntSet f = IntSet.fromList . map f . Set.toList

disjoint :: Ord a => Set a -> Set a -> Bool
disjoint a b = Set.null $ Set.intersection a b
