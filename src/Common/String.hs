module Common.String where

allStrings :: [String]
allStrings = [ c : s | s <- "" : allStrings, c <- ['a'..'z'] ]

boolToYesNo :: Bool -> String
boolToYesNo True = "Yes"
boolToYesNo False = "No"

box :: String -> String
box s = bar ++ nl ++ leftside ++ s ++ rightside ++ nl ++ bar ++ nl
  where l = length s
        width = 2
        margin = 2
        sym = '/'
        nl = "\n"
        bar = replicate (l + 2*(margin + width)) sym
        leftside = replicate width sym ++ replicate margin ' '
        rightside = reverse leftside



---------
-- yes --
---------
