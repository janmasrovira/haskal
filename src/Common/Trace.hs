module Common.Trace where

import Debug.Trace

traceApp :: (a -> String) -> a -> a
traceApp f x = trace (f x) x 
