module Parsing.ExtParser (
  parseModule
  , lexTokenStream
  , parseExp
  , parsePat
  , parseStmt
  , parseDecl
  , parseType
  ) where

--Exts
import qualified Language.Haskell.Exts.Extension as E
import qualified Language.Haskell.Exts.Fixity as F
import qualified Language.Haskell.Exts.Lexer as L
import qualified Language.Haskell.Exts.Parser as P
import qualified Language.Haskell.Exts.Syntax as S
import Parsing.Parser (SimpleModule)

--Parsec
import           Text.Parsec.Pos

--Own
import           Parsing.Position

mode :: String -> P.ParseMode
mode file = P.defaultParseMode {
  P.parseFilename = file,
  P.baseLanguage = E.Haskell2010,
  P.extensions = [],
  P.ignoreLanguagePragmas = True,
  P.ignoreLinePragmas = True,
  P.fixities = Just F.preludeFixities
  }


toSimpleModule :: S.Module -> SimpleModule
toSimpleModule (S.Module loc _ _ _ _ imps decls) = (Just $ S.srcFilename loc, imps, decls)

imports :: S.Module -> [S.ImportDecl]
imports (S.Module _ _ _ _ _ imps _) = imps

parseImports :: String -> String -> [S.ImportDecl]
parseImports src = imports . P.fromParseResult . P.parseModuleWithMode (mode src)

parseModule :: FilePath -> String -> SimpleModule
parseModule src = toSimpleModule . P.fromParseResult . P.parseModuleWithMode (mode src)

parseExp :: String -> S.Exp
parseExp = P.fromParseResult . P.parseExp

parsePat :: String -> S.Pat
parsePat =  P.fromParseResult . P.parsePat

parseStmt :: String -> S.Stmt
parseStmt =  P.fromParseResult . P.parseStmt

parseDecl :: String -> S.Decl
parseDecl =  P.fromParseResult . P.parseDecl

parseType :: String -> S.Type
parseType =  P.fromParseResult . P.parseType


lexTokenStream :: String -> String -> [(SourcePos, L.Token)]
lexTokenStream src = fromLocs . P.fromParseResult . L.lexTokenStreamWithMode (mode src)

