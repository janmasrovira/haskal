module Parsing.Indent (
  getNextLoc, traceln, sameOrIndentedLeft, ParserState, runIndentParser,
  withLeft, withRef, checkIndentRef, withLayout, withoutLayout, IndentParserP) where

import           Control.Concatenative
import           Control.Monad.State
import           Debug.Trace
import qualified Language.Haskell.Exts.Syntax as S
import           Text.Parsec hiding (State)
import           Text.Parsec.Pos

--Own
import           Common.Applicative ((<$>>))
import           Parsing.Position


-- on, left, ref
type ParserState = (Bool, SourcePos, SourcePos)

-- |Parser with stream of tokens paired with its 'SourcePos'
type IndentParserP s a = ParsecT [(SourcePos, s)] () (State ParserState) a

-- |Gets the next @SourcePos@ in the stream
getNextPos :: Show a => IndentParserP a SourcePos
getNextPos = do
    (pos, _) <- lookAhead anyToken
    return pos

-- for compatibility with exts.syntax
getNextLoc :: Show a => IndentParserP a S.SrcLoc
getNextLoc = getNextPos <$>> fromPos


-- | Parses only when indented past the level of the reference
indentedLeft :: Show s => IndentParserP s () 
indentedLeft = do
    pos <- getNextPos
    l <- left              
    whenOn $ when (biAp sourceColumn (>=) l pos) $ parserFail "not indented"


-- -- | Parses only when indented past the level of the reference or on the same line
sameOrIndentedLeft :: Show s => IndentParserP s ()
sameOrIndentedLeft = whenOn $ sameLeft <|> indentedLeft


-- debugging
traceln  :: (Show a, Eq a) => String -> IndentParserP a ()
traceln s = do
  p <- getNextPos
  l <- left
  ntok <- getNext
  r <- ref
  (on,_,_) <- get
  traceM (s ++ "ON: " ++ show on ++ ", next: " ++ showpostok ntok ++
         ", left: " ++ showpos l ++ ", ref: " ++ showpos r)

showpos p = show (sourceLine p, sourceColumn p)
showpostok (pos, tok) = "(" ++ show tok ++ ", " ++ showpos pos ++ ")"

getNext :: Show s => IndentParserP s (SourcePos, s)
getNext = lookAhead anyToken


putLeft :: SourcePos -> IndentParserP s ()
putLeft l = do
  (on,_,reff) <- get
  put (on, l, reff)

putRef :: SourcePos -> IndentParserP s ()
putRef reff = do
  (on,l,_) <- get
  put (on, l, reff)

putOn :: Bool -> IndentParserP s ()
putOn on = do
  (_,l,reff) <- get
  put (on, l, reff)

left :: Show s => IndentParserP s SourcePos
left = do
  (_,l,_) <- get
  return l

ref :: Show s => IndentParserP s SourcePos
ref = do
  (_,_,r) <- get
  return r

whenOn :: IndentParserP s () -> IndentParserP s ()
whenOn x = do
  (on,_,_) <- get
  when on x


block :: Show s => IndentParserP s a -> IndentParserP s [a]
block p = withRef $ many1 (checkIndentRef >> p)

withLayout :: Show s => IndentParserP s a -> IndentParserP s a
withLayout x = do
  st <- get
  r <- putOn True >> x
  put st >> return r


withoutLayout :: Show s => IndentParserP s a -> IndentParserP s a
withoutLayout x = do
  st <- get
  r <- putOn False >> x
  put st >> return r


-- | Parses using the current location for indentation reference
withRef :: Show s => IndentParserP s a -> IndentParserP s a
withRef x = do
  st <- get
  p <- getNextPos
  r <- putRef p >> x
  put st >> return r

-- same column or same line or indented wrt previous left
withLeft :: Show s => IndentParserP s a -> IndentParserP s a
withLeft x = (sameOrIndentedLeft <|> sameColLeft) >> do
  st <- get
  l <- getNextPos
  r <- putLeft l >> x
  put st >> return r
  
sameColLeft :: Show s => IndentParserP s ()
sameColLeft = whenOn $ do
    l <- left
    pos <- getNextPos
    unless (biAp sourceColumn (==) pos l) $ parserFail "indentationn doesn't match"


-- | Ensures the current indentation level matches that of the reference
checkIndentRef :: Show s => IndentParserP s ()
checkIndentRef = whenOn $ do
    reff <- ref
    pos <- getNextPos
    unless (biAp sourceColumn (==) pos reff) $ parserFail "indentationn doesn't match"



sameLeft :: Show s => IndentParserP s ()
sameLeft = whenOn $ do
  pos <- getNextPos
  l <- left
  unless (biAp sourceLine (==) pos l) $ parserFail "over one line"


-- | Run the result of an indentation sensitive parse
runIndent :: SourceName -> State ParserState a -> a
runIndent s = flip evalState (inilayout, inileft, iniref) 
  where inileft = newPos s 0 0
        iniref = newPos s 0 0
        inilayout = True

runIndentParser :: IndentParserP s a -> SourceName -> [(SourcePos, s)] -> Either ParseError a
runIndentParser parser src input = runIndent src $ runParserT parser () src input

