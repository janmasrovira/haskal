module Parsing.Lexer (
  lexTokenStream
  , module Language.Haskell.Exts.Lexer  --hiding (lexTokenStream)
  , module Text.Parsec.Pos
  ) where

import           Control.Applicative          ((<*))
import           Control.Monad.State
import           Data.Functor
import           Data.List
import           Data.Ratio
import           Language.Haskell.Exts.Lexer  hiding (lexTokenStream)
import           Text.Parsec                  hiding (State)
import qualified Text.Parsec.Char             as C
import           Text.Parsec.Combinator       (sepBy)
import qualified Text.Parsec.Language         as L
import           Text.Parsec.Pos
import qualified Text.Parsec.Token            as P


--Own
import           Common.Applicative


-------------------------
-- LANGUAGE DEFINITION --
-------------------------
type IParser a = ParsecT String () (State SourcePos) a
type TParser = IParser Token

haskellDef :: L.GenLanguageDef String () (State SourcePos)
haskellDef = P.LanguageDef {
  P.commentStart   = "{-"
  , P.commentEnd     = "-}"
  , P.commentLine    = "--"
  , P.nestedComments = True
  , P.identStart    = lower <|> oneOf "_" :: IParser Char
  , P.identLetter  = alphaNum <|> oneOf "_'"
  , P.opStart  = P.opLetter haskellDef
  , P.opLetter = oneOf ":!#$%&*+./<=>?@\\^|-~"
  , P.reservedNames = ["let","in","case","of","if","then","else",
                       "do","where", "data", "newtype", "type",
                       "deriving", "module", "import"]
  , P.reservedOpNames = ["::","..","=","\\","|","<-","->","@","~","=>"]
  , P.caseSensitive  = True
  }



---------------------
-- LEXICAL PARSERS --
---------------------

lexer = P.makeTokenParser haskellDef

lexeme = P.lexeme lexer
symbol = P.symbol lexer
parens = P.parens lexer
braces = P.braces lexer
brackets = P.brackets lexer
squares = P.squares lexer
semi = P.semi lexer
comma = P.comma lexer
colon = P.colon lexer
dot = P.dot lexer
semiSep = P.semiSep lexer
semiSep1 = P.semiSep1 lexer
commaSep = P.commaSep lexer
commaSep1 = P.commaSep1 lexer
charLiteral = P.charLiteral lexer
stringLiteral = P.stringLiteral lexer
decimal = P.decimal lexer
hexadecimal = P.hexadecimal lexer
octal = P.octal lexer
natural = P.natural lexer
integer = P.integer lexer
float = P.float lexer
naturalOrFloat = P.naturalOrFloat lexer
identifier = P.identifier lexer
reserved = P.reserved lexer
operator = P.operator lexer
reservedOp = P.reservedOp lexer
whiteSpace = P.whiteSpace lexer

----------------------------------------------
----------------------------------------------

rational :: IParser Rational
rational = lexeme $ do
  a <- cnatural <$>> read
  C.char '.'
  d <- cnatural -- can have preceding zeros (for example: 1.001)
  let numd = read d
  return $ let ld = length d in ((a*10^ld + numd) % 10^ld)
  where cnatural = many1 C.digit


qConId = lexeme $ do
  l <- sepBy1 capitalWord dot
  return $ case l of
    [x] -> ConId x
    xs -> QConId (intercalate "." (init xs), last xs)
    where capitalWord = do
            a <- C.upper
            many (alphaNum <|> oneOf "_'") <$>> (a:)


withPos :: Monad m => ParsecT s u m t -> ParsecT s u m (SourcePos, t)
withPos p = do
  pos <- getPosition
  x <- p
  return (pos, x)


ttoken :: IParser (SourcePos, Token)
ttoken = withPos $
  keyword <|>
  identifier <$>> VarId <|>
  toperator <|>
  delimiter <|>
  separator <|>
  special <|>
  literal <|>
  qConId


literal :: TParser
literal =
  try rational <$>> (\x -> FloatTok (x, show (fromRational x :: Float))) <|>
  natural <$>> (\x -> IntTok (x, show x)) <|>
  stringLiteral <$>> (\x -> StringTok (x, x)) <|> -- not equivalent for strings with special characters, but does not matter
  charLiteral <$>> (\x -> Character (x, [x]))

delimiter :: TParser
delimiter =
  symbol "(" $> LeftParen <|>
  symbol ")" $> RightParen <|>
  symbol "[" $> LeftSquare <|>
  symbol "]" $> RightSquare <|>
  symbol "{" $> LeftCurly <|>
  symbol "}" $> RightCurly <|>
  symbol "`" $> BackQuote

separator :: TParser
separator =
  symbol ";" $> SemiColon <|>
  symbol "," $> Comma

toperator :: TParser
toperator =
  reservedOp "=" $> Equals <|>
  reservedOp "-" $> Minus <|>
  reservedOp ":" $> Colon <|>
  operator <$>> VarSym

special :: TParser
special =
  reservedOp "::" $> DoubleColon <|>
  reservedOp ".." $> DotDot <|>
  reservedOp "@" $> At <|>
  reservedOp "<-" $> LeftArrow <|>
  reservedOp "->" $> RightArrow <|>
  reservedOp "=>" $> DoubleArrow <|>
  reservedOp "|" $> Bar <|>
  reservedOp "\\" $> Backslash

keyword :: TParser
keyword =
  reserved "where" $> KW_Where <|>
  reserved "if" $> KW_If <|>
  reserved "then" $> KW_Then <|>
  reserved "else" $> KW_Else <|>
  reserved "do" $> KW_Do <|>
  reserved "let" $> KW_Let <|>
  reserved "in" $> KW_In <|>
  reserved "case" $> KW_Case <|>
  reserved "of" $> KW_Of <|>
  reserved "type" $> KW_Type <|>
  reserved "data" $> KW_Data <|>
  reserved "newtype" $> KW_NewType <|>
  reserved "deriving" $> KW_Deriving <|>
  reserved "module" $> KW_Module <|>
  reserved "import" $> KW_Import
  <|> reserved "_" $> Underscore
  <|> reserved "qualified" $> KW_Qualified

ttokens :: IParser [(SourcePos, Token)]
ttokens = whiteSpace >> many ttoken <* eof

-- |Lexes a stream of characters to a stream of Haskell tokens associated to a 'SourcePos'
lexTokenStream :: SourceName -> String -> [(SourcePos, Token)]
lexTokenStream src = (\(Right r) -> r) . runSourcePos . run ttokens
  where run p = runParserT p () src
        runSourcePos = flip evalState (initialPos src)
