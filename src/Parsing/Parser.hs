module Parsing.Parser (
  parseFromString'
  , parseFromString
  , parseModule
  , expr
  , declaration
  , stmt
  , term
  , pat
  , typeExpr
  , typeTerm
  , context
  , parseExp
  , parseDecl
  , parseType
  , parsePat
  , parseQOp
  , unknownSrcLoc
  , unknownSrcName
  , parseImport
  , parseDecls
  , SimpleModule
  , module S
  , module Language.Haskell.Exts.Pretty
  ) where

-------------
-- IMPORTS --
-------------

-- own modules
import           Common.Applicative           ((<$>>))
import           Parsing.Indent
import           Parsing.Lexer
import           Parsing.TokenParser


-- src exts
import qualified Language.Haskell.Exts.Fixity as F
import           Language.Haskell.Exts.Syntax as S
import           Language.Haskell.Exts.Lexer  hiding (lexTokenStream)
import           Language.Haskell.Exts.Pretty

-- utilities
import           Data.Function
import           Data.List
import           Control.Monad.State
import           Data.Functor
import           Control.Applicative          ((<*))
import           Data.Either.Unwrap
import           Data.Maybe
import           Control.Arrow                hiding (app)

-- parsec
import           Text.Parsec                  hiding (State)
import           Text.Parsec.Expr             as E


f = parseModule "example" <$> readFile d
    where d = "src/Tests/Data/Parsing.hs"

----------
-- MAIN --
----------

type SimpleModule = (Maybe SourceName, [ImportDecl], [Decl])

-- |Parses something from a 'String'
parseFromString :: MParser a -- ^The parser
                   -> String -- ^The string that will be parsed
                   -> Either ParseError a -- ^Parse result
parseFromString p s = runIndentParser p unknownSrcName $ lexTokenStream unknownSrcName s

unknownSrcLoc :: SrcLoc
unknownSrcLoc = SrcLoc unknownSrcName 1 1

unknownSrcName :: String
unknownSrcName = "<unknown>.hs"

-- |Parses something from a 'String'
-- it is an unsafe version of 'parseFromString'
parseFromString' :: MParser a -- ^The parser
                   -> String -- ^The string that will be parsed
                   -> a -- ^Parse result
parseFromString' p = fromRight . parseFromString p

-- |Parses a haskell module
parseModule :: SourceName -- ^Name of the file
               -> String -- ^Contents of the file, the actual code
               -> Either ParseError SimpleModule -- ^Either a parse error or a list of top level declarations
parseModule src input =
  right (\(b,c) -> (Just src, b,c)) $ runIndentParser modulep src $ lexTokenStream src input

parseExp :: String -> Exp
parseExp = parseFromString' expr

parseImport :: String -> ImportDecl
parseImport = parseFromString' importDecl

parseDecls :: String -> [Decl]
parseDecls = snd <$> parseFromString' modulep

parsePat :: String -> Pat
parsePat = parseFromString' pat

parseType :: String -> Type
parseType = parseFromString' typeExpr

parseDecl :: String -> Decl
parseDecl = parseFromString' declaration

parseQOp :: String -> QOp
parseQOp = parseFromString' qOp

------------
-- LAYOUT --
------------
curliesSemiOrIndent1 :: MParser a -> MParser [a]
curliesSemiOrIndent1 p = curlies (withoutLayout  $ semiSepEndBy1 p) <|>
                         withLayout (semiblock p)

semiblock :: MParser a -> MParser [a]
semiblock p = withRef (many1 $ checkIndentRef >> semiSepEndBy1 p) <$>> concat


-----------------
-- DECLARATION --
-----------------

modulep :: MParser ([ImportDecl], [Decl])
modulep =  do
  optional modulePreface
  imps <-imports
  ds <- (eof $> []) <|> (curliesSemiOrIndent1 declaration  <* eof)
  return (imps, ds)
  where modulePreface = do
          kwmodule
          void qConId <|> void conId
          optional $ parens (commaSep varId)
          kwwhere
        imports = many importDecl

-- INCOMPLETE DEFINITION!!
-- note: KW_As is lexed as a VarId
importDecl :: MParser ImportDecl
importDecl = do
  l <- getNextLoc
  kwimport
  qual <- isJust <$> optionMaybe kwqualified
  (path, name) <- qConId
  specs <- optionMaybe (parens (commaSep varId))
  asModName <- fmap ModuleName <$>
               optionMaybe (kwas >> asModuleName)
  return ImportDecl {
    importLoc = l
    , importModule = ModuleName (path++ "." ++name)
    , importSrc = False
    , importQualified = qual
    , importSafe = False
    , importPkg = Nothing
    , importAs = asModName
    , importSpecs = case specs of
    Nothing -> Nothing
    Just x -> Just (False, [IVar (Ident s) | s <- x])
    }
    where
      asModuleName = try conId <|>
                     qConId <$>> (\(a,b) -> a++"."++b)

-- |Parses a declaration
declaration :: MParser Decl
declaration =
  newtypeDecl <|>
  dataDecl <|>
  typeDecl <|>
  try patBind <|>
  try typeSig <|>
  funBind

typeDecl :: MParser Decl
typeDecl = do
  l <- getNextLoc
  kwtype
  (c, vs) <- simpletype
  equals
  t <- typeExpr
  return $ TypeDecl l c vs t

dataDecl :: MParser Decl
dataDecl = do
  l <- getNextLoc
  kwdata
  (c, vs) <- simpletype
  equals
  cons <- sepBy1 dconstr bar
  der <- option [] dderiving
  return $ DataDecl l DataType [] c vs cons der

dconstr :: MParser QualConDecl
dconstr = do
  l <- getNextLoc
  c <- conId <$>> Ident
  ts <- many typeTerm
  return $ QualConDecl l [] [] (ConDecl c ts)

newtypeDecl :: MParser Decl
newtypeDecl = do
  l <- getNextLoc
  kwnewtype
  (c, vs) <- simpletype
  equals
  cons <- dconstr
  der <- option [] dderiving
  return $ DataDecl l NewType [] c vs [cons] der

dderiving :: MParser [Deriving]
dderiving = do
  kwderiving
  single <|> multi
  where
    single = conId <$>> \c -> [(UnQual $ Ident c, [])]
    multi = parens . commaSep $ do
      c <- conId
      ts <- many typeTerm
      return (UnQual $ Ident c, ts)

simpletype :: MParser (Name, [TyVarBind])
simpletype = do
  c <- conId
  vs <- many varId
  return (Ident c, map (UnkindedVar . Ident) vs)

patBind :: MParser Decl
patBind = withLeft $ do
  l <- getNextLoc
  p <- pat
  r <- rhs equals
  b <- optWhere
  return $ PatBind l p r b


optWhere :: MParser (Maybe Binds)
optWhere = optionMaybe whereBinds
  where whereBinds = kwwhere >> explBinds1

funBind :: MParser Decl
funBind = matches <$>> FunBind

explBinds1 :: MParser Binds
explBinds1 = curliesSemiOrIndent1 declaration <$>> BDecls


typeSig :: MParser Decl
typeSig = withLeft $ do
  l <- getNextLoc
  names <- commaSep1 varId
  t <- doublecolon >> (try tyforall <|> typeExpr)
  return $ TypeSig l (map Ident names) t

-----------
-- MATCH --
-----------

matches :: MParser [Match]
matches = do
  (on,_,_) <- get
  m@(Match _ (Ident name) _ _ _ _) <- firstMatch
  if on
    then do
      -- first line
      mm <- option [] (try $ semi >> semiSepEndBy (match name)) <$>> (m:)
      -- rest
      option [] (semiblock (match name)) <$>> (mm++)
    else try (semi >> semiSepEndBy1 (match name)) <$>> (m:)


firstMatch :: MParser Match
firstMatch = withLeft $ do
  loc <- getNextLoc
  name <- varId
  (pats, r, b) <- matchArgsRhsWhere
  return $ Match loc (Ident name) pats Nothing r b


match :: String -> MParser Match
match name =  do
  loc <- getNextLoc
  thisVarId name
  (pats, r, b) <- matchArgsRhsWhere
  return $ Match loc (Ident name) pats Nothing r b

matchArgsRhsWhere :: MParser ([Pat], Rhs, Maybe Binds)
matchArgsRhsWhere = do
  pats <- many1 pat
  r <- rhs equals
  b <- optWhere
  return (pats, r, b)


---------
-- RHS --
---------

rhs :: MParser eq -> MParser Rhs
rhs eq = unguarded  <|>
         guarded
  where
  unguarded :: MParser Rhs
  unguarded = (eq >> expr) <$>> UnGuardedRhs

  guarded :: MParser Rhs
  guarded = many1 rhsguard <$>> GuardedRhss

  rhsguard :: MParser GuardedRhs
  rhsguard = do
    l <- getNextLoc
    ss <- bar >> commaSep1 stmt
    e <- withLeft eq >> expr
    return $ GuardedRhs l ss e

  -- rhsguard :: MParser GuardedRhs
  -- rhsguard = do
  --   l <- getNextLoc
  --   ss <- bar >> commaSep1 stmt
  --   e <- eq >> expr
  --   return $ GuardedRhs l ss e

----------
-- STMT --
----------
stmt :: MParser Stmt
stmt = try generator <|>
       try qualifier <|> --qualifier must go before letStmt
       letStmt

qualifier :: MParser Stmt
qualifier = expr <$>> Qualifier

letStmt :: MParser Stmt
letStmt =  (kwlet >> explBinds1) <$>> LetStmt



generator :: MParser Stmt
generator = do
  l <- getNextLoc
  p <- pat
  leftarrow
  e <- expr
  return $ Generator l p e


-----------------
-- SPECIAL CON --
-----------------
listcon :: MParser Type
listcon = leftsquare >> rightsquare $> (TyCon $ Special ListCon)

unitcon :: MParser QName
unitcon = leftparen >> rightparen $> Special UnitCon

funcon :: MParser Type
funcon = parens rightarrow $> (TyCon $ Special FunCon)

tuplecon :: MParser QName
tuplecon = parens (many1 comma) <$>> (Special .TupleCon Boxed.(+1).length)


----------------
-- EXPRESSION --
----------------

expr' :: MParser Exp
expr' = buildExpressionParser expTable term

-- |Parses an expression
expr :: MParser Exp
expr = do
  l <- getNextLoc
  e <- expr'
  mb <- optionMaybe (doublecolon >> typeExpr)
  return $ case mb of
    Just b -> ExpTypeSig l e b
    _ -> e


-- |Parses a single term
term :: MParser Exp
term =
  try (parens expr) <$>> Paren <|>
  try tuple <|>
  try leftSection <|>
  try rightSection <|>
  try enum <|>
  try listComp <|>
  var <|>
  caseOf <|>
  doExp <|>
  constructor <|>
  list <|>
  (literal <$>> Lit)  <|>
  lambda <|>
  try letExp  <|>
  ifElse


var :: MParser Exp
var = varId <$>> (Var . UnQual . Ident) <|>
      try parSym

parSym :: MParser Exp
parSym = do t <- parens anyOp
            return $ case t of
              Minus -> Var . UnQual . Symbol $ "-"
              (VarSym s) -> Var . UnQual . Symbol $ s
              Colon -> Con . Special $ Cons


leftSection :: MParser Exp
leftSection =  parens $ do
  e <- expr
  qop <- qOp
  return $ LeftSection e qop

qOp :: MParser QOp
qOp = do
  op <- anyOp <|> backquotes varId <$>> VarId
  return $ case op of
            Minus -> QVarOp . UnQual . Symbol $ "-"
            VarSym s -> QVarOp . UnQual . Symbol $ s
            Colon -> QConOp $ Special Cons
            VarId s -> QVarOp . UnQual . Ident $ s

rightSection :: MParser Exp
rightSection = parens $ do
  qop <- qOp
  e <- expr
  return $ RightSection qop e


ifElse :: MParser Exp
ifElse = do
  kwif
  i <- expr
  optional semi >> kwthen
  t <- expr
  optional semi >> kwelse
  e <- expr
  return $ If i t e

lambda :: MParser Exp
lambda = do
  l <- getNextLoc
  ps <- backslash >> many1 pat
  e <- rightarrow >> expr
  return $ Lambda l ps e

doExp :: MParser Exp
doExp = kwdo >> curliesSemiOrIndent1 (withLeft stmt) <$>> Do

tuple :: MParser Exp
tuple = parens (commaSep2 expr) <$>> Tuple Boxed

list :: MParser Exp
list = brackets (commaSep expr) <$>> List

letExp :: MParser Exp
letExp = do
  kwlet
  bnds <- (kwin >> return (BDecls [])) <|> explBinds1 <* kwin --ugly
  e <- expr
  return $ Let bnds e

constructor :: MParser Exp
constructor = regular
              <|> (try unitcon <$>> Con)
              <|> (try tuplecon <$>> Con)
  where regular = conId <$>> (Con . UnQual . Ident)

literal :: MParser Literal
literal = numLiteral
          <|> (stringLit <$>> String)
          <|> (charLit <$>> Char)

numLiteral :: MParser Literal
numLiteral = (integerLit <$>> Int)
             <|> (floatLit <$>> Frac)


--type OperatorInfo = (Operator, Int)



expTable :: [[Operator [PosTok Token] () (State ParserState) Exp]]
expTable = [juxtApp]:[prefixMinus]:infixTable
  where
    prefixMinus = Prefix (do{ minus; return NegApp })
    juxtApp = Infix app E.AssocLeft

app :: MParser (Exp -> Exp -> Exp)
app = lookAhead term $> App

infixTable  :: [[Operator [PosTok Token] () (State ParserState) Exp]]
infixTable = (\(x:xs) -> (defaultInfix:x):xs) . reverse . map (map fst) . groupBy eq2snd . sortBy cmp2snd .  map toOperatorInfo $ F.preludeFixities
        where cmp2snd = compare `on` snd
              eq2snd = (==) `on` snd
              transAssoc S.AssocNone = E.AssocNone
              transAssoc S.AssocRight = E.AssocRight
              transAssoc S.AssocLeft = E.AssocLeft
              toOperatorInfo (F.Fixity assoc i (S.UnQual (S.Symbol name@"-"))) =
                (Infix (try$ minus>>notFollowedBy rightparen>>return (inf name)) $ transAssoc assoc, i)
              toOperatorInfo (F.Fixity assoc i (S.UnQual (S.Symbol ":"))) =
                (Infix (try$ colon>>notFollowedBy rightparen>>return infcol)$ transAssoc assoc, i)
              toOperatorInfo (F.Fixity assoc i (S.UnQual (S.Symbol name))) =
                (Infix (try$ thisVarSym name>>notFollowedBy rightparen>>return (inf name))$
                 transAssoc assoc, i)
              toOperatorInfo (F.Fixity assoc i (S.UnQual (S.Ident name))) =
                 (Infix (try$backquotes(thisVarId name)>>notFollowedBy rightparen>>return (infid name))
                  $ transAssoc assoc, i)
              infid name a = InfixApp a (QVarOp . UnQual $ Ident name)
              inf name a = InfixApp a (QVarOp . UnQual $ Symbol name)
              infcol a = InfixApp a (QConOp (Special Cons))
              defaultInfix :: Operator [PosTok Token] () (State ParserState) Exp
              defaultInfix = Infix (try $ do
                                       name <- backquotes varId
                                       notFollowedBy rightparen
                                       return (infid name)
                                   ) E.AssocLeft

listComp :: MParser Exp
listComp = brackets $ do
  e <- expr
  bar
  ss <- commaSep1 stmt
  return $ ListComp e (map QualStmt ss)

enum :: MParser Exp
enum = brackets $ do
  from <- expr
  mc <- optionMaybe comma
  case mc of
   Nothing -> do
     mto <- dotdot >> optionMaybe expr
     case mto of
      Just to -> return $ EnumFromTo from to
      _ -> return $ EnumFrom from
   Just _ -> do
     thenn <- expr
     mto <- dotdot >> optionMaybe expr
     case mto of
      Just to -> return $ EnumFromThenTo from thenn to
      _ -> return $ EnumFromThen from thenn

----------
-- CASE --
----------

caseOf :: MParser Exp
caseOf = do
  e <- kwcase >> expr
  kwof
  alts <- curliesSemiOrIndent1 caseAlt
  return $ Case e alts

caseAlt :: MParser Alt
caseAlt = do
  l <- getNextLoc
  p <- pat
  r <- withLeft $ rhs rightarrow
  b <- optWhere
  return $ Alt l p r b


-------------
-- PATTERN --
-------------

-- |Parses a pattern
pat :: MParser Pat
pat = do
  l <- lpat
  m <- optionMaybe colon
  case m of
   Just _ -> do
     r <- pat
     return $ PInfixApp l (Special Cons) r
   _ -> return l

lpat :: MParser Pat
lpat = apat <|>
       pnegativeLit <|>
       try pconstructor1

apat :: MParser Pat
apat = pvar <|>
       try pconstructor0 <|>
       (literal <$>> PLit Signless) <|>
       pwildcard <|>
       try pparen <|>
       ptuple <|>
       plist

pnegativeLit :: MParser Pat
pnegativeLit = do
  m <- optionMaybe minus
  l <- numLiteral
  return $ case m of
   Just _ -> PLit Negative l
   Nothing -> PLit Signless l

pparen :: MParser Pat
pparen = parens pat <$>> PParen

plist :: MParser Pat
plist = brackets (commaSep pat) <$>> PList

ptuple :: MParser Pat
ptuple = parens (commaSep2 pat) <$>> PTuple Boxed

pconstructor1 :: MParser Pat
pconstructor1 = do
  con <- conId
  ps <- many1 apat
  return $ PApp (UnQual (Ident con)) ps

pconstructor0 :: MParser Pat
pconstructor0 = do
  con <- conId
  notFollowedBy apat
  return $ PApp (UnQual (Ident con)) []

pvar :: MParser Pat
pvar = do
       vid <- varId
       mat <- optionMaybe at
       case mat of
        Nothing -> return . PVar . Ident $ vid
        Just _ -> do
          t <- apat
          return $ PAsPat (Ident vid) t

pwildcard :: MParser Pat
pwildcard = underscore $> PWildCard


----------
-- TYPE --
----------

tyvar :: MParser Type
tyvar = varId <$>> TyVar . Ident

typeTable :: [[Operator [PosTok Token] () (State ParserState) Type]]
typeTable = [[Infix (notFollowedBy rightarrow $> TyApp) E.AssocLeft],
             [Infix (rightarrow $> TyFun) E.AssocRight]]

typeConstructor :: MParser Type
typeConstructor = try unitcon <$>> TyCon <|>
           try listcon <|>
           try funcon  <|>
           try tuplecon <$>> TyCon <|>
           conId <$>> TyCon . UnQual . Ident

-- |Parses a single type term
typeTerm :: MParser Type
typeTerm = typeConstructor <|>
           tyvar <|>
           tylist <|>
           try (parens typeExpr) <$>> TyParen <|>
           try (parens $ commaSep2 typeExpr) <$>> TyTuple Boxed
  where
    tylist = brackets typeExpr <$>> TyList

typeExpr :: MParser Type
typeExpr =  buildExpressionParser typeTable typeTerm

-------------
-- CONTEXT --
-------------
tyforall :: MParser Type
tyforall = do
  ctx <- context
  doublearrow
  t <- typeExpr
  return $ TyForall Nothing ctx t

-- |Parses a context of a type signature
context :: MParser Context
context = cclass <$>> pure <|>
          try (parens cclass) <$>> (pure . ParenA) <|>
          parens (commaSep cclass)

-- | class in a context (not a class declaration)
cclass :: MParser Asst
cclass = try single <|>
         multi
  where single = do
          c <- conId
          v <- varId
          return $ ClassA (UnQual $ Ident c) [TyVar $ Ident v]
        multi = do
          c <- conId
          (v, ts) <- parens (do
                                 v <- tyvar
                                 ts <- many1 typeTerm
                                 return (v, ts))
          return $ ClassA (UnQual $ Ident c) [TyParen $ foldl TyApp v ts]
