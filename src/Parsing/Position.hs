module Parsing.Position where

import Control.Concatenative
import Language.Haskell.Exts.SrcLoc as SL
import Text.Parsec.Pos

fromLoc :: SrcLoc -> SourcePos
fromLoc = tri srcFilename srcLine srcColumn newPos


fromLocs :: [Loc t] -> [(SourcePos, t)]
fromLocs = map $ \x -> (spanStartPos $ loc x, unLoc x)

fromPos :: SourcePos -> SrcLoc
fromPos pos = SrcLoc {
  srcFilename = sourceName pos,
  srcLine = sourceLine pos,
  srcColumn = sourceColumn pos
  }


spanStartPos :: SL.SrcSpan -> SourcePos
spanStartPos s = let (l, c) = SL.srcSpanStart s
                 in newPos (SL.srcSpanFilename s) l c                            
