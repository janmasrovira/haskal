module Parsing.TokenParser where


import Control.Monad
import Control.Monad.State
import Language.Haskell.Exts.Lexer
import Text.Parsec                 hiding (State)

--Own
import Parsing.Indent
import Common.Applicative


type PosTok a = (SourcePos, a)
type MParser r = ParsecT [PosTok Token] () (State ParserState) r

---------------
-- PRIMITIVE --
---------------
satisfyT :: (Token -> Bool) -> MParser Token
satisfyT cond = sameOrIndentedLeft >> satisfyT' cond

satisfyT' :: (Token -> Bool) -> MParser Token
satisfyT' cond = tokenPrim showTok nextPos testTok
    where
      showTok = show
      testTok (_, t) = if cond t then Just t else Nothing
      nextPos p _ _ = p


tokenT :: Token -> MParser Token
tokenT c = satisfyT (==c) <?> show [c]

-------------------
-- SIMPLE TOKENS --
-------------------

comma :: MParser Token
comma = tokenT Comma

dot :: MParser Token
dot = tokenT Dot

colon :: MParser Token
colon = tokenT Colon

doublecolon :: MParser Token
doublecolon = tokenT DoubleColon

semi :: MParser Token
semi = tokenT SemiColon

equals :: MParser Token
equals = tokenT Equals

dotdot :: MParser Token
dotdot = tokenT DotDot

underscore :: MParser Token
underscore = tokenT Underscore 

bar :: MParser Token
bar = tokenT Bar

at :: MParser Token
at = tokenT At

backslash :: MParser Token
backslash = tokenT Backslash

minus ::MParser Token
minus = tokenT Minus

rightarrow :: MParser Token
rightarrow = tokenT RightArrow

leftarrow :: MParser Token
leftarrow = tokenT LeftArrow

doublearrow :: MParser Token
doublearrow = tokenT DoubleArrow

kwlet :: MParser Token
kwlet = tokenT KW_Let

kwqualified :: MParser Token
kwqualified = tokenT KW_Qualified

kwas :: MParser Token
kwas = thisVarId "as" <$>> VarId -- tokenT KW_As

kwin :: MParser Token
kwin = tokenT KW_In

kwwhere :: MParser Token
kwwhere = tokenT KW_Where

kwimport :: MParser Token
kwimport = tokenT KW_Import

kwmodule :: MParser Token
kwmodule = tokenT KW_Module

kwif :: MParser Token
kwif = tokenT KW_If

kwthen :: MParser Token
kwthen = tokenT KW_Then

kwelse :: MParser Token
kwelse = tokenT KW_Else

kwcase :: MParser Token
kwcase = tokenT KW_Case

kwof :: MParser Token
kwof = tokenT KW_Of

kwdo :: MParser Token
kwdo = tokenT KW_Do

kwtype :: MParser Token
kwtype = tokenT KW_Type

kwnewtype :: MParser Token
kwnewtype = tokenT KW_NewType

kwdata :: MParser Token
kwdata = tokenT KW_Data

kwderiving :: MParser Token
kwderiving = tokenT KW_Deriving

leftparen :: MParser Token
leftparen = tokenT LeftParen

rightparen :: MParser Token
rightparen = tokenT RightParen

leftsquare :: MParser Token
leftsquare = tokenT LeftSquare

rightsquare :: MParser Token
rightsquare = tokenT RightSquare

leftcurly :: MParser Token
leftcurly = tokenT LeftCurly

rightcurly :: MParser Token
rightcurly = tokenT RightCurly

backquote :: MParser Token
backquote = tokenT BackQuote

----------------
-- COMBINATORS --
-----------------
parens :: MParser a -> MParser a
parens = between leftparen rightparen

brackets :: MParser a -> MParser a
brackets = between leftsquare rightsquare

curlies :: MParser a -> MParser a
curlies = between leftcurly rightcurly

backquotes :: MParser a -> MParser a
backquotes = between backquote backquote

commaSep :: MParser a -> MParser [a]
commaSep p = sepBy p comma

dotSep1 p = sepBy1 p dot

commaSep1 :: MParser a -> MParser [a]
commaSep1 p = sepBy1 p comma

semiSep :: MParser a -> MParser [a]
semiSep p = sepBy p semi

semiSep1 :: MParser a -> MParser [a]
semiSep1 p = sepBy1 p semi

semiSepEndBy :: MParser a -> MParser [a]
semiSepEndBy p = sepEndBy p semi

semiSepEndBy1 :: MParser a -> MParser [a]
semiSepEndBy1 p = sepEndBy1 p semi

sepBy2 :: MParser a -> MParser sep -> MParser [a]
sepBy2 p sep = do
  x <- p
  sep
  y <- p
  xs <- many (sep >> p)
  return (x:y:xs)

commaSep2 :: MParser a -> MParser [a]
commaSep2 p = sepBy2 p comma
                        
--------------------------
-- PARAMETERIZED TOKENS --
--------------------------
varId :: MParser String
varId = liftM (\(VarId s) -> s) $ satisfyT isVarId
  where isVarId (VarId _) = True
        isVarId _ = False

thisVarId :: String -> MParser String
thisVarId t = liftM (\(VarId s) -> s) $ satisfyT' (== VarId t)

conId :: MParser String
conId = liftM (\(ConId s) -> s) $ satisfyT isConId
  where isConId (ConId _) = True
        isConId _ = False

qConId :: MParser (String, String)
qConId = liftM (\(QConId s) -> s) $ satisfyT isQConId
 where isQConId (QConId _) = True
       isQConId _ = False

varSym  :: MParser String
varSym = liftM (\(VarSym s) -> s) $ satisfyT isVarSym
  where isVarSym (VarSym _) = True
        isVarSym _ = False

thisVarSym :: String -> MParser String
thisVarSym t = liftM (\(VarSym s) -> s) $ satisfyT' (== VarSym t)

integerLit :: MParser Integer
integerLit = liftM (\(IntTok (i,_)) -> i) $ satisfyT isInt
  where isInt (IntTok _) = True
        isInt _ = False

stringLit :: MParser String
stringLit = liftM (\(StringTok (s,_)) -> s) $ satisfyT isString
  where isString (StringTok _) = True
        isString _ = False

charLit :: MParser Char
charLit = liftM (\(Character (c,_)) -> c) $ satisfyT isChar
  where isChar (Character _) = True
        isChar _ = False


floatLit :: MParser Rational
floatLit = liftM (\(FloatTok (r,_)) -> r) $ satisfyT isFloat
  where isFloat (FloatTok _) = True
        isFloat _ = False


---------------
-- WILDCARDS --
---------------

anyOp :: MParser Token
anyOp = satisfyT isOp
  where isOp Minus = True
        isOp (VarSym _) = True
        isOp Colon = True
        isOp _ = False
        
