module Analysis.ConstraintsSpec where

import Analysis.Constraints
import Analysis.Queries
import Analysis.Recursion
import Test.Hspec
import Common.List


t = hspec spec

spec :: Spec
spec = describe "constraints" $ do
  let file = "test/data/Constraints.hs"
  it "tells whether a banned functions is used" $ do
    g <- parseDeclGraph file
    checkConstraint g r1 `shouldSatisfy` lengthEQ 1
    checkConstraint g r2 `shouldSatisfy` lengthEQ 2
    checkConstraint g r3 `shouldSatisfy` lengthEQ 0
  it "tells whether a module uses banned imports" $ do
    g <- parseDeclGraph file
    checkConstraint g r4 `shouldSatisfy` lengthEQ 4
    checkConstraint g r5 `shouldSatisfy` lengthEQ 1

r1 = DeclConstraint {
  _description = BanInfiniteEnum
  , _scope = ForFunctions ["usesEnum1"]
  }

r2 = DeclConstraint {
  _description = BanInfiniteEnum
  , _scope = ForFunctions ["usesEnum2","usesEnum1"]
  }

r3 = DeclConstraint {
  _description = BanInfiniteEnum
  , _scope = ForFunctions ["noUsesInfEnum"]
  }

r4 = None

r5 = OnlyMatching "Data.Map*"
