module Analysis.QueriesSpec where

import           Analysis.Helper
import           Analysis.Queries
import           Analysis.Recursion
import           Common.List
import           Control.Lens
import qualified Data.IntMap        as IntMap
import qualified Data.IntSet        as IntSet
import           Data.List
import qualified Data.Map           as Map
import           Data.Maybe
import           Parsing.Parser
import           Test.Hspec

t = hspec spec

spec :: Spec
spec =
  describe "parseDeclGraph" $ do
    let file = "test/data/Queries.hs"
    it "tells whether a names is self-recursively defined" $ do
      g <- parseDeclGraph file
      nameIsSelfRecursive g (Ident "selfRecur") `shouldBe` True
      nameIsSelfRecursive g (Ident "nonDirectSelfRecur") `shouldBe` True
    it "tells whether a names is self-recursively defined" $ do
      g <- parseDeclGraph file
      nameIsSelfRecursive g (Ident "recur") `shouldBe` False
    it "tells whether a names is self-recursively defined" $ do
      g <- parseDeclGraph file
      nameIsSelfRecursive g (Ident "selfRecur") `shouldBe` True
    it "tells whether a names uses recursion in its definition" $ do
      g <- parseDeclGraph file
      nameIsRecursive g (Ident "selfRecur") `shouldBe` True
      nameIsRecursive g (Ident "recur") `shouldBe` True
      nameIsRecursive g (Ident "nonRecur") `shouldBe` False
    it "tells wheter a functions match is selfRecursive" $ do
      g <- parseDeclGraph file
      let d = mbGetNameDecl g (Ident "selfRecur")
      d `shouldSatisfy` isJust
      let (base, recu) = partition (matchIsBase g) (fromJust $ funBindMatches =<< d)
      pendingWith "for now, only checks direct recursion"
      --lengthEQ 3 base `shouldBe` True
    it "returns every expression reachable from a node declaration" $ do
      --reachableExprsFromNode
      pendingWith "test not implemented yet"
