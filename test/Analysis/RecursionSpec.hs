module Analysis.RecursionSpec where

import Analysis.Recursion
import Common.List
import Control.Lens
import Data.IntMap        as IntMap
import Data.IntSet        as IntSet
import Data.Map           as Map
import Parsing.Parser
import Test.Hspec

t = hspec spec

spec :: Spec
spec =
  describe "parseDeclGraph" $ do
    let file = "test/data/Recursion.hs"
    it "generates a declGraph (1)" $ do
      g <- parseDeclGraph file
      lengthEQ 2 (g ^. topDecls) `shouldBe` True
      IntSet.size (g ^. knownSigs) `shouldBe` 2
    it "generates a declGraph (2)" $ do
      g <- parseDeclGraph file
      let numNodes = 4
      Map.size (g ^. name2nodeMap) `shouldBe` numNodes
      IntMap.size (g ^. node2nameMap) `shouldBe` numNodes
      lengthEQ 2 (g ^. imports) `shouldBe` True
      IntSet.size (g ^. nodesWithDecl) `shouldBe` 3
