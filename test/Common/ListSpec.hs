module Common.ListSpec where

import Test.Hspec
import Common.List
import Data.List

spec :: Spec
spec = do
  describe "enumerate" $
    it "enumerates a list starting from zero" $
      enumerate "abc" `shouldBe` [(0, 'a'), (1, 'b'), (2, 'c')]
  describe "enumerate1" $
    it "enumerates a list starting from one" $
      enumerate1 "abc" `shouldBe` [(1, 'a'), (2, 'b'), (3, 'c')]
  describe "splits" $
    it "returns every split of a list" $
    splits [0..2] `shouldBe` [([], [0..2]),([0],[1,2]),([0,1],[2]),([0..2],[])]
  describe "lengthEQ l" $
    it "returns True when the list has exactly length l" $
    lengthEQ 3 [undefined, undefined, 0] `shouldBe` True
  describe "count" $
    it "returns a list [(key, key count)]" $
    sort (count [1,3,3,2,3,1,3]) `shouldBe` sort [(1,2),(2,1),(3,4)]
