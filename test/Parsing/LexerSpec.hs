module Parsing.LexerSpec where

import qualified Parsing.ExtParser         as E
import           Parsing.Lexer
import           Test.Hspec


spec :: Spec
spec =
  describe "lexTokenStream" $
    it "tokenizes a string" $ do
    let str = "abc _a . = | where Just qualified import \n" ++
              "let in do $ ++ case of if then else\n" ++
              ":: => (Int -> Int) {123;123.45 [()]}"
        file = "file"
      in lexTokenStream file str `shouldBe` E.lexTokenStream file str 
    let str = "as"
        file = "file"
      in lexTokenStream file str `shouldBe` [(newPos file 1 1, VarId "as")]
    let e = "1.0001" in lexTokenStream "" e `shouldBe` E.lexTokenStream "" e

t = hspec spec
