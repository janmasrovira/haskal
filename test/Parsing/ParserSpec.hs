module Parsing.ParserSpec where

import qualified Parsing.ExtParser as E
import           Parsing.Parser
import           Test.Hspec

spec :: Spec
spec = do
  describe "parseExo" $
    it "parses an expression from a string" $ do
      let e = "var" in parseExp e `shouldBe` E.parseExp e
      let e = "if True then 1 else 0" in parseExp e `shouldBe` E.parseExp e
      let e = "case x of \n" ++
              "  Just 'a' -> 1\n" ++
              "  Nothing -> 0" in parseExp e `shouldBe` E.parseExp e
      let e = "let x = 12 in x + 12" in parseExp e `shouldBe` E.parseExp e
      let e = "\\x -> x + 1" in parseExp e `shouldBe` E.parseExp e
      let e = "1.00001" in parseExp e `shouldBe` E.parseExp e
      let e = "read \"15\" :: Int" in parseExp e `shouldBe` E.parseExp e
      let e = "(((((((1+))))))) . (+1.00001) . (1.32 -) $ 1 + \n" ++
              "2*3 - 4 + (((((((12))))))) `max` 45" in parseExp e `shouldBe` E.parseExp e
      
  describe "parseDecl" $
    it "parses a declaration from a string" $ do
      let check d = parseDecl d `shouldBe` E.parseDecl d
      let d = "f = 12" in check d
      let d = "a@(x:xs:_) = [1,2]" in check d
      let d = "f (x:xs) (Just 3) = x + 3 y where y = let z = 12 in z" in check d
      let d = "typesig :: (Ord a, Num a) => a -> Maybe " in check d
      let d = "typesig :: (Ord a) => a -> Maybe " in check d
      let d = "typesig :: Ord a => a -> Maybe " in check d
      let d = "type Int2 = (Int, Int)" in check d
      let d = "data ABC = A Int | B (Int, [Char])\n" ++
              "  | C (Int -> Int)" in check d
      let d = "newtype Word = Word String" in check d
  describe "parseImport" $
    it "parses an import declaration from a string" $
      let d = "import Data.List as D" in parseImport d `shouldSatisfy`
                    (\i -> importModule i == ModuleName "Data.List" &&
                    importAs i == Just (ModuleName "D"))
      
  describe "parseModule" $
    it "parses a module from a string" $ do
      f <- readFile file
      (\(Right x) -> x) (parseModule file f) `shouldBe` E.parseModule file f
        where file = "test/data/Parsing.hs"

t = hspec spec
