module Tests.Data.Constraints where

import Data.List
import qualified Data.Map.Lazy as L
import qualified Data.Map.Strict as S
import qualified Data.Map as M

usesEnum1 x = f x
  where
    x = map pred [0::Int ..] ++ [1,2..]
    f = undefined

usesEnum2 x = f x
  where
    x = map pred [0::Int ..] ++ [1,2..]
    f = (++ [3,4..])

noUsesInfEnum x = [1,2,3] ++ [2..5] ++ [1,2..9] ++ x

countIfPF :: (a -> Bool) -> [a] -> Int
countIfPF = (length .) . filter

fib :: [Int]
fib = 0 : 1 : zipWith (+) (tail fib) fib
