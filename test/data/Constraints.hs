module Tests.Data.Constraints where

import Data.List
import Data.Map.Lazy as L
import Data.Map.Strict as S
import qualified Data.Map as M

usesEnum1 x = f x
  where
    x = map pred [0::Int ..] ++ [1,2..]
    f = undefined

usesEnum2 x = f x
  where
    x = map pred [0::Int ..] ++ [1,2..]
    f = (++ [3,4..])

noUsesInfEnum x = [1,2,3] ++ [2..5] ++ [1,2..9] ++ x
