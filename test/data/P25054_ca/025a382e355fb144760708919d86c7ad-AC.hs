myLength :: [Int] -> Int
myLength [] = 0
myLength (x:xs) = 1 + (myLength xs)

myMaximum :: [Int] -> Int
myMaximum (x:xs) = (myMaximumAux xs x)

myMaximumAux :: [Int] -> Int -> Int
myMaximumAux [] max = max
myMaximumAux (x:xs) max
  | x > max           = (myMaximumAux xs x)
  | otherwise         = (myMaximumAux xs max)

average :: [Int] -> Float
average l = averageAux l 0 0

averageAux :: [Int] -> Int -> Int -> Float
averageAux [x] s n = s_f/n_f
  where s_f = fromIntegral (s + x)
        n_f = fromIntegral (n + 1)
averageAux (x:xs) s n = averageAux xs (s+x) (n+1)

myConcat [] x = x
myConcat (x:xs) y = x:(myConcat xs y)

myReverse [] = []
myReverse (x:xs) = myConcat (myReverse xs) [x]

buildPalindrome :: [Int] -> [Int]
buildPalindrome x = myConcat (myReverse x) x

contains x [] = False
contains x (e:l)
	| x == e	= True
	| otherwise = contains x l

remove :: [Int] -> [Int] -> [Int]
remove [] y = []
remove (e:l) y
	| contains e y	= remove l y
	| otherwise = e:(remove l y)

flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (l:ls) = myConcat l (flatten ls)

oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens (e:l)
	| (mod e 2) == 0	= (u,(e:v))
	| otherwise	= ((e:u),v)
	where	(u,v) = oddsNevens l

isPrimeAux :: Int -> Int -> Bool
isPrimeAux x y
  | y > sqrtc        = True
  | (mod x y) == 0   = False
  | otherwise        = isPrimeAux x (y + 6)
  where aux1 = fromIntegral x
        sqrtc = (ceiling (sqrt aux1))

isPrime :: Int -> Bool
isPrime x
  | x == 1              = False
  | (x == 2 || x == 3)  = True
  | ((mod x 2) == 0) || ((mod x 3) == 0)     = False
  | otherwise           = (isPrimeAux x 5) && (isPrimeAux x 7)

primeDivisors :: Int -> [Int]
primeDivisors n = [x| x<-2:[3,5..n], isPrime x, (mod n x) == 0]
