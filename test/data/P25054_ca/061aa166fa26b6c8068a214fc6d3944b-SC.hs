
--Ex1--

myLength [] = 0
myLength (x:xs) = 1 + (myLength xs)

--Ex2--

myMax a b
    | a > b = a
    |otherwise = b

myMaximum [] = 0
myMaximum (x:xs)
    | xs == [] = x
    | otherwise = myMax x (myMaximum xs)

--Ex3--

mySum [] = 0
mySum (x:xs) = x + (mySum xs)

average [] = 0
average l = (mySum l)/(myLength l)

--Ex4--

invertAndConcat [] l = l
invertAndConcat (x:xs) l = invertAndConcat xs (x:l)

buildPalindrome l = invertAndConcat l l

--Ex5--

removeElem y [] = []
removeElem y (x:xs)
    | y == x = removeElem y xs
    | otherwise = x:(removeElem y xs)

remove x [] = x
remove [] y = [] --Per eficiencia, realment no es necessari
remove x (y:ys) = remove (removeElem y x) ys

--Ex6--

flatten [] = []
flatten (l:ls) = l ++ (flatten ls)

--Ex7--

oddsNevens [] = ([], [])
oddsNevens (x:xs)
    | (mod x 2) == 0 = (a, x:b)
    | otherwise = (x:a, b)
    where (a, b) = oddsNevens xs

--Ex8--

primes n i
    | n == 1 = []
    | (mod n i) == 0 = i:(primes (div n i) i)
    | otherwise = primes n (i+1)

primeDivisors n = primes n 2