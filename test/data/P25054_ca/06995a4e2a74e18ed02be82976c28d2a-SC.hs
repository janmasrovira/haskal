myLength::[Int] -> Int 
myLength [] = 0
myLength (x:xs) = 1 + myLength xs

myMaximum::[Int] -> Int
myMaximum [x] = x
myMaximum (x:xs)
  | x > y = x
  | otherwise = y
  where y = myMaximum xs
	
average::[Int]->Float 
average [x] = fromIntegral x
average (x:xs) =
  (suma (x:xs)) / (count (x:xs))
  where suma [] = 0.0
	suma (x:xs) = (fromIntegral x) + (suma xs)
	count [] = 0.0
	count (x:xs) = 1.0 + (count xs) --PREGUNTAR SI ES EFICIENT
	
	
	
	
buildPalindrome::[Int]->[Int]
buildPalindrome [] = []
buildPalindrome (x:xs) = rev (x:xs) ++ (x:xs)
  where rev [] = []
	rev (x:xs) = (rev xs) ++ [x]  --PREGUNTAR ALTERNATIVA
	
remove::[Int]->[Int]->[Int]
remove [] [] = []
remove (x:xs) [] = (x:xs)
remove [] (x:xs) = []
remove (x:xs) (y:ys) = remove (elimina (x:xs) y) (ys)
  where elimina [] x = []
	elimina (x:xs) y
	  | (x == y) = elimina (xs) y
	  | otherwise = [x] ++ elimina xs y

	
flatten [] = []
flatten ([]:xs) = [] ++ flatten xs
flatten ((x:xs):ys) = (x:xs) ++ flatten ys

oddsNevens::[Int]->([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens (x:xs)
  | (x `mod` 2 == 0) = ((fst aux),([x] ++ snd aux))
  | otherwise = (([x] ++ fst aux),(snd aux))
  where aux = oddsNevens xs
	
primeDivisors::Int -> [Int]
primeDivisors x
  | x <= 3 = []
  | otherwise = primeDiv x y 2
  where y = x `div` 2
	primeDiv x y z 
	  | (z > y) = []
	  | ((x `mod` z == 0) && (prime z (floor (sqrt (fromIntegral z))))) = [z] ++ primeDiv x y (z+1)
	  | otherwise = primeDiv x y (z+1)
	  
prime z m 
  | m <= 1 = True
  | (z `mod` m == 0) = False
  | otherwise = prime z (m-1)
	      