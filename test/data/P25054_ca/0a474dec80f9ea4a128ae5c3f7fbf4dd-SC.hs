--1
myLength :: [Int] -> Int 
myLength [] = 0
myLength (x:xs) =  1 + myLength xs

--2
myMaximum :: [Int] -> Int
myMaximum (x:xs)
  | xs == [] = x
  | x > max = x
  | otherwise = max
  where max = myMaximum xs
	
--3
average :: [Int] -> Float
average [] = 0
average xs = fromIntegral (mySum xs) / fromIntegral(myLength xs)

mySum :: [Int] -> Int
mySum [] = 0
mySum (x:xs) = x + mySum xs

--4
buildPalindrome :: [Int] -> [Int]
buildPalindrome [] = []
buildPalindrome [x] = [x]
buildPalindrome (x:xs) = buildPalindrome xs ++ [x] ++ xs

 --5
remove :: [Int] -> [Int] -> [Int] 
remove [] ys = []
remove (x:xs) ys 
  | estaYs x ys = zs
  | otherwise = x:zs
    where zs = remove xs ys
	  
estaYs :: Int -> [Int] -> Bool
estaYs n [] = False 
estaYs n (x:xs) 
  | n == x = True
  | otherwise = estaYs n xs


--6
flatten :: [[Int]] -> [Int] 
flatten [] = []
flatten (x:xs) = x ++ flatten xs

--7
oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens [] = ([],[])  
oddsNevens (x:xs)
  | mod x 2 == 0 = (i,x:p)
  | otherwise = (x:i,p)
  where (i,p) = oddsNevens xs  

--8
primeDivisors :: Int -> [Int]
primeDivisors 0 = []
primeDivisors 1 = []
primeDivisors x = divisible_primos x 2

divisible_primos :: Int -> Int -> [Int]
divisible_primos x y
  | y > x = []
  | mod x y == 0 && isPrime y = y:divisible_primos x (y+1)
  | otherwise = divisible_primos x (y+1) 

isPrime :: Int -> Bool
isPrime n 
  | n == 0 = False
  | n == 1 = False
  | otherwise = primer n (n-1)

primer :: Int -> Int -> Bool
primer n i
  | i == 1 = True
  | mod n i == 0 = False
  | otherwise = primer n (i-1)

