
myLength::[Int]->Int
myLength [] = 0
myLength (x:xs) = 1 + myLength xs

myMaximum::[Int]->Int
myMaximum [x] = x
myMaximum (x:xs) = max x (myMaximum xs)

average::[Int]->Float
average xs = (fromIntegral (sum xs)) / (fromIntegral (length xs))

buildPalindrome::[Int]->[Int]
buildPalindrome xs = (reverse xs) ++ xs

remove::[Int]->[Int]->[Int]
remove xs ys = filter (\x-> not (x `elem` ys)) xs

flatten::[[Int]]->[Int]
flatten [] = []
flatten (xs:xss) = [x|x<-xs] ++ flatten xss

oddsNevens::[Int]->([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens (x:xs)
  | odd x     = (x:odds,evens)
  | otherwise = (odds,x:evens)
  where (odds, evens) = oddsNevens xs

primeDivisors::Int->[Int]
primeDivisors n = [d | d <- candidates, n `mod` d == 0] where
  candidates = takeWhile (<=n) $ primes
  primes = 2:filter isPrime [3,5..] where
    isPrime m = null $ [x | x<-primeCands, m `mod` x == 0]
      where primeCands = takeWhile (\y -> y*y<=m) $ primes

