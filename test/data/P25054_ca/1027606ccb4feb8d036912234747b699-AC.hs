myLength::[Int]->Int
myLength [] = 0
myLength (x:l) = (myLength l) +1 

myMaximum::[Int]->Int
myMaximum [x] = x
myMaximum (x:l)
	|x > myMaximum l = x
	|otherwise = myMaximum l

summ::[Int]->Int
summ [] = 0
summ (x:l) = (summ l) + x

average::[Int]->Float
average l =  fromIntegral(summ l) / fromIntegral(myLength l)

buildAux::[Int]->[Int]->[Int]
buildAux [] l = l
buildAux (x:m) l = (buildAux m l)++[x]

buildPalindrome::[Int]->[Int]
buildPalindrome l = buildAux l [] ++l

removeaux::[Int]->Int->[Int]
removeaux [] y = []
removeaux (x:l) y
	|x == y = removeaux l y
	|otherwise  = x:(removeaux l y)

remove::[Int]->[Int]->[Int]
remove [] c = []
remove c [] = c
remove x (c:y) = remove (removeaux x c) y

flatten::[[Int]]->[Int]
flatten (x:l) = x ++ flatten l
flatten [] = []

oddsNevens::[Int]->([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens (c:l)
	|mod c 2 == 0 = (x,c:y)
	|otherwise = (c:x,y)
	where (x,y) = oddsNevens l

divisible::Int->Int->Bool
divisible n m
	|m <= 1 = False
	|mod n m == 0 = True
	|otherwise = divisible n (m-1)

isPrime::Int->Bool
isPrime n
	|n==1 || n==0 = False
	|divisible n (n-1) = False
	|otherwise = True

nextDivisor::Int->Int->Int
nextDivisor n m
	|m == 1 = m 
	|mod n m == 0 = m
	|otherwise = nextDivisor n (m-1)

divm::Int->Int->Int
divm n m 
	|mod n m /=0 = n
	|otherwise  = divm (div n m) m

primeDivisorsaux::Int->Int->[Int]
primeDivisorsaux n m
	|m > n = []
	|mod n m == 0 = m:(primeDivisorsaux (divm n m) (m+1))
	|otherwise = primeDivisorsaux n (m+1)

primeDivisors::Int->[Int]
primeDivisors 0 = []
primeDivisors 1 = []
primeDivisors x = primeDivisorsaux x 2


