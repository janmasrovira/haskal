
-- Tamany d'una llista:
myLength :: [Int] -> Int
myLength []		= 0
myLength (x:xs)	= 1 + (myLength xs)

-- Maxim d'una llista:
myMaximum :: [Int] -> Int
myMaximum [x]		= x 
myMaximum (x:xs) 	= maxim x (myMaximum xs)

maxim:: Int -> Int -> Int
maxim x y
	| x > y 	= x
	| otherwise	= y

-- Mitjana d'una llista:
average :: [Int] -> Float
average []  	= 0
average (x:xs)	= fromIntegral ((sumal (x:xs))) / fromIntegral l
	where l = myLength(x:xs) 

sumal :: [Int] -> Int
sumal []		= 0
sumal (x:xs)	= x + (sumal xs)

 --Crear un palindrom a partir d'una llista:
buildPalindrome :: [Int] -> [Int]
buildPalindrome xs =  (++) (reverso xs) xs

reverso :: [Int] -> [Int]
reverso [a] = [a]
reverso (x:xs) =  (++) (reverso xs) [x]

 -- eliminar els elements d'una llista en un altre:
remove :: [Int] -> [Int] -> [Int]
remove as bs = [1] -------------------------------------------------------TODO: Falta acabar

 --donat una llista de llistes produir només una llista:
flatten :: [[Int]] -> [Int]
flatten [x] = x
flatten (x:xs) = (++) x (flatten xs)

 --donat una llista retorna una llista de parells i una altre de senars:
oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens (x:xs) = ([x],[x]) --------------------------------------------TODO: Falta acabar

 --donat un enter retorni la llista dels divisors primeres positius:
primeDivisors :: Int -> [Int]
primeDivisors n = [4] ----------------------------------------------------TODO: Falta acabar