-- lenght
myLength :: [Int] -> Int

myLength [] = 0
myLength (x:xs) = (myLength xs) + 1

-- maximum
myMaximum :: [Int] -> Int

myMaximum [] = 0
myMaximum [x] = x
myMaximum (x:xs) 
  |x > n = x
  |otherwise = n
  where n = myMaximum xs

-- average
{-
average:: [Int] -> Float
sumar:: [Int] -> Int

sumar [] = 0
sumar [x] = x
sumar (x:xs) = x+sumar xs 

average xs = div (sumar xs) (length xs)
  -}

-- palindrom

buildPalindrome:: [Int] -> [Int]

buildPalindrome [] = []
buildPalindrome xs = reverse xs ++ xs

-- remove

remove::[Int]->[Int]->[Int]
removeuno::[Int]->Int->[Int]

removeuno [] n = []
removeuno (x:xs) n  
  |x == n = removeuno xs n
  |otherwise = x : removeuno xs n

remove xs [] = xs
remove [] ys = []
remove xs (y:ys) =  remove (removeuno xs y) ys

-- flatten 

flatten:: [[Int]]->[Int]

flatten [] = []
flatten (x:xs) = x ++ (flatten xs)

--oddsNevens

oddsNevens::[Int]->([Int],[Int])
odds::[Int]->[Int]
evens::[Int]->[Int]

odds [] = []
odds (x:xs)
  |mod x 2 == 0 = odds xs
  |otherwise = x: (odds xs)
  
evens [] = []
evens (x:xs)
  |mod x 2 == 0 = x: (evens xs)
  |otherwise = evens xs
  
oddsNevens xs = ((odds xs),(evens xs))

-- primeDivisors
{-
primeDivisors::Int->[Int]
isPrime :: Int -> Bool
division :: Int -> Int -> Bool

division n m 
  |m == 1 = True
  |otherwise = if mod n m == 0 then False 
		else division n (m-1)
isPrime n 
  |n < 2 = False
  |otherwise = division n (n-1)
  
primeDivisors n
  |mod n p == 0 && (isPrime p) =  p:(primeDivisors n)
  |otherwise = primeDivisors n
  where p = -}
