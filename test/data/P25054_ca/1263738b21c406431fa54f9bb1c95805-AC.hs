myLength :: [Int] -> Int
myLength [] = 0
myLength (x:xs) = 1 + myLength xs

myMaximum :: [Int] -> Int
myMaximum (x:[]) = x
myMaximum (x:xs) =
    max x (myMaximum xs)

average :: [Int] -> Float
average l = (fromIntegral (suml l)) / (fromIntegral (myLength l))

suml :: [Int] -> Int
suml (x:[]) = x
suml (x:xs) = x + (suml xs)

buildPalindrome :: [Int] -> [Int]
buildPalindrome l = bP l l

bP :: [Int] -> [Int] -> [Int]
bP [] l = l
bP (x:xs) l = bP xs (x:l)

remove :: [Int] -> [Int] -> [Int]
remove xs [] = xs
remove xs (y:ys) = remove (remove1 xs y) ys

remove1 [] _ = []
remove1 (x:xs) y
    | x == y = remove1 xs y
    | otherwise = x:(remove1 xs y)
    
flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (x:xs) = x++(flatten xs)

oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens [] = ([], [])
oddsNevens (x:xs)
    | x `mod` 2 /= 0 = (x:odds, evens)
    | otherwise = (odds, x:evens)
        where (odds, evens) = oddsNevens xs
    
isPrime :: Int -> Bool
isPrime n = length [x | x <- [2..n], n `mod` x == 0] == 1

primeDivisors :: Int -> [Int]
primeDivisors n = [x | x <- 2:[3,5..n], isPrime x, n `mod` x == 0]
    
    
    
    
    
    
    
    
    
