myLength :: [Int] -> Int
myLength [] = 0
myLength (x:xs) = 1 + (myLength xs)

myMaximum :: [Int] -> Int
myMaximum [x] = x
myMaximum (x:xs) = max x (myMaximum xs)

average :: [Int] -> Float
average xs = (fromIntegral (suma xs))/(fromIntegral (myLength xs))

suma :: [Int] -> Int
suma [x] = x
suma (x:xs) = x + suma xs

buildPalindrome :: [Int] -> [Int]
buildPalindrome [] = []
buildPalindrome xs = (invertirLista xs) ++ xs

invertirLista :: [Int] -> [Int]
invertirLista [x] = [x]
invertirLista (x:xs) = (invertirLista xs) ++ [x]

remove :: [Int] -> [Int] -> [Int]
remove xs ys = removeR xs ys (myLength ys) (myLength ys)

removeR :: [Int] -> [Int] -> Int -> Int -> [Int]
removeR xs [] a n = xs
removeR (x:xs) (y:ys) a n
  | (llistaContains ([x] ++ xs) y) && (a > 0) = removeR (creaLlistaSense ([x] ++ xs) y) ([y] ++ ys) (a-1) n
  | (llistaContains ([x] ++ xs) y) && (a == 0) = removeR (creaLlistaSense ([x] ++ xs) y) ([y] ++ ys) (n) n
  | otherwise = removeR ([x] ++ xs) ys a n
  
llistaContains :: [Int] -> Int -> Bool
llistaContains [x] y
  | x == y = True
  | otherwise = False
llistaContains (x:xs) y
  | x == y = True
  | otherwise = llistaContains xs y

  -- creaLlistaSense devuelve la lista de entrada sin el elemento Int, sólo una vez. Si hay más de uno sólo quitará el primero!!!
creaLlistaSense :: [Int] -> Int -> [Int]
creaLlistaSense [x] y
  | x == y = []
  | otherwise = [x]
creaLlistaSense (x:xs) y
  | x == y = xs
  | otherwise = [x] ++ (creaLlistaSense xs y)
  
flatten :: [[Int]] -> [Int]
flatten xs = flat xs []


flat :: [[Int]] -> [Int] -> [Int]
flat [] (ys) = ys
flat (x:xs) (ys) = flat xs (ys ++ x)

oddsNevens :: [Int] -> ([Int], [Int])
oddsNevens lx = odde lx [] []

odde :: [Int] -> [Int] -> [Int] -> ([Int], [Int])
odde [] lpar limp = (limp, lpar)
odde (x:lx) lpar limp
  | mod x 2 == 0 = odde lx (lpar ++ [x]) limp
  | otherwise = odde lx lpar (limp ++ [x])
  
primeDivisors :: Int -> [Int]
primeDivisors n = prime n 2 []

prime :: Int -> Int -> [Int] -> [Int]
prime n x lx
  | x > sqrt n = lx
  | (mod n x == 0) && (isPrime x) = prime n (x+1) (lx ++ [x])
  | otherwise = prime n (x+1) lx
  
isPrime :: Int -> Bool
isPrime x
  | x <= 1 = False
  | otherwise = isPrimeAux x (x-1)

isPrimeAux :: Int -> Int -> Bool
isPrimeAux x n
  {-
  tengo que jaser todos los múltiplos desde n (que vale x-1) hasta 2 (1 siempre lo es), conque uno sea cierto ya no será primo
  -}
  | n == 1 = True
  | x `mod` n == 0 = False
  | otherwise = isPrimeAux x (n-1)
  