myLenght :: [Int] -> Int
myLenght [] = 0
myLenght (x:xs) = 1+(myLenght xs)


myMaximum :: [Int] -> Int
myMaximum [x] = x
myMaximum (x:xs)
    | x>=(myMaximum xs)	= x 
    | otherwise = (myMaximum xs)

    
average :: [Int] -> Float
average [x] = fromIntegral x 
average (x:xs) = (suma (x:xs)) / fromIntegral (myLenght (x:xs))
    where suma [] = 0.0
	  suma (x:xs) = fromIntegral x+(suma xs)


buildPalindrome :: [Int] -> [Int]
buildPalindrome (x:xs) = (cap (x:xs)) ++ (cua (x:xs))
  where cap [x] = [x]
	cap (x:xs) = (cap xs) ++ [x]
	cua [x] = [x]
	cua (x:xs) = [x] ++ (cua xs)


remove :: [Int] -> [Int] -> [Int]
remove [x] (y:ys)
    | x == y	= []
    | otherwise	= [x]
remove (x:xs) (y:ys)
    | existeix x (y:ys)	= remove xs (y:ys)
    | otherwise		= [x] ++ (remove xs (y:ys))
  where
    existeix x [y]
	| x == y	= True
	| otherwise	= False
    existeix x (y:ys)
	| x == y	= True
	| otherwise 	= existeix x ys

	
flatten :: [[Int]] -> [Int]
flatten [x] = x
flatten (x:xs) = x ++ flatten xs 

oddNevens :: [Int] -> ([Int],[Int])
oddNevens (x:xs) = ((parells (x:xs)),(senars (x:xs)))
  where
    parells [x]
	| mod x 2 == 0	= [x]
	| otherwise	= []
    parells (x:xs)
	| mod x 2 == 0	= [x] ++ parells xs
	| otherwise	= parells xs
    senars [x]
	| mod x 2 /= 0	= [x]
	| otherwise	= []
    senars (x:xs)
	| mod x 2 /= 0	= [x] ++ senars xs
	| otherwise	= senars xs




