myLength :: [Int] -> Int
myLength [] = 0
myLength (x:xs) = 1 + (myLength xs)

myMaximum :: [Int] -> Int
myMaximum [x] = x
myMaximum (x:xs) = max x (myMaximum xs)

average :: [Int] -> Float
average xs = (fromIntegral (suma xs))/(fromIntegral (myLength xs))

suma :: [Int] -> Int
suma [x] = x
suma (x:xs) = x + suma xs

buildPalindrome :: [Int] -> [Int]
buildPalindrome [x] = [x]
buildPalindrome xs = (invertirLista xs) ++ xs

invertirLista :: [Int] -> [Int]
invertirLista [] = []
invertirLista [x] = [x]
invertirLista (x:xs) = (invertirLista xs) ++ [x]