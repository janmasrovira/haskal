myLength :: [Int] -> Int
myLength = len 0
  where
    len n [] = n
    len n (_ : xs) = len (n + 1) xs

myMaximum :: [Int] -> Int
myMaximum (x : xs) = foldl max x xs
    
average :: [Int] -> Float
average l = fromIntegral (sum l) / fromIntegral (length l)

buildPalindrome :: [Int] -> [Int]
buildPalindrome l = foldl (flip (:)) l l

remove :: [Int] -> [Int] -> [Int]
remove xs ys = filter (\x -> not (x `elem` ys)) xs

flatten :: [[Int]] -> [Int]
flatten = foldr (++) []

oddsNevens :: [Int] -> ([Int], [Int])
oddsNevens xs = (filter odd xs, filter (not . odd) xs)

primeDivisors :: Int -> [Int]
primeDivisors = reverse . listDivs []
  where
    listDivs divs n
      | n == 1 = divs
      | otherwise = listDivs divs' (n `div` firstDiv)
        where
          firstDiv = head $ filter (\d -> n `mod` d == 0) [2..]
          divs' = if (firstDiv `elem` divs) then divs else firstDiv : divs


