myLength :: [Int] -> Int
myLength [] = 0
myLength (x : xs) = 1 + myLength xs

myMaximum :: [Int] -> Int
myMaximum (x : xs)
    | xs == []  = x
    | otherwise = max x (myMaximum xs)

average :: [Int] -> Float
average l = fromIntegral (mySum l) / fromIntegral (myLength l)
    where
    mySum :: [Int] -> Int
    mySum (x : xs) = x + sum xs

buildPalindrome :: [Int] -> [Int]
buildPalindrome xs = invert xs ++ xs
    where 
    invert :: [Int] -> [Int] 
    invert [] = []
    invert (y : ys) = invert ys ++ [y]

remove :: [Int] -> [Int] -> [Int]
remove xs [] = xs
remove xs (y : ys) = remove (removeElem xs y) ys
    where 
    removeElem :: [Int] -> Int -> [Int]
    removeElem [] y = []
    removeElem (x : xs) y
        | x == y    = removeElem xs y
        | otherwise = x : removeElem xs y

flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (x : xs) = x ++ flatten xs

oddsNevens :: [Int] -> ([Int], [Int])
oddsNevens [] = ([], [])
oddsNevens (x : xs)
    | odd x     = (x : odds, evens)
    | otherwise = (odds, x:evens)
    where (odds, evens) = oddsNevens xs

primeDivisors :: Int -> [Int]
primeDivisors n = lstPrimeDivisors n 2
    where
    lstPrimeDivisors n f
        | f ^ 2 >= n     = [n]
        | mod n f == 0	= f : lstPrimeDivisors (div n f) f
        | otherwise		= lstPrimeDivisors n (f + 1)

