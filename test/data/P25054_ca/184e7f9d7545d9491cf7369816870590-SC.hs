
 
-- llargada
myLength:: [Int] -> Int
myLength [] = 0
myLength (_ : xs) = 1 + myLength xs


-- myMaximum
myMaximum:: [Int] -> Int
myMaximum [x] = x
myMaximum (x:xs) = max x (myMaximum xs)

-- average
average:: [Int] -> Float
average xs = (fromIntegral (suma xs)) / (fromIntegral (myLength xs))
  where
      suma [] = 0
      suma (x:xs) = x + suma xs
      

-- buildPalindrome
buildPalindrome:: [Int] -> [Int]
buildPalindrome xs = (girar xs) ++ xs
  where
    girar [] = []
    girar (y:ys) = girar ys ++ [y]
    









