
-- Tamany d'una llista:
myLength :: [Int] -> Int
myLength []		= 0
myLength (x:xs)	= 1 + (myLength xs)

-- Maxim d'una llista:
myMaximum :: [Int] -> Int
myMaximum [x]		= x 
myMaximum (x:xs) 	= maxim x (myMaximum xs)

maxim:: Int -> Int -> Int
maxim x y
	| x > y 	= x
	| otherwise	= y

-- Mitjana d'una llista:
average :: [Int] -> Float
average []  	= 0
average (x:xs)	= fromIntegral ((sumal (x:xs))) / fromIntegral l
	where l = myLength(x:xs) 

sumal :: [Int] -> Int
sumal []		= 0
sumal (x:xs)	= x + (sumal xs)

--Crear un palindrom a partir d'una llista:
buildPalindrome :: [Int] -> [Int]
buildPalindrome xs =  (++) (reverso xs) xs

reverso :: [Int] -> [Int]
reverso [a] = [a]
reverso (x:xs) =  (++) (reverso xs) [x]

-- eliminar els elements d'una llista en un altre:
remove :: [Int] -> [Int] -> [Int]
remove as [] = as
remove as (b:bs) = remove (existe as b) bs

existe :: [Int] -> Int -> [Int]
existe [] b = []
existe (x:xs) b 
	| x == b 	= existe xs b
	| otherwise	= [x] ++ (existe xs b)


--donat una llista de llistes produir només una llista:
flatten :: [[Int]] -> [Int]
flatten [x] = x
flatten (x:xs) = (++) x (flatten xs)

--donat una llista retorna una llista de parells i una altre de senars:
oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens xs = ((buscaImpares xs), (buscaPares xs))

buscaImpares :: [Int] -> [Int]
buscaImpares [] = []
buscaImpares (x:xs)
	| (even x) = buscaImpares xs
	| otherwise = [x] ++ buscaImpares xs

buscaPares :: [Int] -> [Int]
buscaPares [] = []
buscaPares (x:xs)
	| (odd x) = buscaPares xs
	| otherwise = [x] ++ buscaPares xs

--donat un enter retorni la llista dels divisors primeres positius:
primeDivisors :: Int -> [Int]
primeDivisors n = (divisors n 2)

divisors :: Int -> Int -> [Int]
divisors n m
	| m > n = []
	| (isDiv2 n m) && (isPrime m) = [m] ++ (divisors n (m+1))
	| otherwise = divisors n (m+1)

isDiv2 :: Int -> Int -> Bool
isDiv2 a b
	| mod a b == 0 = True
	| otherwise = False

-- Es numero primer:
isPrime :: Int -> Bool
isPrime 0 = False
isPrime 1 = False
isPrime 2 = True
isPrime n = (isDiv n (n-1))

isDiv :: Int -> Int -> Bool
isDiv a b
    | b <= 1        = True
    | mod a b == 0  = False
    | otherwise     = (isDiv a (b-1))