-- exemple ús if then else

myLength [] = 0
myLength (x:y) = 1+(myLength y)

myMaximum (a:[]) = a
myMaximum (a:b) = (max a (myMaximum b))

suma [] = 0
suma (a:b) = a + (suma b)
average a = (suma a)/(myLength a)

contains [] y = False
contains (a:b) y
	| a == y = True
	| otherwise = contains b y 

remove :: [Int]->[Int]->[Int]
remove [] b = []
remove (x:y) b
	| (contains b x) = (remove y b)
	| otherwise = [x]++(remove y b)

flatten [] = []
flatten	(x:y) = x++(flatten y)

odds [] = []
odds (x:y)
	| (mod x 2) == 0 = (x:odds y)
	| otherwise = (odds y)


evens [] = []
evens (x:y)
	| (mod x 2) == 1 = (x:evens y)
	| otherwise = (evens y)
oddsNevens n = (evens n,odds n)

primeDiv n  m
	| n == 1 = []
	| (mod n m) == 0 = [m]++(primeDiv (div n m) m)
	| otherwise = primeDiv n (m+1)
primeDivisors n = primeDiv n 2