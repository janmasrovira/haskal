
myLength :: [Int] -> Int
myLength [] = 0
myLength (cap:cua) = 1 + myLength cua

myMaximum :: [Int] -> Int
myMaximum [x] = x
myMaximum (x:xs) = max x (myMaximum xs)

average :: [Int] -> Float
average xs = fromIntegral s / fromIntegral l
  where s = sum xs
	l = myLength xs

invert :: [Int] -> [Int]
invert [] = []
invert (x:xs) = (invert xs)++[x]

buildPalindrome :: [Int] -> [Int]
buildPalindrome [] = []
buildPalindrome l = (invert l)++l

delete :: [Int] -> Int -> [Int]
delete [] y = []
delete (x:xs) y  
    | y /= x 		= x:(delete xs y)
    | otherwise 	= delete xs y
    
remove :: [Int] -> [Int] -> [Int]
remove xs [] = xs
remove [] ys = []
remove xs (y:ys) = remove (delete xs y) ys

flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (x:xs) = x++(flatten xs)

oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens (x:xs)
  |mod x 2 == 0		= (s,[x]++p)
  |otherwise		= ([x]++s,p)
  where (s,p) = oddsNevens xs

divisors :: Int -> [Int] -> [Int]
divisors n [] = []
divisors n (d:ds)
  |n `mod` d == 0	= d:(divisors n ds)
  |otherwise 		= divisors n ds

arePrime :: [Int] -> [Int]
arePrime [] = []
arePrime (x:xs) 
  |myLength (divisors x [1..x]) == 2	= x:(arePrime xs)
  |x /= 1				= arePrime xs
  |otherwise				= arePrime xs

primeDivisors :: Int -> [Int]
primeDivisors 1 = []
primeDivisors n = arePrime(divisors n [1..n])