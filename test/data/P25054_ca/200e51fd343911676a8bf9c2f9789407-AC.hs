myLength::[Int] -> Int
myLength [] = 0
myLength (x:xs) = 1 + myLength xs

myMaximum::[Int] -> Int
myMaximum [] = 0
myMaximum (x:xs)
  | null xs = x
  | x >= y = x
  | otherwise = y
  where y = myMaximum xs

--revisar
average::[Int] -> Float
average [] = 0
average xs = (fromIntegral (sum xs)) / (fromIntegral (myLength xs))

--revisar
buildPalindrome::[Int]->[Int]
buildPalindrome [] = []
buildPalindrome xs = reverse xs ++ xs

remove::[Int] -> [Int] -> [Int]
remove xs [] = xs
remove xs (y:ys) = remove (remove_aux xs y) ys

remove_aux::[Int] -> Int -> [Int]
remove_aux [] y = []
remove_aux (x:xs) y
  | x == y = remove_aux xs y
  | otherwise = [x] ++ (remove_aux xs y)

flatten::[[Int]] -> [Int]
flatten [] = []
flatten (x:xs) = x ++ (flatten xs)

oddsNevens::[Int] -> ([Int], [Int])
oddsNevens xs = oddsNevens2 xs ([], []) 

oddsNevens2::[Int] -> ([Int], [Int]) -> ([Int], [Int])
oddsNevens2 [] (e, o) = (e, o)
oddsNevens2 (x:xs) (e, o)
  | mod x 2 == 0 = oddsNevens2 xs (e, (o ++ [x]))
  | otherwise = oddsNevens2 xs ((e ++ [x]), o)

primeDivisors::Int -> [Int]
primeDivisors n = primeDivisorsAux n n

primeDivisorsAux::Int -> Int -> [Int]
primeDivisorsAux n k
  | n < 2 = []
  | mod k n == 0 && isPrime (n) = (primeDivisorsAux (n-1) k) ++ [n]
  | otherwise = primeDivisorsAux (n-1) k
  
--se puede optimizar usando raíz de n y salto de 2 en dos
isPrime::Int -> Bool
isPrime n
  | n < 2 = False
  | n == 2 = True
  | otherwise = isPrimeAux n (n-1)

isPrimeAux::Int -> Int -> Bool
isPrimeAux n k
  | k <= 1         = True
  | mod n k == 0   = False
  | otherwise      = (isPrimeAux n (k-1))