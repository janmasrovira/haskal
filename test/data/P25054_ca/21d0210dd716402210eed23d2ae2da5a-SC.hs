myLenght::[Int]->Int
myLenght [] = 0
myLenght (a:b) = 1+ (myLenght b)

myMaximum :: [Int] -> Int
myMaximum [a] = a
myMaximum (a:b)
     | a > myMaximum (b) = a
     | otherwise = myMaximum(b)
     
     
sumar :: [Int] -> Int
sumar [] = 0
sumar (x:xs) = x+(sumar xs)     
     
average :: [Int] -> Float
average (a:b) = (fromIntegral (sumar (a:b))) / (fromIntegral (myLenght (a:b)))

buildPalindrome :: [Int] -> [Int]
buildPalindrome [a] = [a]
buildPalindrome (a:b) = girar(a:b)++(a:b)

girar :: [Int] -> [Int]
girar [a] = [a]
girar (a:b) = (girar b)++[a]

founded :: Int -> [Int] -> Bool
founded a [] = False;
founded a (b:c)
       | a == b = True
       | otherwise = founded a c

remove :: [Int] -> [Int] -> [Int]
remove [] [] = []
remove [] (c:d) = []
remove (a:b) [] = (a:b)
remove (a:b) (c:d)
       | not (founded a (c:d)) = a:(remove b (c:d))
       | otherwise = (remove b (c:d))


flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (a:b) =  a++flatten b


listaPares ::[Int] -> [Int]
listaPares [] = []
listaPares (a:b)
      | even a = a:listaPares b
      | otherwise = listaPares b

listaImpares ::[Int] -> [Int]
listaImpares [] = []
listaImpares (a:b)
      | odd a = a:listaImpares b
      | otherwise = listaImpares b

oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens (a) = (listaImpares a,listaPares a)


divisores :: Int -> Int -> [Int]
divisores n 1 = [1]
divisores n p 
       | n `mod` p == 0 = p:(divisores n (p-1)) 
       | otherwise = (divisores n (p-1))

isPrime :: Int -> Bool
isPrime 0 = False
isPrime 1 = False
isPrime n = length (divisores n n) == 2

divisoresPrimos :: Int -> Int -> [Int]
divisoresPrimos n 1 = []
divisoresPrimos n p 
       | n `mod` p == 0 && isPrime p = p:(divisoresPrimos n (p-1)) 
       | otherwise = (divisoresPrimos n (p-1))

primeDivisors :: Int -> [Int]
primeDivisors 0 = []
primeDivisors n = (divisoresPrimos n n)
