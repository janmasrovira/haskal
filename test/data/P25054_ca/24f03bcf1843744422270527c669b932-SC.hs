--6
myLength :: [Int] -> Int 
myLength [] = 0
myLength (x:xs) =  1 + myLength xs

--1
myMaximum :: [Int] -> Int
myMaximum (x:xs)
  | xs == [] = x
  | x > max = x
  | otherwise = max
  where max = myMaximum xs
	
--2
average :: [Int] -> Int
average xs = sum xs `div` length xs  
 
  