myLength::[Int] -> Int
myLength [] = 0
myLength (s:xs) = 1 + (myLength xs)

myMaximum::[Int] -> Int
myMaximum [a] = a
myMaximum (s:xs) = max s (myMaximum xs)

mySum::[Int] -> Int
mySum [] = 0
mySum (s:xs) = s + (mySum xs)

average::[Int] -> Float
average [] = 0.0
average s = (fromIntegral (mySum s)) / (fromIntegral (myLength s)) 

myReverse::[Int] -> [Int]
myReverse [] = []
myReverse (a:as) = (myReverse as) ++ [a]

buildPalindrome::[Int] -> [Int]
buildPalindrome as = (myReverse as) ++ as

remove1::[Int] -> Int -> [Int]
remove1 [] y = []
remove1 (x:xs) y
	| x == y    = remove1 xs y
	| otherwise = x:(remove1 xs y) 

remove :: [Int] -> [Int] -> [Int]
remove xs [] = xs
remove xs (y:ys) = remove (remove1 xs y) ys 

flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (l:ls) = l ++ flatten ls

myOdds:: [Int] -> [Int]
myOdds [] = []
myOdds (a:as) 
	| mod a 2 == 0 = myOdds as
	| otherwise    = a : (myOdds as)

myEvens:: [Int] -> [Int]
myEvens [] = []
myEvens (a:as) 
	| mod a 2 == 0 = a : (myEvens as)
	| otherwise    = myEvens as

oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens as = (myOdds as, myEvens as)

isPrime:: Int -> Bool
isPrime n
  | n < 2           = False
  | n == 2          = True
  | (mod n 2) == 0  = False
  | otherwise       = primeTest n 3

primeTest:: Int -> Int -> Bool
primeTest n m
  | m >= n          = True
  | (mod n m) == 0  = False
  | otherwise       = primeTest n (m+2)

myPrimeSearch:: Int -> Int -> [Int]
myPrimeSearch p n 
	| p == n                    = []
	| mod n p == 0 && isPrime p = p : (myPrimeSearch (p+1) n)
	| otherwise                 = myPrimeSearch (p+1) n

primeDivisors :: Int -> [Int]
primeDivisors n = myPrimeSearch 2 n