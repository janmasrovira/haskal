import Data.Function


myLength :: [Int] -> Int
myLength [] = 0
myLength (x:xs) = 1 + myLength xs

myMaximum :: [Int] -> Int
myMaximum (x:[]) = x
myMaximum (x:xs)
	| x < y = y
	| otherwise = x
	where y = myMaximum xs

average :: [Int] -> Float
average x = doAvg x 0 0

doAvg :: [Int] -> Int -> Int -> Float
doAvg (x:[]) s n = fromIntegral (s+x) / fromIntegral (n+1)
doAvg (x:xs) s n = doAvg xs (s+x) (n+1)

--buildPalindrome :: [Int] -> [Int]
--buildPalindrome (x:[]) = [x,x]
--buildPalindrome (x:xs) = concat (concat y [x]) (x:y)
--	where y = buildPalindrome xs

remove :: [Int] -> [Int] -> [Int]
remove (x:[]) (y:[]) 
	| x == y = []
	| otherwise = [x]
remove (x:[]) (y:ys)
	| x == y = []
	| otherwise = remove [x] ys
remove (x:xs) (y:[])
	| x == y = remove xs [y]
	| otherwise = x:remove xs [y]
remove (x:xs) (y:ys)
	| x == y = remove xs (y:ys)
	| otherwise = concat [(remove [x] ys),(remove xs (y:ys))]

--flatten :: [[Int]] -> [Int]

oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens (x:[])
	| x `mod` 2 == 0 = ([x], [])
	| otherwise = ([], [x])
oddsNevens (x:xs)
	| x `mod` 2 == 0 = (x:cp, ci)
	| otherwise = (cp, x:ci)
	where (cp, ci) = oddsNevens(xs)