mylist = [1,2,3,4,5]:: [Int]

myLength::[Int] -> Int
myLength [] = 0
myLength (x:xs) = 1 + myLength xs

myMaximum::[Int] -> Int
myMaximum (x:xs)
    | x > z = x
    | otherwise = z
    where z = myMaximum xs

average::[Int] -> Float
average x = fromIntegral (sum x) / fromIntegral (length x)

buildPalindrome::[Int] -> [Int]
buildPalindrome x = reverse x ++ x

remove::[Int] -> [Int] -> [Int]
remove x [] = x
remove xs (y:ys) = remove (remove' xs y) ys

remove' [] _ = []
remove' (x:xs) y
    | x == y = remove' xs y
    | otherwise = x : remove' xs y

--flatten::[[Int]] -> [Int]

--oddsNeven::[Int] ->([Int],[Int])
--oddsNeven l =
    