myLength[] = 0
myLength(x:xs) = 1 + (myLength xs)

myMaximum [x]=x
myMaximum (x:xs)
  | x>=a = x
  | otherwise = a where a = myMaximum xs

average:: [Int]->Float
suma [] = 0
suma (x:xs) = x+(suma xs)
mida [] = 0
mida (x:xs) = 1+(mida xs)
average xs = (fromIntegral(suma xs))/(fromIntegral(mida xs))


buildPalindrome::[Int]->[Int]
buildPalindrome xs = (reverseP xs)++(xs)

reverseP::[Int]->[Int]
reverseP []=[]
reverseP (x:xs) = ((reverseP xs) ++[x])

remove:: [Int]->[Int]->[Int]
remove xs [] = xs
remove xs (y:ys) = (remove (auxRemove xs y) ys)

auxRemove [] _ = []
auxRemove (x:xs) y
  |x==y = (auxRemove xs y)
  |otherwise = ([x]++(auxRemove xs y))


flatten::[[Int]]->[Int]
flatten [] = []
flatten (x:xs) = x++(flatten xs)


oddsNevens [] = ([],[])
oddsNevens (x:xs)
  |(x `mod` 2 == 0 ) = (odds,x:evens)
  |otherwise = (x:odds,evens)
  where (odds,evens) = oddsNevens xs

{- cada cop que trobo un divisor aplicarlo succesivament -}

primeDivisors:: Int->[Int]
primeDivisors x = auxPrimeD 2 x

auxPrimeD:: Int->Int->[Int]
auxPrimeD a x
  |a>x = []
  |(x `mod` a == 0)= a:(auxPrimeD (a+1) (divSuc x a))
  |otherwise = auxPrimeD (a+1) x

divSuc::Int->Int->Int
divSuc x a
  | (x `mod` a == 0) = divSuc (x `div` a) a
  | otherwise = x
  
  
{-
isPrime 0 = False
isPrime 1 = False
isPrime x = auxIsPrime 2 x
auxIsPrime a n
  |a*a>n = True
  |n `mod` a == 0 = False
  |otherwise = auxIsPrime (a+1) n

  
  
primeDivisors x = auxPrimeD 1 x
auxPrimeD a x
  |a>x = []
  |(x `mod` a == 0)&& (isPrime a) = [a]++(auxPrimeD (a+1) x)
  |otherwise = auxPrimeD (a+1) x
  -}