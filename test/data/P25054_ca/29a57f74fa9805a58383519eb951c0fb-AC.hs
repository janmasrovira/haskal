myLength [] = 0
myLength (x:xs) = 1 + myLength xs

myMaximum [x] = x
myMaximum (x:xs)
    | x > y     = x
    | otherwise = y
    where y = myMaximum xs
          
suma [] = 0
suma (x:xs) = x + suma xs          

average xs = suma(xs)/myLength(xs)

buildPalindrome :: [Int] -> [Int]
buildPalindrome [] = []
buildPalindrome x = buildP x x 


buildP [] ys = ys
buildP (x:xs) ys = buildP xs (x:ys) 

remove xs [] = xs
remove xs (y:ys) = remove (removeE xs y) ys

removeE [] y = []
removeE (x:xs) y 
    | x == y    = n
    | otherwise = (x:n)
    where n = removeE xs y

flatten [] = []
flatten (x:xs) = x++(flatten xs)

parells [] = []
parells (x:xs) 
    | mod x 2 == 0  = x:p
    | otherwise     = p
    where p = parells xs
          
senars [] = []
senars (x:xs) 
    | mod x 2 == 1  = x:s
    | otherwise     = s
    where s = senars xs
        
        
oddsNevens [] = ([],[])
oddsNevens (x:xs)
    | mod x 2 == 1  = (x:odds, evens)
    | otherwise     = (odds, x:evens)
    where (odds, evens) = oddsNevens xs
      
      
isPrime n = length [ x | x <- [2..n], mod n x == 0] == 1
primeDivisors n = [ x | x <- [2..n], mod n x == 0, isPrime x] 
               
