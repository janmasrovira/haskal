myLength [] = 0
myLength (x:xs) = 1 + myLength xs
-----------------------------------------

myMaximum [x] = x
myMaximum (x:xs)
	| m > x = m
	| otherwise = x
	where {m = myMaximum xs}
-----------------------------------------

suma [x] = x
suma (x:xs) = x + suma xs
average x = (suma x)/(myLength x)
-----------------------------------------

pal [] = []
pal (x:xs) = 
	n ++ [x]
	where {n = pal xs}
buildPalindrome x = pal x ++ x
-----------------------------------------

exists x [] = False
exists x (y:ys) 
	| x == y = True
	| otherwise = exists x ys 

remove [] y = []
remove (x:xs) y = 
	if not (exists x y) then [x]++remove xs y
	else remove xs y
-----------------------------------------

flatten [] = []
flatten [x] = x
flatten (x:xs) = 
	x ++ flatten xs
-----------------------------------------

oddsNevens [] = ([],[])
oddsNevens (x:xs) 
	| mod x 2 == 0 = (fst n, [x] ++ snd n)
	| otherwise = ([x] ++ fst n, snd n)
	where {n = oddsNevens xs}
-----------------------------------------

prime x y = 
	if y == 1 then True
	else if mod x y == 0 then False
	else prime x (y-1)

isPrime x = 
	if x == 0 then False
	else if x == 1 then False
	else prime x (x-1)


primeDivisors x = 
	divs x x
	where
		divs _ 0 = []
		divs x y
			| (mod x y == 0) && (isPrime y) = n ++ [y]
			| otherwise = n
			where {n = divs x (y-1)}
