myLenght :: [Int]->Int 

myLenght [] = 0
myLenght (x:xs) = 1 + myLenght xs

myMaximum :: [Int]->Int 

myMaximum [x] = x
myMaximum (x:xs) 
  |  x >= myMaximum xs = x
  |otherwise = myMaximum xs

sumlist :: [Int]-> Int
sumlist [] = 0
sumlist (x:xs) = x + sumlist xs
  
average :: [Int]->Float

average [] = fromIntegral 0
average [x] = fromIntegral x
average (x:xs) =  fromIntegral(sumlist (x:xs)) / (fromIntegral(myLenght (x:xs))) 


buildPalindrome :: [Int] -> [Int]

buildPalindrome [] = []
buildPalindrome [x] = [x]
buildPalindrome (x:xs) = reverse (x:xs) ++ (x:xs)


remove1 :: [Int]->Int->[Int]

remove1 [] x = []
remove1 (x:xs) y 
  | y==x = remove1 xs y
  |otherwise = x:remove1 xs y



remove :: [Int]->[Int]->[Int]

remove xs [] = xs  

remove xs (y:ys) = remove (remove1 xs y) ys


flatten :: [[Int]]->[Int]

flatten [[]] = []
flatten[[x]] = [x]
flatten (x:xs) = x ++ flatten xs

odds :: [Int]->[Int]

odds [] = []
odds (x:xs) 
  |odd x = [x] ++ odds xs
  |otherwise = odds xs
  
  
evens :: [Int]->[Int]

evens [] = []
evens (x:xs) 
  |even x = [x] ++ evens xs
  |otherwise = evens xs

oddsNevens :: [Int]->([Int],[Int])

oddsNevens [] = ([],[])
oddsNevens xs = (odds xs, evens xs)		