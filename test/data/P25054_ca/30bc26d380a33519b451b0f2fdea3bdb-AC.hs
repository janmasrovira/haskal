-- Donada una llista d’enters, calcula la seva llargada
myLength :: [Int] -> Int
myLength [] = 0
myLength (n:ns) = 1 + myLength ns

-- Donada una llista d’enters no buida, calcula el seu màxim
myMaximum :: [Int] -> Int
myMaximum (x:[]) = x
myMaximum (x:xs) = 
    if x > y then x
    else y
    where y = myMaximum xs

-- Donada una llista d’enters no buida, calcula la seva mitjana
average :: [Int] -> Float
average [] = 0
average x = (fromIntegral (mySum x)) / (fromIntegral (myLength x))

mySum :: [Int] -> Int
mySum [] = 0
mySum (x:xs) = x + (mySum xs)

-- Donada una llista, retorna el palíndrom que comença amb la 
-- llista invertida
buildPalindrome :: [Int] -> [Int]
buildPalindrome x = (inverse x) ++ x

inverse :: [Int] -> [Int]
inverse [] = []
inverse (x:xs) = inverse(xs) ++ (x:[])

-- Donada una llista d’enters x i una llista d’enters y, retorna 
-- la llista x havent eliminat totes les ocurrències dels elements
-- en y
remove :: [Int] -> [Int] -> [Int]
remove [] y = []
remove (x:xs) y = 
    if hasElement y x then (remove xs y)
    else x : (remove xs y)

hasElement :: [Int] -> Int -> Bool
hasElement [] y = False
hasElement (x:xs) y =
    if x == y then True
    else hasElement xs y

-- Aplana una llista de llistes produint una llista d’elements
flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (x:xs) = x ++ (flatten xs)

-- donada una llista d’enters, retorna dues llistes, una que 
-- conté els parells i una que conté els senars, en el mateix 
-- ordre relatiu que a l’original
oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens (x:xs) = 
    if mod x 2 == 0 then (o,x:e)
    else (x:o, e)
    where (o,e) = oddsNevens xs

-- Retorna la llista de divisors primers d’un enter estrictament 
-- positiu
primeDivisors :: Int -> [Int]
primeDivisors 0 = []
primeDivisors 1 = []
primeDivisors x = divisors 2 x

divisors :: Int -> Int -> [Int]
divisors n x = 
    if n == x then if (isPrime n) then n:[] else []
    else if (mod x n == 0) && (isPrime n) then n:(divisors (n+1) x)
    else divisors (n+1) x

isPrime :: Int -> Bool
isPrime 0 = False
isPrime 1 = False
isPrime n = hasDivisors n 2

hasDivisors :: Int -> Int -> Bool
hasDivisors x a
    | x <= a            = True
    | (mod x a) == 0    = False
    | otherwise         = hasDivisors x (a+1)