myLength [] = 0
myLength (x:l) = 1 + (myLength l)

myMaximum [x] = x
myMaximum (x:l) 
	| a > x = a
	| otherwise = x
	where a = (myMaximum l)

auxaverage [x] = (x, 1)
auxaverage (x:l) = ((x + (fst llista) * n) / (n + 1), n + 1)
					where llista = (auxaverage l);
							n = (snd llista)
average [x] = x
average (x:l) = (x + (fst llista) * n) / (n + 1)
				where llista = (auxaverage l);
						n = (snd llista)

buildReversePalindrome [] = []
buildReversePalindrome (a:l) = [a]++(buildReversePalindrome l)++[a]

buildPalindrome llista = buildReversePalindrome (reverseP llista)

reverseP [] = []
reverseP (x:l) = (reverseP l)++[x]

remove x [] = x
remove x (a:l) = remove (removeElem x a) l

removeElem [] num = []
removeElem (a:l) num
	| a == num  = removeElem l num
	| otherwise = [a]++(removeElem l num)

flatten [] = []
flatten (a:l) = a++(flatten l)

oddsNevens [] = ([], [])
oddsNevens (a:l)
	| mod a 2 == 0 = ((fst rec), [a]++(snd rec))
	| otherwise    = ([a]++(fst rec), (snd rec))
	where rec = oddsNevens l

primeDivisors n = searchDivisors n 2

searchDivisors n m
	| n < m = []
	| not (isPrime m) = searchDivisors n (m+1)
	| mod n m == 0    = [m]++(searchDivisors (div n m) (m+1))
	| otherwise       = searchDivisors n (m+1)

isPrime n
  | n == 1 = False
  | n == 2 = True
  | mod n 2 == 0 = False
  | otherwise = not (hasDiv n ((div n 2)+1) 3)

hasDiv n m it
  | it >= m = False
  | mod n it == 0 = True
  | otherwise = hasDiv n m (it+2)