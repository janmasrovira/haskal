myLength::[Int]->Int
myLength [] = 0
myLength (x:xs) = 1+length xs

myMaximum::[Int]->Int
myMaximum (x:[]) = x
myMaximum (x:(y:ys))
	|x>y = myMaximum(x:ys)
	|otherwise = myMaximum(y:ys)

average::[Int]->Float
average [] = 0
average l = (fromIntegral (sum l)) / (fromIntegral (myLength l))
	where
		sum::[Int]->Int
		sum [] = 0
		sum (x:[]) = x
		sum (x:xs) = x + sum xs
		
buildPalindrome::[Int]->[Int]
buildPalindrome [] = []
buildPalindrome l = reverse l ++ l
	where
		reverse (x:[]) = [x]
		reverse (x:xs) = reverse xs ++ [x]
		
remove::[Int]->[Int]->[Int]
remove x [] = x
remove [] _ = []
remove x (y:ys) = remove (rem x y) ys
	where
		rem::[Int]->Int->[Int]
		rem [] _ = []
		rem (l:ls) x
			|l == x = rem ls x
			|otherwise = [l]++rem ls x
			
flatten::[[Int]]->[Int]
flatten [] = []
flatten (x:xs) = x ++ flatten xs

oddsNevens::[Int]->([Int],[Int])
oddsNevens l = (nevens l, odds l)
	where
		odds [] = []
		odds (x:xs)
			|mod x 2 == 0 = [x] ++ odds xs
			|otherwise = odds xs
		nevens [] = []
		nevens (x:xs)
			|mod x 2 /= 0 = [x] ++ nevens xs
			|otherwise = nevens xs
			
isPrime::Int -> Bool
isPrime x
	|x<=1 = False
	|otherwise = noDiv x (x-1)
	where
		noDiv x 1 = True
		noDiv x n
			|mod x n == 0 = False
			|otherwise = noDiv x (n-1)
			
primeDivisors::Int->[Int]
primeDivisors 1 = []
primeDivisors x = divs x (x-1)
	where
		divs x y
			|y == 1 = []
			|mod x y == 0 && isPrime y = (divs x (y-1)) ++ [y]
			|otherwise = divs x (y-1)