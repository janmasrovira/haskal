-- myLength::[Int] -> Int
myLength [] = 0
myLength (x: xs) = 1 + myLength xs

-- myMaximum::[Int] -> Int
myMaximum [x] = x
myMaximum (x: xs)
	| x > m = x
	| otherwise = m
	where m = myMaximum xs

-- average::[Int] -> Float
average xs
	| otherwise = (auxAverage xs)/(myLength xs)
	where auxAverage (x: xs)
		  	| xs == []  = x
			| otherwise = x + (auxAverage xs)

-- buildPalindrome::[Int] -> [Int]
buildPalindrome [] = []
buildPalindrome (x: xs)
	|otherwise = auxPal(x: xs)++(x: xs)
	where auxPal [] = []
	      auxPal (x: xs) = (auxPal xs)++[x]

auxRemove _ [] = []
auxRemove n (x: xs)
	| n == x = auxRemove n xs
	| otherwise = [x]++auxRemove n xs

-- remove::[Int] -> [Int] -> [Int]
remove xs [] = xs
remove [] xs = []
remove xs (y: ys) = remove (auxRemove y xs) ys

-- flatten::[[Int]] -> [Int]
flatten [] = [] 
flatten ((xs):xss) = (xs)++(flatten xss)

-- oddsNevens::[Int] -> ([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens (x:xs)
	| x `mod` 2 /= 0 = ((x: ns), ms)
	| otherwise 	 = (ns, (x: ms))
	where (ns,ms) = oddsNevens (xs)

-- isPrime::Int -> Bool
auxPrime x n
	| n <= 1 	     = True
	| x `mod` n == 0 = False
	| n > 1          = auxPrime x (n - 1)

isPrime 0 = False
isPrime 1 = False
isPrime x = auxPrime x (x - 1)

auxPDiv n 1 = []
auxPDiv n m
	| (isPrime m) && (n `mod` m == 0) = (auxPDiv n (m - 1))++[m]
	| otherwise 					= auxPDiv n (m - 1)

-- primeDivisors::Int -> [Int]
primeDivisors 0 = []
primeDivisors 1 = []
primeDivisors x = auxPDiv x x


