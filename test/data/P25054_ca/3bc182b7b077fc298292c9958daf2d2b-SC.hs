myLength :: [Int] -> Int
myLength [] = 0
myLength (x:xs) = 1 + myLength xs

myMaximum :: [Int] -> Int
myMaximum [] = 0
myMaximum (x:[]) = x
myMaximum (x:xs)
  |x > myMaximum(xs) = x
  |otherwise = myMaximum(xs)

--average :: [Int] -> Float

--buildPalindrome :: [Int] -> [Int]

--remove :: [Int] -> [Int] -> [Int]

--flatten :: [[Int]] -> [Int]

--oddsNevens :: [Int] -> ([Int],[Int])

--primeDivisors :: Int -> [Int]