absValue :: Int -> Int
absValue x
  | x >= 0	= x
  | otherwise 	= -x

power :: Int -> Int -> Int

power a b
  | b == 0	= 1
  | otherwise = a * power a (b-1)
  
isPrime n
  | n <= 1 = False
  | otherwise = noTeDivisors 2
  
  where
  noTeDivisors :: Int -> Bool

  noTeDivisors  i
    | i*i > n = True
    |mod n i == 0  = False
    |otherwise 	= noTeDivisors  (i+1) 
    
slowFib :: Int -> Int

slowFib n
  | n <= 1	= n
  |otherwise = slowFib (n-1) + slowFib (n-2)
  
quickFib :: Int -> Int

quickFib i = fst(fib' i)
  
  where
    fib' :: Int -> (Int, Int)
    
    fib' 0 = (0, 0)
    fib' 1 = (1, 0) 
    fib' i = (x+y, x) 
      where 
	 (x, y) = fib' (i-1)
    
    
llargada :: [Int] -> Int
llargada [] = 0
llargada (x:L) = 1  + llargada L

myMaximum :: [Int] -> Int

myMaximum 
  myMaximum [x]  = x
  myMaximum [x:xs] =  max (x myMaximum(xs))
  
  
 
    