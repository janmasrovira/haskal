myLength 	:: [Int] -> Int
myMaximum 	:: [Int] -> Int
average 	:: [Int] -> Float
buildPalindrome :: [Int] -> [Int]
remove		:: [Int] -> [Int] -> [Int]
removeElement	:: [Int] -> Int -> [Int]
flatten		:: [[Int]] -> [Int]
oddsNevens	:: [Int] -> ([Int],[Int])
primeDivisors	:: Int -> [Int]
takeListPrime	:: Int -> Int -> [Int]
nextPrime	:: Int -> Int
isPrime		:: Int -> Bool
primality 	:: Int -> Int -> Bool

myLength [] = 0
myLength (x:xs) = (length xs) + 1

myMaximum [x] = x
myMaximum (x:xs) | x > m = x
	| otherwise = m
	where m = myMaximum xs
		
average ln = fromIntegral(sum ln) / fromIntegral (length ln)
	  
buildPalindrome ln@(x:xs) =  (auxPalindrome ln [])++ln
auxPalindrome [] ln = ln
auxPalindrome (x:xs) ln = auxPalindrome xs (x:ln)

removeElement [] r = []
removeElement (x:xn) r | x == r = lm
		       | otherwise = (x:lm)
	where lm = removeElement xn r

remove ln [] = ln
remove ln (x:xr) = removeElement lm x
	where lm = remove ln (removeElement (x:xr) x)

flatten [] = []
flatten (x:xn) = m
	where m = x  ++ flatten xn

oddsNevens [] = ([], [])
							 
oddsNevens (x:xn) | x `mod` 2 == 0 = (o, (x:e))
			  | otherwise = ((x:o), e)
	where (o, e) = oddsNevens xn
		  
isPrime 0 = False;
isPrime 1 = False;
isPrime n = primality n 2
primality n i | (i*i) > n = True
			  | n `mod` i == 0 = False
     | otherwise = primality n (i+1)

nextPrime p | isPrime p = p
			| otherwise = nextPrime (p+1)
													
takeListPrime n p | p > n = []
				  |  n `mod` p == 0 = (p:lp)
	  | otherwise = lp
	where {
		lp = takeListPrime n np;
		np = nextPrime (p+1)
	}
	
					   
primeDivisors n = takeListPrime n 2