myLength :: [Int] -> Int
myLength xs = sum [1 | _ <- xs]

myMaximum :: [Int] -> Int
myMaximum [x] = x
myMaximum (x:xs) = max x (myMaximum xs)

average :: [Int] -> Float
average xs = (fromIntegral (sum xs)) / (fromIntegral (length xs))

buildPalindrome :: [Int] -> [Int]
buildPalindrome xs = (reverse xs) ++ (xs)

remove :: [Int] -> [Int] -> [Int]
remove xs ys = [x | x<-xs, not (elem x ys)]

flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (l:ls) = l ++ (flatten ls)

oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens xs = ([x | x<-xs, odd x], [x | x<-xs, even x])

primeDivisors :: Int -> [Int]
primeDivisors n = [x | x<-[2..div n 2], isPrime x, mod n x == 0]

isPrime :: Int -> Bool
isPrime 1 = False
isPrime n | even n = n == 2
	  | otherwise = null [p | p<-[3,5..floor (sqrt (fromIntegral n))], mod n p == 0]
