myLength:: [Int] -> Int
myLength [] = 0
myLength (x : xs) = myLength xs + 1


myMaximum:: [Int] -> Int
myMaximum (x : xs) = 
  if xs == [] then x
  else max x (myMaximum(xs))

  
gDiv:: Int -> Int -> Float
gDiv a b = (fromIntegral a) / (fromIntegral b) 
  
average:: [Int] -> Float
average x = gDiv (sum x) (myLength x)



invert:: [Int] ->  [Int]
invert [] = []
invert (x : xs) = ((invert xs)++[x])

buildPalindrome:: [Int] -> [Int]
buildPalindrome [] = []
buildPalindrome x = ((invert x)++x)




removeX:: Int -> [Int] -> Bool
removeX x [] = False
removeX x (y:ys) =
  if x == y then True
	    else removeX x ys

remove:: [Int] -> [Int] -> [Int]
remove [] y = []
remove (x:xs) y = 
  if removeX x y then remove xs y
		 else [x]++(remove xs y)
		 	 
		 
flatten:: [[Int]] -> [Int]
flatten [] = []
flatten (x:xs) = x++(flatten xs)


auxOE:: [Int] -> ([Int], [Int]) -> ([Int], [Int])
auxOE [] (o,e) = (o,e)
auxOE (x:xs) (o,e) = 
  if mod x 2 == 0 then auxOE xs (o,e++[x])
		  else auxOE xs (o++[x],e)

oddsNevens:: [Int] -> ([Int], [Int])
oddsNevens x = auxOE x ([],[])


checkDiv:: Int -> Int -> Bool
checkDiv n p =
    if p*p > n then True
	       else if mod n p == 0 then False
			     else checkDiv n (p+1)
	
isPrime:: Int -> Bool
isPrime n = 
  if n > 1 then checkDiv n 2
	   else False

divisor:: Int -> Int -> Int
divisor x y =
  if isPrime x then x
	       else if mod x y == 0 then y
		  else divisor x (y+1)
	  
auxPD:: Int -> [Int] -> [Int]
auxPD x d = 
  if x == 1 then d
	    else auxPD (div x z) [z]++d
  where z = divisor x 2

primeDivisors:: Int -> [Int]
primeDivisors x = auxPD x []