
--myLength :: [Int] -> Int

myLength [] = 0
myLength (x:xs) = 1+(myLength xs)

--myMaximum :: [Int] -> Int

myMaximum [] = 0
myMaximum (x:xs) = (maximLlista x xs)

maximLlista m [] = m
maximLlista m (x:xs)
    |x>m = (maximLlista x xs)
    |otherwise = (maximLlista m xs)

--average :: [Int] -> Float

average [] = 0
average (s) = (mySum s)/(myLength s)
mySum [] = 0
mySum (x:xs) = x+(mySum xs)

--buildPalindrome :: [Int] -> [Int]

buildPalindrome [] = []
buildPalindrome (xs) = (invertir xs xs)

invertir [] ys = ys
invertir (x:xs) (ys) = (invertir xs (x:ys))

{--invertir2 l1 l2 = (inv l1) ++ l2
invertir [] = []
invertir (x:xs) = (invertir xs)++[x]
--}

--remove :: [Int] -> [Int] -> [Int]

remove [][] = []
remove (x:xs) [] = (x:xs)
remove [] (y:ys) = []
remove (x:xs)(y:ys)
    |borrarelem x (y:ys) = remove xs (y:ys)
    |otherwise = x:remove xs (y:ys)

borrarelem x [] = False
borrarelem x (y:ys)
    |x==y = True
    |otherwise = borrarelem x ys

--flatten :: [[Int]] -> [Int]

flatten [] = []
flatten (x:xs) = x++(flatten xs) 

--oddsNevens :: [Int] -> ([Int],[Int])

oddsNevens [] = ([],[]) 
oddsNevens (x:xs)
    |esparell x = (ss,x:ps)
    |otherwise =  (x:ss,ps)
    where (ss,ps) = oddsNevens xs
    
esparell x
    |(x`mod`2)==0 = True
    |otherwise = False

{--oddsNevens (xs) = (clasificar (invertir xs []) [] [])

clasificar [] ss ps = (ss,ps) 
clasificar (x:xs) ss ps
    |esparell x = clasificar xs ss (x:ps)
    |otherwise = clasificar xs (x:ss) ps
--}
  
--primeDivisors :: Int -> [Int]

primeDivisors x
    |x<2 = []
    |otherwise = getdivisors x 2
    
getdivisors x y
    |x<2 = []
    |y>(div x 2) = [x]
    |z==x = getdivisors x (y+1)
    |otherwise = y:(getdivisors z (y+1))
    where z = reduce x y
    
reduce x y
    |(x`mod`y)==0 = reduce (x`div`y) y
    |otherwise = x
 



