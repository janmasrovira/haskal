myLength::[Int]->Int
myLength [] = 0
myLength (x:y) = 1 + myLength y

average [a] = partAverage [a] 0 0


partAverage [] sum elem = sum/elem
partAverage (x:y) sum elem = partAverage y (sum + x) (elem + 1) 


myMaximum::[Int]->Int
myMaximum a = findMax a 0

findMax::[Int]->Int->Int
findMax [] a = a
findMax (x:y) a 
	| x > a = findMax y x
	| otherwise = findMax y a

