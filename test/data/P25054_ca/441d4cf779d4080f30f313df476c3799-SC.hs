myLength:: [Int]->Int
myLength [] = 0
myLength (x:xs) = 1 + myLength xs

myMaximum:: [Int]->Int
myMaximum [x] = x
myMaximum(x:xs)
  | x > m = x
  | otherwise = m
  where m = myMaximum(xs)

average:: [Int]->Float
average [x] = fromIntegral x
average l@(x:xs) = fromIntegral( sumFunc l ) / fromIntegral(length l)

sumFunc:: [Int]->Int
sumFunc [x] = x
sumFunc (x:xs) = ( x + sumFunc(xs) )

buildPalindrome:: [Int]->[Int]
buildPalindrome [] = []
buildPalindrome (x:xs) = (auxPalindrome (x:xs))++(x:xs)

auxPalindrome:: [Int]->[Int]
auxPalindrome [] = []
auxPalindrome (x:xs) = auxPalindrome(xs)++[x]

removeaux:: Int->[Int]->[Int]
removeaux _ [] = []
removeaux y (x:xs)
  | x == y = removeaux y xs
  | otherwise = [x]++removeaux y xs 

remove:: [Int]->[Int]->[Int]
remove [] xs = []
remove xs [] = xs
remove xs (y:ys) = remove (removeaux y xs) ys 

flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (xs:xss) = xs++flatten(xss) 

oddsNevens:: [Int]->([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens (x:xs)
  | x `mod` 2 == 0 = (x: pl,sl)
  | otherwise = (pl,x: sl)
  where (pl,sl) = oddsNevens xs

isPrime:: Int->Bool
isPrime n = auxPrimeFunc (n) (n-1)

auxPrimeFunc:: Int->Int->Bool
auxPrimeFunc f s
  | s < 1 = False
  | s == 1 = True
  | f `mod` s == 0 = False  
  | otherwise = auxPrimeFunc f (s-1)

primeDivisors:: Int->[Int]
primeDivisors x
  | x == 0 = []
  | x == 1 = []
  | otherwise = auxprimeDivisors x x

auxprimeDivisors::Int->Int->[Int]
auxprimeDivisors 1 m = []
auxprimeDivisors n m
  | (isPrime n) && (m `mod` n  == 0) = (auxprimeDivisors (n-1) m)++[n]
  | otherwise = auxprimeDivisors (n-1) m