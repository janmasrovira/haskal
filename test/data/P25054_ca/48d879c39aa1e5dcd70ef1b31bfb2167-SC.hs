

--returns the length of a list
myLength[] = 0;
myLength (x:xs) = ( 1 + (myLength xs) )

--returns the maximum number of the list ***
myMaximum [x] = x
myMaximum (x:xs)
	| x < y = y
	| otherwise = x
	where y = myMaximum xs
	

--returns the average of a list
-- use the sum function which sum the elements of the list
--use the myLength function defined above
suma[] = 0
suma(x:xs) = x + (suma xs)
average l@(x:xs) = suma l / myLength l 

--returns the palindrome of the parameter list ***
buildPalindrome [] = []
buildPalindrome [x] = [x]
buildPalindrome (x:xs) = [x] ++ buildPalindrome xs ++[x]

--returns the elements of the first list when removed 
--the elements of the second list
--uses the function included which means the list inlcude the element
included a [] = False
included a (x:xs)
	| a == x = True
	| otherwise = included a xs

remove [] elements = []
remove (x:xs) elements
	| included x elements = remove xs elements
	| otherwise = x:remove xs elements
	
--retorna la llista de elements de cada llista de una llista de llistes
flatten [] = []
flatten (x:xs) = x ++ flatten xs


--donada una llista d'enters retorna dues llistes, una que
--conte els parells i una que conte els senars en l'ordre original
oddsNevens [] = ([],[])
oddsNevens (x:xs)
	| even x = (x:a, b)
	| odd x = (a, x:b)
	where (a,b) = oddsNevens xs

--primedivisiors donat un nombre retorna una llista de nombres primers divisors 
-- del nombre donat.****
mult x y 
    | (x `mod` y) == 0 = True
    | otherwise = False
isPrime 1 = False
isPrime x = (isprime x (x-1))
isprime x 1 = True
isprime x y
    | mult x y = False
    | otherwise  = isprime x (y-1)
multPrimes _ 1 (t:xs) = (t:xs) 
multPrimes x y l@(t:xs)
	| ( (mult x y) && (isPrime y) )  = multPrimes x (y-1) (y:(t:xs))
    | otherwise = multPrimes x (y-1) (t:xs)
multPrimes x y []
	| ( (mult x y) && (isPrime y) )  = multPrimes x (y-1) (y:[])
    | otherwise = multPrimes x (y-1) ([])
primeDivisors 1 = [] 
primeDivisors x = multPrimes x x []





