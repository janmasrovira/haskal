myLength :: [Int] -> Int
myLength [] = 0
myLength (a:b) = 1 + (myLength b)

myMaximum :: [Int] -> Int
myMaximum (a:[]) = a
myMaximum (a:b)
  | a > aux = a
  | otherwise = aux
  where aux = myMaximum b
        
average :: [Int] -> Float
average (a:[]) = fromIntegral a
average a = fromIntegral (mySuma a) / fromIntegral (myLength a)
  
mySuma :: [Int] -> Int
mySuma [] = 0
mySuma (a:b) = a + (mySuma b)



remove :: [Int] -> [Int] -> [Int]
remove [] y = []
remove (x:xs) y
  | myIn y x = remove xs y
  | otherwise = (x : (remove xs y))

myIn :: [Int] -> Int -> Bool
myIn [] x = False
myIn (a:b) x
  | a == x = True
  | otherwise = myIn b x
  
flatten :: [[Int]] -> [Int]
flatten [] = []
flatten ((a:[]):c) = (a : (flatten c))
flatten ((a:b):c) = (a : (flatten c))