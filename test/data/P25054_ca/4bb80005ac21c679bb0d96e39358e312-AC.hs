myLength::[Int] -> Int
myLength []  = 0
myLength (x:l) = 1+(myLength l)

myMaximum::[Int] -> Int
myMaximum [a] = a
myMaximum (a:l) = (max a (myMaximum l))

twoThings::[Int] -> (Int,Int)
twoThings []    = (0,0)
twoThings (a:l) = 
    let aux = (twoThings l)
    in ( (fst aux)+1, (snd aux)+a )

average:: [Int] -> Float
average l = 
    let tt = (twoThings l)
    in ((fromIntegral (snd tt)) / (fromIntegral (fst tt) ))
    
buildPalindrome::[Int] -> [Int]
buildPalindrome l = (reverse l) ++ l

partialRemove::[Int] -> Int -> [Int]
partialRemove [] _ = []
partialRemove (a:l) b
    | (a==b) = ans
    | otherwise = a:ans
    where ans = (partialRemove l b)

remove::[Int] -> [Int] -> [Int]
remove [] _ = []
remove a [] = a
remove l1 (b:l2)  = (remove (partialRemove l1 b) l2)


flatten::[[Int]] -> [Int]
flatten [] = []
flatten (a:l) = a++(flatten l)

oddsNevens::[Int] -> ([Int],[Int])
oddsNevens []    = ([],[])
oddsNevens (a:l)
    | (odd a)  = (a:(fst rec), snd rec)
    | otherwise = (fst rec, a:(snd rec))
    where rec = (oddsNevens l)
          
divAll::Int -> Int -> Int
divAll n f
    | (mod n f)==0 = (divAll (div n f) f)
    | otherwise = n
 
primeList::Int->Int->[Int]
primeList n f
    | n==1 = []
    | (f*f)>n = [n]
    | (mod n f)==0 = f:aux
    | otherwise = aux
    where aux = (primeList (divAll n f) (f+1))
          
primeDivisors::Int -> [Int]
primeDivisors 0 = []
primeDivisors 1 = []
primeDivisors n = (primeList n 2)