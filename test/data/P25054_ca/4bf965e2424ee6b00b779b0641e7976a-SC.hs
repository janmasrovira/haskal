
--MY LENGHT 

myLenght :: [Int] -> Int

--myLenght [] = 0
--myLenght (x:xs) = 1 + myLenght xs

myLenght a = sum[1 | _ <- a]


--MY MAXIMUM

myMaximum :: [Int] -> Int

myMaximum (x:[]) = x
myMaximum (x:y:xs) = if x > y then myMaximum (x:xs) else myMaximum (y:xs)



--AVERAGE

average :: [Int] -> Float

average a = (fromIntegral (sum a)) / (fromIntegral (myLenght a))


--BUILD PALINDROME

buildPalindrome :: [Int] -> [Int]

buildPalindrome a = reverse a ++ a



--REMOVE

remove :: [Int] -> [Int] -> [Int]

remove a b = [x | x <- a, not (elem x b)]



--FLATTEN

flatten :: [[Int]] -> [Int]

flatten (x:xs) = x ++ flatten xs
flatten [] = []



--ODDS AND EVENS

oddsNevens :: [Int] -> ([Int], [Int])

oddsNevens a = ([x | x <- a, odd x], [y | y <- a, even y])


--PRIME DIVISORS

primeDivisors :: Int -> [Int]

primeDivisors x = [z | z <- [2..x], mod x z == 0 && isprime z]
	where
		isprime :: Int -> Bool
		isprime a = if findivisors a == 0 then True else False
			where
				findivisors :: Int -> Int
				findivisors a = sum [1 | x <- [2..e], (mod a x) == 0]
					where e = div a 2 
	 