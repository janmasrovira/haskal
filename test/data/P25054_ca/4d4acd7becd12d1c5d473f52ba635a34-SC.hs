myLength :: [Int] -> Int

myLength [] = 0
myLength (x:xs) = 1 + myLength(xs)

myMaximum :: [Int] -> Int
myMaximum [x] = x
myMaximum (x:xs) = max x (myMaximum xs)

sumaLlista :: [Int] -> Int
sumaLlista [] = 0
sumaLlista (x:xs) = x + sumaLlista xs

average :: [Int] -> Float
average [] = 0
average l = fromIntegral (sumaLlista l) / fromIntegral(myLength l)

listaInversa :: [Int] -> [Int]
listaInversa [] = []
listaInversa (x:xs) = listaInversa xs ++ [x]

buildPalindrome :: [Int] -> [Int]
buildPalindrome l = listaInversa l ++ l

removeItem :: [Int] -> Int -> [Int]
removeItem [] x = []
removeItem (x:xs) y
    | x == y = removeItem xs y
    | otherwise = (x : removeItem xs y)

remove :: [Int] -> [Int] -> [Int]
remove x [] = x
remove x (y:ys) = remove yk ys where yk = removeItem x y

flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (xl:ll) = xl ++ (flatten ll)

ordena :: ([Int], [Int]) -> [Int] -> ([Int], [Int])
ordena (x,y) [] = (x,y)
ordena (x,y) (z:zs)
    | mod z 2 == 0 = ordena (x,y++[z]) zs
    | otherwise = ordena (x++[z],y) zs

oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens l = ordena ([],[]) l
    
divisor :: Int -> Int -> Bool
divisor x y
    | y*y > x = True
    | mod x y == 0 = False
    | otherwise = divisor x (y+2)

isPrime :: Int -> Bool
isPrime x 
    | x <= 1 = False
    | x == 2 = True
    | otherwise = divisor x 3

divisorsk :: Int -> Int -> [Int]
divisorsk y d
    | d*d == y = [d]
    | d*d > y = []
    | mod y d == 0 = [d] ++ ( [div y d] ++ divisorsk y (d+2))
    | otherwise = divisorsk y (d+2)

borraNoPrimos :: [Int] -> [Int]
borraNoPrimos [] = []
borraNoPrimos (x:xs)
    | mod x 2 == 0 = borraNoPrimos xs
    | isPrime x == True = [x] ++ borraNoPrimos xs
    | otherwise = borraNoPrimos xs

preProcesa :: Int -> [Int]
preProcesa y = borraNoPrimos k where k = divisorsk y 3

primeDivisors :: Int -> [Int]
primeDivisors y
    | y == 1 = []
    | y == 2 = [2]
    | mod y 2 == 0 && mod y 4 /= 0 = [2] ++ ([div y 2] ++ res)
    | mod y 2 == 0 = [2] ++ res
    | res == [] = [y]
    | otherwise = res
    where res = preProcesa y
