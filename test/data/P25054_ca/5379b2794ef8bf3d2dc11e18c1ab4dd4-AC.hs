myLength :: [Int] -> Int
myLength [] = 0
myLength (x:xs) = 1+(myLength xs)


myMaximum :: [Int] -> Int
myMaximum [x] = x
myMaximum (x:xs)
    | x>=(myMaximum xs)	= x 
    | otherwise = (myMaximum xs)


average :: [Int] -> Float
average [x] = fromIntegral x 
average (x:xs) = (suma (x:xs)) / fromIntegral (myLength (x:xs))
    where suma [] = 0.0
	  suma (x:xs) = fromIntegral x+(suma xs)


buildPalindrome :: [Int] -> [Int]
buildPalindrome [] = []
buildPalindrome (x:xs) = (cap (x:xs)) ++ (cua (x:xs))
  where cap [x] = [x]
	cap (x:xs) = (cap xs) ++ [x]
	cua [x] = [x]
	cua (x:xs) = [x] ++ (cua xs)


remove :: [Int] -> [Int] -> [Int]
remove [] [] = []
remove (x:xs) [] = (x:xs)
remove [] (x:xs) = []
remove [x] (y:ys)
    | x == y	= []
    | otherwise	= remove [x] ys 
remove (x:xs) (y:ys)
    | existeix x (y:ys)	= remove xs (y:ys)
    | otherwise		= [x] ++ (remove xs (y:ys))
  where
    existeix x [y]
	| x == y	= True
	| otherwise	= False
    existeix x (y:ys)
	| x == y	= True
	| otherwise 	= existeix x ys

	
flatten :: [[Int]] -> [Int]
flatten [] = []
flatten [x] = x
flatten (x:xs) = x ++ flatten xs 

oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens (x:xs) = ((senars (x:xs)),(parells (x:xs)))
  where
    parells [x]
	| mod x 2 == 0	= [x]
	| otherwise	= []
    parells (x:xs)
	| mod x 2 == 0	= [x] ++ (parells xs)
	| otherwise	= (parells xs)
    senars [x]
	| mod x 2 /= 0	= [x]
	| otherwise	= []
    senars (x:xs)
	| mod x 2 /= 0	= [x] ++ (senars xs)
	| otherwise	= (senars xs)


primeDivisors :: Int -> [Int]
primeDivisors x = auxPrime x x
    where
	auxPrime x 1 = []
	auxPrime x y 
	    | mod x y == 0 && isPrime y	= (auxPrime (div x y) (y-1)) ++ [y] 
	    | otherwise	= (auxPrime x (y-1))
	    
isPrime :: Int -> Bool
isPrime n
	| n <= 3		= n >= 2
	| mod n 2 == 0	= False
	| mod n 3 == 0	= False
	| otherwise		= f n 5
	where
	    f :: Int -> Int -> Bool
	    f x a 
		| a*a > x 		= True
		| mod x a == 0		= False
		| mod x (a+2) == 0	= False
		| otherwise 		= f x (a+6)
	


