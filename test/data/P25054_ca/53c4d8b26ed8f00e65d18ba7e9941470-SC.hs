-- myMaximum [4,3,1,5,4,5,2]   ->  5

myMaximum :: [Int]->Int
myMaximum [] = error "maximum of empty list"
myMaximum [x] = x
myMaximum (x:xs) 
	| x >= myMaximum (xs) = x
	| otherwise = myMaximum (xs)


-- average [1,2,3]   ->   2.0

average :: [Int]->Float
average [] = error "average of empty list"
average (ms) = fromIntegral(sum ms) / fromIntegral(length ms)

-- buildPalindrome [2,4,6]   ->  [6,4,2,2,4,6]

buildPalindrome :: [a]->[a]
buildPalindrome [] = []
buildPalindrome (ms) = concat [ reverse(ms) , (ms) ]

-- flatten [[2,6],[8,1,4],[],[1]]   [2,6,8,1,4,1]

flatten :: [[a]]->[a]
flatten [] = []
flatten (x:xs) = x ++ flatten (xs)

-- remove [1,4,5,3,4,5,1,2,7,4,2] [2,4]     ->    [1,5,3,5,1,7]

remove :: [Int]->[Int]->[Int]
remove ms [] = ms 
remove xs (y:ys) = remove (filter (/=y) xs)  ys

 
-- myLength [1,3..10]   ->  5

myLength :: [a] -> Int
myLength [] = 0
myLength (x:xs) = 1 + myLength(xs)


-- oddsNevens [1,4,5,3,4,5,1,2,7,4,2]  ->   ([1,5,3,5,1,7],[4,4,2,4,2])

oddsNevens :: [Int] -> [[Int]]
oddsNevens [] = [[],[]]
oddsNevens ms = [filter odd ms , filter even ms]

-- primeDivisors 255   ->  [3,5,17]

isPrime :: Int -> Bool
isPrime n = if n <= 1 then False
		      else prime n (n-1)

prime :: Int -> Int -> Bool
prime n m = if m == 1 then True
		      else if mod n m == 0 then False
			 else prime n (m-1)

primeDivisors :: Int -> [Int]
primeDivisors 0 = []
primeDivisors n = primeDivisors2 n (n)

primeDivisors2 :: Int -> Int -> [Int]
primeDivisors2 x 0 = []
primeDivisors2 x 1 = []
primeDivisors2 x n =  if (mod x (n) == 0 && isPrime (n)) then primeDivisors2 x (n-1) ++ [n]
	else primeDivisors2 x (n-1)

-- primeDivisors :: Int -> [Int]
-- primeDivisors x = filter p (llistaprimers 255)
--   where p y = x `mod` y == 0
 
-- llistaprimers :: Int -> [Int]
-- llistaprimers x = ( filter [2..(x `div`2)]
