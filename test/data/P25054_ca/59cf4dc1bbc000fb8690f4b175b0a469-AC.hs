myLength :: [Int] -> Int
myLength [] = 0
myLength (x:xs)
	| xs == [] = 1
	| otherwise = 1 + (myLength xs)

myMax :: Int -> Int -> Int
myMax a b
	| a > b = a
	| otherwise = b

myMaximum :: [Int] -> Int
myMaximum (x:xs)
	| xs == [] = x
	| otherwise = (max x (myMaximum xs))

sumTotal :: [Int] -> Int
sumTotal [] = 0
sumTotal (x:xs)
	| xs == [] = x
	| otherwise = x + (sumTotal xs)

average :: [Int] -> Float
average [] = 0
average (x:xs) = (fromIntegral (sumTotal (x:xs))) / (fromIntegral (myLength (x:xs)))

-- bpal l1 l2 = (inv l1) ++ l2
bpal :: [Int] -> [Int] -> [Int]
bpal (x:xs) (y:ys)
	| xs == [] = (x:y:ys)
	| otherwise = (bpal xs (x:y:ys))

buildPalindrome :: [Int] -> [Int]
buildPalindrome [] = []
buildPalindrome (x:xs) = bpal (x:xs) (x:xs)
	
remove :: [Int] -> [Int] -> [Int]
remove [] y = []
remove (x:xs) [] = (x:xs)
remove (x:xs) y
	| (elem x y) = (remove xs y)
	| otherwise = [x] ++ (remove xs y)

flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (x:xs) = x ++ (flatten xs)

oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens (x:xs)
	| (mod x 2) == 0 = (s, x:p)
	| otherwise = (x:s, p)
	where (s, p) = (oddsNevens xs)

isPrime2 :: Int -> Int -> Bool
isPrime2 d n
	| d == n = True
	| (mod n d) == 0 = False
	| otherwise = isPrime2 (d + 2) n

isPrime :: Int -> Bool
isPrime n
	| n <= 1 = False
	| n == 2 = True
	| n == 3 = True
	| (mod n 2) == 0 = False
	| otherwise = isPrime2 3 n

primeDivisors2 :: Int -> Int -> [Int]
primeDivisors2 x d
	| d > x = []
	| ((mod x d) == 0) && (isPrime d) = [d] ++ (primeDivisors2 x (d + 2))
	| otherwise = (primeDivisors2 x (d + 2))

primeDivisors :: Int -> [Int]
primeDivisors x
	| x <= 1 = []
	| (mod x 2) == 0 = [2] ++ (primeDivisors2 x 3)
	| otherwise = (primeDivisors2 x 3)
	























