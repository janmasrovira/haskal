-- myLength [1,3..10]

myLength :: [Int] -> Int
myLength l 
	| l==[]		= 0
	| otherwise = 1 + sum
	where sum = myLength (tail l)

-- myMaximum [4,3,1,5,4,5,2]

myMaximum :: [Int] -> Int
myMaximum (x:xs) 
	| xs==[]	= x 
	| x > max	= x
	| otherwise = max
	where max = myMaximum xs
	
-- average [1,2,3]

sumAll :: [Int] -> Int
sumAll (x:xs) 
	| xs==[]	= x 
	| otherwise = x + sum
	where sum = sumAll xs

average :: [Int] -> Float
average l = ((fromIntegral (sumAll l))/ (fromIntegral (myLength l)))

-- buildPalindrome [2,4,6] -> [6,4,2,2,4,6]

buildPalindrome :: [Int] -> [Int]
buildPalindrome l 
	| l==[]	= l 
	| otherwise = (a:(buildPalindrome b))++[a]
	where { a = last l; 
			b = init l }

-- remove [1,4,5,3,4,5,1,2,7,4,2] [2,4] -> [1,5,3,5,1,7]

filtro :: [Int] -> Int -> [Int]
filtro l n 
	| l==[]		= l
	| x==n 		= filtro xs n
	| otherwise = x:(filtro xs n)
	where (x:xs)= l


remove :: [Int] -> [Int] -> [Int]
remove l l2
	| l2==[]	= l
	| otherwise = remove l3 (tail l2)
	where l3 = filtro l (head l2)

-- flatten [[2,6],[8,1,4],[],[1]] -> [2,6,8,1,4,1]

flatten :: [[Int]] -> [Int]
flatten l
	| l==[]	= []
	| otherwise = x++(flatten xs)
	where (x:xs)= l

-- oddsNevens [1,4,5,3,4,5,1,2,7,4,2]

oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens l
	| l==[]			= ([],[])
	| mod x 2 == 0 	= (a,x:b)
	| otherwise		= (x:a,b)
	where {(x:xs)	= l;
		   (a,b)	= (oddsNevens xs)}

-- primeDivisors 255

isPrime :: Int -> Bool
isPrime n
	| n <= 1	= False
	| otherwise	= (isPrimeI n 2)

isPrimeI :: Int -> Int -> Bool
isPrimeI n i
	| i > (div n 2)		= True
	| (mod n i) == 0	= False  
	| otherwise			= (isPrimeI n (i+1))	

primeDivisors :: Int -> [Int]
primeDivisors n = (listPrime n 2)

listPrime :: Int -> Int -> [Int]
listPrime n m
	| m > n 			= [] 
	| not (isPrime m) 	= next	
	| (mod n m) > 0		= next
	| otherwise			= m:next
	where next 			= listPrime n (m+1)

-- Others


{-
contiene :: [Int] -> Int -> Bool
contiene (x:xs) n
	| xs==[]	= x==n
	| x==n 		= True
	| otherwise = contiene xs n

5
2.0
[6,4,2,2,4,6]
[2,6,8,1,4,1]
[1,5,3,5,1,7]
5
([1,5,3,5,1,7],[4,4,2,4,2])
[3,5,17]

-}
