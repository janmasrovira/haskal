myLength 		:: [Int] -> Int
myMaximum 		:: [Int] -> Int
average 		:: [Int] -> Float
buildPalindrome :: [Int] -> [Int]
remove			:: [Int] -> [Int] -> [Int]
removeElement	:: [Int] -> Int -> [Int]
flatten			:: [[Int]] -> [Int]
oddsNevens		:: [Int] -> ([Int],[Int])
primeDivisors	:: Int -> [Int]
takeListPrime	:: Int -> Int -> [Int]
nextPrime		:: Int -> Int
isPrime	:: Int -> Bool
primality :: Int -> Int -> Bool

myLength ln = length ln

myMaximum ln = maximum ln
		
average ln = fromIntegral(sum ln) / fromIntegral (length ln)
		  
buildPalindrome ln = reverse ln ++ ln

removeElement [] r = []
removeElement ln r | (head ln) == r = lm
				   | otherwise = [(head ln)] ++ lm
	where lm = removeElement (tail ln) r

remove ln [] = ln
remove ln lr = removeElement lm (head lr)
	where lm = remove ln (removeElement lr (head lr))

flatten [] = []
flatten lln = m
	where m = head lln  ++ flatten (tail lln)

oddsNevens [] = ([], [])
							 
oddsNevens ln | (head ln) `mod` 2 == 0 = (o, [(head ln)]++e)
			  | otherwise = ([(head ln)]++o, e)
	where (o, e) = oddsNevens (tail ln)
		  
isPrime 0 = False;
isPrime 1 = False;
isPrime n = primality n 2
primality n i | (i*i) > n = True
			  | n `mod` i == 0 = False
	 | otherwise = primality n (i+1)

nextPrime p | isPrime p = p
			| otherwise = nextPrime (p+1)
													
takeListPrime n p | p > n = []
				  |  n `mod` p == 0 = [p] ++ lp
	  | otherwise = lp
	where {
		lp = takeListPrime n np;
		np = nextPrime (p+1)
	}
	
					   
primeDivisors n = takeListPrime n 2