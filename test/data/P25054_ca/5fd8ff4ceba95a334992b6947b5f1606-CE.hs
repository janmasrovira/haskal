--Feu una funció myLength :: [Int] -> Int que, donada una llista d’enters no buida, calculi la seva llargada.
myLength :: [Int] -> Int
myLength [x] = 1
myLength (cap:cua) = 1 + myLength(cua);



--Feu una funció myMaximum :: [Int] -> Int que, donada una llista d’enters no buida, calculi el seu màxim.
myMaximum :: [Int] -> Int
myMaximum [x] = x
myMaximum (cap:cua)
    | cap > myMaximum(cua) = cap
    | otherwise = myMaximum(cua)
    

    

mySum :: [Int] -> Int
mySum [x] = x
mySum (cap:cua) = cap + mySum(cua)
    
--Feu una funció average :: [Int] -> Float que, donada una llista d’enters no buida, calculi la seva mitjana.
average :: [Int] -> Float
average [x] = fromIntegral x
average (cap:cua) = (fromIntegral(mySum(cap:cua)) / fromIntegral(myLength(cap:cua)))
    
-- Dado una lista de enteros, devolver la lista inversa
myReverse :: [Int] -> [Int]
myReverse [x] = [x]
myReverse (cap:cua) = myReverse cua ++ [cap]

--Feu una funció buildPalindrome :: [Int] -> [Int] que, donada una llista, retorni el palíndrom que comença amb la llista invertida.
buildPalindrome :: [Int] -> [Int]
buildPalindrome [x] = [x]
buildPalindrome l = myReverse l ++ l



-- Dado una lista de enteros y un enteros, elimina de la lista el entero
removeValue :: [Int] -> Int -> [Int]
removeValue [] x = []
removeValue (cap:cua) x
  | cap == x = removeValue cua x
  | otherwise = [cap] ++ removeValue cua x

-- Feu una funció remove :: [Int] -> [Int] -> [Int] que donada una llista d’enters x i una llista d’enters y, retorna la llista x havent eliminat totes les ocurrències dels elements en y.
remove :: [Int] -> [Int] -> [Int]
remove l [] = l
remove l (cap:cua) = remove (removeValue l cap) cua


myConcatList :: [Int] -> [Int] -> [Int]
myConcatList l [] = l
myConcatList l (cap:cua) = myConcatList (l++[cap]) cua

-- Feu una funció flatten :: [[Int]] -> [Int] que aplana una llista de llistes produint una llista d’elements.
flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (l1:l2) = myConcatList l1 (flatten l2) 

-- Concatena dos pares de listas de Int
myConcatListPair :: ([Int],[Int]) -> ([Int],[Int]) -> ([Int],[Int])
myConcatListPair p ([],[]) = p
myConcatListPair p p2 = ((myConcatList (fst p) (fst p2)), ((myConcatList (snd p) (snd p2))))


-- Dado un Int, lo clasifica en un Pair mirando si es par o impar
saveOddsNevens :: Int -> ([Int],[Int]) -> ([Int],[Int])
saveOddsNevens x p
  | odd x = ([x]++fst p, snd p)
  | otherwise = (fst p, [x]++snd p)
  
-- Feu una funció oddsNevens :: [Int] -> ([Int],[Int]) que, donada una llista d’enters, retorni dues llistes, una que conté els parells i una que conté els senars, en el mateix ordre relatiu que a l’original.
oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens (cap:cua) = saveOddsNevens cap (oddsNevens cua)



--Feu una funció isPrime :: Int -> Bool que, donat un natural, indiqui si aquest és primer o no.
isPrime :: Int -> Bool
isPrime n
    | n <= 1 = False
    | not(teFactor n (n-1)) = True
    | otherwise = False

    
teFactor :: Int -> Int -> Bool
teFactor n m
    | m <= 1 = False
    | n `mod` m == 0 = True
    | otherwise = teFactor n (m-1)

myDivisorsPrimesList :: Int -> Int -> [Int]
myDivisorsPrimesList n 0 = []
myDivisorsPrimesList n m
  | n `mod` m == 0 && isPrime m = myDivisorsPrimesList n (m-1) ++[m]
  | otherwise = myDivisorsPrimesList n (m-1)
    
myDivisorsPrimes :: Int -> [Int]
myDivisorsPrimes n = myDivisorsPrimesList n (n`div`2)

-- Feu una funció primeDivisors :: Int -> [Int] que retorni la llista de divisors primers d’un enter estrictament positiu.
primeDivisors :: Int -> [Int]
primeDivisors n = myDivisors n
