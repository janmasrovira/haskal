myLength::[Int] -> Int
myLength  [] = 0
myLength (_:xs) = myLength xs +1

myMaximum::[Int] -> Int
myMaximum [x] = x
myMaximum (x:xs) = max x (myMaximum xs)

average::[Int] -> Float
average x = fromIntegral (sum x) / (fromIntegral (myLength x))

buildPalindrome::[Int] -> [Int]
buildPalindrome x = girar x ++ x
	where
		girar [] = []
		girar (y:ys) = girar ys ++ [y]

remove::[Int] -> [Int] -> [Int]
remove [] _ = []
remove (x:xs) ys =
	if not (hies ys)
	then
		(x:resta)
	else
		resta
	where
		resta = remove xs ys
		hies [] = False
		hies (y:ys) = (x == y) || hies ys

oddsNevens::[Int] -> ([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens (x:xs)
	| odd x	=(x : senars, parells)
	| otherwise	= (senars, x : parells)
	where
		(senars,parells) = oddsNevens xs

primeDivisors::Int -> [Int]
primeDivisors n = divs n 2


divs 1 _ = []
divs n i
	| mod n i == 0 = i: divs (minDivide n) (i+1)
	| otherwise = divs n (i+1)
	where 
		minDivide n
			| mod n i == 0 = minDivide (quot n i)
			| otherwise = n

