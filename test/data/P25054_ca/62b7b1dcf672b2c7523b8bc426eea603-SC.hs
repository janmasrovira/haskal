myLength :: [Int]->Int
myLength [] = 0
myLength (x:xs) = 1+(myLength xs)

myMaximum :: [Int]->Int
myMaximum [x] = x
myMaximum (x:xs)
	| x > y = x
	| otherwise = y
	where y = myMaximum xs
	
average :: [Int]->Float
average xs = fromIntegral(suma(xs)) / fromIntegral(myLength(xs))

suma [] = 0
suma (x:xs) = x+(suma xs)

buildPalindrome :: [Int]->[Int]
buildPalindrome xs = revers xs ++ xs

revers :: [Int]->[Int]
revers [] = []
revers (x:xs) = revers xs ++ [x]

removeNum :: [Int]->Int->[Int]
removeNum [] x = []
removeNum (y:ys) x
	| x==y = removeNum ys x
	| otherwise = [y] ++ removeNum ys x 
	
remove :: [Int]->[Int]->[Int]
remove ys [] = ys
remove ys (x:xs) = remove (removeNum ys x) xs

flatten :: [[Int]]->[Int]
flatten [x] = x
flatten (x:xs) = x ++ flatten xs

oddsNevens :: [Int]->([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens (x:xs)
	| mod x 2 == 0 = (s, [x] ++ p)
	| otherwise = ([x] ++ s, p)
	where (s,p) = oddsNevens xs

primeDivisors :: Int -> [Int]
primeDivisors x = iprimeDivisors x 2

iprimeDivisors :: Int->Int->[Int]
iprimeDivisors 1 d = []
iprimeDivisors x d
	| mod x d == 0 = [d] ++ iprimeDivisors y d
	| otherwise = iprimeDivisors x c
	where 	y = div x d
		c = d+1
