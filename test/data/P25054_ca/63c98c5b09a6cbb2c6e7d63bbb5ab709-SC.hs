myLength :: [Int] -> Int
myLength [] = 0
myLength (e:l) = 1 + myLength l

myMaximum :: [Int] -> Int
myMaximum [e] = e
myMaximum (e:l)
	| e > e2 = e
	| otherwise = e2
	where e2 = myMaximum l

sumList :: [Int] -> Int
sumList [] = 0
sumList (e:l) = e + sumList l

average :: [Int] -> Float
average l = fromIntegral (div (fromIntegral (sumList l)) (fromIntegral (myLength l)))

buildPalindrome :: [Int] -> [Int]
buildPalindrome e = (reverseList e ++ e)

reverseList :: [Int] -> [Int]
reverseList [] = []
reverseList (e:l) = (reverseList l) ++ [e]

remove :: [Int] -> [Int] -> [Int]
remove [] y = []
remove (e:l) y 
	| has e y = remove l y
	| otherwise = e : remove l y 

has :: Int -> [Int] -> Bool
has i [] = False
has i (e:l) 
	| i == e = True
	| otherwise = has i l 

flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (l:ll) = l ++ flatten ll

oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens (e:l)
	| (e `mod` 2) == 0 = (odds,e:evens)
	| otherwise = (e:odds,evens)
	where (odds,evens) = oddsNevens l

divideall :: Int -> Int -> Int
divideall x n
	| (x `mod` n) == 0 = divideall (div x n) n
	| otherwise = x

primeaux :: Int -> Int -> [Int]
primeaux x n
	| (x == n) = []
	| (x == x2) = recur
	| otherwise = n : recur
	where {x2 = divideall x n; recur = primeaux x (n+1)}

primeDivisors :: Int -> [Int]
primeDivisors n = primeaux n 2
