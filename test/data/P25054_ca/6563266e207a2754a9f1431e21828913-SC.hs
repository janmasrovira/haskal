
absValue :: Int -> Int

absValue x
  | x >= 0	= x
  | otherwise	= -x
  
power :: Int -> Int -> Int

power a b
  | b == 0 	= 1
  | otherwise 	 = a * power a (b - 1)


isPrime :: Int -> Bool
 
isPrime n
  | n <= 1 = False
  | otherwise = noTeDivisors 2
  
  where
      noTeDivisors ::Int -> Bool

      noTeDivisors i
	| i*i > n = True
	| mod n i == 0 = False
	| otherwise =noTeDivisors (i+1)
	
slowFib :: Int -> Int

slowFib n
  | n <= 1 = n
  |otherwise = slowFib(n-1) + slowFib(n-2)
  
quickFib :: Int -> Int

quickFib i = fst (fib' i)

fib' :: Int -> (Int, Int)

fib' 0 = (0,0)
fib' 1 = (1,0)
fib' i = (x + y, x)
  where (x,y) = fib'(i-1)
	

myLength :: [Int] -> Int

myLength [] = 0
myLength (_:cua) = 1 + myLength cua

myMaximum :: [Int] -> Int

myMaximum (cap:cua) 
  | cua == [] = cap
  | cap > x= cap
  | otherwise = x
  where x = myMaximum(cua)
	
average :: [Int] -> Float

average xs = (fromIntegral (suma xs)) / (fromIntegral(myLength xs))
  where 
    suma [] = 0
    suma (x:xs) = x + suma xs
 
buildPalindrome :: [Int] -> [Int]

buildPalindrome xs = (girar xs) ++ xs
  where 
    girar [] = []
    girar (y:ys) = girar ys ++ [y]
    
    
remove :: [Int] -> [Int] -> [Int]

remove [] ys = []
remove (x:xs) ys = 
	if not (hies ys)
		      then x:resta
	else resta
  where 
	resta = remove xs ys
	hies [] = False
	hies (y:ys) = (x == y) || (hies ys)
	

flatten :: [[Int]] -> [Int]

flatten (y:[]) = y
flatten (x:xs) = x ++ flatten(xs) 


oddsNevens :: [Int] -> ([Int], [Int])
oddsNevens [] = ([],[])
oddsNevens (x:xs)
  | odd x	= (x: senars, parells)
  | otherwise 	= (senars, x : parells)
  where
      (senars, parells) = oddsNevens xs
      
primeDivisors :: Int -> [Int]

primeDivisors n = divisors n 2 0
  where
      divisors ::Int -> Int -> Int-> [Int]
      
      divisors n i l
	| n == 1 = []
	| (mod n i == 0) && (i > l) = i:divisors (div n i) i i
	| mod n i == 0 = divisors (div n i) i i
	| otherwise = divisors n (i+1) l
