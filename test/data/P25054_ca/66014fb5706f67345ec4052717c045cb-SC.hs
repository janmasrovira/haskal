--1
myMaximum :: [Int] -> Int
myMaximum (x:xs)
  | xs == [] = x
  | x > max = x
  | otherwise = max
  where max = myMaximum xs
	
--2
average :: [Int] -> Float
average xs = fromIntegral (sum xs) / fromIntegral (length xs)  
  
--6
myLength :: [Int] -> Int 
myLength [] = 0
myLength (x:xs) =  1 + myLength xs
