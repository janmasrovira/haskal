myLength :: [Int] -> Int
myLength [] = 0
myLength (_:xs) = 1 + myLength xs

myMaximum :: [Int] -> Int
myMaximum = foldl1 (\a x -> if x > a then x else a)

average :: [Int] -> Float
average l = suma l / (fromIntegral (myLength l))
    where suma [] = 0
          suma (x:xs) = fromIntegral x + suma xs

buildPalindrome :: [Int] -> [Int]
buildPalindrome l = reverse l  ++ l

remove :: [Int] -> [Int] -> [Int]
remove [] _ = []
remove (e:es) x
    | e `elem` x = remove es x
    | otherwise  = e : remove es x

flatten :: [[Int]] -> [Int]
flatten = concat      --    :^)

oddsNevens :: [Int] -> ([Int], [Int])
oddsNevens l = (filter odd l, filter even l)

primeDivisors :: Int -> [Int]
primeDivisors 2 = [2]
primeDivisors n = filter (\x -> esDivisor x && esPrimer x) (2:[3,5..n`div`2]++[n])
    where esDivisor d = n`mod`d == 0
          cand n = (2:[3, 5..floor $ sqrt (fromIntegral n)])
          esPrimer 1 = False
          esPrimer 2 = True
          esPrimer p = f p (cand p) -- p > 1
              where f _ []             = True
                    f p (d:ds)
                        | p`mod`d == 0 = False
                        | otherwise    = f p ds