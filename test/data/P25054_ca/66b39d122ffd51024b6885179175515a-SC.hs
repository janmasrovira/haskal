myLength :: [Int] -> Int
myLength [] = 0
myLength (x:xs)
  |xs == [] = 1
  |otherwise = (myLength xs)+1
  
{-
myLength :: [Int] -> Int
myLength [] = 0
myLength (x:xs)
  |xs == [] = 1
  |otherwise = (myLength xs)+1
  
myLength :: [Int] -> Int
myLength [] = 0
myLength (x:xs) =
  if xs == [] then 1
	      else (myLength xs)+1
	      
myLength :: [Int] -> Int
myLength x = length x

myLength :: [Int] -> Int
myLength [] = 0
myLength (x:xs) =
  case xs of
       [] -> 1
       xs -> (myLength xs)+1
-}

myMaximum :: [Int] -> Int
myMaximum [] = 0
myMaximum (x:xs)
  |x > myMaximum xs = x
  |otherwise = myMaximum xs
  
{-
myMaximum :: [Int] -> Int
myMaximum [] = 0
myMaximum (x:xs)
  |x > myMaximum xs = x
  |otherwise = myMaximum xs
  
myMaximum :: [Int] -> Int
myMaximum [] = 0
myMaximum (x:xs) =
  if x > myMaximum xs then x
  else myMaximum xs
  
myMaximum :: [Int] -> Int
myMaximum x = maximum x
-}

average :: [Int] -> Float
average x = (fromIntegral (mySum x)) / (fromIntegral (length x))

mySum :: [Int] -> Int
mySum [] = 0
mySum (x:xs) = x+mySum xs

buildPalindrome :: [Int] -> [Int]
buildPalindrome x = (myReverse x) ++ x

myReverse :: [Int] -> [Int]
myReverse [] = []
myReverse (x:xs) 
  |xs == [] = [x]
  |otherwise = (myReverse xs)++[x]
  
remove :: [Int] -> [Int] -> [Int]
remove x [] = x
remove x (y:ys) = (remove(removeNum x y) ys)

removeNum :: [Int] -> Int -> [Int]
removeNum [] y = []
removeNum (x:xs) y
  |x == y = removeNum xs y
  |otherwise = [x] ++ removeNum xs y
  
flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (x:xs) = x ++ (flatten xs)

oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens x = (inpares x, pares x)
  
pares :: [Int] -> [Int]
pares [] = []
pares (x:xs) = 
  if odd x then (pares xs)
	   else [x] ++ (pares xs)

inpares :: [Int] -> [Int]
inpares [] = []
inpares (x:xs) = 
  if odd x then [x] ++ (inpares xs)
	   else (inpares xs)

primeDivisors :: Int -> [Int]
primeDivisors 0 = []
primeDivisors 1 = []
primeDivisors x = getDivisors x 2
	   
getDivisors :: Int -> Int -> [Int]
getDivisors x y
  |y > x = []
  |mod x y == 0 && isPrime y = y:getDivisors x (y+1)
  |otherwise = getDivisors x (y+1)
	   
isPrime :: Int -> Bool
isPrime x =
  case x of
       0 -> False
       1 -> False
       x -> noDivisor x (x-1)

noDivisor :: Int -> Int -> Bool
noDivisor x y
  |y == 1 = True
  |mod x y == 0 = False
  |otherwise = noDivisor x (y-1)