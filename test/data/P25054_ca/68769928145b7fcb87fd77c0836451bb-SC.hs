-- myLength :: [Int] -> Int
myLength [] = 0
myLength (x:xs) = 1 + myLength xs
  
-- myLength :: [Int] -> Int
myMaximum [x] = x 
myMaximum (x:xs) = max x (myMaximum xs)

--sumaList :: [Int] - > Int
sumaList [x] = x
sumaList (x:xs) = x + sumaList xs

--average :: [Int] -> Float
average [x] = x
average (x:xs) = (x + sumaList xs) / (1 + myLength xs)
