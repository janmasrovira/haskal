myMaximum::[Int]->Int


myMaximum [x] = x
myMaximum (x:xs)
	| x>y = x
	| otherwise = y
	where y = myMaximum xs