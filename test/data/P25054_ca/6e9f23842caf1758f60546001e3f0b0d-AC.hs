
-- #1
-- retorna la llargada d'una llista
myLength :: [Int] -> Int
myLength []     = 0
myLength (x:xs) = 1 + (myLength xs)



-- #2
-- retorna el maxim d'una llista no buida
myMaximum :: [Int] -> Int
myMaximum (x:xs)
    | xs == []  = x
    | otherwise = max x (myMaximum xs)

    

-- #3
-- retorna la mitjana d'una llista no buida
average :: [Int] -> Float
average (x:xs) = fromIntegral(suma (x:xs)) / fromIntegral(myLength (x:xs))
    
suma :: [Int] -> Int
suma (x:xs)
    | xs == []  = x
    | otherwise = x + (suma xs)



-- #4
-- retorna el palindrom que comença amb la llista invertida
buildPalindrome :: [Int] -> [Int]
buildPalindrome []      = []
buildPalindrome (x:xs)  = invers (x:xs) ++ (x:xs)
    
invers :: [Int] -> [Int]
invers (x:xs)
    | xs == []  = [x]
    | otherwise = invers xs ++ [x]



-- #5
-- retorna la llista x eliminant les ocurrencies dels elements en y
remove :: [Int] -> [Int] -> [Int]
remove [] []        = []
remove [] (y:ys)    = []
remove (x:xs) []    = (x:xs)
remove (x:xs) (y:ys)
    | find x (y:ys) = remove xs (y:ys)
    | otherwise     = [x] ++ remove xs (y:ys)

find :: Int -> [Int] -> Bool
find x []       = False
find x (y:ys)
    | x == y    = True
    | otherwise = find x ys



-- #6
-- retorna una llista amb els elements de varies llistes
flatten :: [[Int]] -> [Int]
flatten []      = []
flatten (x:xs)
    | xs == []  = x
    | otherwise = x ++ flatten xs



-- #7
-- retorna dues llistes (senars,parells) dels elements d'una llista
oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens []   = ([],[])
oddsNevens (x:xs)
    | mod x 2 == 0  = (s,[x]++p)
    | otherwise     = ([x]++s,p)
    where (s,p) = oddsNevens xs



-- #8
-- retorna una llista de divisors primers d'un enter positiu
primeDivisors :: Int -> [Int]
primeDivisors n
    | n == 0    = []
    | n == 1    = []
    | otherwise = llista n 2

llista :: Int -> Int -> [Int]
llista n i
    | i > n                             = []
    | (mod n i == 0) && (primer i 2)    = [i] ++ l
    | otherwise                         = l
    where l = llista n (i+1)

primer :: Int -> Int -> Bool
primer n i
    | i >= n            = True
    | (mod n i) == 0    = False
    | otherwise         = primer n (i+1)