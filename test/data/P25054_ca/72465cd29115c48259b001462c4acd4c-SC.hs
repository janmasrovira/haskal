power :: Int -> Int -> Int
power x p
    | p == 0    = 1
    | otherwise = x * power x (p-1)

myLength :: [Int] -> Int
myLength [] = 0
myLength (_:cua) = 1 + myLength cua

myMaximum :: [Int] -> Int
myMaximum [x] = x
myMaximum (x:xs) = max x (myMaximum xs)

sumes :: [Int] -> Int
sumes [] = 0
sumes (x:xs) = x + sumes xs

average :: [Int] -> Float
average xs = (fromIntegral (sumes xs)) / (fromIntegral (myLength xs))

buildPalindrome :: [Int] -> [Int]
buildPalindrome [] = []
buildPalindrome (xs) = reverse xs ++ xs

hi_es :: Int -> [Int] -> Bool
hi_es x [] = False
hi_es x (y:ys) = x == y || hi_es x ys

remove :: [Int] -> [Int] -> [Int]
remove [] y = []
remove (x:xs) y
    | hi_es x y = remove xs y
    | otherwise = [x] ++ (remove xs y)

flatten :: [[Int]] -> [Int]
flatten xs = foldl (++) [] xs

oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens x = (filter odd x,filter even x)

isPrimeRec :: Int -> Int -> Bool
isPrimeRec n m
    | mod n m == 0  = False
    | power m 2 > n = True
    | otherwise = isPrimeRec n (m+2)

isPrime :: Int -> Bool
isPrime n
    | n == 1         =  False
    | n == 2         = True
    | n == 3        = True
    | mod n 2 == 0   =  False
    | otherwise = isPrimeRec n 3

divrec :: Int -> Int -> [Int]
divrec n m
    | mod n m == 0 && isPrime m = [m] ++ (divrec n (m+2))
    | power m 2 < n = divrec n (m+2)
    | otherwise = []

primeDivisors :: Int -> [Int]
primeDivisors n
    | isPrime n = [n]
    | mod n 2 == 0 = [2] ++ (divrec n 3)
    | otherwise = divrec n 3

