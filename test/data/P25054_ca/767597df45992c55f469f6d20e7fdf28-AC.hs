myLength :: [Int] -> Int
myLength [] = 0
myLength (x:xs) = 1 + myLength xs


myMaximum :: [Int] -> Int
myMaximum [x] = x
myMaximum (x:xs)
	| x > x2 = x
	| otherwise = x2
	where x2 = myMaximum xs
	
mySumAndLen :: [Int] -> (Int, Int)
mySumAndLen [x] = (x, 1)
mySumAndLen (x:xs) = 
    let (suma, long) = mySumAndLen xs in
    (suma + x, long + 1)

average :: [Int] -> Float
average xs = 
    let (suma, long) = mySumAndLen xs in
    (fromIntegral suma) / (fromIntegral long)


inv :: [Int] -> [Int]
inv [] = []
inv (x:xs) = (inv xs) ++ [x]

buildPalindrome :: [Int] -> [Int]
buildPalindrome xs = (inv xs) ++ xs

removeElement :: [Int] -> Int -> [Int]
removeElement [] _ = []
removeElement (x:xs) e
    | x == e = (removeElement xs e)
    | otherwise = x : (removeElement xs e)

remove :: [Int] -> [Int] -> [Int]
remove xs [] = xs
remove xs (y:ys) = remove (removeElement xs y) ys


flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (x:xs) = x ++ (flatten xs)


oddsNevens :: [Int] -> ([Int], [Int])
oddsNevens [] = ([], [])
oddsNevens (x:xs)
    | mod x 2 == 0 = (senars, x:parells)
    | otherwise = (x:senars, parells)
    where (senars, parells) = oddsNevens xs   


isPrime :: Int -> Bool
isPrime n
    | n < 2 = False
	| otherwise = not(isDivisible n 2 (n - 1))

isDivisible :: Int -> Int -> Int -> Bool
isDivisible n i f
    | i > f = False
	| otherwise = mod n i == 0 || isDivisible n (i + 1) f
    
primeDivisors :: Int -> [Int]
primeDivisors x = [s|s <- [1..x], (mod x s == 0) && (isPrime s)]
