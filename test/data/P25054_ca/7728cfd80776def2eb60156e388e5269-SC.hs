
 
-- llargada
myLength:: [Int] -> Int
myLength [] = 0
myLength (_ : xs) = 1 + myLength xs


-- myMaximum
myMaximum:: [Int] -> Int
myMaximum [x] = x
myMaximum (x:xs) = max x (myMaximum xs)

-- average
average:: [Int] -> Float
average xs = (fromIntegral (suma xs)) / (fromIntegral (myLength xs))
  where
      suma [] = 0
      suma (x:xs) = x + suma xs
      

-- buildPalindrome
buildPalindrome:: [Int] -> [Int]
buildPalindrome xs = (girar xs) ++ xs
  where
    girar [] = []
    girar (y:ys) = girar ys ++ [y]
    

-- remove
remove :: [Int] -> [Int] -> [Int]
remove [] y = []
remove (x:xs) y
    |conte y x  = remove xs y
    |otherwise  = [x] ++ remove xs y
    where
        conte:: [Int] -> Int -> Bool
        conte [] s = False
        conte (b:bs) s = (b == s) || conte bs s


-- flatten
-- flatten :: [[Int]] -> [Int]




-- oddsNevens
-- oddsNevens :: [Int] -> ([Int],[Int])




-- primeDivisors
-- primeDivisors :: Int -> [Int]






