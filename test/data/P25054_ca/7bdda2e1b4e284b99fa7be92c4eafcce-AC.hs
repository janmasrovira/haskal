
 
-- llargada
myLength:: [Int] -> Int
myLength [] = 0
myLength (_ : xs) = 1 + myLength xs


-- myMaximum
myMaximum:: [Int] -> Int
myMaximum [x] = x
myMaximum (x:xs) = max x (myMaximum xs)

-- average
average:: [Int] -> Float
average xs = (fromIntegral (suma xs)) / (fromIntegral (myLength xs))
  where
      suma [] = 0
      suma (x:xs) = x + suma xs
      

-- buildPalindrome
buildPalindrome:: [Int] -> [Int]
buildPalindrome xs = (girar xs) ++ xs
  where
    girar [] = []
    girar (y:ys) = girar ys ++ [y]
    

-- remove
remove :: [Int] -> [Int] -> [Int]
remove [] y = []
remove (x:xs) y
    |conte y x  = remove xs y
    |otherwise  = [x] ++ remove xs y
    where
        conte:: [Int] -> Int -> Bool
        conte [] s = False
        conte (b:bs) s = (b == s) || conte bs s


-- flatten
flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (x:xs) = x ++ flatten xs



-- oddsNevens
oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens (x:xs)
    |mod x 2 == 0   = (s, [x] ++ p)
    |otherwise      = ([x] ++ s, p)
    where (s,p) = oddsNevens xs



-- primeDivisors
primeDivisors :: Int -> [Int]
primeDivisors n = divisors n 2 0
    where
        divisors ::Int -> Int -> Int-> [Int]
        divisors n i l
            |n == 1 = []
            |(mod n i == 0) && (i > l) = i:divisors (div n i) i i
            |mod n i == 0 = divisors (div n i) i i
            |otherwise = divisors n (i+1) l



