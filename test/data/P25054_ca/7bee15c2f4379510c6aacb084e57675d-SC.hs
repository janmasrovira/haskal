myLength::[Int]->Int
myLength [] = 0
myLength (x:y) = 1 + myLength y
-- y es la resta de la llista, x es el primer element

myMaximumWeird::[Int]->Int
myMaximumWeird (x:y)
    | y == [] = x   --si la llista següent esta buida llavors es el primer element
    | otherwise = if x > myMaximumWeird y then x 
                                   else myMaximumWeird y

myMaximum::[Int]->Int
myMaximum [x] = x --llista d'un element es x el màxim
myMaximum (x:y) = max x (myMaximum y)
                     
                     
mySum::[Int]->Int
mySum [] = 0
mySum (x:y) = x + (mySum y)
                                   
average::[Int]->Float
average l = (fromIntegral (mySum l)) / (fromIntegral (myLength l))

reverseList:: [Int] -> [Int]
reverseList [x] = [x]
reverseList (x:xs) = reverseList xs ++ [x]

buildPalindrome::[Int]->[Int]
buildPalindrome l = reverseList l ++ l

isInList::Int->[Int]->Bool
isInList i (x:xs)  
    | i == x = True
    | xs == [] = False
    | otherwise = isInList i xs

remove::[Int]->[Int]->[Int]
remove [] l = []
remove (x:xs) l 
    | isInList x l = remove xs l
    | otherwise = (x: remove xs l)
    
flatten::[[Int]] -> [Int]
flatten [] = []
flatten (x:xs) = x ++ flatten xs

getEven::[Int]->[Int]
getEven [] = []
getEven (x:xs) = if (mod x 2) == 0 then [x] ++ getEven xs
                                   else getEven xs

oddsNevens::[Int]->([Int],[Int])
oddsNevens l = (filter odd l, getEven l)

