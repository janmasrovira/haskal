myLength :: [Int] -> Int

myLength [] = 0
myLength (x:xs) = 1 + myLength(xs)

myMaximum :: [Int] -> Int
myMaximum [x] = x
myMaximum (x:xs) = max x (myMaximum xs)

sumaLlista :: [Int] -> Int
sumaLlista [] = 0
sumaLlista (x:xs) = x + sumaLlista xs

average :: [Int] -> Float
average [] = 0
average l = fromIntegral (sumaLlista l) / fromIntegral(myLength l)

listaInversa :: [Int] -> [Int]
listaInversa [] = []
listaInversa (x:xs) = listaInversa xs ++ [x]

buildPalindrome :: [Int] -> [Int]
buildPalindrome l = listaInversa l ++ l

removeItem :: [Int] -> Int -> [Int]
removeItem [] x = []
removeItem (x:xs) y
    | x == y = removeItem xs y
    | otherwise = (x : removeItem xs y)

remove :: [Int] -> [Int] -> [Int]
remove x [] = x
remove x (y:ys) = remove yk ys where yk = removeItem x y

flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (xl:ll) = xl ++ (flatten ll)

ordena :: ([Int], [Int]) -> [Int] -> ([Int], [Int])
ordena (x,y) [] = (x,y)
ordena (x,y) (z:zs)
    | mod z 2 == 0 = ordena (x,y++[z]) zs
    | otherwise = ordena (x++[z],y) zs

oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens l = ordena ([],[]) l

divisor y d 
    | d == y = d
    | mod y d == 0 = d
    | otherwise = divisor y (d + 1)

primeDivisors :: Int -> [Int]
primeDivisors y
    | yk == y = [y]
    | otherwise = [yk] ++ primeDivisors (div y yk) where yk = divisor y 2


