myLength :: [Int] -> Int
myLength [] = 0
myLength (x:xs) = 1 + (myLength xs)

myMaximum :: [Int] -> Int
myMaximum [x] = x
myMaximum (x:xs) = max x (myMaximum xs)

average :: [Int] -> Float
average xs = (fromIntegral (suma xs))/(fromIntegral (myLength xs))

suma :: [Int] -> Int
suma [x] = x
suma (x:xs) = x + suma xs

buildPalindrome :: [Int] -> [Int]
buildPalindrome [] = []
buildPalindrome xs = (invertirLista xs) ++ xs

invertirLista :: [Int] -> [Int]
invertirLista [x] = [x]
invertirLista (x:xs) = (invertirLista xs) ++ [x]

remove :: [Int] -> [Int] -> [Int]
remove xs [] = xs
remove (x:xs) (y:ys) 
  | llistaContains ([x] ++ xs) y = remove (creaLlistaSense ([x] ++ xs) y) ys
  | otherwise = remove ([x] ++ xs) ys
  
llistaContains :: [Int] -> Int -> Bool
llistaContains [x] y
  | x == y = True
  | otherwise = False
llistaContains (x:xs) y
  | x == y = True
  | otherwise = llistaContains xs y

  -- creaLlistaSense devuelve la lista de entrada sin el elemento Int, sólo una vez. Si hay más de uno sólo quitará el primero!!!
creaLlistaSense :: [Int] -> Int -> [Int]
creaLlistaSense [x] y
  | x == y = []
  | otherwise = [x]
creaLlistaSense (x:xs) y
  | x == y = xs
  | otherwise = [x] ++ (creaLlistaSense xs y)