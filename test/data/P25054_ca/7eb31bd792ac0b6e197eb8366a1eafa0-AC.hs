myLength :: [Int] -> Int
myLength l =
	length l

myMaximum :: [Int] -> Int
myMaximum l = 
	maximum l

average :: [Int] -> Float
average [] = 0;
average (x:xs) = fromIntegral ((suma xs) + x) / fromIntegral ((length xs) + 1)

suma [] = 0
suma (x:xs) = x + suma xs

buildPalindrome :: [Int] -> [Int]
buildPalindrome l =
	l2 ++ l
	where l2 = reverse l

flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (x:xs) = x ++ (flatten xs)

remove :: [Int] -> [Int] -> [Int]
remove [] [] = []
remove [] (x:xs) = []
remove (x:xs) [] = x : xs
remove (x:xs) (y:ys) = remove (busca (x : xs) y) ys

busca :: [Int] -> Int -> [Int]
busca [] m = []
busca (x : xs) m = if x == m then (busca xs m)
	else x : (busca xs m)


oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens (x:xs)
	| mod x 2 == 0 = ((fst (oddsNevens xs)), (x : (snd (oddsNevens xs))))
	| otherwise    = ((x : (fst (oddsNevens xs))), (snd (oddsNevens xs)))

primeDivisors :: Int -> [Int]
primeDivisors n
	| n <= 1 	= []
	| otherwise = divisor n 2
	where ls = []

divisor :: Int -> Int -> [Int]
divisor n m
	| m > n 	= []
	| otherwise = if mod n m == 0 && isPrime m then m : (divisor n (m + 1))
	else divisor n (m + 1) 
	

isPrime :: Int -> Bool
isPrime n
	| n <= 1    = True
	| otherwise = prime n (div n 2)

prime :: Int -> Int-> Bool
prime n m
	| m <= 1    = True 
	| y == 0    = False
	| otherwise = prime n (m-1)
	where y = mod n m