myLength :: [Int] -> Int
myLength [] = 0
myLength (x:xs) = 1  + myLength(xs)

myMaximum :: [Int] -> Int
myMaximum [] = 0
myMaximum(x:[]) = x
myMaximum(x:xs) = max x (myMaximum(xs))

suma :: [Int] -> Int
suma[] = 0
suma(x:xs) = x + suma(xs)

average :: [Int] -> Float
average [] = 0.0
average(x:xs) = fromIntegral(suma(x:xs)) / fromIntegral(myLength(x:xs) )

revers :: [Int] -> [Int]
revers[] = []
revers(x:xs) = (revers(xs) ++ [x])

buildPalindrome :: [Int] -> [Int]
buildPalindrome [] = []
buildPalindrome(x:xs) = (revers(x:xs) ++ (x:xs))

remove_element :: [Int] -> Int -> [Int]
remove_element []y = []
remove_element (x:xs) y
	| x == y = remove_element xs y
	| otherwise = [x] ++ (remove_element xs y)

remove :: [Int] -> [Int] -> [Int]
remove x [] = x
remove x (y:ys) = (remove (remove_element x y) ys)	

flatten::[[Int]] -> [Int]
flatten[] = []
flatten(x:xs) 
	| x == [] = flatten(xs)	
	| otherwise = x ++ (flatten(xs))

oddsNevens :: [Int] -> ([Int], [Int])
oddsNevens[] = ([],[])
oddsNevens(x:xs) 
	| odd x = ([x] ++ fst(oddsNevens(xs)), snd(oddsNevens(xs)))
	| otherwise = (fst(oddsNevens(xs)), [x] ++ snd(oddsNevens(xs)))
	
primeDivisors :: Int -> [Int]
primeDivisors x 
	| isPrime(x) = [x]
	| otherwise = trobaDivisor x 2
	

trobaDivisor :: Int -> Int -> [Int]
trobaDivisor x y
		| x == 1 = []
		| (mod x y == 0) && (isPrime(y) == True) = [y]  ++ (trobaDivisor (quot x y) y)
		|otherwise = trobaDivisor x (y+1)	
isPrime :: Int -> Bool
isPrime n
  | n < 1 = False
  | otherwise = noTeDivisors 2
  
  where
  noTeDivisors :: Int -> Bool

  noTeDivisors  i
    | i*i > n = True
    |mod n i == 0  = False
    |otherwise 	= noTeDivisors  (i+1)
