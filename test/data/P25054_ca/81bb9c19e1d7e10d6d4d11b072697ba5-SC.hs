myLength :: [Int] -> Int

myLength [] 	= 0
myLength (x:xs) = 1 + myLength xs

-----------------------------



-----------------------------

average :: [Int] -> Float

average l = fromIntegral (mySum l) / fromIntegral (myLength l)
	where
		mySum []		= 0
		mySum (x:xs)	= x + (mySum xs)

-----------------------------

buildPalindrome	:: [Int] -> [Int]

buildPalindrome l = (myReverse l) ++ l
	where
		myReverse []	= [] 
		myReverse [x] 	= [x]
		myReverse (x:xs)	= (myReverse xs) ++ [x]

------------------------------

remove :: [Int] -> [Int] -> [Int]

remove l1 []		= l1
remove l1 (x:xs) 	= remove (removeOne l1 x) xs
	where
		removeOne [] i		= []
		removeOne (x:xs) i
			| x == i 		= removeOne xs i
			| otherwise		= (x:(removeOne xs i))

-------------------------------

flatten :: [[Int]] -> [Int]

flatten [] 		= []
flatten (x:xs)	= (x++flatten(xs))

-------------------------------



-------------------------------

primeDivisors :: Int -> [Int]

primeDivisors 1 = [1]
primeDivisors n = worker n 2
	where
		worker n i
			| n < i 			= []
			| (mod n i) == 0	= (i:(worker (reduce n i) (i+1)))
			| otherwise 		= worker n (i+1)

		reduce n i
			| (mod n i) == 0 	= reduce (div n i) i
			| otherwise			= n