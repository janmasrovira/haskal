
myMaximum :: [Int] -> Int
myMaximum [x] = x
myMaximum (x:xs) = max x $ myMaximum xs


average :: [Int] -> Float
average xs = (fromIntegral $ sum xs) / (fromIntegral $ length xs)


buildPalindrome :: [Int] -> [Int]
buildPalindrome xs = (reverse xs) ++ xs


flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (x:xs) = x ++ (flatten xs)


remove :: [Int] -> [Int] -> [Int]
remove xs [] = xs
remove xs (y:ys) = remove (removeOne xs y) ys
    where
        removeOne [] _ = []
        removeOne (a:as) b
            | a == b    = removeOne as b
            | a /= b    = a:removeOne as b


oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens xs = (filter odd xs, filter even xs)



myLength :: [Int] -> Int
myLength [] = 0
myLength (x:xs) = 1 + myLength xs



primeDivisors :: Int -> [Int]
primeDivisors n = [x | x <- [1..n], mod n x == 0, isPrime x]


isPrime :: Int -> Bool
isPrime 0 = False
isPrime 1 = False
isPrime n = not $ findDivisor 2
    where
        findDivisor i
            | i >= n        = False
            | otherwise     = mod n i == 0 || findDivisor (i+1)


