
--myLength :: [Int] -> Int

myLength [] = 0
myLength (x:xs) = 1+(myLength xs)

--myMaximum :: [Int] -> Int

myMaximum [] = 0
myMaximum (x:xs) = (maximLlista x xs)

maximLlista m [] = m
maximLlista m (x:xs)
    |x>m = (maximLlista x xs)
    |otherwise = (maximLlista m xs)

--average :: [Int] -> Float

average [] = 0
average (s) = (mySum s)/(myLength s)
mySum [] = 0
mySum (x:xs) = x+(mySum xs)

--buildPalindrome :: [Int] -> [Int]

buildPalindrome [] = []
buildPalindrome (xs) = (invertir xs xs)

invertir [] ys = ys
invertir (x:xs) (ys) = (invertir xs (x:ys))

{--invertir2 l1 l2 = (inv l1) ++ l2
invertir [] = []
invertir (x:xs) = (invertir xs)++[x]
--}

--remove :: [Int] -> [Int] -> [Int]

remove [][] = []
remove (x:xs) [] = (x:xs)
remove [] (y:ys) = []
remove (x:xs)(y:ys)
    |borrarelem x (y:ys) = remove xs (y:ys)
    |otherwise = x:remove xs (y:ys)

borrarelem x [] = False
borrarelem x (y:ys)
    |x==y = True
    |otherwise = borrarelem x ys

--flatten :: [[Int]] -> [Int]

--flatten [[]] = []

--oddsNevens :: [Int] -> ([Int],[Int])

oddsNevens (xs) = (clasificar (invertir xs []) [] [])

clasificar [] ss ps = (ss,ps) 
clasificar (x:xs) ss ps
    |esparell x = clasificar xs ss (x:ps)
    |otherwise = clasificar xs (x:ss) ps
  
esparell x
    |(x`mod`2)==0 = True
    |otherwise = False

--primeDivisors :: Int -> [Int]

primeDivisors x
    |x<2 = []
    |otherwise = getdivisors x 2
    
getdivisors x y
    |z==x = getdivisors x (y+1)
    |otherwise = y:(getdivisors z (y+1))
    where z = reduce x y
    
reduce x y
    |(x`mod`y)==0 = reduce (x`div`y) y
    |otherwise = x

{--
  FER VERSIO ALTERNATIVA SENSE GUARDAR PARAMETRES NI FER TANTES CONSULTES
  buscar tots els divisors del nostre valor i despres quedar-nos amb els que
  siguin primers

primeDivisors x
    |x<2 = []    
    |otherwise = checkprimedivs(getdivisors x 2 [])
    
checkprimedivs [] = []
checkprimedivs (z:zs)
    |isPrime z = z:(checkprimedivs zs)
    |otherwise = checkprimedivs zs

getdivisors x y zs
    |y>x = zs
    |(x`mod`y)==0 = getdivisors (reduce x y) 2 (y:zs)
    |otherwise = getdivisors x (y+1) zs

reduce x y
    |(x`mod`y)==0 = reduce (x`div`y) y
    |otherwise = x

isPrime x
    |x==0 = False
    |x==1 = False
    |x==2 = True 
    |otherwise = (checkDiv x 2)
    
checkDiv x d
    |(x`mod`d)==0 = False
    |d>(x`div`2) = True
    |otherwise = (checkDiv x (d+1))
----------------
primeDivisors x
    |x<2 = []
    |otherwise = (checkprimedivs x (x-1) [])

checkprimedivs x y zs
    |y<2 = zs
    |(isPrime y)&&((x`mod`y)==0) = (checkprimedivs x (y-1) (y:zs))
    |otherwise = checkprimedivs x (y-1) zs

isPrime x
    |x==0 = False
    |x==1 = False
    |x==2 = True 
    |otherwise = (checkDiv x 2)
    
checkDiv x d
    |(x`mod`d)==0 = False
    |d>(x`div`2) = True
    |otherwise = (checkDiv x (d+1))
--} 



