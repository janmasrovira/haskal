-- myLength :: [Int] -> Int

myLength [] = 0
myLength (x:xs) = 1 + myLength xs

-- myMaximum :: [Int] -> Int

myMaximum [x] = x
myMaximum (x:xs)
	|x > maxEnd = x
	|otherwise = maxEnd
	where maxEnd = myMaximum xs

-- average :: [Int] -> Float

sumOfAll [] = 0.0
sumOfAll (x:xs) = x + sumOfAll xs

average [] = 0.0
average l = sumOfAll l / myLength l


-- buildPalindrome :: [Int] -> [Int]

myReverse [] = []
myReverse (x:xs) = myReverse xs ++ [x]

buildPalindrome l = myReverse l ++ l

-- remove :: [Int] -> [Int] -> [Int]

remove_n [] _ = []
remove_n (x:xs) y
	|x == y = remove_n xs y
	|otherwise = x:(remove_n xs y)

remove x [] = x
remove x (y:ys) = (remove (remove_n x y) ys)

-- flatten [[Int]] -> [Int]

flatten l = concat l

-- oddsNevens :: [Int] -> ([Int],[Int])

oddsNevens l = (a, b)
	where 
		a = filter odd l
		b = filter even l

-- primeDivisors Int -> [Int]

isPrimei n m
	| m <= 1  = True 
	| otherwise = let x = mod n m
		in if (x == 0) then False
		   else isPrimei n (m-1)



isPrime n = 
	if n <= 1 then False
	else isPrimei n (n-1)

primeDivisors_i n m
    |n <= 1  = []
    |isPrime n = [n]
    |(isPrime m) && (mod n m == 0) = (primeDivisors_i (div n m) m) ++ [m]
    |otherwise = primeDivisors_i n (m-1) 

primeDivisors n
	|n <= 1  = []
	|isPrime n  = [n]
	|otherwise = primeDivisors_i n (n-1)














