sizeL [] n = n
sizeL (x:xs) n = sizeL xs n+1

myLength [] = 0
myLength (x:xs) = sizeL xs 1

myMaximum (x:[]) = x
myMaximum (x:xs) = max x (myMaximum xs)

buildPalindrome x = palindrome x [] ++ x

palindrome [] l = l
palindrome (x:xs) l  = (palindrome xs (x:l))

suma (x:[]) = x
suma (x:xs) = x + suma xs

average x  = (fromIntegral $ suma x)  / (fromIntegral $  myLength x)

belongs x [] = False
belongs x (y:ys)
        | x == y = True
        | otherwise = belongs x ys
        

remove [] _ = []
remove (x:xs) ys
       | belongs x ys = remove xs ys 
       | otherwise = (x:(remove xs ys))

flatten [] = []
flatten (x:xs) = x ++ flatten xs

oddsNevens [] = ([],[])
oddsNevens (x:xs)
           | x `mod` 2 == 0 = ((x:(fst n)),snd n)
           | otherwise = (fst n,(x:(snd n)))
           where n = oddsNevens xs

exhaustDivisor n k
               | (mod n k) == 0  = exhaustDivisor (div n k) k
               | otherwise = n

primeFactors n i list
    |n < i = list    
    |(n `mod` i) == 0 = (i:(primeFactors (exhaustDivisor n i) (i+1) list))
    |otherwise = primeFactors n (i+1) list

primeDivisors n = primeFactors n 2 []
