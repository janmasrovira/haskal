myMaximum :: [Int] -> Int
myMaximum [x] = x
myMaximum (x:xs) = max x (myMaximum xs)

average :: [Int] -> Float
average xs = fromIntegral suma / fromIntegral llargada
  where suma = sum xs
	llargada = myLength xs

myLength :: [Int] -> Int
myLength [] = 0
myLength (cap:cua) = 1 + myLength cua

buildPalindrome :: [Int] -> [Int]
buildPalindrome [] = []
buildPalindrome (x:xs) = (giraLlista xs)++[x]++[x]++xs

giraLlista :: [Int] -> [Int]
giraLlista [] = []
giraLlista [x] = [x]
giraLlista (x:xs) = (giraLlista xs)++[x]

remove :: [Int] -> [Int] -> [Int]
remove xs [] = xs
remove [] ys = []
remove xs (y:ys) = remove (esborra y xs) ys

esborra :: Int -> [Int] -> [Int]
esborra y [] = []
esborra y (x:xs) 
    | y /= x = x:(esborra y xs)
    | otherwise = esborra y xs

flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (x:xs) = x++(flatten xs)

oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens xs = (senars xs, parells xs)

parells [] = []
parells (x:xs)
    | (x `mod` 2) == 0 = [x]++(parells xs)
    | otherwise = parells xs

senars [] = []
senars (x:xs)
    | (x `mod` 2) == 1 = [x]++(senars xs)
    | otherwise = senars xs

primeDivisors :: Int -> [Int]
primeDivisors x 
    | x > 2 = divisor x 2
    | otherwise = []

divisor :: Int -> Int -> [Int]
divisor x y
    | x == y = []
    | ((x `mod` y) == 0) && (primer y (y-1)) = [y]++(divisor x (y+1))
    | otherwise = divisor x (y+1)

primer :: Int -> Int -> Bool
primer n m
    | m == 1 = True
    | mod n m == 0 = False
    | otherwise = primer n (m-1)









