myLength :: [Int] -> Int 
myLength [] = 0
myLength (_:xs) = myLength xs + 1

myMaximum :: [Int] -> Int 
myMaximum [x] = x
myMaximum (x:xs) = max x (myMaximum xs)

average :: [Int] -> Float
average xs  = 
   (/) (fromIntegral (sum xs)) (fromIntegral(myLength xs))
  
--buildPalindrome :: [Int] -> [Int] 
--remove :: [Int] -> [Int] -> [Int] 
--flatten :: [[Int]] -> [Int] 
--oddsNevens :: [Int] -> ([Int],[Int])
--primeDivisors :: Int -> [Int] 


