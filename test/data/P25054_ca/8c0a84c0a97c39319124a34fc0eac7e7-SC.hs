
myLength :: [Int] -> Int
-- myLength [] = 0
-- myLength (x:xs) = 1 + (myLength xs) 
myLength ls = 
    case ls of
        [] -> 0
        (hd:tl) -> 1 + (myLength tl)

myMaximum :: [Int] -> Int
myMaximum (x:[]) = x
myMaximum (hd:tl) = 
    let x = myMaximum(tl)
    in if hd > x then hd else x

myLeftReduce f def ls = 
    case ls of
        []      -> def
        [x]     -> x
        (x:xs)  -> f x (myLeftReduce f def xs)
        
mySum = myLeftReduce (+) 0 

average :: [Int] -> Float
average ls = (fromIntegral (mySum ls))/(fromIntegral (myLength ls)) 

myReverse ls = 
    let 
        rev [] acc = acc
        rev (x:xs) acc = rev xs (x:acc)
    in
        rev ls []

remove :: [Int] -> [Int] -> [Int]
remove x y = 
    let 
        rem [] e acc = acc
        rem (z:zs) e acc = 
            let rec = rem zs e acc
            in 
                if z == e then rec
                else (z:rec)
    in 
        case y of
            []      -> x
            (hd:tl) -> remove (rem x hd []) tl

flatten :: [[Int]] -> [Int]
flatten ls = 
    let 
        g [] l2 = l2
        g (x:xs) l2 = (x:(g xs l2))
        f [] acc = acc
        f (l:ll) acc = g l (f ll acc)
    in
        f ls []

buildPalindrome :: [Int] -> [Int]
buildPalindrome ls = flatten [(myReverse ls), ls]

oddsNevens :: [Int] -> ([Int], [Int])
oddsNevens [] = ([], [])
oddsNevens (x:xs) =
    let
        rec = oddsNevens xs
        odd = fst rec
        even = snd rec
    in 
        if (mod x 2) == 0
        then (odd, x:even)
        else (x:odd, even)

isDivisibleBy :: Int -> Int -> Bool
isDivisibleBy x y = (mod x y) == 0

hasPrimeDivisorLessThan :: Int -> Int -> Bool
hasPrimeDivisorLessThan x 1 = False
hasPrimeDivisorLessThan x y = isDivisibleBy x y || hasPrimeDivisorLessThan x (y - 1) 

isPrime :: Int -> Bool
isPrime 1 = False
isPrime 2 = True
isPrime x = not (hasPrimeDivisorLessThan x (x - 1))
    
primeDivisors :: Int -> [Int]
primeDivisors x = 
    let 
        primeDivisorsGreaterThan 1 _ = []
        primeDivisorsGreaterThan y n
            | isDivisibleBy y n     = (n:(primeDivisorsGreaterThan (div y n) n))
            | otherwise             = primeDivisorsGreaterThan y (n + 1)
    in
        primeDivisorsGreaterThan x 2


            
