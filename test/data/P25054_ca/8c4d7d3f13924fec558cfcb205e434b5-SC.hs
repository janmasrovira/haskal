myLength :: [Int] -> Int
myLength [] = 0
myLength (_:xs) = 1 + myLength xs

myMaximum :: [Int] -> Int
myMaximum [x] = x
myMaximum (x:xs) = max x (myMaximum xs)
  where
    max :: Int -> Int -> Int
    max x y
      |x > y = x
      |otherwise = y
      
average :: [Int] -> Float
average l = (fromIntegral (sumaL l)) / (fromIntegral (myLength l))
  where
    sumaL :: [Int] -> Int
    sumaL [] = 0
    sumaL [x] = x
    sumaL (x:xs) = x + sumaL xs
    
buildPalindrome :: [Int] -> [Int]
buildPalindrome [] = []
buildPalindrome l = (inv l) ++ l
  where
    inv :: [Int] -> [Int]
    inv [] = []
    inv (x:xs) = (inv xs) ++ [x]
