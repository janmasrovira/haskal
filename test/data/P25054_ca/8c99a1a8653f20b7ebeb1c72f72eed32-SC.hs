
myMaximum :: [Int] -> Int
myMaximum [x] = x
myMaximum (x:xs) = max x $ myMaximum xs


average :: [Int] -> Float
average xs = (i2f $ sum xs) / (i2f $ length xs)
    where i2f x = fromIntegral x :: Float


buildPalindrome :: [Int] -> [Int]
buildPalindrome xs = (reverse xs) ++ xs


flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (x:xs) = x ++ (flatten xs)


insert :: [Int] -> Int -> [Int]
insert [] x = [x]
insert (y:ys) x
    | y < x     = y : insert ys x
    | otherwise = x : y : ys



oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens xs = (filter odd xs, filter even xs)



myLength :: [Int] -> Int
myLength [] = 0
myLength (x:xs) = 1 + myLength xs



primeDivisors :: Int -> [Int]
primeDivisors n = [x | x <- [1..n], mod n x == 0, isPrime x]


isPrime :: Int -> Bool
isPrime 0 = False
isPrime 1 = False
isPrime n = not $ findDivisor 2
    where
        findDivisor i
            | i >= n        = False
            | otherwise     = mod n i == 0 || findDivisor (i+1)


