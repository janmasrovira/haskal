myLength::[Int]->Int
myLength [] = 0
myLength (x:xs) = 1 + (myLength xs)

myMaximum::[Int]->Int
myMaximum [x] = x
myMaximum (x:xs) = max x (myMaximum xs)

sumar :: [Int] -> Int
sumar [] = 0
sumar (x:xs) = x+(sumar xs)

average::[Int]->Float
average (x:xs) = fromIntegral (sumar(x:xs)) / fromIntegral (myLength (x:xs))

invert::[Int]->[Int]
invert [e] = [e]
invert (e:es) =  (invert es)++[e]

buildPalindrome::[Int]->[Int]
buildPalindrome (x:xs) = (invert (x:xs))++(x:xs)

removeElem::[Int]->Int->[Int]
removeElem [] y = []
removeElem [x] y
  | x==y = []
  | otherwise = [x]
removeElem (x:xs) y = (removeElem [x] y)++(removeElem xs y)

remove::[Int]->[Int]->[Int]
remove [] _ = []
remove (x:xs) [] = (x:xs)
remove (x:xs) (y:ys) = remove (removeElem (x:xs) y) ys

--flatten::[[Int]]->[Int]