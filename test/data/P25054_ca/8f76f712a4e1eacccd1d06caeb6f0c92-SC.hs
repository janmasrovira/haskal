myLength [] = 0
myLength (x:l) = 1 + (myLength l)

myMaximum [x] = x
myMaximum (x:l) 
	| a > x = a
	| otherwise = x
	where a = (myMaximum l)

auxaverage [x] = (x, 1)
auxaverage (x:l) = ((x + (fst llista) * n) / (n + 1), n + 1)
					where llista = (auxaverage l);
							n = (snd llista)
average [x] = x
average (x:l) = (x + (fst llista) * n) / (n + 1)
				where llista = (auxaverage l);
						n = (snd llista)

buildReversePalindrome [] = []
buildReversePalindrome (a:l) = [a]++(buildReversePalindrome l)++[a]

buildPalindrome llista = buildReversePalindrome (reverseP llista)

reverseP [] = []
reverseP (x:l) = (reverseP l)++[x]

-- remove [a] [b]