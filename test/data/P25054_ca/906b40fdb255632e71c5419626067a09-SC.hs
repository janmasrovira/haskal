myLength :: [Int] -> Int
myLength [] = 0
myLength x = 1 + myLength (tail x)

myMaximum :: [Int] -> Int
myMaximum [x] = x
myMaximum x = max (head x) (myMaximum (tail x))

average :: [Int] -> Float
average x = (fromIntegral (foldl (+) 0 x)) / (fromIntegral (myLength x))

buildPalindrome :: [Int] -> [Int]
buildPalindrome x = palindrome x []

palindrome [] y = y
palindrome (x:xs) y = palindrome xs ([x] ++ y ++ [x])

remove :: [Int] -> [Int] -> [Int]
remove x y = filter (\x -> not (x `elem` y)) x

flatten :: [[Int]] -> [Int]
flatten [x] = x
flatten (x:xs) = x ++ (flatten xs)

oddsNevens :: [Int] -> ([Int], [Int])
oddsNevens x = ((filter odd x), (filter even x))

primeDivisors :: Int -> [Int]
primeDivisors 0 = []
primeDivisors n = if factors == [] then [n]
	else factors ++ primeDivisors (n `div` (head factors))
	where factors = take 1 $ filter (\x -> (n `mod` x) == 0) [2 .. n-1]
