-- myLength

myLength :: [Int] -> Int
myLength [] = 0
myLength (x:xs) = 1 + (myLength xs)

-- myMaximum

myMaximumAux :: Int -> [Int] -> Int
myMaximumAux x [] = x
myMaximumAux x (x2:xs)
    | x >= x2 = (myMaximumAux x xs)
    | otherwise = (myMaximumAux x2 xs)

myMaximum :: [Int] -> Int
myMaximum (x:xs) = (myMaximumAux x xs)

-- average

sumLlista :: [Int] -> Int
sumLlista [x] = x
sumLlista (x:xs) = x + (sumLlista xs)

average :: [Int] -> Float
average x = (fromIntegral (sumLlista x))/(fromIntegral (myLength x))

-- buildPalindrome

gira :: [Int] -> [Int]
gira [x] = [x]
gira (x:xs) = (gira xs) ++ [x]

buildPalindrome :: [Int] -> [Int]
buildPalindrome [] = []
buildPalindrome x = (gira x) ++ x

-- remove

pertany :: Int -> [Int] -> Bool
pertany x [] = False
pertany x (y:ys)
	| x == y = True
	| otherwise = (pertany x ys)

remove :: [Int] -> [Int] -> [Int]
remove x y = [t | t <- x, (not (pertany t y))]

-- flaten

flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (x:xs) = x ++ flatten(xs)

-- oddsNevens

oddsNevens :: [Int] -> ([Int], [Int])
oddsNevens x = ([t | t <- x, (mod t 2) /= 0], [s | s <- x, (mod s 2) == 0])

-- primeDivisors

isPrimeAux :: Int -> Int -> Bool
isPrimeAux n t
	| n == t = True
	| (mod n t) == 0 = False
	| otherwise = (isPrimeAux n (t + 2))

isPrime :: Int -> Bool
isPrime n
	| n == 1 = False
	| n == 2 = True
	| (mod n 2) == 0 = False 
	| otherwise = (isPrimeAux n 3)

primeDivisors :: Int -> [Int]
primeDivisors n = [x | x <- [1..n], (mod n x) == 0, (isPrime x)]
