myLength :: [Int] -> Int
myLength n = length n

myMaximum :: [Int] -> Int
myMaximum [] = 0
myMaximum (x:xs)
  | x > myMaximum xs = x
  | otherwise = myMaximum xs

sumar :: [Int] -> Int
sumar [] = 0
sumar (x:xs) = x+(sumar xs)

average :: [Int] -> Float
average [] = 0
average n = fromIntegral(sumar n) / fromIntegral(length n)

buildPalindrome :: [Int] -> [Int]
buildPalindrome xs = reverse xs ++ xs

delNumber :: [Int] -> Int -> [Int]
delNumber [] _ = []
delNumber (x:xs) y
	| x == y = delNumber xs y
	| otherwise = [x] ++ (delNumber xs y)

remove :: [Int] -> [Int] -> [Int]
remove x [] = x
remove [] _ = []
remove x (y:ys) = remove (delNumber x y) ys

flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (x:xs) = x ++ flatten xs

parells :: [Int] -> [Int]
parells [] = []
parells (x:xs)
	| mod x 2 == 0 = [x] ++ parells xs
	| otherwise = parells xs

senars :: [Int] -> [Int]
senars [] = []
senars (x:xs)
	| mod x 2 /= 0 = [x] ++ senars xs
	| otherwise = senars xs

oddsNevens :: [Int] -> ([Int], [Int])
oddsNevens [] = ([],[])
oddsNevens l = (senars l, parells l)

primeDivisors :: Int -> [Int]
primeDivisors 0 = []
primeDivisors 1 = []
primeDivisors x = divisible_primos x 2

divisible_primos :: Int -> Int -> [Int]
divisible_primos x y
  | y > x = []
  | mod x y == 0 && isPrime y = y:divisible_primos x (y+1)
  | otherwise = divisible_primos x (y+1) 

isPrime :: Int -> Bool
isPrime n 
  | n == 0 = False
  | n == 1 = False
  | otherwise = primer n (n-1)

primer :: Int -> Int -> Bool
primer n i
  | i == 1 = True
  | mod n i == 0 = False
  | otherwise = primer n (i-1)