myLength [] = 0
myLength (x:xs) = 1 + myLength xs

myMaximum [] = 0
myMaximum (x:xs)
    | x > y     = x
    | otherwise = y
    where y = myMaximum xs
          
suma [] = 0
suma (x:xs) = x + suma xs          

average xs = suma(xs)/myLength(xs)

buildPalindrome [] = []
buildPalindrome x = buildP x x 
      
buildP [] ys = ys
buildP (x:xs) ys = buildP xs (x:ys) 

remove xs [] = xs
remove xs (y:ys) = remove (removeE xs y) ys

removeE [] y = []
removeE (x:xs) y 
    | x == y    = n
    | otherwise = (x:n)
    where n = removeE xs y

flatten [] = []
flatten (x:xs) = x++flatten xs

parells [] = []
parells (x:xs) 
    | mod x 2 == 0  = x:p
    | otherwise     = p
    where p = parells xs
          
senars [] = []
senars (x:xs) 
    | mod x 2 == 1  = x:s
    | otherwise     = s
    where s = senars xs
          
oddsNevens xs = (s,p)
    where s = senars xs
          p = parells xs
          
