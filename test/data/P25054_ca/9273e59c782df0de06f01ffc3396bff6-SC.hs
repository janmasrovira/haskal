

myLength::[Int] -> Int
myLength [] = 0
myLength (x:xs) = 1 +(myLength xs)
---------------------------------------------
myMaximum::[Int] -> Int
myMaximum [x] = x
myMaximum (x:xs) = if x > myMaximum xs then x
					else myMaximum xs
----------------------------------------------
--average::[Int]-> Float
--average [x] = transforma x
--average (x:xs) = average1 ((x:xs) (myLength (x:xs)))

--average1::[Int]-> Int ->Float
--average1 [x] p = (transforma x) / (transforma p)
--average1 (x:xs) p = ((transforma x)/ (transforma p)) + average1 xs p

transforma::Int->Float
transforma x = fromIntegral x :: Float
-------------------------------
buildPalindrome::[Int] -> [Int]
buildPalindrome [x] = [x]
buildPalindrome (x:xs) =   invierte (x:xs) ++ (x:xs)

invierte::[Int] -> [Int]
invierte [x] = [x]
invierte (x:xs) = (invierte xs) ++ [x]
-------------------------------
remove::[Int]->[Int]->[Int]
remove [] [] = []
remove (x:xs) [] = (x:xs) 
remove [] (y:ys) = []
remove (x:xs) (y:ys) = if removelem (y:ys) x then remove xs (y:ys)
						else [x] ++ remove xs (y:ys)

removelem::[Int] -> Int -> Bool
removelem [x] y = x == y
removelem (x:xs) y = x == y || removelem xs y

------------------

flatten::[[Int]] -> [Int]
flatten ([]) = []
flatten ([]:ys) = flatten (ys)
flatten ([(x:xs)]) = (x:xs)
flatten ((x:xs):ys) = (x:xs) ++ flatten (ys)
------------------------
oddsNevens::[Int] -> ([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens (x:xs) = (senars (x:xs) , parells (x:xs))

parells::[Int]->[Int]
parells [] = []
parells (x:xs) = if mod x 2 == 0 then [x] ++ parells (xs)
				  else parells (xs)

senars::[Int]->[Int]
senars [] = []
senars (x:xs) = if mod x 2 == 0 then senars (xs)
				 else [x] ++ senars (xs)
----------------------------------------------
