myLength :: [Int] -> Int
myLength [] = 0
myLength (a:b) = 1 + (myLength b)

myMaximum :: [Int] -> Int
myMaximum (a:[]) = a
myMaximum (a:b)
  | a > aux = a
  | otherwise = aux
  where aux = myMaximum b

mySuma :: [Int] -> Int
mySuma [] = 0
mySuma (a:b) = a + (mySuma b)
  
average :: [Int] -> Float
average [] = 0
average (a:[]) = fromIntegral a
average a = fromIntegral (mySuma a) / fromIntegral (myLength a)

buildPalindrome :: [Int] -> [Int]
buildPalindrome x =  auxP x []

auxP [] a = a
auxP (y:ys) a = auxP ys (y:a)
  
remove :: [Int] -> [Int] -> [Int]
remove [] y = []
remove (x:xs) y
  | myIn y x = remove xs y
  | otherwise = (x : (remove xs y))

myIn :: [Int] -> Int -> Bool
myIn [] x = False
myIn (a:b) x
  | a == x = True
  | otherwise = myIn b x
  
flatten :: [[Int]] -> [Int]
flatten [] = []
flatten ((a:[]):c) = (a : (flatten c))
flatten ((a:b):c) = (a : (flatten c))

oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens (x:xs)
 | mod x 2 == 1 = (x:(fst (oddsNevens xs)),snd (oddsNevens xs))
 | otherwise = (fst (oddsNevens xs),x:(snd (oddsNevens xs)))
  
isPrime :: Int -> Bool
isPrime a
  | isPrimeRec a (quot a 2) = True
  | otherwise = False
  
isPrimeRec :: Int -> Int -> Bool
isPrimeRec a b
  | a <= 1 = False
  | b == 1 = True
  | (mod a b) == 0 = False
  | isPrimeRec a (b-1) = True
  | otherwise = False
  
primeDivisors :: Int -> [Int]
primeDivisors n = primeDivisorsAux 2 n
 
primeDivisorsAux :: Int -> Int -> [Int]
primeDivisorsAux n m
 | isPrime n && (mod m n) == 0 = n:(aux)
 | n > m = []
 | otherwise = aux
 where aux = primeDivisorsAux (n+1) m
  
  