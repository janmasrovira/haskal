myLength :: [Int] -> Int
myLength [] = 0
myLength (_:cua) = ( 1 + myLength cua )

myMaximum :: [Int] -> Int
myMaximum [x] = x
myMaximum (xs:cua) = max xs ( myMaximum cua )

averege:: [Int] -> Float
averege a = (fromIntegral (suma a)) / ( fromIntegral (myLength a))
	where
		suma:: [Int] -> Int
		suma [] = 0
		suma (a:b) = a + suma b

buildPalindrome:: [Int] -> [Int]
buildPalindrome xs = ( girar xs ) ++ xs
	where
		girar:: [Int] -> [Int]
		girar [] = []
		girar (y:ys) = girar ys ++ [y]

remove:: [Int] -> [Int] -> [Int]
remove x [] = x
remove x (ya:yb) = remove ( eliminay x ya ) yb
	where
		eliminay:: [Int] -> Int -> [Int]
		eliminay [] y = []
		eliminay (a:x) y
			| a == y = eliminay x y
			| otherwise = [a] ++ eliminay x y

flatten:: [[Int]] -> [Int]
flatten [] = []
flatten (x:y) = x ++ flatten y

oddsNevens:: [Int] -> ([Int], [Int])
oddsNevens x = (odd x, even x)
	where
		odd:: [Int] -> [Int]
		odd [] = []
		odd (x:xs)
			| mod x 2 == 1 = x:(odd xs)
			| otherwise = odd xs
		even:: [Int] -> [Int]
		even [] = []
		even (x:xs)
			| mod x 2 == 0 = x:(even xs)
			| otherwise = even xs

primeDivisors :: Int -> [Int]
primeDivisors x
	| mod x 2 == 0 = [2] ++ divisors x 3
	| otherwise = divisors x 3
	where
		divisors:: Int -> Int -> [Int]
		divisors x y
			| y >= x = []
			| gcd y x == y && isPrime y = y:( divisors x ( y + 2 ) )
			| otherwise = divisors x ( y + 2 )

isPrime:: Int -> Bool
isPrime 0 = False
isPrime 1 = False
isPrime 2 = True
isPrime n
	| mod n 2 == 0 = False
	| noTeDivisor 3 = True
	| otherwise = False
	where
		noTeDivisor:: Int  -> Bool
		noTeDivisor i
			| i * i > n = True
			| mod n i == 0 = False
			| otherwise = noTeDivisor (i + 2)
