myLength::[Int]->Int
myLength [] = 0
myLength (x:y) = 1 + myLength y

average::[Int]->Float
average a = partAverage a 0 0


partAverage::[Int]->Int->Int->Float
partAverage [] sum elem = (fromIntegral sum)/(fromIntegral elem)
partAverage (x:y) sum elem = partAverage y (sum + x) (elem + 1) 


myMaximum::[Int]->Int
myMaximum (x:a) = findMax a x

findMax::[Int]->Int->Int
findMax [] a = a
findMax (x:y) a 
	| x > a = findMax y x
	| otherwise = findMax y a


buildPalindrome :: [Int] -> [Int]
buildPalindrome a = (invertir a )++a 


invertir :: [Int] -> [Int] 
invertir [] = []
invertir (x:y)  = (invertir y )++[x]


isContained::Int -> [Int] -> Bool
isContained x [] = False
isContained x (elem:y)
	| elem == x = True
	| otherwise = isContained x y

remove :: [Int] -> [Int] -> [Int]
remove [] y = [] 
remove (elem:list) x 
	| isContained elem x = remove list x
	| otherwise = [elem]++(remove list x) 


flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (a:x) = a++ (flatten x) 

odds::[Int]->[Int]
odds [] = []
odds (x:y)
	| (mod x 2) == 0 = (x:odds y)
	| otherwise = (odds y)

evens::[Int]->[Int]
evens [] = []
evens (x:y)
	| (mod x 2) == 1 = (x:evens y)
	| otherwise = (evens y)
oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens n = (evens n,odds n)


addNoRep m [] = [m];
addNoRep m (x:y)
	| m == x = (x:y)
	| otherwise = (m:(x:y))
primeDiv n  m
	| n == 1 = []
	| (mod n m) == 0 = addNoRep m (primeDiv (div n m) m)
	| otherwise = primeDiv n (m+1)

primeDivisors :: Int -> [Int]
primeDivisors n = primeDiv n 2

