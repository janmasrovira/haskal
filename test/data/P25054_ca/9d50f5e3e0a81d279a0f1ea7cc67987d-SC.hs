myLength::[Int]->Int
myLength [] = 0
myLength (x:xs) = 1+ myLength xs

myMaximum::[Int]->Int
myMaximum (x:[]) = x
myMaximum (x:xs)
  | x > y = x
  | otherwise = y
  where y = myMaximum xs