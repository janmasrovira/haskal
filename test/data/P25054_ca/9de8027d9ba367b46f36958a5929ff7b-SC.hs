-- funció myLength que calcula la llargada d'una llista

myLength n  = length n

-- funció myMaximum calcula el maxim d'una llista

myMaximum n = maximum n

-- funció average calcula el promig d'una llista d'enters

average n =  div (sum n)  (length n)

-- funció buildPalindrome dona una llista

buildPalindrome n = reverse n ++ n

-- funció remove donada una llista d'entrs x i una de enters y, retorna 
-- la llista x eliminant totes les ocurrencies de y

remove n y 
  | n == [] = []
  | elem (head n) y = remove (tail n) y
  | otherwise = [head n] ++ remove (tail n) y
  
-- funció flatten que aplana una llista de llistes produint 
-- una llista d’elements.

flatten n = concat n
  
-- funció oddsNevens donada una llista d'enters, retorni dues llistes, una conté els
-- parells i l'altre els senars en el mateix ordre relatiu que a l'original
  
oddsNevens n = (senars n, parells n)

parells n
  | n == [] = []
  | (head n) `mod` 2 == 0 = [(head n)] ++ parells (tail n)
  | otherwise = parells (tail n)

senars n
  | n == [] = []
  | (head n) `mod` 2 == 1 = [(head n)] ++ senars (tail n)
  | otherwise = senars (tail n)

-- funció primeDivisors que retorna la llista de divisors primers d'un enter 
-- estrictament positiu

primeDivisors n
  | n < 3 = []
  | otherwise = (primeDivisorsAux n 3)

primeDivisorsAux n i
  | i > n = []
  | n `mod` i == 0 = (isPrime i) ++ primeDivisorsAux n (i+2)
  | otherwise = primeDivisorsAux n (i+2)
  
isPrime n = (isPrimeAux n (n-1))

isPrimeAux n m 
    | m == 1 = [n]
    | m < 1  = []
    | (mod n m) > 0 = (isPrimeAux n (m-1))
    | otherwise = []

  