--1 calcula la llargada duna llista
myLength [] = 0
myLength (x:xs) = 1+myLength xs 

--2 calcula el maxim duna llista
myMaximum [x] = x
myMaximum (x:xs) = max x (myMaximum xs)

--3 calcula la mitja duna llista
average :: [Int] -> Float
average xs = (fromIntegral(sum xs)) / (fromIntegral(length xs))


