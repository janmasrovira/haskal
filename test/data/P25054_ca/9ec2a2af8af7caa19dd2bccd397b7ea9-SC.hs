myLength :: [Int] -> Int
myLength [] = 0
myLength (x:xs) = 1 + myLength xs

myMaximum :: [Int] -> Int
myMaximum [] = 0
myMaximum (x:[]) = x
myMaximum (x:xs)
  |x > myMaximum(xs) = x
  |otherwise = myMaximum(xs)

average :: [Int] -> Float
average (x:xs) = ((fromIntegral (sum (x:xs))) / (fromIntegral (length(x:xs))))

buildPalindrome :: [Int] -> [Int]
buildPalindrome l = concat [(reverse l),  l]

remove :: [Int] -> [Int] -> [Int]
remove [] l = []
remove (x:xs) l =  if belongs x l then remove (xs) l  else x:remove(xs) l

belongs :: Int -> [Int] -> Bool
belongs n [] = False
belongs n (x:xs) = (n == x) || belongs n (xs)

flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (x:xs) = x ++ flatten(xs)

--oddsNevens :: [Int] -> ([Int],[Int])

--primeDivisors :: Int -> [Int]