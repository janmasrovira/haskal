myLength :: [Int] -> Int
myLength [] 		= 0
myLength (_:cua) 	= 1 + myLength cua

myMaximum :: [Int] -> Int
myMaximum [x] 		= x
myMaximum (x:xs) 	= max x (myMaximum xs)

average :: [Int] -> Float
average xs = (fromIntegral (suma xs)) / (fromIntegral (myLength xs))
	where
		suma [] 		= 0
		suma (x:xs) 	= x + suma xs

buildPalindrome :: [Int] -> [Int]
buildPalindrome xs = (girar xs) ++ xs
	where 
		girar [] 		= []
		girar (y:ys) 	= girar ys ++ [y]

flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (x:xs) = x ++ flatten xs

remove :: [Int] -> [Int] -> [Int]
remove [] ys 		= []
remove (x:xs) ys 	=	 
	if not (hies ys)
	then
		(x:resta)
	else
		resta
	where 
		resta 	= remove xs ys
		hies :: [Int] -> Bool
		hies [] 		= False
		hies (y:ys)		= x == y || (hies ys)

oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens [] 	= ([],[])
oddsNevens (x:xs)
	| odd x		= (x : senars, parells)
	| even x	= (senars, x : parells)
	where
		(senars, parells) = oddsNevens xs
		
primeDivisors :: Int -> [Int]
primeDivisors x	= divisors' 2 x
	where
		divisors' n x 
			| n*2 > x		= []
			| mod x n == 0	= 
				if (isPrime n)
				then
					(n: divisors' (n+1) x)
				else
					divisors' (n+1) x
			| otherwise		= divisors' (n+1) x
			where
				isPrime :: Int -> Bool
				isPrime n
					| n <= 1	= False
					| otherwise	= noTeDivisors  2
					where
						noTeDivisors :: Int -> Bool
						noTeDivisors i
							| i*i > n	= True
							| mod n i == 0	= False
							| otherwise	= noTeDivisors (i+1)