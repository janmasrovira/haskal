--Problema 1
myLength :: [Int] -> Int
myLength [] = 0
myLength (x:xs) = 1 + myLength xs

--Problema 2
myMaximum :: [Int] -> Int
myMaximum [x] = x
myMaximum (x:xs) = myMax x (myMaximum xs)

myMax :: Int -> Int -> Int
myMax x y
    | x >= y    = x
    | otherwise = y

--Problema 3
average :: [Int] -> Float 
average xs = a / b
    where 
        a = fromIntegral (mySum xs)
        b = fromIntegral (myLength xs)

mySum :: [Int] -> Int
mySum [x] = x
mySum (x:xs) = x + mySum xs

--Problema 4
buildPalindrome :: [Int] -> [Int]
buildPalindrome xs = palindrome xs []

palindrome :: [Int] -> [Int] -> [Int]
palindrome [] ys = ys 
palindrome (x:xs) ys = palindrome xs ([x]++ys++[x])

--Problema 5
remove :: [Int] -> [Int] -> [Int]
remove [] ys = []
remove (x:xs) ys
    | pert ys x = (remove xs ys)
    | otherwise = x : (remove xs ys)

pert :: [Int] -> Int -> Bool
pert [] y = False
pert (x:xs) y
    | x == y    = True
    | otherwise = (pert xs y)

--Problema 6
flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (xs:xss) =  xs++(flatten xss) 

--Problema 7
oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens xs = ((odds xs),(evens xs))

evens :: [Int] -> [Int]
evens [] = []
evens (x:xs) 
    | a == 0    = x :(evens xs)
    | otherwise = (evens xs)
    where a = mod x 2

odds :: [Int] -> [Int]
odds [] = []
odds (x:xs) 
    | a /= 0    = x : (odds xs)
    | otherwise = (odds xs)
    where a = mod x 2

--Problema 8
primeDivisors :: Int -> [Int]
primeDivisors n = listDivisors n (2)

listDivisors :: Int -> Int -> [Int]
listDivisors n p
    | p > c    = []
    | a == 0 && b = p : (listDivisors n (p+1))
    | otherwise = (listDivisors n (p+1))
    where 
        a = mod n p
        b = isPrime p
        c = div n 2

isPrime :: Int -> Bool
isPrime n
    | n <= 1    = False
    | otherwise = prime n (n-1)

prime :: Int -> Int -> Bool
prime n p
    | p == 1    = True
    | x == 0    = False
    | otherwise = prime n (p-1)
    where x = mod n p

