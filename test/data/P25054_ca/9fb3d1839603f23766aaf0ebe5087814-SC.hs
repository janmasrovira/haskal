myLength :: [Int] -> Int
myLength [a] = 1
myLength l = 1 + (myLength t)
    where (h:t) = l

myMaximum :: [Int] -> Int
myMaximum [a] = a
myMaximum l = max h (myMaximum t)
    where (h:t) = l

comp_avg :: (Int, Int, [Int]) -> (Int, Int, [Int])
comp_avg (c, s, []) = (c, s, [])
comp_avg (c, s, l) = comp_avg (c+h, s+1, t)
    where (h:t) = l

average :: [Int] -> Float
average l = (fromIntegral c)/(fromIntegral s)
    where (c, s, _) = comp_avg (0, 0, l)

buildPalindrome :: [Int] -> [Int]
buildPalindrome l = (reverse l) ++ l

remove :: [Int] -> [Int] -> [Int]
remove [] y = []
remove x y = if elem h y then remove t y else [h] ++ (remove t y)
    where (h:t) = x

flatten :: [[Int]] -> [Int]
flatten [] = []
flatten l = h ++ (flatten t)
    where (h:t) = l

oddsNevensAux :: ([Int], [Int], [Int]) -> ([Int], [Int], [Int])
oddsNevensAux (e, o, []) = (e, o, [])
oddsNevensAux (e, o, n) = if even h then oddsNevensAux (e ++ [h], o, t) else oddsNevensAux (e, o ++ [h], t)
    where (h:t) = n

oddsNevens :: [Int] -> ([Int], [Int])
oddsNevens l = (odds, evens)
    where (evens, odds, _) = oddsNevensAux ([], [], l)

divisible_within :: Int -> Int -> Int -> Bool
divisible_within x a b
  | a > b = False
  | a == b = mod x a == 0
  | otherwise = mod x b == 0 || (divisible_within x a (b-1))

isPrime :: Int -> Bool
isPrime 1 = False
isPrime 2 = True
isPrime x = not (divisible_within x 2 (div x 2))

listPrimeDivisors :: (Int, Int, [Int]) -> (Int, Int, [Int])
listPrimeDivisors (n, e, d)
    | e <= 0 = (n, e, d)
    | mod n e == 0 && isPrime e = listPrimeDivisors (n, e-1, [e] ++ d)
    | otherwise = listPrimeDivisors (n, e-1, d)

primeDivisors :: Int -> [Int]
primeDivisors n = d
    where (_, _, d) = listPrimeDivisors (n, n, [])




