
factorial :: Integer -> Integer

factorial 0 = 1
factorial n = n * factorial (n-1)

absValue :: Int -> Int
absValue n
  |n >= 0	= n
  |otherwise	= (-n)
  
power :: Int -> Int -> Int 
power n p
  |p == 0	= 1
  |otherwise	= n * (power n (p-1))
  
isPrime :: Int -> Bool
isPrime n
  |n == 0	= False
  |n == 1	= False
  |otherwise	= noDivisors n 2
    where
      noDivisors :: Int -> Int -> Bool

      noDivisors n i
	|i == n	= True
	|mod n i == 0	= False
	|otherwise	= noDivisors n (i+1)

selectPrime :: [Int] -> [Int]
selectPrime [] = []
selectPrime (x:xs)
  |isPrime x  = [x] ++ selectPrime xs
  |otherwise  = selectPrime xs
	
slowFib :: Int -> Int
slowFib n 
  |n == 0	= 0
  |n == 1	= 1
  |n >= 2	= slowFib (n-1) + slowFib (n-2)
  
quickFib :: Int -> Int
quickFib n = fst (fib n)
  where
    fib 0 = (0,0)
    fib 1 = (1,0)
    fib i = (a+b, a)
      where
	(a,b) = fib (i-1)

  

myLength :: [a] -> Integer
myLength [] = 0
myLength (x:xs) = 1 + myLength xs

myMaximum :: [Int] -> Int
myMaximum [] = 0
myMaximum [x] = x
myMaximum (x:xs)
  |x > a	= x
  |otherwise	= a
  where
    a = myMaximum xs
    
average :: [Int] -> Float
average l@(x:xs) = (fromIntegral (mySum l)) / (fromIntegral (myLength l))
  where
      mySum :: [Int] -> Int
      mySum [] = 0
      mySum (x:xs) = x + mySum(xs)
      
-- inv l = inv2 l []
-- inv2 l1 l2 = (inv l1) ++ l2
-- inv2 [] l2 = l2
-- inv2 (x:xs) l2 = inv2...
buildPalindrome :: [Int] -> [Int]
buildPalindrome [] = []
-- buildPalindrome [x] = [x]
buildPalindrome l@(x:xs) = (invertList l) ++ l
  where 
    invertList :: [Int] -> [Int]
    invertList [] = []
    invertList (x:xs) = (invertList xs) ++ [x]
    
remove :: [Int] -> [Int] -> [Int]
remove l [] = l
remove [] l = []
remove l@(x:xs) [y]
	|x == y 	= remove xs [y]
	|otherwise	= [x] ++ remove xs [y]
remove l@(x:xs) (y:ys) = remove (remove l [y]) ys

flatten :: [[Int]] -> [Int]
flatten [] = []
-- flatten [[x]] = [x]
flatten (x:xs) = x ++ flatten xs

oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens l = (odds l,evens l)
  where
    odds [] = []
    odds (x:xs)
      |x `mod` 2==1   = [x] ++ odds xs
      |otherwise  = odds xs
    evens [] = []
    evens (x:xs)
      |x `mod` 2==0   = [x] ++ evens xs
      |otherwise  = evens xs

primeDivisors :: Int -> [Int]
primeDivisors 0 = []
primeDivisors 2 = [2]
primeDivisors 3 = [3]
primeDivisors 5 = [5]
primeDivisors 7 = [7]
primeDivisors x = selectPrime (divisors x 2)
  where
    divisors 0 x = []
    divisors 1 x = []
    divisors a i
      |a == i   = []
      |a `mod` i==0   = [i] ++ divisors a (i+1)
      |a /= i   = divisors a (i+1) 
  
