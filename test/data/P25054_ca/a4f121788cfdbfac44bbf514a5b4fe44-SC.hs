myLength 		:: [Int] -> Int
myMaximum 		:: [Int] -> Int
average 		:: [Int] -> Float
buildPalindrome :: [Int] -> [Int]
remove			:: [Int] -> [Int] -> [Int]
removeElement	:: [Int] -> Int -> Int -> [Int]
flatten			:: [[Int]] -> [Int]
oddsNevens		:: [Int] -> ([Int],[Int])
primeDivisors	:: Int -> [Int]
takeListPrime	:: Int -> Int -> [Int]
nextPrime		:: Int -> Int
isPrime	:: Int -> Bool
primality :: Int -> Int -> Bool

myLength ln = length ln

myMaximum ln = maximum ln


{-myMaximum [n] = n
myMaximum ln = if m > n then m
					   else n
	where 
		m = myMaximum (tail ln)
		n = head ln-}
		
average ln = fromIntegral(sum ln) / fromIntegral (length ln)
		  
buildPalindrome ln = reverse ln ++ ln

removeElement ln r n = 
	if overLength then ln  
			   else if ln!!(n-1) == r then (take (n-1) lm) ++ (drop n lm)
							 else lm
	where {
		lm = removeElement ln r (n+1);
		overLength = (length ln < n)
	}

remove ln [] = ln
remove ln lr = removeElement lm (head lr) 1
	where lm = remove ln (tail lr)

flatten [] = []
flatten lln = m
	where m = head lln  ++ flatten (tail lln)

oddsNevens [] = ([], [])
							 
oddsNevens ln = if (head ln) `mod` 2 == 0 then (o, [(head ln)]++e)
										   else ([(head ln)]++o, e)
	where (o, e) = oddsNevens (tail ln)
		  
isPrime 0 = False;
isPrime 1 = False;
isPrime n = primality n 2
primality n i = if (i*i) > n then True;
							 else if n `mod` i == 0 then False;
													else primality n (i+1)

nextPrime p = if isPrime p then p
						   else nextPrime (p+1)
													
takeListPrime n p = if p > n then []
								 else if n `mod` p == 0 then [p] ++ lp
						  else lp
	where {
		lp = takeListPrime n np;
		np = nextPrime (p+1)
	}
	
					   
primeDivisors n = takeListPrime n 2