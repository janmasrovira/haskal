myLength :: [Int] -> Int

myLength [] = 0
myLength (_:xs) = 1 + myLength xs

myMaximum :: [Int] -> Int
myMaximum [x] = x
myMaximum (x:xs)
  |x > maxresta = x
  |otherwise = maxresta
  where 
    maxresta = myMaximum xs
    
average :: [Int] -> Float
average xs = fromIntegral(suma xs) / fromIntegral(myLength xs)
  where
    suma [] = 0
    suma (y:ys) = y + suma ys
    
buildPalindrome :: [Int] -> [Int]
buildPalindrome xs = girar xs ++ xs
  where
  girar [] = []
  girar (y:ys) = girar ys ++ [y]

remove :: [Int] -> [Int] -> [Int]  
remove [] ys = []
remove (x:xs) ys =
   if not (hies x ys)
      then x:resta
      else resta
  where resta = remove xs ys
	hies x [] = False
	hies x (y:ys) = x == y || hies x ys

flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (x:xs) = x ++ flatten xs

oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens (x:xs) = 
  if even x 
    then (senars,[x]++parells)
    else ([x]++senars,parells)
    where (senars,parells) = oddsNevens xs

primeDivisors :: Int -> [Int]
primeDivisors x =  divisoresprimos x (x-1)
where
  divisoresprimos x 1 = []
  divisoresprimos x y = 
    if isPrime y && mod x y == 0
    then [y] ++ divisoresprimos x (y-1)
    else divisoresprimos x (y-1)


isPrime :: Int -> Bool
isPrime x
  |x <= 1 = False
  |otherwise = prime (x-1)
  where
    prime y
      |y == 1 = True
      |mod x y == 0 = False
      |otherwise = prime (y-1)
      