--1 calcula la llargada duna llista
myLength [] = 0
myLength (x:xs) = 1+myLength xs 

--2 calcula el maxim duna llista
myMaximum [x] = x
myMaximum (x:xs) = max x (myMaximum xs)

--3 calcula la mitja duna llista
average :: [Int] -> Float
average xs = (fromIntegral(sum xs)) / (fromIntegral(length xs))

--4 funcio que ratorni el palindrom que comensa amb la llista invertida 
buildPalindrome :: [Int] -> [Int]
buildPalindrome xs = (gira xs)++xs
	where
		gira [] = []
		gira (x:xs) = (gira xs)++[x]

--5 elimina les ocurrencies de y en la llista x
remove::[Int]->[Int]->[Int]
remove xs [] = xs
remove xs (y:ys) = remove (borraun xs y) ys
	where
		borraun [] y = []
		borraun (a:as) y
			| a == y = borraun as y
			| otherwise = a:borraun as y

--6 aplana una llista de llistes
flatten [] = []
flatten (x:xs) = x++flatten xs

--7 donada una llista retorna una amb senars i una amb parells
oddsNevens [] = ([], [])
oddsNevens xs = (llistsen xs, llistpars xs)
	where
		llistpars [] = []
		llistpars (x:xs)
			| mod x 2 == 0 = [x]++llistpars xs
			| otherwise = llistpars xs
		llistsen [] = []
		llistsen (x:xs)
			| mod x 2 == 0 = llistsen xs
			| otherwise = [x]++llistsen xs

--es primer

isPrime 0 = False
isPrime 1 = False
isPrime x = not (divisor x (x-1))
divisor x a
	|a == 1 = False
	| mod x a == 0 = True
	| otherwise = divisor x (a-1)

--8 llista de divisors primers dun enter estrictament positiu
primeDivisors n = [x|x<-[2..n], mod n x == 0, isPrime x] 

