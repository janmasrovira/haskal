myLength :: [Int] -> Int
myLength [] = 0
myLength (_:xs) = 1 + myLength xs

myMaximum :: [Int] -> Int
myMaximum [x] = x
myMaximum (x:xs) = max x (myMaximum xs) 

sumL :: [Int] -> Int
sumL [x] = x
sumL (x:xs) =  x + (sumL xs) 

average :: [Int] -> Float
average [x] = fromIntegral x
average x = fromIntegral (sumL x) / fromIntegral(myLength x)

buildPalindrome :: [Int] -> [Int]
buildPalindrome [] = []
buildPalindrome (x:xs) = (buildPalindrome xs) ++ [x]


remove :: [Int] -> [Int] -> [Int]
remove [] [] = []
remove [x] [] = [x]
remove [] [x] = []
remove x y = filter (\x -> not (x `elem` y)) x

flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (x:xs) = x ++ flatten(xs)

oddsNevens :: [Int] -> ([Int], [Int])
oddsNevens [] = ([],[])
oddsNevens x = (filter odd(x), filter even(x))

divisio :: Int -> Int -> Bool
divisio n d
	| d == 1 = False
	| mod n d == 0 = True
	| otherwise = divisio n (d - 1)

isPrime :: Int -> Bool
isPrime 0 = False
isPrime 1 = False
isPrime n = not (divisio n (n - 1))

mirarPrimers :: Int -> Int -> [Int]
mirarPrimers x 1 = []
mirarPrimers x y
	| (mod x y) == 0 && isPrime y = mirarPrimers x (y - 1) ++ [y]
	| otherwise = mirarPrimers x (y - 1)

primeDivisors :: Int -> [Int]
primeDivisors 1 = []
primeDivisors x
	| isPrime x = [x]
	| otherwise = mirarPrimers x (x - 1)


