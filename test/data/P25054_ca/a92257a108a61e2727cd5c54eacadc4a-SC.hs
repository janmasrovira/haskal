myLength :: [Int] -> Int

myLength [] = 0
myLength (x:resta) = myLength(resta) + 1

myMaximum :: [Int] -> Int

myMaximum [] = 0
myMaximum (cap:cua)
  | cap < myMaximum cua = myMaximum cua
  | otherwise = cap
  
buildPalindrome :: [Int] -> [Int]

buildPalindrome [] = []
buildPalindrome (cap:cua) = (cap:resta)++[cap]
  where resta = buildPalindrome cua
	
remove :: [Int] -> [Int] -> [Int]

remove [] _ = []
remove (cap:cua) llista 
  | member cap llista = remove cua llista
  | otherwise = (cap:remove cua llista)
  where 
    member :: Int -> [Int] -> Bool
    member _ [] = False
    member x (y:cua)
      | x == y = True
      | otherwise = member x cua
      
flatten :: [[Int]] -> [Int]

flatten [] = []
flatten [[]] = []
flatten (llista:llistes) = (llista++flatten llistes)

oddsNevens :: [Int] -> ([Int],[Int])

oddsNevens [] = ([],[])
oddsNevens (cap:cua) 
  | mod cap 2 == 0 = (cap:odds, evens)
  | otherwise = (odds, cap:evens)
  where
    (odds, evens) = oddsNevens cua
    
