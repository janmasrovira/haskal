myLength [] = 0
myLength (l:ls) = 1 + myLength ls

maxim x y
  | x > y = x
  | otherwise = y

myMaximum (x:xs) 
  | (myLength xs) == 0 = x
  | otherwise = maxim x (myMaximum xs)

suma (x:xs)
  | myLength xs == 0 = x
  | otherwise = x + suma (xs)

average xs 
  | myLength xs == 1 = suma(xs)
  | otherwise = (suma(xs)) / (myLength xs)
  
buildPalindrome xs
  | myLength xs == 0 = xs
  | otherwise = palindro xs xs
  
palindro (x:xs) xl
  | myLength xs == 0 = x:xl
  | otherwise = palindro xs (x:xl)

remove xs ys
  | myLength xs == 0 = []
  | myLength ys == 0 = xs
  | otherwise = remo xs ys
  
remosame (x:xs) y
  | myLength xs == 0  && x==y = []
  | myLength xs == 0 = x:[]
  | x==y = remosame xs y
  | otherwise = (x : (remosame xs y))
  
remo (x:xs) (y:ys) = remove (remosame (x:xs) y) ys

flatten xs
  | myLength xs == 0 = []
  | otherwise = flat xs
 
flat (x:xs)
  | myLength xs == 0 = x
  | otherwise = myConcatB x (flat xs)
  
myConcatB xs ys
  | myLength xs == 0 = ys
  | myLength ys == 0 = xs
  | otherwise = myConcat xs ys

myConcat (x:xs) ys
  | myLength xs == 0 = x:ys
  | otherwise = ( x : myConcat xs ys)
  
oddsNevens xs
  | myLength xs == 0 = ( [] , [])
  | otherwise = oddsN2 xs
  
oddsN2 (x:xs)
  | myLength xs == 0 && mod x 2 == 0 = ([] , x:[])
  | myLength xs == 0 = (x:[] , [])
  | mod x 2 == 0 = (fst a, x:(snd a))
  | otherwise = (x:(fst a), snd a)
  where a = oddsN2 xs

  
  
  
  
primeDivisors x = primeD x 2
  
primeD x n
  | n > x = []
  | n == x && isPrime n = x:[]
  | (mod x n)== 0 && (isPrime n) = n : primeD x (n+1)
  | otherwise = primeD x (n+1)
  
  
isPrime n
  | n==0 = False
  | n==1 = False
  | otherwise = primers n 2

primers n m
  | n <= m = True
  | (mod n m) == 0 = False
  | otherwise = primers n (m+1)
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  