
myLength :: [Int] -> Int
myLength [] = 0
myLength (x:xs) = 1 + (myLength xs)

myMaximum :: [Int] -> Int 
myMaximum [] = minBound :: Int
myMaximum (x:xs) = let m = myMaximum(xs)
		   in if x > m then x
		      else m

		      
sumar [] = 0
sumar (x:xs) = x+(sumar xs)
		  
average :: [Int] -> Float
average [] = 0
average x =  (fromIntegral (sumar x)) / (fromIntegral (myLength x))



buildPalindrome :: [Int] -> [Int]
buildPalindrome [] = []
buildPalindrome x = (reverse x) ++ x


remove1 [] y = []
remove1 (x:xs) y 
  | (x == y) = (remove1 xs y)
  | otherwise = x:(remove1 xs y)

remove :: [Int] -> [Int] -> [Int]
remove [] y = []
remove x [] = x
remove x (y:ys) = remove (remove1 x y) ys



flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (x:xs) = x ++ (flatten xs)



oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens (x:xs) = let m = (mod x 2)
			(o,e) = oddsNevens(xs)
		    in if m == 0 then (o, x:e)
		   else (x:o, e)

		   
isPrimeAux n m
  | m == 1 = True
  | (mod n m) == 0 = False
  | otherwise = isPrimeAux n (m-1)
    
isPrime n 
  | n <= 1 = False
  | otherwise = isPrimeAux n (div n 2)		   
	
	
primeDivisorsAux :: Int -> Int -> [Int]
primeDivisorsAux x 0 = []
primeDivisorsAux x m = let rec = primeDivisorsAux x (m-1) 
			   d = ((mod x m) == 0)
			   p = isPrime m
		       in if (d && p) then m:rec
			  else rec		   
		   
primeDivisors :: Int -> [Int] 
primeDivisors 0 = []
primeDivisors 1 = []
primeDivisors x = reverse (primeDivisorsAux x (div x 2) )



