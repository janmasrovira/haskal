myLength :: [Int] -> Int
myLength [] 	= 0
myLength (x:xs)	= 1 + myLength xs

myMaximum :: [Int] -> Int
myMaximum (x:xs)
	| null xs	= x
	| m > x 	= m
	| otherwise = x
	where m = myMaximum xs

listSum :: [Int] -> Int
listSum [] = 0
listSum (x:xs) =
	fromIntegral x + listSum xs

average :: [Int] -> Float
average [] 	= 0
average l = 
	(fromIntegral (listSum l)) / (fromIntegral (myLength l))

--buildPalindrome :: [Int] -> [Int]
--buildPalindrome [] = []
--buildPalindrome (x:xs) =
--	x:buildPalindrome xs
	--where (y:ys) = buildPalindrome xs

isInList :: Int -> [Int] -> Bool
isInList n [] = False
isInList n (x:xs)
	| n == x 	= True
	| otherwise = isInList n xs

remove :: [Int] -> [Int] -> [Int]
remove x [] = x
remove [] y = []
remove (x:xs) y
	| isInList x y 	= remove xs y
	| otherwise		= x:remove xs y

flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (x:xs) =
	x ++ flatten xs

isEven :: Int -> Bool
isEven n
    | m == 0    = True
    | otherwise = False
    where m = mod n 2

oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens (l:ls)
	| isEven l 	= (fst (oddsNevens ls), l:snd (oddsNevens ls))
	| otherwise = (l:fst (oddsNevens ls), snd (oddsNevens ls))

auxPrime :: Int -> Int -> Bool
auxPrime n p
    | p*p > n  = True
    | (mod n p) == 0  = False
    | otherwise = (auxPrime n (p+1))
    
isMultThree :: Int -> Bool
isMultThree n
    | m == 0    = True
    | otherwise = False
    where m = mod n 3
    
isPrime :: Int -> Bool
isPrime n
    | n < 2     = False
    | n == 2    = True
    | n == 3    = True
    | isEven(n) = False
    | isMultThree(n) = False
    | otherwise = auxPrime n 3

auxPrimeDivisors :: Int -> Int -> [Int]
auxPrimeDivisors n 0 	= []
auxPrimeDivisors n i 
	| (mod n i) == 0 && isPrime i 	= i:auxPrimeDivisors n (i-1)
	| otherwise = auxPrimeDivisors n (i-1)

primeDivisors :: Int -> [Int]
primeDivisors n = 
	reverse (auxPrimeDivisors n n)
