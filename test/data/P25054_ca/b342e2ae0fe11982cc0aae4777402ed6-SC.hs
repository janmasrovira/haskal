
myLength [] = 0
myLength [x] = 1
myLength (x: xs) = 1 + myLength xs

myMaximum [x] = x
myMaximum (x:xs)
	| q < x = x
	| otherwise = q
	where q = myMaximum xs

average::[Int] -> Float
average l = (fromIntegral(suma l)) / (fromIntegral(myLength l))


suma [x] = x
suma (x:xs) = (x + (suma xs))

buildPalindrome:: [Int] -> [Int]
buildPalindrome l = (palindrom l)++l

palindrom [] = []
palindrom (x: xs) = (palindrom xs)++[x]

remove:: [Int]->[Int]->[Int]
remove [] m = []
remove (x: xs) m = (remove2 [x] m)++remove xs m 

remove2 [x] [] = [x]
remove2 [x] (m:ms)
	| x == m =[]
	| otherwise = remove2 [x] ms

flatten:: [[Int]] -> [Int]
flatten [] = []
flatten (x:xs) = x++(flatten xs)

oddsNevens::[Int] -> ([Int],[Int])
oddsNevens l = oddsNevens2 l

oddsNevens2:: [Int] -> ([Int],[Int])
oddsNevens2 [] = ([],[])
oddsNevens2 (x:xs)
	| p == 0 = (z,[x]++y)
	|otherwise = ([x]++z,y)
	where {p = mod x 2;
	(z,y) = oddsNevens2 xs}

primeDivisors:: Int -> [Int]
primeDivisors 1 = []
primeDivisors n = primeDivisor2 n (n-1)

primeDivisor2 n 1 = []
primeDivisor2 n m  
	| b = x++[m] 
	| otherwise = x
	where  {p = mod n m;
x = primeDivisor2 n (m-1);
b = ((p == 0) && (prime m (m-1)))}

prime m 1 = True
prime m n
	| p == 0 = False
	| otherwise = prime m (n-1)
	where p = mod m n 

