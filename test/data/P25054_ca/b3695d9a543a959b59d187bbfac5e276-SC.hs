myLength :: [Int] -> Int
myLength [] = 0
myLength (_:xs) = 1 + myLength xs

myMaximum :: [Int] -> Int
myMaximum [x] = x
myMaximum (x:xs) = max x (myMaximum xs) 

sumL :: [Int] -> Int
sumL [x] = x
sumL (x:xs) =  x + (sumL xs) 

average :: [Int] -> Float
average [x] = fromIntegral x
average x = fromIntegral (sumL x) / fromIntegral(myLength x)

buildPalindrome :: [Int] -> [Int]
buildPalindrome [] = []
buildPalindrome (x:xs) = (buildPalindrome xs) ++ [x]

deleteN :: [Int] -> Int -> [Int]
deleteN [] _ = []
deleteN (x:xs) y
		| x == y = deleteN xs y
		| otherwise = [x] ++ (deleteN xs y) 


remove :: [Int] -> [Int] -> [Int]
remove [] [] = []
remove [x] [] = [x]
remove y (x:xs) = remove (deleteN y x) xs

flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (x:xs) = x ++ flatten(xs)

oddsNevens :: [Int] -> ([Int], [Int])
oddsNevens [] = ([],[])
oddsNevens x = (filter odd(x), filter even(x))



