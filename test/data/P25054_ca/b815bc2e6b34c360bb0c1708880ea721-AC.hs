myLength::[Int]->Int
myLength [] = 0
myLength (x:xs) = 1 + (myLength xs)

myMaximum::[Int]->Int
myMaximum [x] = x
myMaximum (x:xs) = max x (myMaximum xs)

sumar :: [Int] -> Int
sumar [] = 0
sumar (x:xs) = x+(sumar xs)

average::[Int]->Float
average xs = fromIntegral (sumar xs) / fromIntegral (myLength xs)

invert::[Int]->[Int]
invert [] = []
invert (e:es) =  (invert es)++[e]

buildPalindrome::[Int]->[Int]
buildPalindrome xs = (invert xs)++(xs)

removeElem::[Int]->Int->[Int]
removeElem [] y = []
removeElem (x:xs) y
  | x==y = removeElem xs y
  | otherwise = x:(removeElem xs y)

remove::[Int]->[Int]->[Int]
remove [] _ = []
remove (x:xs) [] = (x:xs)
remove (x:xs) (y:ys) = remove (removeElem (x:xs) y) ys

flatten::[[Int]]->[Int]
flatten [] = []
flatten (e:es) = e++(flatten es)

oddsNevens::[Int]->([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens [e]
  | odd e = ([e],[])
  | otherwise = ([],[e])
oddsNevens (e:es)
  | odd e = ([e]++(fst (oddsNevens es)),[]++(snd (oddsNevens es)))
  | otherwise = ([]++(fst (oddsNevens es)),[e]++(snd (oddsNevens es)))

primeDivisors::Int->[Int]
primeDivisors n = primeDivisorsAux n 2
  where primeDivisorsAux::Int->Int->[Int]
        primeDivisorsAux n i
          | i <= (div n i) && (mod n i == 0) = i:(primeDivisorsAux (reduce n i) (i+1))
          | i <= (div n i) = primeDivisorsAux n (i+1)
          | n /= 1 = [n]
          | otherwise = []
          where reduce::Int->Int->Int
                reduce n i
                  | mod n i /= 0 = n
                  | otherwise = reduce (div n i) i