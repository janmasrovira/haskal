
myLength :: [Int] -> Int
myLength [] = 0
myLength (n:ns) = 1 + myLength ns


myMaximum :: [Int] -> Int
myMaximum [n] = n
myMaximum (n:ns)
  | n > m = n
  | otherwise = m
  where m = myMaximum ns


average :: [Int] -> Float
average (n:ns) =
	fromIntegral (fst a) / fromIntegral (snd a)
	where a = averageIt (n:ns)
	
averageIt :: [Int] -> (Int, Int)
averageIt [n] = (n, 1)
averageIt (n:ns) =
	(fst a + n, snd a + 1)
	where a = averageIt ns
	

buildPalindrome :: [Int] -> [Int]
buildPalindrome [] = []
buildPalindrome n = invert n ++ n

invert :: [Int] -> [Int]
invert [] = []
invert (n:ns) = invert ns ++ [n]


remove :: [Int] -> [Int] -> [Int]
remove x y
	| null x  = []
	| null y = x
	| otherwise = remove (delete x (head y)) (tail y)

delete :: [Int] -> Int -> [Int]
delete x y
	| null x = []
	| head x == y = delete (tail x) y
	| otherwise = [head x] ++ delete (tail x) y


flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (x:xs) = x ++ flatten xs


oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens (x:xs)
	| mod x 2 == 0 = (fst l, [x] ++ snd l)
	| otherwise = ([x] ++ fst l, snd l)
	where l = oddsNevens(xs)

	
primeDivisors :: Int -> [Int]
primeDivisors n
	| n < 2 = []
	| mod n 2 == 0 = [2] ++ primeDivisors(quot n 2) 
	| otherwise = listPrimes 3 n
	
listPrimes :: Int -> Int -> [Int]
listPrimes k n
	| k > n = []
	| mod n k == 0 = [k] ++ listPrimes k (quot n k)
	| otherwise = listPrimes (k+2) n 

isPrime :: Int -> Bool
isPrime n
  | n < 2 = False
  | otherwise = not.divides n 2 $ (n-1)

divides :: Int -> Int -> Int -> Bool
divides x a b
  | a > b = False
  | otherwise = mod x a == 0 || divides x (a+1) b
  