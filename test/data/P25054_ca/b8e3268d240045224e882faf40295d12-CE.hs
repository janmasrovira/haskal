myLength :: [Int] -> Int
myLength [] = 0
myLength (_:cua) = 1 + myLength cua

myMaximum :: [Int] -> Int
myMaximum [x] = x
myMaximum (cap:cua) = max cap (myMaximum cua)


average :: [Int] -> Float
average xs = (fromIntegral (suma xs)) / (fromIntegral (myLength xs))
  where
    suma [] = 0
    suma (x:xs) = x + suma xs

buildPalindrome :: [Int] -> [Int]
buildPalindrome [] = []
buildPalindrome [x] = [x,x]
buildPalindrome (x:xs) = reverse (x:xs) ++ (x:xs)

remove :: [Int] -> [Int] -> [Int]
remove [] ys = []
remove (x:xs) ys =
  if not (exist ys)
  then x:resta
  else resta
    where
      resta = remove xs ys
      exist [] = False
      exist (y:ys) = x == y || exist ys

oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens (x:xs)
  | odd x = (x : senar, parell)
  | otherwise = (senar, x : parell)
    where
      (senar, parell) = oddsNevens xs
      
flatten :: [[Int]]->[Int]
 
flatten [] = []
flatten (x:xs) = x ++ flatten xs

primeDivisors :: Int -> [Int]
primeDivisors 0 = []
primeDivisors 1 = []
primeDivisors n
  | factors == []  = [n]
  | otherwise = removeDupl (factors ++ primeDivisors (n `div` (head factors)))
  where factors = take 1 $ filter (\x -> (n `mod` x == 0)) [2 .. n-1]