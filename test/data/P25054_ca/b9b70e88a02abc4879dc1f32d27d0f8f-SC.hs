-- [4,3,1,5,4,5,2] = 5
myMaximum :: [Int] -> Int

myMaximum [x]   = x
myMaximum (x:l)
    | x > myMaximum l = x
    | otherwise = myMaximum l

-- average [1,2,3]                      = 2.0
average :: [Int] -> Float

average l = fromIntegral (sum l) / fromIntegral (length l)
    where
        sum :: [Int] -> Int
        sum [x] = x
        sum (x:sl) = x + sum sl

-- buildPalindrome [2,4,6]              = [6,4,2,2,4,6]
buildPalindrome :: [Int] -> [Int]

buildPalindrome l = halve l ( div (length l) 2)
    where
        halve :: [Int] -> Int -> [Int]
        halve (d:l) n
            | n == 1 = l
            | otherwise = halve l (n-1)

-- flatten [[2,6],[8,1,4],[],[1]]       = [2,6,8,1,4,1]
-- remove [1,4,5,3,4,5,1,2,7,4,2] [2,4] = [1,5,3,5,1,7]
-- myLength [1,3..10]                   = 5
-- oddsNevens [1,4,5,3,4,5,1,2,7,4,2]   = ([1,5,3,5,1,7],[4,4,2,4,2])
-- primeDivisors 255                    = [3,5,17]
