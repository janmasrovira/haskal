
-- Donada una llista d'enters retorna la seva llargada
myLength :: [Int] -> Int
myLength [] = 0
myLength (x:xs) = 1 + myLength xs

-- Donada una llista d'enters no buida dona el seu maxim
myMaximum :: [Int] -> Int
myMaximum []  = 0
myMaximum [x] = x
myMaximum (x:xs) = if x > maxim then x
                    else maxim
            where maxim = myMaximum xs

-- Donada una llista d'enters no buida dona la seva mitjana
average :: [Int] -> Float

