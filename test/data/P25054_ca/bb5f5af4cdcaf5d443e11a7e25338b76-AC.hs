
myLength :: [Int] -> Int
myLength [] = 0
myLength (x:xs) = 1 + (myLength xs)

myMaximum :: [Int] -> Int
myMaximum l = maximum l

average :: [Int] -> Float
average l = fromIntegral(sum l) / fromIntegral(myLength l)

buildPalindrome :: [Int] -> [Int]
buildPalindrome l = (reverse l)++l

remove :: [Int] -> [Int] -> [Int]
remove x [] = x
remove x (y:[]) = rmv x y
remove x (y:ys) = remove (rmv x y) ys

rmv :: [Int] -> Int -> [Int]
rmv [] y = []
rmv (x:xs) y
	| x == y = rmv xs y
	| otherwise = (x:(rmv xs y))

flatten :: [[Int]] -> [Int]
flatten [] = []
flatten ([]:xs) = flatten xs
flatten (x:xs) = x ++ (flatten xs)

oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens (x:xs)
	| odd x = (x : o,e)
	| otherwise = (o,x : e)
	where (o,e) = oddsNevens(xs)

primeDivisors :: Int -> [Int]
primeDivisors x
	| x < 2 = []
	| (mod x 2) == 0 = 2: pd x 3
	| otherwise = pd x 3

pd :: Int -> Int -> [Int]
pd x y
	| x < y = []
	| otherwise = if ((mod x y)==0 && isPrime2 y 3) then y:xs else xs
	where xs = pd x (y+2)

isPrime2 :: Int -> Int -> Bool
isPrime2 p i
	| i*i > p = True
	| mod p i == 0 = False
	| otherwise = isPrime2 p (i+2)
