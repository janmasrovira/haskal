myLength::[Int]->Int
myLength [] = 0
myLength (x:y) = 1 + myLength y

myMaximum::[Int]->Int
myMaximum [x] = x
myMaximum (x:y)
	| x > myMaximum(y)	= x
	| otherwise 		= myMaximum(y)

average::[Int]->Float
average x = average2 x 0 0

average2::[Int]->Int->Int->Float
average2 [x] t s = ((fromIntegral (s+x))/(fromIntegral (t+1)))
average2 (x:y) t s = average2 y (t+1) (s+x)

buildPalindrome::[Int]->[Int]
buildPalindrome x = (reverse x) ++ x

remove::[Int]->[Int]->[Int]
remove [x] t
    | existeix x t      = []
    | otherwise         = x:[]

remove (x:y) t
    | existeix x t      = remove y t
    | otherwise         = [x] ++ (remove y t)

existeix::Int->[Int]->Bool
existeix t [x]
    | x==t      = True
    | otherwise = False
existeix t (x:y)
    | x==t      = True
    | otherwise = existeix t y

flatten::[[Int]]->[Int]
flatten = foldl (++) []

oddsNevens::[Int]->([Int],[Int])
oddsNevens x = (filter odd x, filter even x) 