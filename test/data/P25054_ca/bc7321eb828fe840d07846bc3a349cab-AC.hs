

myLength::[Int]->Int
myLength [] = 0
myLength (x:xs) = 1+ myLength xs

myMaximum::[Int]->Int
myMaximum [] = 0
myMaximum [x] = x
myMaximum (x:xs) 
	| x > maxT = x
	|otherwise = maxT
	where maxT = myMaximum xs

average::[Int]->Float
average (x:xs) = n / m
	where m = fromIntegral (myLength (x:xs));
		  n = fromIntegral (sumar (x:xs))


sumar [] = 0
sumar (x:xs) = x+(sumar xs)

division n m 
    | n < m = (0,n)
    | otherwise = (q+1,r)
    where (q,r) = division (n-m) m

buildPalindrome::[Int]->[Int]
buildPalindrome [] = []
buildPalindrome s = p ++ s
	where p = reverse s

remove::[Int]->[Int]->[Int]
remove [] _ = []
remove (a:as) [] = (a:as)
remove (a:as) (b:bs) = remove (removeItem (a:as) b) bs

removeItem [] _ = []
removeItem (a:as) b
	| a == b = removeItem as b
	| otherwise = a: removeItem as b

flatten::[[Int]]->[Int]
flatten [] = []
flatten (x:xs) = x++flatten xs
	
oddsNevens::[Int]->([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens (x:xs)
	|even x = (yp, x:xp)
	|otherwise = ( x:yp, xp)
	where (yp, xp) = oddsNevens xs 

primeDivisors::Int->[Int]
primeDivisors n = divisors n q
	where q = [x | x <- [2..n], isPrime x == True]

divisors n [] = []
divisors n (q:qs)
	| divisible n q = q : divisors n qs
	| otherwise = divisors n qs

isPrime::Int->Bool
isPrime n
	| n == 0 = False
	| n == 1 = False
	| otherwise = divis n 2

divis::Int->Int->Bool
divis x n
	|n > div x 2 = True
  	|r == 0  = False
  	|otherwise = divis x (n+1)
 	where r = mod x n


divisible::Int->Int->Bool
divisible x n
  	|r == 0  = True
  	|otherwise = False
 	where r = mod x n