myLength :: [Int] -> Int
myLength [] 		= 0
myLength (_:cua) 	= 1 + myLength cua

myMaximum :: [Int] -> Int
myMaximum [x] 		= x
myMaximum (x:xs) 	= max x (myMaximum xs)

average :: [Int] -> Float
average xs = (fromIntegral (suma xs)) / (fromIntegral (myLength xs))
	where
		suma [] 		= 0
		suma (x:xs) 	= x + suma xs

buildPalindrome :: [Int] -> [Int]
buildPalindrome xs = (girar xs) ++ xs
	where 
		girar [] 		= []
		girar (y:ys) 	= girar ys ++ [y]

flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (x:xs) = x ++ flatten xs

remove :: [Int] -> [Int] -> [Int]
remove [] ys 		= []
remove (x:xs) ys 	=	 
	if not (hies ys)
	then
		(x:resta)
	else
		resta
	where 
		resta 	= remove xs ys
		hies :: [Int] -> Bool
		hies [] 		= False
		hies (y:ys)		= x == y || (hies ys)

oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens [] 	= ([],[])
oddsNevens (x:xs)
	| odd x		= (x : senars, parells)
	| even x	= (senars, x : parells)
	where
		(senars, parells) = oddsNevens xs
		
primeDivisors :: Int -> [Int]
primeDivisors n	= primeDivisors' primes
	where
		primeDivisors' [] = []
		primeDivisors' (x:xs)
			| mod n x == 0	=	x:primeDivisors' xs
			| otherwise 	=	primeDivisors' xs
		primes = garbell (scanl (+) 2 (iterate id 1))
			where garbell (p:xs)
				| p <= n 		=	p:(garbell (filter (\x -> mod x p /= 0) xs))
				| otherwise		=	[]