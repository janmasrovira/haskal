myLength :: [Int] -> Int

myLength [] = 0
myLength(x:xs) = 1 + myLength(xs)


myMaximum :: [Int] -> Int

myMaximum [x] = x
myMaximum (x:xs) = max x (myMaximum xs)

sumallista :: [Int] -> Int

sumallista []     = 0
sumallista [x]    = x
sumallista (x:xs) = x + (sumallista xs)


average :: [Int] -> Float
average l = fromIntegral t / fromIntegral	 g
    where
    	t = sumallista l
    	g = myLength l


gira :: [Int] -> [Int]

gira []     = []
gira [x]    = [x]
gira (x:xs) = (gira xs) ++ [x]

buildPalindrome :: [Int] -> [Int]

buildPalindrome []     = []
buildPalindrome [x]    = [x]++[x]
buildPalindrome l      =gira l ++ l



elimina :: [Int] -> Int -> [Int]
elimina [] y     = []
elimina l y    = [ x | x <- l, x /= y]


remove :: [Int] -> [Int] -> [Int]
remove x []  = x
remove [] _  = []
remove x (y:ys) = (remove (elimina x y) ys)




flatten :: [[Int]] -> [Int]

flatten [[]]      = []
flatten []        = []
flatten (ll:lls)  = [] ++ ll ++ flatten lls


senars :: [Int] -> [Int]
senars [] = []
senars l  = [ x | x <- l, x `mod` 2 /= 0]

parells :: [Int] -> [Int]
parells [] = []
parells l = [ x | x <- l, x `mod` 2 == 0]



oddsNevens :: [Int] -> ([Int],[Int])

oddsNevens []          = ([],[])
oddsNevens  l          =  (odds, evens)
	where
		odds = senars l
		evens = parells l



teFactor :: Int -> Int -> Bool

teFactor x y

   | y == 1 = False
   |mod x y == 0    = True
   |mod x y /= 0    = teFactor x (y - 1)


isPrime :: Int -> Bool
isPrime 0 = False;
isPrime 1 = False;
isPrime x = not (teFactor x (div x 2))




buscarDivisors :: Int -> Int -> [Int]
buscarDivisors x y
	|y == 0                            = []
	|isPrime y && x `mod` y == 0       = (buscarDivisors x (y-1)) ++ [y]
	|otherwise                         = (buscarDivisors x (y-1))


primeDivisors :: Int -> [Int]
primeDivisors x
	|isPrime x =[x]
	|otherwise = (buscarDivisors x  (div x 2))

