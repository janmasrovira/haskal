-- P25054_ca

myLength :: [Int] -> Int
myLength [] = 0
myLength (x:xs) = 1 + (myLength xs)

myMaximum :: [Int] -> Int
myMaximum (x:xs)
  | xs == []	= x
  | x > max	= x
  | otherwise	= max
  where max = myMaximum xs

sumAll :: [Int] -> Int
sumAll (x:xs)
  | xs == []	= x
  | otherwise	= x + (sumAll xs)
  
average :: [Int] -> Float
average l = (fromIntegral (sumAll l)) / (fromIntegral (myLength l))

myReverse :: [Int] -> [Int]
myReverse [] = []
myReverse [x] = [x]
myReverse (x:xs) = (myReverse xs)++([x])

buildPalindrome :: [Int] -> [Int]
buildPalindrome l = (myReverse l)++l

contains :: [Int] -> Int -> Bool
contains (x:xs) e
  | xs == []	= e == x
  | e == x	= True
  | otherwise	= contains xs e
  
remove :: [Int] -> [Int] -> [Int]
remove [] l2	= []
remove l1 []	= l1
remove (x:xs) l2
  | (contains l2 x)	= (remove xs l2)
  | otherwise	= x:(remove xs l2)
  
flatten :: [[Int]] -> [Int]
flatten (x:xs)
  | xs == []	= x
  | otherwise	= x++(flatten xs)
  
oddsNevens :: [Int] -> ([Int], [Int])
oddsNevens [] = ([],[])
oddsNevens (x:xs)
  | (mod x 2) == 0 = (od, [x]++ev)
  | otherwise	= ([x]++od,ev)
  where (od, ev) = oddsNevens xs
  
isPrime :: Int -> Bool
isPrime n
  | n == 0 || n == 1	= False
  | otherwise	= not (isPrimeAux n 2)
    
isPrimeAux :: Int -> Int -> Bool
isPrimeAux n x
  | x > (div n 2)	= False
  | otherwise	= (((mod n x) == 0) || (isPrimeAux n (x + 1)))
  
primeDivisorsAux :: Int -> Int -> [Int]
primeDivisorsAux n x
  | x == 1	= []
  | ((mod n x) == 0) && (isPrime x)	= r++[x]
  | otherwise	= r
  where r = primeDivisorsAux n (x - 1)
   
primeDivisors :: Int -> [Int]
primeDivisors n
  | n <= 3	= []
  | otherwise	= primeDivisorsAux n (n - 1)