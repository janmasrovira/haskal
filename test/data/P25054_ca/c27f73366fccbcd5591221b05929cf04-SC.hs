countIf :: (Int -> Bool) -> [Int] -> Int
countIf f []	=	0
countIf f (x:xs)
	| f x		=	1 + countIf f xs
	| otherwise	=	countIf f xs

pam :: [Int] -> [Int -> Int] -> [[Int]]
pam xs fs	= [map f xs | f <- fs]

		-- [1,2,3] [+1,+2,^2] [2,3,4][3,4,5][1,3,9]
pam2 :: [Int] -> [Int -> Int] -> [[Int]]
pam2 xs fs	= map g xs
  where g x = [f x | f <- fs]

filterFoldl :: (Int -> Bool) -> (Int -> Int -> Int) -> Int -> [Int] -> Int
filterFoldl cond f n []	= n
filterFoldl cond f n (x:xs)
	| cond x	=	filterFoldl cond f (f n x) xs
	| otherwise	=	filterFoldl cond f n xs

insert :: (Int -> Int -> Bool) -> [Int] -> Int -> [Int]
insert f [] n = [n]
insert f (x:xs) n
	| f x n		=	x:insert f xs n
	| otherwise	=	[n,x]++xs

insertionSort :: (Int -> Int -> Bool) -> [Int] -> [Int]
insertionSort f []		=	[]
insertionSort f (x:xs)	=	insert f (insertionSort f xs) x