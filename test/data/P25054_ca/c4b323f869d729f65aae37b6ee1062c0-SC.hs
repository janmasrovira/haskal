
myLength:: [Int] -> Int
myLength [] = 0
myLength (x:xs) = 1 + myLength xs

myMaximum :: [Int] -> Int
myMaximum [x] = x
myMaximum (x:xs) = if x > myMaximum xs then x
				       else myMaximum xs
				       
average :: [Int] -> Float
average [] = 0.0
average (xs) = fromIntegral (sum xs) / fromIntegral (myLength xs)

buildPalindrome :: [Int] -> [Int]
buildPalindrome [] = []
buildPalindrome (xs) = buildPalindrome2 (xs) ++ xs

buildPalindrome2 :: [Int] -> [Int]
buildPalindrome2 [] = []
buildPalindrome2 (xs) = last xs : buildPalindrome2 (init xs)

remove :: [Int] -> [Int] -> [Int]
remove (xs) [] = xs
remove (xs) (y:ys) = remove (filter (/=y) xs)(ys)

flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (x:xs) = x ++ flatten(xs)

oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens (xs) = (filter odd xs, filter even xs)

isPrime :: Int -> Bool
isPrime n = if n <= 1 then False
		      else prime n (n-1)
prime :: Int -> Int -> Bool
prime n m = if m == 1 then True
		      else if mod n m == 0 then False
			 else prime n (m-1)

primeDivisors :: Int -> [Int]
primeDivisors 0 = []
primeDivisors n = primeDivisors2 n (n-1)

primeDivisors2 :: Int -> Int -> [Int]
primeDivisors2 x 0 = []
primeDivisors2 x 1 = []
primeDivisors2 x n =  if (mod x (n) == 0 && isPrime (n)) then primeDivisors2 x (n-1) ++ [n]
	else primeDivisors2 x (n-1)
