myLenght :: [Int] -> Int
myLenght [] = 0
myLenght (x:xs) = 1+(myLenght xs)

myMaximum :: [Int] -> Int
myMaximum [x] = x
myMaximum (x:xs)
  | x >= max	= x
  | otherwise	= max
    where
    max = myMaximum (xs)
    
average :: [Int] -> Float
average x =(fromIntegral(suma x)) / (fromIntegral(myLenght x))
  where 
    suma :: [Int] -> Int
    suma [] = 0
    suma (x:xs) = x+(suma xs)

buildPalindrome :: [Int] -> [Int]
buildPalindrome l = (invert l) ++ l
  where
    invert :: [Int] -> [Int]
    invert [] = []
    invert (x:xs) = invert(xs) ++ [x]
 
remove :: [Int] -> [Int] -> [Int]
remove l [] = l
remove [] l = []
remove l@(x:xs) (y:ys) = remove (elimina l y) ys
    where
    elimina :: [Int] -> Int -> [Int]
    elimina [] y = []
    elimina (x:xs) y
      | x == y	= elimina xs y
      | otherwise	= [x] ++ (elimina xs y)	

flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (x:xs) = x ++ flatten xs

oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens (x:xs)
	| mod x 2 == 1	= (([x] ++ fst (oddsNevens xs)),(snd (oddsNevens xs)))
	| otherwise	= ((fst (oddsNevens xs)),([x] ++ snd (oddsNevens xs)))

primeDivisors :: Int -> [Int]
primeDivisors n
	| n == 0 || n == 1	= []
	| otherwise 	= buscaprims n n []
	where
	buscaprims :: Int -> Int -> [Int] -> [Int]
	buscaprims n p l
		| p == 1	= l
		| mod n p == 0 && (isPrime p)	= buscaprims n (p-1) ([p] ++ l)
		| otherwise 	= buscaprims n (p-1) l

	isPrime :: Int -> Bool
	isPrime n 
	  | n == 0	= False
	  | n == 1	= False
	  | otherwise 	= prim n (div n 2)
	  where 
	    prim :: Int -> Int-> Bool
	    prim n m 
	      | m == 1		= True
	      | mod n m == 0	= False 
	      | otherwise		= prim n (m-1)	
	
	
	





