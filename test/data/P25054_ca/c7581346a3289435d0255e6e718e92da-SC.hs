myLength :: [Int] -> Int
myLength [] = 0
myLength (x:xs) = 1 + myLength xs

{-
myMaximum :: [Int] -> Int
myMaximum (x:xs)
  | xs == [] = x
  | otherwise = max x (myMaximum xs)
-}

myMaximum :: [Int] -> Int
myMaximum [x] = x
myMaximum (x:xs) = max x (myMaximum xs)
        
        
mySum :: [Int] -> Int
mySum [] = 0
mySum (x:xs) = x + mySum xs

        
-- Operacio generica de conversio de tipus enter a un numeric
-- Els integrals son els Ints i els Integer
-- from Integral (valor)
average :: [Int] -> Float
average l = fromIntegral(mySum l) / fromIntegral(myLength l)

aux :: [Int] -> [Int]
aux [] = []
aux (x:xs) = aux xs ++ [x]
-- [2,4,6] -> 6 4 2 2 4 6
buildPalindrome :: [Int] -> [Int]
buildPalindrome [] = []
buildPalindrome x =  aux x ++ x


disjoint :: Int -> [Int] -> [Int]
disjoint a [] = [a]
disjoint a (x:xs)
  | a == x = []
  | otherwise = disjoint a xs

remove :: [Int] -> [Int] -> [Int]
remove [] bs = []
remove (a:as) bs = disjoint a bs ++ remove as bs

{-
flatten :: [[Int]] -> [Int]
flatten [x] = x
flatten (a:xs) = a ++ flatten xs
-}

oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens [x]
  | x `mod` 2 == 0 = ([],[x])
  | otherwise = ([x], [])
oddsNevens (a:as) = ((fst (oddsNevens [a])) ++ l, (snd (oddsNevens [a])) ++ r)
  where (l, r) = oddsNevens as


divides :: Int -> Int -> Bool
divides n y
  | y == 1 = False
  | otherwise = if mod n y == 0 then True
                                else divides n (y-1)
   
isPrime :: Int -> Bool
isPrime n
  | n == 1 = False
  | n == 2 = True
  | otherwise = not (divides n (n-1))
  
primeDivisors :: Int -> [Int]
primeDivisors n = [i | i<-[1..n], isPrime i, n `mod` i == 0]
