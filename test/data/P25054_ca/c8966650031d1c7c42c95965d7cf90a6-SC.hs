-- [4,3,1,5,4,5,2] = 5
myMaximum :: [Int] -> Int
myMaximum [x]   = x
myMaximum (x:l)
    | x > myMaximum l = x
    | otherwise = myMaximum l

-- average [1,2,3]                      = 2.0
average :: [Int] -> Float
average l = fromIntegral (sum l) / fromIntegral (length l)
    where
        sum :: [Int] -> Int
        sum [x] = x
        sum (x:sl) = x + sum sl

-- buildPalindrome [2,4,6]              = [6,4,2,2,4,6]
buildPalindrome :: [Int] -> [Int]
buildPalindrome l = myReverse l ++ l
    where
        myReverse :: [Int] -> [Int]
        myReverse [x]   = [x]
        myReverse (x:l) = (myReverse l) ++ [x]

-- flatten [[2,6],[8,1,4],[],[1]]       = [2,6,8,1,4,1]
flatten :: [[Int]] -> [Int]
flatten [l]    = l
flatten (l:ll) = l ++ flatten ll

-- remove [1,4,5,3,4,5,1,2,7,4,2] [2,4] = [1,5,3,5,1,7]
remove :: [Int] -> [Int] -> [Int]
remove (x:ll) lr
    | ll == [] = []
    | match x lr     = remove ll lr
    | otherwise      = x : (remove ll lr)
    where
        match :: Int -> [Int] -> Bool
        match x [y]   = (x == y)
        match x (y:l) = (x == y || match x l)

-- myLength [1,3..10]                   = 5
myLength :: [Int] -> Int
myLength [x] = 1
myLength (x:l) = 1 + myLength l

-- oddsNevens [1,4,5,3,4,5,1,2,7,4,2]   = ([1,5,3,5,1,7],[4,4,2,4,2])
oddsNevens :: [Int] -> ([Int], [Int])
oddsNevens [x]
    | odd x = ([x], [])
    | otherwise = ([], [x])
oddsNevens (x:l)
    | otherwise = put x (oddsNevens l)
    where
        put :: Int -> ([Int], [Int]) -> ([Int], [Int])
        put x t
            | odd x     = (x : fst t, snd t)
            | otherwise = (fst t, x : snd t)

-- primeDivisors 255                    = [3,5,17]
primeDivisors :: Int -> [Int]
primeDivisors n = calcPrimeDivisors n
    where
        calcPrimeDivisors :: Int -> [Int]
        calcPrimeDivisors m
            | m == 1                    = []
            | isPrime m && mod n m == 0 = calcPrimeDivisors (m-1) ++ [m]
            | otherwise                 = calcPrimeDivisors (m-1)
            where
                isPrime :: Int -> Bool
                isPrime 0 = False
                isPrime 1 = False
                isPrime n = noDivisors 2
                    where
                        noDivisors :: Int -> Bool

                        noDivisors i
                            | i == n        = True
                            | mod n i == 0  = False
                            | otherwise     = noDivisors (i+1)
