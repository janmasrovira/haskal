myLength :: [Int] -> Int 
myLength [] = 0
myLength (_:xs) = myLength xs + 1

myMaximum :: [Int] -> Int 
myMaximum [x] = x
myMaximum (x:xs) = max x (myMaximum xs)

average :: [Int] -> Float
average xs  = 
   (/) (fromIntegral (sum xs)) (fromIntegral(myLength xs))
  
buildPalindrome :: [Int] -> [Int]
buildPalindrome xs = reverse(xs)++xs

remove :: [Int] -> [Int] -> [Int]
remove (xs:x) y
  |x==[] && elem xs y = []
  |x==[] = [xs]
  |(elem xs y) = (remove x y)
  |otherwise = xs:(remove x y)  
  
--flatten :: [[Int]] -> [Int]
oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens xs = (filter (odd) xs, filter (even) xs)

--primeDivisors :: Int -> [Int] 


