-- myMaximum [4,3,1,5,4,5,2]
myMaximum [x] = x
myMaximum (x:xs) 
	| y>=x = y
	| otherwise = x
   where y = myMaximum xs

-- myLength [1,3..10]
myLength [] = 0
myLength (x:xs) = 1 + myLength xs

--buildPalindrome [2,4,6]
buildPalindrome l = invertAndConcat l l

invertAndConcat [] l = l
invertAndConcat (x:xs) l = invertAndConcat xs (x:l)

--average [1,2,3]
average [] = 0
average l = (sumar l) / (myLength l)

sumar [] = 0
sumar (x:xs) = x+(sumar xs)

flatten :: [[Int]] -> [Int] 
--flatten [[2,6],[8,1,4],[],[1]]
flatten [] = []
flatten (l:ls) = l ++ flatten ls

-- remove [1,4,5,3,4,5,1,2,7,4,2] [2,4]
remove l [] = l
remove l (x:l1) = remove (remove' x l) l1 

remove' _ [] = []
remove' x (y:l) 
  | x==y      = remove' x l 
  | otherwise = y:(remove' x l)

-- oddsNevens [1,4,5,3,4,5,1,2,7,4,2]
oddsNevens [] = ([],[])
oddsNevens (x:xs) 
       | mod x 2 == 0 = (s,x:p)
       | otherwise = (x:s,p)
       where (s,p) = oddsNevens xs

-- primeDivisors 255
primeDivisors n = divprim1 n 2

divprim1 n m 
       | m > n = []
       | n /= d = m:divprim1 d (m+1)
       | otherwise = divprim1 n (m+1)
  where d = divall m n 

divall m n 
       | (mod n m) == 0 = divall m (div n m)
       | otherwise = n
