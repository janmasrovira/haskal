
 
-- llargada
myLength:: [Int] -> Int
myLength [] = 0
myLength (_ : xs) = 1 + myLength xs


-- myMaximum
myMaximum:: [Int] -> Int
myMaximum [x] = x
myMaximum (x:xs) = max x (myMaximum xs)

-- average
average:: [Int] -> Float
average xs = (fromIntegral (suma xs)) / (fromIntegral (myLength xs))
  where
      suma [] = 0
      suma (x:xs) = x + suma xs
      

-- buildPalindrome
buildPalindrome:: [Int] -> [Int]
buildPalindrome xs = (girar xs) ++ xs
  where
    girar [] = []
    girar (y:ys) = girar ys ++ [y]
    

-- remove
remove :: [Int] -> [Int] -> [Int]
remove [] y = []
remove (x:xs) y
    |conte y x  = remove xs y
    |otherwise  = [x] ++ remove xs y
    where
        conte:: [Int] -> Int -> Bool
        conte [] s = False
        conte (b:bs) s = (b == s) || conte bs s


-- flatten
flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (x:xs) = x ++ flatten xs



-- oddsNevens
oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens (x:xs)
    |mod x 2 == 0   = (s, [x] ++ p)
    |otherwise      = ([x] ++ s, p)
    where (s,p) = oddsNevens xs



-- primeDivisors
primeDivisors :: Int -> [Int]
primeDivisors s = esDivisor 3 s
    where
        esDivisor:: Int -> Int -> [Int]
        esDivisor d x
            |d == x                             = []
            |(mod x d == 0) && esPrimer d 2     = [d] ++ esDivisor (d+1) x
            |otherwise                          = esDivisor (d+1) x
            where
                esPrimer:: Int -> Int -> Bool
                esPrimer n i
                    |i*i > n		= True
                    |mod n i == 0	= False
                    |otherwise		= esPrimer n (i+1)





