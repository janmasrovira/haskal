
myMaximum :: [Int] -> Int
myMaximum [x] = x
myMaximum (x:xs) = max x $ myMaximum xs


average :: [Int] -> Float
average xs = (i2f $ sum xs) / (i2f $ length xs)
    where i2f x = fromIntegral x :: Float


buildPalindrome :: [Int] -> [Int]
buildPalindrome xs = (reverse xs) ++ xs


flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (x:xs) = x ++ (flatten xs)


insert :: [Int] -> Int -> [Int]
insert [] x = [x]
insert (y:ys) x
    | y < x     = y : insert ys x
    | otherwise = x : y : ys

