myLength :: [Int] -> Int
myLength [] = 0
myLength (_:xs) = 1 + myLength xs

myMaximum2 :: [Int] -> Int -> Int
myMaximum2 [] n = n
myMaximum2 (x:xs) n | x > n = myMaximum2 xs x
					| otherwise = myMaximum2 xs n

myMaximum :: [Int] -> Int
myMaximum (x:xs) = myMaximum2 xs x

suma :: [Int] -> Int
suma [] = 0
suma (x:xs) = x + suma xs 

average :: [Int] -> Float
average (x:xs) = (fromIntegral (suma (x:xs))) / (fromIntegral (myLength (x:xs)))

invertir :: [Int] -> [Int]
invertir [] = []
invertir (x:xs) = (invertir xs)++[x]

build :: [Int] -> [Int]
build [] = []
build (x:xs) = [x]++(build xs)++[x]

buildPalindrome :: [Int] -> [Int]
buildPalindrome [] = []
buildPalindrome (x:xs) = build (invertir (x:xs))

remove2 :: [Int] -> Int -> [Int]
remove2 [] n = []
remove2 (x:xs) n | x == n = (remove2 xs n)
				| otherwise = [x]++(remove2 xs n)

remove :: [Int] -> [Int] -> [Int]
remove (x:xs) [] = (x:xs)
remove (x:xs) (y:ys) = remove (remove2 (x:xs) y) (ys)

flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (x:xs) = x++(flatten xs)

pares :: [Int] -> [Int]
pares [] = []
pares (x:xs) | mod x 2 == 0 = [x]++(pares xs)
			 | otherwise = pares xs

impares :: [Int] -> [Int]
impares [] = []
impares (x:xs) | mod x 2 == 1 = [x]++(impares xs)
			 | otherwise = impares xs

oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens (x:xs) = (impares (x:xs), pares (x:xs))

oddsNevens2 :: [Int] -> ([Int],[Int])
oddsNevens2 [] = ([],[])
oddsNevens2 (x:xs) | mod x 2 == 0 = ((a:as)++[x] , (b:bs))
				  | otherwise = ((a:as) , (b:bs)++[x])
	where ((a:as),(b:bs)) = oddsNevens2 xs

compruebaPrimo :: Int -> Int -> [Int]
compruebaPrimo x y | y == 1 = [x]
					| mod x y == 0 = []
					| otherwise = compruebaPrimo x (y-1) 

isPrime :: Int -> [Int]
isPrime n | n == 1 = []
			| otherwise = compruebaPrimo n (div (n+1) 2)

primeDivisors2 :: Int -> Int -> [Int]
primeDivisors2 x 1 = []
primeDivisors2 x n | mod x n == 0 = (primeDivisors2 x (n-1))++(isPrime n)
				   | otherwise = primeDivisors2 x (n-1)

primeDivisors :: Int -> [Int]
primeDivisors 1 = []
primeDivisors n = primeDivisors2 n (div (n+1) 2)
