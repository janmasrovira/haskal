myLength :: [Int] -> Int
myLength n = length n

sumar :: [Int] -> Int
sumar [] = 0
sumar (x:xs) = x+(sumar xs)

average :: [Int] -> Float
average [] = 0
average n = fromIntegral(sumar n) / fromIntegral(length n)
	  