  
--llargada::[Int] -> Int


myLength [] = 0
myLength (_:cua) = 1 + myLength cua

myMaximum::[Int] -> Int
myMaximum [] = 0
myMaximum (cap:cua) 
  |cap > myMaximum cua 	= cap
  |otherwise 	= myMaximum cua

  