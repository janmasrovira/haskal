myLength :: [Int] -> Int
myLength [] = 0
myLength (x:xs) = (myLength xs) + 1

myMaximum :: [Int] -> Int
myMaximum [x] = x
myMaximum [x,y] = if x > y then x else y
myMaximum (x:xs) = if x > myMaximum xs then x else myMaximum xs

average :: [Int] -> Float
average a = fromIntegral(sumaTotal a)/ fromIntegral(myLength a)
	
sumaTotal :: [Int] -> Int
sumaTotal [] = 0
sumaTotal (x:xs) = x + (sumaTotal xs)

buildPalindrome :: [Int] -> [Int]
buildPalindrome a = (reverse a) ++ a

remove::[Int] -> [Int] -> [Int]
remove x [] = x
remove xs (y:ys) = remove (remove' xs y) ys

remove' [] _ = []
remove' (x:xs) y
    | x == y = remove' xs y
    | otherwise = x : remove' xs y