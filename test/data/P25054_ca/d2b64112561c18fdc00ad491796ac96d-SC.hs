--1 calcula la llargada duna llista
myLength [] = 0
myLength (x:xs) = 1+myLength xs 

--2 calcula el maxim duna llista
myMaximum [x] = x
myMaximum (x:xs) = max x (myMaximum xs)

--3 calcula la mitja duna llista
average [x] = x
average xs = (suml xs) / myLength xs

suml[] = 0
suml [x] = x
suml (x:xs) = x + suml xs
