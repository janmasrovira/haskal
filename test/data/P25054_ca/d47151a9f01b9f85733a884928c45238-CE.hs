myMaximum :: [a] -> a

myMaximum [x] = x
myMaximum (x,xs)
  | x >= (myMaximum xs)	=x
  | otherwise = myMaximum xs
