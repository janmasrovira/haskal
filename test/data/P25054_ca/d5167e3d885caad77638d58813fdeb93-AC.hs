
myLength :: [Int] -> Int
-- donada una llista d’enters, calculi la seva llargada.
myLength [] = 0
myLength (x:xs) = 1 + myLength xs

maxAux :: [Int] -> Int -> Int
maxAux [] x = x
maxAux (y:ys) x
	| x > y = maxAux ys x
	| True = maxAux ys y

myMaximum :: [Int] -> Int
-- donada una llista d’enters no buida, calculi el seu màxim.
myMaximum [] = 0
myMaximum (x:xs) = maxAux xs x

sumar :: [Int] -> Int
sumar [] = 0
sumar (x:xs) = x+(sumar xs)

average :: [Int] -> Float
-- donada una llista d’enters no buida, calculi la seva mitjana.
average [] = 0
average x = (fromIntegral (sumar x)) / (fromIntegral (myLength x))

buildInverse :: [Int] -> [Int]
buildInverse [] = []
buildInverse (x:xs) = (buildInverse xs) ++ [x]

buildPalindrome :: [Int] -> [Int]
-- donada una llista, retorni el palíndrom que comença amb la llista invertida.
buildPalindrome [] = []
buildPalindrome l = buildInverse l ++ l

removeForward :: [Int] -> [Int] -> [Int]
removeForward [][] = []
removeForward [](x) = []
removeForward (x:xs)[] = [x]
removeForward (x:xs)(y:ys)
	| (x /= y) = (removeForward (x:xs) (ys))
	| True = []

remove :: [Int] -> [Int] -> [Int]
-- donada una llista d’enters x i una llista d’enters y, retorna la llista x 
-- havent eliminat totes les ocurrències dels elements en y.
remove [][] = []
remove [](x) = []
remove (x)[] = x
remove (x:xs)(y:ys)
	| (x == y) = (remove (xs) (y:ys))
	| True = (removeForward (x:xs) (ys)) ++ (remove (xs) (y:ys))

flatten :: [[Int]] -> [Int]
-- aplana una llista de llistes produint una llista d’elements.
flatten [] = []
flatten (x:xs) = x ++ flatten xs

oddsL :: [Int] -> [Int]
oddsL [] = []
oddsL (h:t) 
	| (mod h 2 /= 0) = h : oddsL t
	| True = oddsL t

evensL :: [Int] -> [Int]
evensL [] = []
evensL (h:t) 
	| (mod h 2 == 0) = h : evensL t
	| True = evensL t

oddsNevens :: [Int] -> ([Int],[Int])
-- donada una llista d’enters, retorni dues llistes, una que conté els parells 
-- i una que conté els senars, en el mateix ordre relatiu que a l’original.
oddsNevens [] = ([],[])
oddsNevens l = ((oddsL l),(evensL l))

primeAux :: Int -> Int -> Bool
primeAux n d
	| (d*d) > n = True
	| (mod n d == 0) = False
	| True = primeAux n (d+1) 

isPrime :: Int -> Bool
isPrime 0 = False
isPrime 1 = False
isPrime n = primeAux n 2

isDivisor :: Int -> Int -> Bool
isDivisor n d
	| (mod n d == 0) = True
	| True = False

listPrimeDivisors :: Int -> Int -> [Int]
listPrimeDivisors n d
	| d > n = []
	| ((isDivisor n d) && isPrime d) = d : (listPrimeDivisors n (d+1))
	| True = listPrimeDivisors n (d+1)

primeDivisors :: Int -> [Int] 
-- retorna la llista de divisors primers d’un enter estrictament positiu
primeDivisors 0 = []
primeDivisors 1 = []
primeDivisors n = listPrimeDivisors n 2
