myLength :: [Int] -> Int
myLength [] = 0
myLength (x:xs) = 1 + myLength xs


myMaximum :: [Int] -> Int
myMaximum [x] = x
myMaximum (x:xs) = maxim x (myMaximum xs)
	where
		maxim :: Int -> Int -> Int 
		maxim y z
			| y > z = y
			| otherwise = z

average :: [Int] -> Float
average [x] = fromIntegral x
average (x:xs) = (fromIntegral (x + suma xs)) / (fromIntegral (1 + myLength xs))
	where 
		suma :: [Int] -> Int
		suma [x] = x
		suma (x:xs) = x + suma xs 

buildPalindrome :: [Int] -> [Int]
buildPalindrome xs = girar xs ++ xs
	where
		girar[] = []
		girar(y:ys) = girar(ys) ++ [y]

remove :: [Int] -> [Int] -> [Int]
remove [] [] = []
remove [] (x) = []
remove (x) [] = (x)
remove (x) (y:ys) =  remove (quitar x y) ys
	where 
		quitar :: [Int] -> Int -> [Int]
		quitar [] v = []
		quitar (w:ws) v  
			| w == v = quitar ws v
			| otherwise = [w] ++ quitar ws v

flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (x:xs) = x ++ flatten xs 

oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens [x:xs] 
	| odd x  = (x ; senars, parells)
	| otherwise  = (senars, x; parells)
	where (senars; parells) = oddsNevens xs

