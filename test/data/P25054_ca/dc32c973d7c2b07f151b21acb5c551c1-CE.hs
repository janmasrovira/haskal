myLength :: [Int]->Int
myLength [] = 0
myLength (x:l) = 1+myLength l

myMaximum :: [Int]->Int
myMaximum [l] = l
myMaximum (x:l) 
  |x > myMaximum l = x
  |otherwise = myMaximum l

suma :: [Int]->Int
suma [] = 0
suma (x:l) = x+suma l

average :: [Int]->Float
average l = fromIntegral(suma l) / fromIntegral(myLength l)
  
-- inversa de una lista si 2o arg es [], pone los el del 1r arg del reves delante del 2o arg
invers::[Int]->[Int]->[Int]
invers []l2 = l2
invers (x:l)l2 = invers (l) (x:l2)
  
buildPalindrome::[Int]->[Int]
buildPalindrome l = invers l[] ++ l
 
remove :: [Int]->[Int]->[Int]
