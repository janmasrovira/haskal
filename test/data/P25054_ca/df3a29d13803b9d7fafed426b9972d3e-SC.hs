myLength :: [Int] -> Int

myLength [] = 0
myLength (x:xs) = 1 + myLength xs

myMaximum :: [Int] -> Int

myMaximum [] = 0
myMaximum (x:xs)
	| x > myMaximum xs	= x
	| otherwise			= myMaximum xs

average :: [Int] -> Float

average [] = 0.0
average l = fromIntegral (sum l) / fromIntegral (myLength l)

buildPalindrome :: [Int] -> [Int]

buildPalindrome l = (rev l)++l
	where
		rev :: [Int] -> [Int]
		rev []		= []
		rev (x:xs)	= (rev xs)++[x]

remove :: [Int] -> [Int] -> [Int]

remove l []	= l
remove l (y:ys)	= remove (delete y l) ys
	where
		delete :: Int -> [Int] -> [Int]
		delete y [] = []
		delete y (l:ls)
			| l == y	= delete y ls
			| otherwise	= [l]++(delete y ls)

flatten :: [[Int]] -> [Int]

flatten []	= []
flatten (x:xs)	= x++(flatten xs)

oddsNevens :: [Int] -> ([Int],[Int])

oddsNevens l = (odds l, evens l)
		where
			evens []		= []
			evens (x:xs)
				| even x 	= [x]++(evens xs)
				| otherwise	= evens xs
			odds []			= []
			odds (x:xs)
				| odd x 	= [x]++(odds xs)
				| otherwise	= odds xs

				
isPrime :: Int -> Bool

isPrime 0 = False
isPrime 1 = False
isPrime n = noDivisors 2
	where
		noDivisors i
			| i == n		= True
			| mod n i == 0	= False
			| otherwise		= noDivisors (i+1)
			
primeDivisors :: Int -> [Int]

primeDivisors n = allprimes (filter (\y -> rem n y == 0) [2..n])
	where
		allprimes [] = []
		allprimes (x:xs)
			| isPrime x 	= x:(allprimes xs)
			| otherwise		= allprimes xs