myLength :: [Int] -> Int
myLength l
	| l == []   = 0
	| otherwise = (1+(myLength xs)) 
	where (x:xs) = l
 
myMaximum :: [Int] -> Int
myMaximum (x:xs)
	| xs == []  = x
	| x >= m 		= x
	| otherwise = m
	where m = (myMaximum xs)

average :: [Int] -> Float
average l =
	((fromIntegral(sumAll l))/(fromIntegral(myLength l)))

sumAll :: [Int] -> Int
sumAll (x:xs)
	| xs == [] = x
	| otherwise = (x+(sumAll xs))

buildPalindrome :: [Int] -> [Int]
buildPalindrome l = let 
	ult = (last l)
	lis = (init l)
	in if l == [] then []
		 else if lis == [] then [ult,ult]
	 	 else [ult]++(buildPalindrome lis)++[ult]

remove :: [Int] -> [Int] -> [Int]
remove l1 l2
	| l2 == [] = l1
	| otherwise = (remove (myRemove l1 x) xs)
	where (x:xs) = l2

myRemove :: [Int] -> Int -> [Int]
myRemove l n
	| l == []   = l
	| x == n    = (myRemove xs n)
	|	otherwise = [x]++(myRemove xs n)
	where (x:xs) = l
	
flatten :: [[Int]] -> [Int]
flatten l
	| l == []   = []
	| otherwise = x++(flatten xs)
	where (x:xs) = l

oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens l 
	| l == []   = ([],[])
	| otherwise = let f = (head l)
							 in ((modulN l (mod f 2)),(modulN l (mod (f+1) 2)))

modulN :: [Int] -> Int -> [Int]
modulN l n
	| l == [] = []
	| (mod x 2) == n = [x]++(modulN xs n)
	| otherwise = (modulN xs n)
	where (x:xs) = l

primeDivisors :: Int -> [Int]
primeDivisors n
	= (primeDivisorsAux n 2)

primeDivisorsAux :: Int -> Int -> [Int]
primeDivisorsAux n m
	| m > (div n 2) 								= if (isPrime n) then [n]
																		else []
	| (mod n m) == 0 && (isPrime m) = [m]++(primeDivisorsAux n (m+1))
	| otherwise 										= (primeDivisorsAux n (m+1))

isPrime :: Int -> Bool
isPrime n
  | n == 1         = False
  | n == 2         = True
  | (mod n 2) == 0 = False
  | otherwise      = (not (divisible n 3))
  
divisible :: Int -> Int -> Bool
divisible n m
  | m > (div n 2)  = False
  | (mod n m) == 0 = True
  | otherwise      = (divisible n (m+1))