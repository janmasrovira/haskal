--myLength
myLength :: [Int] -> Int
myLength [] = 0
myLength (x:xs) = 1 + myLength xs

--myMaximum
myMaximum :: [Int] -> Int
myMaximum [x] = x 
myMaximum (x:xs) = 
            if x > r then x
                else r
            where r = myMaximum xs

--average
average :: [Int] -> Float
average x = fromIntegral (div (fromIntegral (suml x)) (fromIntegral (myLength x)))

--suml
suml :: [Int] -> Int
suml [] = 0
suml (x:xs) = x + suml xs


