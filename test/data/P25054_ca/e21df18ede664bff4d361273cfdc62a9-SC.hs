
myLength :: [Int] -> Int
myLength [] = 0
myLength (_:xs) = 1 + myLength xs

myMaximum :: [Int] -> Int
myMaximum [x] = x
myMaximum (x:xs) = max x (myMaximum xs)
  where
    max :: Int -> Int -> Int
    max x y
      |x > y = x
      |otherwise = y
      
average :: [Int] -> Float
average l = (fromIntegral (sumaL l)) / (fromIntegral (myLength l))
  where
    sumaL :: [Int] -> Int
    sumaL [] = 0
    sumaL [x] = x
    sumaL (x:xs) = x + sumaL xs
    
buildPalindrome :: [Int] -> [Int]
buildPalindrome [] = []
buildPalindrome l = (inv l) ++ l
  where
    inv :: [Int] -> [Int]
    inv [] = []
    inv (x:xs) = (inv xs) ++ [x]

remove :: [Int] -> [Int] -> [Int]
remove x [] = x
remove [] _ = []
remove (x:xs) y
  |(isIn x y) = (remove xs y)
  |otherwise = [x] ++ (remove xs y)
    where
      isIn :: Int -> [Int] -> Bool
      isIn x [] = False
      isIn x (y:ys)
        |(x == y) = True
        |otherwise = (isIn x ys)

flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (x:xs) = x ++ (flatten xs)

oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens l = ((parells l),(senars l))
  where
    parells :: [Int] -> [Int]
    parells [] = []
    parells (l:ls)
      |odd l = [l] ++ (parells ls)
      |otherwise = parells ls
    senars :: [Int] -> [Int]
    senars [] = []
    senars (l:ls)
      |not (odd l) = [l] ++ (senars ls)
      |otherwise = senars ls

