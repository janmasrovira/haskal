myLength:: [Int] -> Int
myLength [] = 0
myLength (_:llista) = 1+(myLength llista)

myMaximum:: [Int] -> Int
myMaximum [head] = head
myMaximum (elem:llista)
		| elem > recmax = elem
		| otherwise = recmax
		where recmax = myMaximum llista


getaverage::[Int] -> Int -> Int -> Float
getaverage [] suma elements = ( fromIntegral suma)/fromIntegral(elements )
getaverage (elem:llista) suma elements = getaverage llista (suma+elem) (1+elements)
		
average:: [Int] -> Float
average llista = getaverage llista 0 0

palindrom:: [Int] -> [Int] -> [Int]
palindrom [] ret = ret
palindrom (elem:llista) ret = (palindrom llista ([elem]++ret) )

buildPalindrome:: [Int] -> [Int]
buildPalindrome llista = (palindrom llista []) ++ llista

noHiEs::[Int] -> Int -> Bool
noHiEs [] _ = True
noHiEs (elem:llista) x = (elem/=x)&&(noHiEs llista x)

remove::[Int] -> [Int] -> [Int]
remove [] _ = []
remove (elem:llista) prohibits 
	| ok = [elem]++(remove llista prohibits)
	| otherwise = (remove llista prohibits)
	where ok = noHiEs prohibits elem
	
flatten::[[Int]] -> [Int]
flatten [] = []
flatten (llista:matriu)= llista++flatten(matriu)

afegeixTupla:: ([Int],[Int]) -> ([Int],[Int]) -> ([Int],[Int])
afegeixTupla (x1, y1) (x2, y2) =(x1++x2, y1++y2)

oddsNevens:: [Int] -> ([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens (elem:llista)
	|ok = (afegeixTupla ([elem],[]) (oddsNevens (llista)) ) 
	|otherwise = (afegeixTupla ([],[elem]) (oddsNevens (llista)) )
	where ok = ((mod elem 2)/=0)
	
iteraDivs:: Int -> Int -> [Int]
iteraDivs n fact
	| (fact>n) = []
	| ok = [fact]++(iteraDivs (div n fact) (fact) )
	| otherwise = (iteraDivs n (fact+1))
	where ok = (mod n fact)==0
	
primeDivisors:: Int -> [Int]
primeDivisors n = (iteraDivs n 2)



		
	