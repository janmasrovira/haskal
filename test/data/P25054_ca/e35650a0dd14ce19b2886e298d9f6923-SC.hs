myLength :: [Int] -> Int
myLength [] = 0
myLength (x:xs) = 1 + (myLength xs)

myMaximum :: [Int] -> Int
myMaximum [x] = x
myMaximum (x:xs) = myMaximum2 x xs

myMaximum2 :: Int -> [Int] -> Int
myMaximum2 m [x] = if x > m then x else m
myMaximum2 m (x:xs) = if x > m then myMaximum2 x xs
                               else myMaximum2 m xs

average :: [Int] -> Float
average l = average_rec l 0 0
    where 
        average_rec :: [Int] -> Int -> Int -> Float
        average_rec (x:xs) sum length = average_rec xs (sum + x) (length + 1)
        average_rec [] sum length = (fromIntegral sum) / (fromIntegral length)

buildPalindrome :: [Int] -> [Int] 
buildPalindrome l = buildPalindrome2 l []
    where
        buildPalindrome2 :: [Int] -> [Int] -> [Int]
        buildPalindrome2 [] l = l
        buildPalindrome2 (x:xs) l = buildPalindrome2 xs ([x] ++ l)

remove :: [Int] -> [Int] -> [Int]
remove list (x:xs) = remove (remove2 list x) xs
    where
        remove2 :: [Int] -> Int -> [Int]
        remove2 [] r = []
        remove2 (x:xs) r
            | x /= r = [x] ++ (remove2 (xs) r) 
            | otherwise = remove2 xs r
remove list [] = list
    
flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (x:xs) = x++(flatten xs)

oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens (x:xs)
    | (mod x 2) == 0 = ((fst rec),x:(snd rec))
    | otherwise = (x:(fst rec),(snd rec))
        where rec = oddsNevens xs


isPrime :: Int -> Bool
isPrime x
    | x == 1 = False 
    | otherwise = isPrime2 x 2
       
isPrime2 :: Int -> Int -> Bool
isPrime2 x n
    | x == n = True
    | (mod x n) == 0 = False
    | otherwise = isPrime2 x (n+1)

primeDivisors :: Int -> [Int]
primeDivisors x = primeDivisors2 x 2
    where
        primeDivisors2 x y
            | y > x = []
            | and [(isPrime y),((mod x y) == 0)] = y:(primeDivisors2 x (y+1))
            | otherwise = primeDivisors2 x (y+1)
