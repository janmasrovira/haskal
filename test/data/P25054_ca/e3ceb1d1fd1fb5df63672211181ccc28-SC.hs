myLength :: [Int] -> Int
myLength [] = 0
myLength (x:xs) = 1 + myLength xs

myMaximum :: [Int] -> Int
myMaximum (x:xs)
    | xs == []  = x
    | otherwise = max x (myMaximum xs)

average :: [Int] -> Float
average l = fromIntegral (mySum l) /  fromIntegral (myLength l)
    where
    mySum :: [Int] -> Int
    mySum (x:xs) = x + sum xs

buildPalindrome :: [Int] -> [Int]
buildPalindrome xs = invert xs ++ xs
    where 
    invert :: [Int] -> [Int] 
    invert [] = []
    invert (y:ys) = invert ys ++ [y]

remove :: [Int] -> [Int] -> [Int]
remove xs [] = xs
remove xs (y:ys) = remove (removeElem xs y) ys
    where 
    removeElem :: [Int] -> Int -> [Int]
    removeElem [] y = []
    removeElem (x:xs) y
        | x == y    = removeElem xs y
        | otherwise = (x:(removeElem xs y))

flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (x:xs) = x ++ flatten xs

oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens [] = ([], [])
oddsNevens (x:xs)
    | odd x     = (x:odds, evens)
    | otherwise = (odds, x:evens)
    where (odds, evens) = oddsNevens xs

primeDivisors :: Int -> [Int]
primeDivisors n = lstPrimeDivisors 2 n

lstPrimeDivisors e n
	| e == n						= []
	| isPrime e && mod n e == 0 	= e:(lstPrimeDivisors (e + 1) n)
	| otherwise						= lstPrimeDivisors (e + 1) n

isPrime::Int -> Bool
isPrime n
	| n == 0    = False
	| n == 1    = False
	| n >= 2    = not (hasDivisors 2)
		where
		hasDivisors::Int -> Bool
		hasDivisors i
			| i*i > n       = False
			| mod n i == 0  = True
			| otherwise     = hasDivisors (i + 1)

