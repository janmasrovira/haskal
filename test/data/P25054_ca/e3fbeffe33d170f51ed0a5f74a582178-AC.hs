
-- Donada una llista d'enters retorna la seva llargada
myLength :: [Int] -> Int
myLength [] = 0
myLength (x:xs) = 1 + myLength xs

-- Donada una llista d'enters no buida dona el seu maxim
myMaximum :: [Int] -> Int
myMaximum []  = 0
myMaximum [x] = x
myMaximum (x:xs) = if x > maxim then x
                    else maxim
            where maxim = myMaximum xs

-- Donada una llista d'enters no buida dona la seva mitjana
average :: [Int] -> Float
average [] = 0
average xs = fromIntegral (suma xs) / fromIntegral (myLength xs)

-- Suma tots els elements d'una llista
suma :: [Int] -> Int
suma [] = 0
suma (x:xs) = x + suma xs

-- Retorna el palindrom de la llista donada
buildPalindrome :: [Int] -> [Int]
buildPalindrome [] = []
buildPalindrome xs = (revers xs) ++ xs

revers :: [Int] -> [Int]
revers [] = []
revers (x:xs) = (revers xs) ++ [x]

-- Retorna x - y
remove :: [Int] -> [Int] -> [Int]
remove xs [] = xs
remove xs (y:ys) = remove (remove_aux xs y) ys

remove_aux :: [Int] -> Int -> [Int]
remove_aux [] y = []
remove_aux (x:xs) y
  | x == y = remove_aux xs y
  | otherwise = [x] ++ (remove_aux xs y)

-- Aplana una llista de llistas de enters
flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (x:xs) = x ++ (flatten xs)

-- Donada una llista d'enters, retorna dues llistes, una que conte els parells
-- i una que conte els senars, en el mateix ordre relatiu que en el origianl
oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens (x:xs)
    | (mod x 2) == 0 = (fst aux, [x] ++ (snd (aux)))
    | otherwise      = ([x] ++ (fst (aux)), snd aux)
    where aux = oddsNevens xs

-- Que retorni la llista de divisors primers
primeDivisors :: Int -> [Int]
primeDivisors n
    | n < 2     = []
    | otherwise = aux (divisibles n n)

aux :: [Int] -> [Int]
aux [] = []
aux (x:xs)
    | isPrime x = (aux xs) ++ [x]
    | otherwise = aux xs

-- Indica si un enter es primer
isPrime :: Int -> Bool
isPrime n 
        | n < 2     = False
        | otherwise = length (divisibles n n) == 2

-- Retorna la llista de enters divisibles entre un enter donat
divisibles :: Int -> Int -> [Int]
divisibles x y
        | y == 1         = [1]
        | (mod x y) == 0 = y:(divisibles x (y - 1))
        | otherwise      = divisibles x (y - 1)
