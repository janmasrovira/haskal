
myLength :: [Int] -> Int

myLength [] = 0
myLength (x : xs) = 1+myLength(xs)

myMaximum :: [Int] -> Int

myMaximum (x : xs) = 
  if myLength(xs) == 0
     then x
     else max x (myMaximum(xs))

     
     
gDiv:: Int -> Int -> Float
gDiv a b = (fromIntegral a) / (fromIntegral b)

average :: [Int] -> Float
average x = gDiv (sum x) (myLength x)

invertir:: [Int] -> [Int]
invertir [] = []
invertir (x : xs) = (invertir xs)++[x]

buildPalindrome:: [Int] -> [Int]
buildPalindrome [] = []
buildPalindrome x = (invertir x)++(x)

removei:: Int -> [Int] -> [Int]
removei n [] = []
removei n (x : xs) = 
  if n == x
     then removei n xs
     else (x:(removei n xs))

remove:: [Int] -> [Int] -> [Int]
remove x [] = x;
remove x (y : ys) = remove (removei y x) ys

flatten:: [[Int]] -> [Int]
flatten [] = []
flatten (x : xs) = x++(flatten xs)

getodds:: [Int]->[Int]
getodds [] = []
getodds (x : xs) =
  if mod x 2 == 1
     then (x : (getodds xs))
     else getodds xs
     
getevens:: [Int]->[Int]
getevens [] = []
getevens (x : xs) =
  if mod x 2 == 0
     then (x : (getevens xs))
     else getevens xs

oddsNevens:: [Int] -> ([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens x = ((getodds x),(getevens x))