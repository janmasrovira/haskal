--Funcions amb llistes

    --Longitud de llista de int
myLength :: [Int] -> Int
myLength [] = 0
myLength (x:xs) = 1+(myLength xs)

    --Maxim d'una llista
myMaximum :: [Int] -> Int
myMaximum (x:[]) = x
myMaximum (x:xs)
    | m > x	= m
    | otherwise	= x
    where m = myMaximum (xs)
	
    --Mitjana d'una llista d'enters
sumar [x] = x
sumar (x:xs) = x+(sumar xs)
average :: [Int] -> Float
average (x) = a/b
    where a = fromIntegral (sumar x)
	  b = fromIntegral (myLength x)
	  
    --Palindrom amb llista duplicada
buildPalindrome :: [Int] -> [Int]
buildPalindrome (x:[]) = [x]
buildPalindrome l@(x:xs) = (invertir l) ++ l

invertir (x:[]) = [x]
invertir (x:xs) = invertir(xs)++[x]

    --Esborrar elements de llista x que apareguin a llista y
remove :: [Int] -> [Int] -> [Int]
remove [] []            = []
remove [] (b:bs)        = []
remove (a:as) []        = (a:as)
remove l@(a:as) (b:bs)  = remove (filter (/=b) (a:as)) (bs)

    --Passar de llista de llista d'enters a una sola llista d'enters
flatten :: [[Int]] -> [Int]
flatten []      = []
flatten [x]     = x
flatten (x:xs)  = x ++ flatten xs

    --Dividir en dues llistes els numeros senars i parells d'una llista inicial
oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens []           = ([],[])
oddsNevens (x:xs) 
    | mod x 2 == 0      = (fst t, [x] ++ snd t) 
    | otherwise         = ([x] ++ fst t, snd t) 
    where t = oddsNevens xs