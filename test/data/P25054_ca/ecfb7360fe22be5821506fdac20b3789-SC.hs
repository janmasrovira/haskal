--funcio 1

myLength :: [Int] -> Int
myLength [] = 0
myLength (x:xs) = 1 + myLength xs

--funcio 2

myMaximum :: [Int] -> Int
myMaximum (x:xs)
		| myLength xs == 0 = x
		| x < y = y
		| otherwise = x
		where y = myMaximum xs
		
--funcio 3

myPlus :: [Int] -> Int
myPlus [] = 0
myPlus (i:ls) = i + myPlus ls		

average :: [Int] -> Float
average (x:xs)
		| k == 1 = fromIntegral x
		| otherwise = a / k
		where 
			k = fromIntegral (1 + myLength xs) :: Float
			a = fromIntegral (x + myPlus xs) :: Float   
		
--funcio 4

buildPalindrome :: [Int] -> [Int]
buildPalindrome [] = []
buildPalindrome (xs) = ys++xs
				where ys = invertir xs
				
invertir:: [Int] -> [Int]
invertir [] = []
invertir (x:xs) = (invertir xs)++[x]
				

--funcio 5

remove :: [Int] -> [Int] -> [Int]
remove [] ys = []
remove (x:xs) ys	
	| found x ys = remove xs ys
	| otherwise = x:(remove xs ys)

found :: Int -> [Int] -> Bool
found x [] = False 
found x (y:ys)
	| x==y = True
	| otherwise = found x ys

--funcio 6

flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (xs:ls) = xs++flatten ls

--funcio 7

oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens (x:xs)
		| mod x 2 == 0 = (a,x:b)		
		| otherwise = (x:a,b)
		where (a,b) = oddsNevens xs
	
--funcio 8 //acabar

division :: Int -> Int -> Int
division n m 
    	| m == n = m
        | otherwise = let x = mod n m ; y = div n m
        			  in if x == 0 then m
        			  	 else division n (m+1)
    
isPrime :: Int -> Bool
isPrime n
		| n == 0 = False
		| n == 1 = False
		| otherwise = if x == n then True
					  else False
					  where x = division n 2

primeDivisors :: Int -> [Int]
primeDivisors n
		| n==1 = []
		| isPrime x = x:(primeDivisors (div n x))
		| otherwise = primeDivisors (div n x)
		where x = division n 2
		


