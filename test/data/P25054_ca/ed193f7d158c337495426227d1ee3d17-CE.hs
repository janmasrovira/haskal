--1 calcula la llargada duna llista
myLength [] = 0
myLength (x:xs) = 1+myLength xs 

--2 calcula el maxim duna llista
myMaximum [x] = x
myMaximum (x:xs) = max x (myMaximum xs)

--3 calcula la mitja duna llista
average [x] = x
average xs = fromIntegral(suml xs) / fromIntegral(myLength xs)

--suml retorna la suma dels elements duna llista
suml::[Int]->Int
suml[] = 0
suml (x:xs) = x + suml xs
