myLength[] = 0
myLength(x:xs) = 1 + (myLength xs)

myMaximum [x]=x
myMaximum[x,y]
  | x>=y = x
  | otherwise = y
  
myMaximum(x:(y:xs))
  | x>=y = (myMaximum (x:xs))
  | otherwise = (myMaximum (y:xs))


auxAvg n sum [x] = ((sum+x) / (n+1))
auxAvg n sum (x:xs) = (auxAvg (n+1) (sum+x) xs)

average xs = (auxAvg 0 0 xs)

buildPalindrome::[Int]->[Int]
buildPalindrome xs = (reverseP xs)++(xs)

reverseP::[Int]->[Int]
reverseP []=[]
reverseP (x:xs) = ((reverseP xs) ++[x])

remove:: [Int]->[Int]->[Int]
remove xs [] = xs
remove xs (y:ys) = (remove (auxRemove xs y) ys)

auxRemove [] _ = []
auxRemove (x:xs) y
  |x==y = (auxRemove xs y)
  |otherwise = ([x]++(auxRemove xs y))
  
  
flatten::[[Int]]->[Int]

flatten [] = []
flatten (x:xs) = x++(flatten xs)


oddsNevens xs = ((oddN xs),(evenN xs))

evenN [] = []
evenN (x:xs)
  |(x `mod` 2 == 0 ) = [x]++(evenN xs)
  |otherwise = evenN xs

oddN [] = []
oddN (x:xs)
  |(x `mod` 2 == 1 ) = [x]++(oddN xs)
  |otherwise = oddN xs

isPrime 0 = False
isPrime 1 = False
isPrime x = auxIsPrime 2 x

auxIsPrime a n
  |a*a>n = True
  |n `mod` a == 0 = False
  |otherwise = auxIsPrime (a+1) n
  
primeDivisors x = auxPrimeD 1 x
auxPrimeD a x
  |a>x = []
  |(x `mod` a == 0)&& (isPrime a) = [a]++(auxPrimeD (a+1) x)
  |otherwise = auxPrimeD (a+1) x
