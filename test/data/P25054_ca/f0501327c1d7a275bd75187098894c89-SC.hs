--myMaximum
myMaximum [x] = x
myMaximum (x:xs) = max x (myMaximum xs)

--average
average [x] = x
average l@(x:xs) = sum l / myLength l

--myLength
myLength [] = 0
myLength (x:xs) = 1 + myLength xs

--buildPalindrome
buildPalindrome l@(x:xs) = 
 	(darvuelta [x] xs) ++ l
 	where 
		darvuelta xs [] = xs  
		darvuelta lp@(p:ps) ly@(y:ys) = 
			darvuelta ([y]++lp) ys

--flatten
flatten [] = []
flatten [x] = x
flatten (x:xs) =  x ++ (flatten xs)

--remove 
--Por que esto([x]) no funciona aquí remove [x] [y] = x
remove [] (y:ys) = []
remove (x:xs) [] = (x:xs)
remove [] [] = []
remove l1@(x:xs) l2@(y:ys) = remove (filter (/=y) l1) ys

--oddsNevens
oddsNevens [] = ([],[])
oddsNevens l@(x:xs) = (filter odd l, filter even l)

--primeDivisors
primeDivisors x = filter isPrime (primeDivisors2 x 1)
   where
      primeDivisors2 x y
         | y > x = []
         | 0 == (mod x y) = y:primeDivisors2 x (y+1)
         | otherwise = primeDivisors2 x (y+1)

--isPrime
isPrime n = if n <= 1 then False
      else isPrime2 n 2
   where
      isPrime2 n m
         | n < (m * m) = True
         | res /= 0  = isPrime2 n (m + 1)
         | otherwise = False
         where
            res = mod n m
