myLength:: [Int] -> Int
myLength [] = 0
myLength (x : xs) = myLength xs + 1


myMaximum:: [Int] -> Int
myMaximum (x : xs) = 
  if xs == [] then x
  else max x (myMaximum(xs))

  
gDiv:: Int -> Int -> Float
gDiv a b = (fromIntegral a) / (fromIntegral b) 
  
average:: [Int] -> Float
average x = gDiv (sum x) (myLength x)



invert:: [Int] ->  [Int]
invert [] = []
invert (x : xs) = ((invert xs)++[x])

buildPalindrome:: [Int] -> [Int]
buildPalindrome [] = []
buildPalindrome x = ((invert x)++x)




removeX:: Int -> [Int] -> Bool
removeX x [] = False
removeX x (y:ys) =
  if x == y then True
	    else removeX x ys

remove:: [Int] -> [Int] -> [Int]
remove [] y = []
remove (x:xs) y = 
  if removeX x y then remove xs y
		 else [x]++(remove xs y)
		 	 
		 
flatten:: [[Int]] -> [Int]
flatten [] = []
flatten (x:xs) = x++(flatten xs)


--oddsNevens:: [Int] -> ([Int], [Int])
--oddsNevens 