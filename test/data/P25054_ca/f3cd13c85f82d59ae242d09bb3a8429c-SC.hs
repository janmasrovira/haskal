myLength [] = 0
myLength (x:xs) = 1 + myLength (xs)

myMaximum [x] = x
myMaximum (x:xs) | myMaximum (xs) > x = myMaximum (xs)
				 | otherwise = x
				 
average l = (sum l)/(myLength(l))

buildPalindrome l = (reverse l) ++ l

flatten [] = []
flatten (x:xs) = x ++ flatten (xs)

odds [x] | odd x == True = [x]
         | otherwise = []

odds (x:xs)  | odd x == True = [x] ++ odds (xs)
             | otherwise = odds (xs)

evens [x] | odd x == False = [x]
          | otherwise = []

evens (x:xs)  | odd x == False = [x] ++ evens (xs)
              | otherwise = evens (xs)
 
oddsNevens l = (odds(l), evens(l))

isPrime_aux x y | (mod x y) == 0 = False
				| (y > (div x 2)) = True
		    	| otherwise = isPrime_aux x (y+1)

isPrime x | x == 1 = False
 		  | x == 2 = True
 		  | otherwise = isPrime_aux x 2

getDivisors x y | (y > (div x 2)) = []
					| ((mod x y) == 0) = (y : getDivisors x (y+1))
					| otherwise = getDivisors x (y+1)

primeDivisors x = [y | y <- (getDivisors x 2), isPrime(y)]

remove_from_list (a:as) b | (as == []) && (a /= b) = [a]
						  | (as == []) && (a == b) = []
						  | a /= b = a:(remove_from_list as b)
						  | otherwise = remove_from_list as b

remove a (b:bs) = remove (remove_from_list a b) bs
remove a[] = a