  
--llargada::[Int] -> Int


myLength [] = 0
myLength (_:cua) = 1 + myLength cua

myMaximum::[Int] -> Int
myMaximum (cap:cua) = myMaximumv2 cua cap
myMaximumv2 [] maxim = maxim
myMaximumv2 (cap:cua) maxim = myMaximumv2 cua (max maxim cap)
  
average::[Int] -> Float
average x = (fromIntegral (suma x))/(fromIntegral (myLength x))
suma [] = 0;
suma (cap:cua) = cap + (suma cua)

remove :: [Int] -> [Int] -> [Int]
remove [] ys = []
remove (x:xs) ys 
  | not (hies ys) 	= (x:resta)
  | otherwise 		= resta
  where 
    resta = remove xs ys
    hies [] = False
    hies (y:ys) = x == y || hies ys
    
flatten::[[Int]] -> [Int]
flatten [] = []
flatten (x:xs) = passe x
  where
    passe [] = flatten xs;
    passe (y:ys) = y:(passe ys)
    
  --girar [] = []
  --girar (y:ys) = girar ys 
oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens (x:xs)
  | odd x 	= (x:senars, parells)
  | otherwise	= (senars, x:parells)
  where 
    (senars, parells) = oddsNevens xs
    
primeDivisors::Int -> [Int]
primeDivisors x 
  | x<2 	= []
  |otherwise	= primerDiv 2
  where
    primerDiv i
      |(mod x i) == 0 	= (i:(primeDivisors (div x i)) )
      |otherwise	= primerDiv (i+1)

      
buildPalindrome::[Int] -> [Int]
buildPalindrome xs = (invertir xs xs)
  where
    invertir list [] = list
    invertir list (y:ys) = invertir (y:list) ys

    
