myLength::[Int]->Int
myLength [] = 0
myLength (x:xs) = 1 + myLength xs

myMaximum::[Int]->Int
myMaximum [x] = x
myMaximum (x:xs) 
 		| x > myMaximum xs = x
 		|otherwise = myMaximum xs

suma::[Int]->Int
suma [x] = x
suma (x:xs) = x + suma xs

average::[Int]->Float
average [x] = fromIntegral x
average x = fromIntegral(suma x) / fromIntegral(myLength x)


buildPalindrome::[Int] -> [Int]
buildPalindrome [s] = [s]
buildPalindrome s = (reverse s) ++ s

deleter:: [Int]->Int->[Int]
deleter [] _= []
deleter (x:xs) y
			| x == y = deleter xs y
			| otherwise = [x] ++ (deleter xs y)


remove::[Int]->[Int]->[Int]
remove x [] = x
remove [] _= []
remove x (y:ys) = remove (deleter x y) ys

flatten::[[Int]]->[Int]
flatten [] = []
flatten (x:xs) = x ++ (flatten xs)

par::[Int] -> [Int]
par [] = []
par (x:xs)
	| mod x 2 == 0 = [x] ++ (par xs)
	|otherwise = par xs

impar::[Int]->[Int]
impar [] = []
impar (x:xs)
	|mod x 2 == 1 = [x] ++ (impar xs)
	|otherwise = impar xs

oddsNevens::[Int]->([Int],[Int])
oddsNevens [] = ([],[])
oddsNevens x = (impar x,par x)

divipr :: Int -> Int -> Bool 
divipr n m   | m > 1 && mod n m == 0 = False
	     | m > 1 && mod n m > 0 = divipr n (m-1)
	     | otherwise = True

isPrime :: Int -> Bool
isPrime n  
		| n == 0 = False
	    | n == 1 = False
	    | otherwise = divipr n (n-1)

ponerdivisorespr::Int->Int->[Int]
ponerdivisorespr x 1 = []
ponerdivisorespr x y
			| (isPrime y) && ((mod x y) == 0) = [y] ++ ponerdivisorespr x (y-1)
			| otherwise = ponerdivisorespr x (y-1)

primeDivisors::Int->[Int]
primeDivisors 0 = []
primeDivisors 1 = []
primeDivisors x = ponerdivisorespr x (x-1)
