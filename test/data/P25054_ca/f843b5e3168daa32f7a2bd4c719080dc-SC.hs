myLength2 [] = 0
myLength2 (x:xs)
    | xs == [] = 1
    | otherwise = 1 + myLength2 xs

myLength a = length a

myMaximum (x:xs) = foldl max x xs

average a = (foldl (+) 0 a) / myLength2 a

buildPalindrome2 [] = []
buildPalindrome2 (x:xs) = (buildPalindrome2 xs) ++ [x]

buildPalindrome a = (reverse a) ++ a

remove a b = filter (\x -> all (x /=) b) a

flatten a = foldl (++) [] a

oddsNevens a = (filter odd a, filter even a)

primes = sieve [2..]
  where
    sieve (p:xs) = p : sieve [x | x <- xs, x `mod` p > 0]

primeDivisors n = filter (\x -> (n `mod` x) == 0) (takeWhile (<n) primes)
