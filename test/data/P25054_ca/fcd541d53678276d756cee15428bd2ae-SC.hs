myLength :: [Int] -> Int
myLength [] = 0
myLength [x] = 1
myLength (cap:cua) = 1 + myLength cua

myMaximum :: [Int] -> Int
myMaximum [x] = x
myMaximum (x:xs) = max x (myMaximum xs)

sumlist [] = 0
sumlist [x] = x
sumlist (x:xs) = x + (sumlist xs)

average :: [Int] -> Float
average l = fromIntegral suma /  fromIntegral n
    where
        suma =  sumlist l
        n = myLength l

rotateList :: [Int] -> [Int]
rotateList [x] = [x]
rotateList (x:xs) = l ++ [x]
    where l = rotateList xs

buildPalindrome :: [Int] -> [Int]
buildPalindrome [] = []
buildPalindrome l = ls ++ l
    where ls = rotateList l

flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (x:xs) = x ++ (flatten xs)

listOdds :: [Int] -> [Int]
listOdds [] = []
listOdds (x:xs)
  | r /= 0 = [x] ++ listOdds xs
  | otherwise = listOdds xs
  where r = mod x 2

listEvens :: [Int] -> [Int]
listEvens [] = []
listEvens (x:xs)
  | r == 0 = [x] ++ listEvens xs
  | otherwise = listEvens xs
  where r = mod x 2

oddsNevens :: [Int] -> ([Int],[Int])
oddsNevens l = (l1 , l2)
  where l2 = listEvens l
        l1 = listOdds l

teFactor :: Int -> Int -> Bool
teFactor n c
    | c == 1 = False
    | n `mod` c == 0 = True
    | otherwise = teFactor n (c-1)

isPrime :: Int -> Bool
isPrime n
    | n == 0 = False
    | n == 1 = False
    | otherwise = not(teFactor n (n-1))

divsList :: Int -> Int -> [Int]
divsList x 1 = []
divsList x y =
    if (mod x y == 0) && isPrime y then
        l ++ [y]
    else l
    where l = divsList x (y - 1)


primeDivisors :: Int -> [Int]
primeDivisors 1 = []
primeDivisors x
    | isPrime x = [x]
    | otherwise = divsList x (x - 1)