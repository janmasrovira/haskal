flatten :: [[Int]] -> [Int]
flatten = foldl (++) []

myLength = sum . map (\_->1)

myReverse = foldl (flip (:)) []

countIn ls x = (map length . map (filter (==x))) ls

firstWord = takeWhile esLletra . dropWhile (not.esLletra)
    where esLletra x = ('a' <= x && x <= 'z') ||
                       ('A' <= x && x <= 'Z')