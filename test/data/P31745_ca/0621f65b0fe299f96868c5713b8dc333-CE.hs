
flatten :: [[Int]] -> [Int]
flatten xs = concat xs

myLength :: String -> Int
myLength [] = 0
myLength s = 1+(myLength (tail s))

myReverse :: [Int] -> [Int]
myReverse [] = []
myReverse xs = (last xs):(myReverse (init xs))

countIn :: [[Int]] -> Int -> [Int]
countIn [] n = []
countIn (x:xs) n = (myLength (filter (==n) x)):(countIn xs n)

firstWord :: String -> String
firstWord s = head (words s)
