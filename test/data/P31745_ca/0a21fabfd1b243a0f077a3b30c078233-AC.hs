flatten :: [[Int]] -> [Int]
myLength :: String -> Int
myReverse :: [Int] -> [Int]
countIn :: [[Int]] -> Int -> [Int]
firstWord :: String -> String

flatten lln = foldl (++) [] lln

myLength ss = length ss

myReverse ln = reverse ln

countIn [] n = []
countIn (x:xs) n = [length [y | y <- x, y == n]] ++ (countIn xs n)

firstWord s = unwords (take 1 $ words s)