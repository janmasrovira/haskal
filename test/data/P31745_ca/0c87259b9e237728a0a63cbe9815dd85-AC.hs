flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (x:xs) =
	x ++ flatten xs

myLength :: String -> Int
myLength "" = 0
myLength (x:xs) =
	1 + myLength xs

myReverse :: [Int] -> [Int]
myReverse [] = []
myReverse (x:xs) =
	(myReverse xs) ++ [x]

auxCount :: [Int] -> Int -> Int
auxCount [] n = 0
auxCount (x:xs) n
	| x == n	= 1 + auxCount xs n
	| otherwise = auxCount xs n

countIn :: [[Int]] -> Int -> [Int]
countIn l n =
	map (\x -> (auxCount x n)) l

auxFirstWord :: String -> String
auxFirstWord l =
	takeWhile (/= ' ') l

firstWord :: String -> String
firstWord (x:xs)
	| x == ' '	= firstWord xs
	| otherwise = auxFirstWord (x:xs)