flatten :: [[Int]]->[Int]

flatten [] = []
flatten (x:xs) = x ++ flatten xs

myLength :: String -> Int

myLength [] = 0
myLength (x:xs) = 1 + myLength xs

myReverse :: [Int] -> [Int]

myReverse [] = []
myReverse [x] = [x]
myReverse (x:xs) = myReverse xs ++ [x]

ocurrence :: [Int] -> Int -> Int

ocurrence [] y = 0;
ocurrence (x:xs)  y
  | x == y = 1 + ocurrence xs y
  |otherwise = ocurrence xs y


countIn :: [[Int]] -> Int -> [Int]

countIn [[]] y = [0];
countIn [xs] y = [(ocurrence xs y)]
countIn (x:xs) y = [(ocurrence x y)] ++ (countIn xs y)


firstspace :: String -> String

firstspace [] = ""
firstspace (x:xs)
  | x /= ' ' = x : firstspace xs
  |otherwise = ""

firstWord :: String -> String

firstWord [] = ""
firstWord (x:xs)
  | x == ' ' = firstWord xs
  |otherwise = x : firstspace xs