flatten :: [[Int]] -> [Int]
flatten l = foldl (++) [] l

myLength :: String -> Int
myLength xs = foldl (\n x -> n+1) 0 xs

myReverse :: [Int] -> [Int]
myReverse l = foldl (flip (:)) [] l

times :: [Int] -> Int -> Int
times ls x = length ((filter (\a -> a == x)) ls)

countIn :: [[Int]] -> Int -> [Int]
countIn ls x = foldr (\a b -> (times a x):b) [] ls

firstWord :: String -> String
firstWord "" = ""
firstWord (a:as) = if a == ' ' then firstWord as
                   else a:(takeWord as)

takeWord :: String -> String
takeWord "" = ""
takeWord (a:as) = if a == ' ' then ""
                  else a:(takeWord as)

