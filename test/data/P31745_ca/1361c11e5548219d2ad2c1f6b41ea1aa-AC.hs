
flatten::[[Int]] -> [Int]
flatten xs = foldl (++) [] xs

myLength::String -> Int
myLength xs = foldl (\x y -> x + 1) 0 xs

myReverse::[Int] -> [Int]
myReverse xs = foldl (flip (:)) [] xs

countIn::[[Int]] -> Int -> [Int]
countIn xs x = map (howMany x) xs
	where howMany x ys = (length (filter (==x) ys))

firstWord::String -> String
firstWord xs = takeWhile (\x -> x/=' ') (dropWhile (\x -> x==' ') xs)