flatten l = foldr (++) [] l

myLength l = foldr (\x y -> (y + 1)) 0 l

-- myLength = foldr (const (+1)) 0

myReverse = foldl (\l1 x -> (x:l1)) []

countIn ll x = map (\l -> myLength (filter (==x) l)) ll

--countIn ll x = map myLength (map (filter (==x)) ll)

firstWord s = takeWhile (/= (' ')) (dropWhile (== (' ')) s)

