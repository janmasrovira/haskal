flatten :: [[Int]] -> [Int]
flatten xs = foldl (++) [] xs

myLength :: String -> Int
myLength x = foldl (+) (0) (map (f) (map (/= ' ') x))
	where
		f True = 1
		f False = 0

myReverse :: [Int] -> [Int]
myReverse xs = foldl (flip (:)) [] xs

countIn :: [[Int]] -> Int -> [Int]
countIn xs y = [(length2 (filter (==y) x))| x <- xs]
	where
		length2 [] = 0
		length2 (x:xs) = length (x:xs)


firstWord :: String -> String
firstWord xs = takeWhile (/= ' ') (dropWhile (==' ')  xs)
