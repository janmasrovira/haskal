flatten::[[Int]] -> [Int]
flatten xs = foldl (++) [] xs

myLength::String->Int
myLength xs = foldl (+) 0 (map f xs)
	where f x = 1

myReverse::[Int]->[Int]
myReverse xs = foldl f [] xs
	where f xs y = y:xs

countIn::[[Int]]-> Int->[Int]
countIn xs x = map f xs
	where f ys = length (filter (==x) ys)

firstWord::String->String
firstWord xs = takeWhile (/=' ') (dropWhile (==' ') xs)

