flatten::[[Int]]->[Int]
flatten x = foldr (++) [] x

myLength::String->Int
myLength s = foldr (\_ n -> 1 + n) 0 s

myReverse::[Int]->[Int]
myReverse x = foldl (flip (:)) [] x

--countIn::[[Int]]->Int->[Int]
--countIn x l = foldl (_ myLength (filter (==l))) 0 x

firstWord::String->String
firstWord s = (words s) !! 0