flatten:: [[Int]] -> [Int] 
flatten x = 
	foldl (++) [] x

myLength:: String -> Int
myLength s = 
	foldr (+1) 0 s

--myReverse:: [Int] -> [Int]
--myReverse x =
