-- exemple ús if then else
flatten = foldr (++) []
myLength = foldr (\a b -> b + 1) 0
myReverse = foldr (\a b -> b++[a]) []

countN x l = length (filter (==x) l)
countIn l x = map (countN x) l
firstWord s = takeWhile (/= ' ')  (dropWhile (== ' ') s)