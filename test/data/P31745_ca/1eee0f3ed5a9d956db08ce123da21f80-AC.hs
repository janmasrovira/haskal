--concat
flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (a) = foldl (++) [] (a)

-- length
myLength :: String -> Int
myLength s = foldr (\ _ n -> 1+n) 0 s
--myLength :: String -> Int
--myLength s = sum [1 | x <- s]

--reverse
myReverse :: [Int] -> [Int]
myReverse [] = []
myReverse (xs) = foldl (flip(:)) [] (xs)


--contar
apariciones :: Int -> [Int] -> Int
apariciones b (a) = length (filter (==b) a)

countIn :: [[Int]] -> Int -> [Int]
countIn (a) b =  map (apariciones b) (a)

--firstWord
firstWord :: String -> String
firstWord a = takeWhile(/= ' ') (dropWhile (==' ') a)
