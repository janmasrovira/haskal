flatten :: [[Int]] -> [Int]
flatten xs = foldr (++) [] xs

myLength :: String -> Int
myLength s = foldr (+) 0 (map (const 1) s)

myReverse :: [Int] -> [Int]
myReverse xs = foldl (flip (:)) [] xs

countIn :: [[Int]] -> Int -> [Int]
countIn lst e = map (length) (map (filter (== e)) lst)

firstWord :: String -> String
firstWord s = takeWhile (/= ' ') (dropWhile (== ' ') s)

