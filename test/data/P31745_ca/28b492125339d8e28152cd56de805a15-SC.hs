flatten::[[Int]] -> [Int]
flatten l = foldr (++) [] l 

myLength:: String -> Int
myLength l = foldr (sum1) 0 l

sum1:: Char -> Int -> Int
sum1 c n = n + 1

myReverse::[Int] -> [Int]
myReverse l = foldr concat2 [] l

concat2:: Int -> [Int] -> [Int]
concat2 l n = n++[l]
