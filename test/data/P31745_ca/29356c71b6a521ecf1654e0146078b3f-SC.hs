flatten = foldl (++) []
myLength::String->Int
myLength = foldl (\n x -> n + 1) 0
myReverse = foldl (\x y ->y:x) []
countIn xs x = map (count x) xs  

count x [] = 0
count x (y:ys)
      | x == y = 1 + count x ys
      | otherwise = count x ys

firstWord = takeWhile(/=' ').dropWhile(==' ')
