flatten::[[Int]]->[Int]
flatten [] = []
flatten xs = foldr (++) [] xs

myLength::String->Int
myLength [] = 0
myLength cs = foldr (+) 0 (f cs)
  where f::String->[Int]
        f [] = []
        f (e:es) = 1:(f es)

countIn::[[Int]]->Int->[Int]
countIn [] _ = []
countIn (e:es) x = (length (filter (== x) e)):(countIn es x)

firstWord::String->String
firstWord [] = ""
firstWord (c:cs)
  | c == ' ' = firstWord cs
  | otherwise = takeWhile (/= ' ') (c:cs)

