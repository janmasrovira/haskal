flatten :: [[Int]] -> [Int]
flatten = foldr (++) []

--myLength :: String -> Int
--myLength x = foldl (+) 0 (map (=1) x)
-- transformarlos en 1 i sumarlos

myReverse :: [Int] -> [Int]
myReverse = foldl (flip (:)) [] 

cont :: [Int] -> Int -> Int
cont xs n :: filter (==n) xs

--countIn :: [[Int]] -> Int -> [Int]
--countIn	xs n	=	map filter (==n) xs 
-- countIn [[3,2,3],[3],[], [2,2]] 3
-- [2,1,0,0]

firstWord :: String -> String
firstWord xs = takeWhile ((/=) ' ') (dropWhile ((==) ' ') xs)