flatten :: [[Int]] -> [Int]
flatten [h] = h
flatten (h:t) = h ++ flatten t

myLength :: String -> Int
myLength [] = 0
myLength xs = length xs

myReverse :: [Int] -> [Int]
myReverse xs = reverse xs

countIn :: [[Int]] -> Int -> [Int]
countIn xs x = map (length . filter (==x)) xs

firstWord :: String -> String
firstWord xs = takeWhile (/=' ') (dropWhile (==' ')  xs)