flatten :: [[Int]] -> [Int]
flatten x = foldr (\ele acc -> ele ++ acc) [] x

flatten2 :: [[Int]] -> [Int]
flatten2 x = foldl (++) [] x  

myLength :: String -> Int
myLength x = foldl (\acc _ -> acc+1) 0 x

myLength2 :: String -> Int
myLength2 x = sum.map (\_ -> 1) $ x

myReverse :: [Int] -> [Int]
myReverse x = foldl (flip(:)) [] x

countIn :: [[Int]] -> Int -> [Int]
countIn l e = foldr (\li acc -> (length.filter (== e) $ li) : acc) [] l

firstWord :: String -> String
firstWord x = takeWhile (/= ' ') (dropWhile (== ' ') x) 