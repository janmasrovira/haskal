-- --aplana una llista de llistes d'enters
flatten :: [[Int]]->[Int]
flatten xs = concat xs

-- retorna llargada cadena de caracters
myLength :: String -> Int
myLength xs = sum(foldr (\x acc -> 1:acc) [] xs)

-- Inverteix els elements d'una llista d'enters
myReverse :: [Int]->[Int]
myReverse xs = foldl (\acc x -> x:acc)[] xs

-- donada una llista de llistes d'elements k
-- i un element x ens torna la llista que indica quants cops
-- apareix x en cada llista de l
countIn :: [[Int]]->Int->[Int]
countIn xs e = map length ( map (filter (== e)) xs)

-- donat un string amb blancs i caracters alfabetics,
-- retorni la primera paraula
firstWord :: String->String
firstWord xs = takeWhile (/= ' ')(dropWhile (== ' ') xs) 