flatten:: [[Int]] -> [Int]
flatten m = foldr (++) [] m

myLength:: String -> Int
myLength s = length s

myReverse:: [Int] -> [Int]
myReverse l = reverse l

countIn:: [[Int]] -> Int -> [Int]
countIn[] b = []
countIn (elem:llista) b = [(length ( filter(\x->x == b) elem ) )] ++ (countIn llista b)

firstWord:: String->String
firstWord frase = head (words frase)
