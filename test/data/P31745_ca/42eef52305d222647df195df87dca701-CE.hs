flatten :: [[Int]] -> [Int]
flatten (x:xs) = foldr (++) [] (x:xs)

myLength :: String -> Int
myLength x = foldr (\_ n -> 1 + n) 0 x

myReverse :: [Int] -> [Int]
myReverse (x:xs) = foldr (\_ n -> [] ++ [n]) x xs