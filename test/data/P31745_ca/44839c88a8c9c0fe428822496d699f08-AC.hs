flatten :: [[Int]] -> [Int]
flatten x = foldl (++) [] x

myLength :: String -> Int
myLength x = foldl (ignoreSum1) 0 x

ignoreSum1 x y = x + 1

myReverse :: [Int] -> [Int]
myReverse x = foldl (hodor) [] x

hodor x y = y:x

countIn :: [[Int]] -> Int -> [Int]
countIn x y = map (myCount y) x

myCount y x = foldl (myEqual y) 0 x

myEqual y a b
  | b == y = a + 1
  | otherwise = a
  
firstWord :: String -> String
firstWord = (takeWhile (/= ' ')).(dropWhile (== ' '))