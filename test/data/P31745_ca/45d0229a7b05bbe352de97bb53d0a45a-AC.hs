flatten::[[Int]]->[Int]
flatten l = foldl (++) [] l


myLength::[a]->Int
myLength xs = sum [1|x<-xs] --sumatori de 1's en length the xs

myLengthZwei::[a]->Int
myLengthZwei xs = foldl (\n _ -> 1+n) 0 xs  

-- \n funció anònima (qualsevol)
-- _ qualsevol valor

myReverse::[Int]->[Int]
myReverse l = foldl (flip (:)) [] l  --we flip the operator :, that has input x:xs to xs:x

--the function flip (:) is used as a parameter in fold, then will iterate through the whole list

countIn::[[Int]]->Int->[Int]
countIn l x = map myLength (map (filter (==x)) l) 

firstWord::String->String
firstWord s = takeWhile (/=' ') (dropWhile  (==' ') s)