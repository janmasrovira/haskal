--flatten::[[Int]]->[Int]
--flatten (x:xs) = foldl (++) x xs

--myLength::String->Int
--myLength s = foldl (\x z-> (x+1)) 0 s

f::[Int]->Int->[Int]
f l x = [x]++l

myReverse::[Int]->[Int]
myReverse l = foldl f [] l

nAppearance::[Int]->Int->Int
nAppearance [] y = 0
nAppearance (x:l) y
	|x == y = 1+ nAppearance l y
	|otherwise = nAppearance l y

firstWord::String->String
firstWord [] = []
firstWord s = x
	where (x:xs) = (words s)
