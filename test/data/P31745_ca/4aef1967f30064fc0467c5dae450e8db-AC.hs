



flatten::[[Int]]->[Int]
flatten xs = foldl (++) [] xs

afegir1 num s = num+1;

myLength::String->Int
myLength s = foldl (afegir1) 0 s

myReverse ::[Int]->[Int]
myReverse xs = foldl (flip (:)) [] xs

countIn::[[Int]]->Int->[Int]
countIn xs num = map result xs
  where
    result ys = length (filter (== num) ys) 

firstWord::String->String
firstWord s = takeWhile (/= ' ') (dropWhile (== ' ') s)
