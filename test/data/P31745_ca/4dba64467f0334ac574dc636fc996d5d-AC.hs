-- flatten

flatten :: [[Int]] -> [Int]
flatten x = (foldr (++) [] x)

-- myLength

myLength :: String -> Int
myLength s = (foldr (\x acc -> acc + 1) 0 s)

-- myReverse

myReverse :: [Int] -> [Int]
myReverse = (foldl (\acc x -> x : acc)) []

-- countIn

countIn :: [[Int]] -> Int -> [Int]
countIn l x = (map (\l2 -> (length (filter (x==) l2))) l)

-- firstWord

firstWord :: String -> String
firstWord s = (takeWhile (' '/=) (dropWhile (' '==) s))
