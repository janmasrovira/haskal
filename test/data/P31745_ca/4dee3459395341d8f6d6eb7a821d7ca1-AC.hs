flatten :: [[Int]] -> [Int]
flatten xs = foldl (++) [] xs

myLength :: String -> Int
myLength s = foldl(\x y -> x + 1) 0 s

myReverse :: [Int] -> [Int]
myReverse xs = foldl (flip(:)) [] xs

countIn :: [[Int]] -> Int -> [Int]
countIn xs n = map (count n) xs
	where count n ys = length (filter (==n) ys)

firstWord :: String -> String
firstWord s = takeWhile (\x -> x/=' ') (dropWhile(\x -> x==' ') s)