flatten :: [[Int]] -> [Int]
flatten xs = foldl (++) [] xs

--myLength :: String -> Int
 -- myLength xs = foldl (+) 0 (map )

myReverse :: [Int] -> [Int]
myReverse xs = foldl (flip (:)) [] xs

--countIn :: [[Int]] -> Int -> [Int]
--countIn xs x = map (filter ==x) xs

firstWord :: String -> String
firstWord xs = takeWhile (/= ' ') xs