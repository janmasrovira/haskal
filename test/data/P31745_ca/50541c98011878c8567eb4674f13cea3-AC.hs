--llista de llistes transformar en una llista:
flatten :: [[Int]] -> [Int]
flatten = foldl (++) []

--retorna el tamany d'una cadena de caracters
myLength :: String -> Int
myLength xs = sum [1 | x<-xs]
-----------------------------
--myLength xs = foldl f 0 xs
--    where f n x = 1+n
----------------------------
--myLength xs = foldl (\n _ -> 1+n) 0 xs
----------------------------------------

--donada una llista d'enters retorna el reverse
myReverse :: [Int] -> [Int]
myReverse = foldl (flip (:)) []

--donada una llista de llistes y un element et retorna quants cops apareix x a cada llista.
countIn :: [[Int]] -> Int -> [Int]
countIn xs a = [length (filter (==a) x1) | x1<-xs]

--trobar la primera paraula donada una frase:
firstWord :: String -> String
firstWord xs = takeWhile(/= ' ') (dropWhile(== ' ') xs)