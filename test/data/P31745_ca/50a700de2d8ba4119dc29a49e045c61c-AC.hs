flatten :: [[Int]] -> [Int]
flatten x = foldr (++) [] x

myLength :: String -> Int
myLength x = foldr (\_ b->b+1) 0 x

myReverse :: [Int] -> [Int]
myReverse l = foldl (\x y->y:x) [] l

mylength [] = 0
mylength (x:xs) = 1 + mylength xs

countIn :: [[Int]] -> Int -> [Int] 
countIn x n = map (\a->mylength (filter (==n) a)) x

isN a b
	| a == b = 1
	| otherwise = 0

countIn2 :: [[Int]] -> Int -> [Int] 
countIn2 x n = map (\a->foldr (\s t->t+(isN s n)) 0 a) x

firstWord :: String -> String
firstWord x = takeWhile (/= ' ') (dropWhile (==' ') x)