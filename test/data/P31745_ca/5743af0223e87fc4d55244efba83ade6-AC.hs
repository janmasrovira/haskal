flatten :: [[Int]] -> [Int]
flatten l = foldl (\flattenedList sublist -> flattenedList ++ sublist) [] l

myLength :: String -> Int
myLength s = foldl (\acc letter -> acc + 1) 0 s

myReverse :: [Int] -> [Int]
myReverse l = foldl (flip (:)) [] l

countIn :: [[Int]] -> Int -> [Int]
countIn l x = map (\subL -> length $ filter (== x) subL) l

firstWord :: String -> String
firstWord s = takeWhile (not . isSpace) $ dropWhile isSpace s
    where isSpace = (\char -> char == ' ')