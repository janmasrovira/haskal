flatten::[[Int]]->[Int]
flatten l = foldl (++) [] l

myLength::String->Int
myLength s = foldl (\x _ -> (x+1)) 0 s

myReverse::[Int]->[Int]
myReverse s = foldl (\l x -> [x]++l) [] s

countIn::[[Int]]->Int->[Int]
countIn l x = map (\l2 -> length (filter (==x) l2)) l

firstWord::String->String
firstWord s = takeWhile (esLletra) (dropWhile (not.esLletra) s)
  where
    esLletra x = elem x (['A'..'Z']++['a'..'z'])