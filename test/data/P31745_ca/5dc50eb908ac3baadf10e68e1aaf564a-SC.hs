flatten::[[Int]]->[Int]
flatten [] = []
flatten (x:xs) = foldr (++) [] (x:xs)

{-myLength::String->Int
myLength [] = 0
myLength (c:cs) = 1 + myLength cs 

myReverse::[Int]->[Int]
myReverse [] = []
myReverse (x:xs) = (myReverse xs)++[x]
-}

countIn::[[Int]]->Int->[Int]
countIn [] _ = []
countIn (e:es) x = (length (filter (== x) e)):(countIn es x)

firstWord::String->String
firstWord [] = ""
firstWord (c:cs)
  | c == ' ' = firstWord cs
  | otherwise = takeWhile (/= ' ') (c:cs)
