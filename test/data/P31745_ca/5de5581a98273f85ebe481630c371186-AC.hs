flatten :: [[Int]] -> [Int]
myLength :: String -> Int
myReverse :: [Int] -> [Int]
countIn :: [[Int]] -> Int -> [Int]
firstWord :: String -> String

flatten lln = foldl (++) [] lln

myAdd n x = n + 1
myLength ss = foldl (myAdd) 0 ss

myAppend a b = b:a
myReverse ln = foldl myAppend  [] ln

myCount n x = length [z | z <- x, z == n]
countIn ln n = map (myCount n) ln

firstWord s = unwords (take 1 $ words s)