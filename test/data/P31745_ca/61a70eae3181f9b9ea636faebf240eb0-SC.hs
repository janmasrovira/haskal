flatten :: [[Int]] -> [Int]
flatten = foldl1 (++)

myLength = sum . map (^0)

myReverse = foldl (flip (:)) []

countIn ls x = (map length . map (filter (==x))) ls

firstWord = head . words