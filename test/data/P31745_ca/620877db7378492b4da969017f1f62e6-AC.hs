{-
  Feu una funció flatten :: [[Int]] -> [Int] que aplana una llista de llistes d’enters en una llista d’enters.
  Feu una funció myLength :: String -> Int que retorna la llargada d’una cadena de caràcters.
  Feu una funció myReverse :: [Int] -> [Int] que inverteix els elements d’una llista d’enters.
  Feu una funció countIn :: [[Int]] -> Int -> [Int] que, donada una llista de llistes d’elements ℓ i un element x ens torna la llista que indica quants cops apareix x en cada llista de ℓ.
  Feu una funció firstWord :: String -> String que, donat un string amb blancs i caràcacters alfabètics), en retorna la primera paraula.
-}

flatten :: [[Int]] -> [Int]
flatten l = foldr (++) [] l

myLengthAux :: Char -> Int -> Int
myLengthAux = \_ -> (+1)

myLength :: String -> Int
myLength str = foldr (\_->(+1)) 0 str

myReverse :: [Int] -> [Int]
myReverse l = foldl (flip(:)) [] l

countIn :: [[Int]] -> Int -> [Int]
countIn l x = map (\e -> (length (filter ( == x) e))) l

firstWord :: String -> String
firstWord s = takeWhile (/= ' ') (dropWhile (== ' ') s)
