flatten::[[Int]]->[Int]
flatten [] = []
flatten (x:xs) = x++flatten xs

myLength::String->Int
myLength s = length s

myReverse::[Int]->[Int]
myReverse = foldl (\l x -> x : l) []

countIn::[[Int]]->Int->[Int]
countIn l x = foldr (\ll acc -> (countElem ll x):acc) [] l 

countElem [] e = 0
countElem (x:xs) e
	| x == e = 1 + (countElem xs e)
	| otherwise = (countElem xs e)

firstWord::String->String
firstWord s = takeWhile (/=' ') $ dropWhile(==' ') s