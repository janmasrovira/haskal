flatten :: [[Int]] -> [Int]
flatten = foldl (++) []

myLength :: String -> Int
myLength = foldl (\acc x -> succ acc) 0

myReverse :: [Int] -> [Int]
myReverse = foldl (\acc x -> x : acc) []

countIn :: [[Int]] -> Int -> [Int]
countIn l i = [ length $ filter (==i) elem | elem <- l]

firstWord :: String -> String
firstWord s = takeWhile (/=' ') $ drop (length $ takeWhile (== ' ') s) s
