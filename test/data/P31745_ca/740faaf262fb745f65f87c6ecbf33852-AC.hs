



flatten::[[Int]]->[Int]
flatten xs = foldl (++) [] xs

myLength::String->Int
myLength s = length s

add::Int->[Int]->[Int]
add x xs = xs++[x]

myReverse ::[Int]->[Int]
myReverse xs = foldr (add) [] xs

countIn::[[Int]]->Int->[Int]
countIn xs num = map result xs
  where
    result ys = length (filter (== num) ys) 

firstWord::String->String
firstWord s = takeWhile (/= ' ') (dropWhile (== ' ') s)
