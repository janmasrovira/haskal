flatten :: [[Int]] -> [Int]
flatten l = foldr (++) [] l

myLength :: String -> Int
myLength s = foldr (const (+1)) 0 s

myLength2 :: String -> Int
myLength2 s = foldr (\_ x -> x + 1) 0 s

myReverse :: [Int] -> [Int]
myReverse l = foldl (\x y -> y:x) [] l

countIn :: [[Int]] -> Int -> [Int]
countIn l n = map (\x -> length (filter (==n) x)) l

firstWord :: String -> String
firstWord s = takeWhile (/=' ') (dropWhile (==' ') s)