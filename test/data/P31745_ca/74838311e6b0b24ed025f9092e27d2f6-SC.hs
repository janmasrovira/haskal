eql :: [Int] -> [Int] -> Bool
eql l m 
	|length l /= length m	= False
	|otherwise	= all (==0) (zipWith (-) l m)

prod :: [Int] -> Int
prod [] = 1
prod (x:xs) = foldr (*) x xs

prodOfEvens :: [Int] -> Int
prodOfEvens [] = 1
prodOfEvens l 
	|length onlyEven == 0 	= 1
	|otherwise	= foldr (*) (head onlyEven) (tail onlyEven)
		where
			onlyEven = (filter (even) l)

powersOf2 :: [Int]
powersOf2 = iterate (2*) 1

scalarProduct :: [Float] -> [Float] -> Float
scalarProduct [] [] = 1
scalarProduct l m = foldr (+) 0 (zipWith (*) l m)