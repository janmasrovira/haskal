flatten::[[Int]] -> [Int]
flatten = foldl (++) []

myLength::String -> Int
myLength as = sum [1 | a <- as]
  
myReverse::[Int] -> [Int]
myReverse x = foldl (flip (:)) [] x --sacado de google, porque llevaba una hora y media llorando

--cops que apareix x a cada llista del primer paràmetre
countIn::[[Int]] -> Int -> [Int]
countIn x y = [length (filter (==y) z) | z<-x]
  
firstWord:: String -> String
firstWord str = takeWhile (/=' ') (dropWhile(==' ') str)