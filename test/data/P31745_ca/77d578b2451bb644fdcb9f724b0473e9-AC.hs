-- 1 funcio k diu si dos llistes son iguals
eql :: [Int] -> [Int] -> Bool
eql xs ys
	| (length xs) == (length ys) = and (zipWith (==) xs ys)
	| otherwise = False

--2 funcio k calculi el producte duna llista
prod :: [Int] -> Int
prod xs = foldl (*) 1 xs

--3 multiplica els nombres parells duna llista 
prodOfEvens :: [Int] -> Int
prodOfEvens xs = foldl (*) 1 (filter even xs)

--4 genera les potencies de 2
powersOf2 :: [Int]
powersOf2 = iterate (*2) 1

--5 calcula el producte escalar de dos llistes
scalarProduct :: [Float] -> [Float] -> Float
scalarProduct xs ys = foldl (+) 0 (zipWith (*) xs ys)

---------------------------------------------

--1 aplana una llista de llistes 
flatten :: [[Int]] -> [Int]
flatten xss = foldl (++) [] xss

--2 llargada de cadena de caracters
myLength :: String -> Int
myLength xs = length xs

--3 inverteix els elements duna llista
myReverse :: [Int] -> [Int]
myReverse xs = foldl (flip (:)) [] xs

--4 retorna la llista que indica kuans cops apareix x en cada llista de l
countIn :: [[Int]] -> Int -> [Int]
--countIn xss n = foldr (\x y -> (length (filter (==n) x)):y) [] xss
countIn xss n = map(\x -> (length(filter (== n) x))) xss

--5 retorna la primera paraula
firstWord :: String->String
firstWord s =  takeWhile ( /= ' ') (dropWhile (== ' ') s)

--------------------------------------------
--1 emula map usant llistes per compresio
myMap :: (a -> b) -> [a] -> [b]
myMap f xs = [(f x) | x <- xs]



