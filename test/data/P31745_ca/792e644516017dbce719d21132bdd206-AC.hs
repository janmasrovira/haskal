flatten::[[Int]]->[Int]
flatten ll = foldl (++) [] ll

myLength::String->Int
myLength str = foldl (\x y -> x+1) 0 str

myReverse::[Int]->[Int]
myReverse l = foldl (flip (:)) [] l

count l x = foldl (\a b -> if b==x then a+1 else a) 0 l

countIn::[[Int]]->Int->[Int]
countIn l x = foldr (\a b -> (count a x):b) [] l

firstWord::String->String
firstWord str = takeWhile (\c -> c /= ' ') $
								dropWhile (\c -> c == ' ') str
