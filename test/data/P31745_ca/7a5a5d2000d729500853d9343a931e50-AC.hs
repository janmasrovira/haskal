flatten :: [[Int]] -> [Int]
flatten l = foldl (++) [] l

myLength :: String -> Int
myLength s = length s

myReverse :: [Int] -> [Int]
myReverse l = reverse l

countIn :: [[Int]] -> Int -> [Int]
countIn l x = map (conta x) l
  where	conta :: Int -> [Int] -> Int
	conta x [] = 0
	conta x l@(y:ys)
	  | x == y 	= 1 + conta x ys 
	  | otherwise	= 0 + conta x ys
	  
firstWord :: String -> String
firstWord s = takeWhile dif (dropWhile eq s)
  where eq :: Char -> Bool
	eq a = a == ' '
	
	dif :: Char -> Bool
	dif a = not $ eq a