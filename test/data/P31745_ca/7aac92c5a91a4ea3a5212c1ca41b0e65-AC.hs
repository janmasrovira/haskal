flatten:: [[Int]] -> [Int] 
flatten x = 
	foldl (++) [] x

mLaux:: Int -> Char -> Int
mLaux x c = 
	x + 1	

myLength:: String -> Int
myLength s = 
	foldl mLaux 0 s

myReverse:: [Int] -> [Int]
myReverse x = reverse x

countAux:: Int -> [Int] -> Int
countAux x = length . filter (\x' -> x' == x)

countIn:: [[Int]] -> Int -> [Int]
countIn l x =
	map (countAux x) l

firstWord:: String -> String
firstWord s =
	takeWhile (/=' ') (dropWhile (==' ') s)