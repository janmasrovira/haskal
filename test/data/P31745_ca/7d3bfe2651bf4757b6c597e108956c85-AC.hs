
flatten::[[Integer]] -> [Integer]
flatten = foldr (++) []

-- flatten l = foldr (++) [] l

myLength l = foldr (\x y -> (y + 1)) 0 l

-- myLength = foldr (const (+1)) 0

myReverse = foldl (\l1 x -> (x:l1)) []

countInOne x l = length $ filter (==x) l

countIn ll x = map (countInOne x) ll

--countIn ll x = map myLength (map (filter (==x)) ll)

firstWord s = takeWhile (/= (' ')) (dropWhile (== (' ')) s)

{-
per propers exercicis, fer servir: read, fromIntegral, show, print, words

main = do
    x <- getLine
    putStrLn $ show (factorial (read x)) -- es equivalent a "print (factorial (read x))"
    --el $ es equivalent als parentesis
    
factorial 0 = 1
factorial n = n * factorial (n - 1)
-}

{-
main = do
    x <- getLine
    f <- return $ factorial $ read x
    putStrLn $ show f

factorial 0 = 1
factorial n = n * factorial $ n-1 --comprovar si aquest $ funciona
-}