flatten [] = [] 
flatten (x:xs) = x ++ flatten xs

myLength l = foldr (+) 0 [1 | x <- l]

myReverse :: [Int] -> [Int]
myReverse [] = []
myReverse (x:y:xs) = (myReverse xs) ++ [x]

countIn l n = myLength (filter (==n) l)

firstWord l = takeWhile (\x -> x /= ' ') l