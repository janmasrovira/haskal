flatten :: [[Int]] -> [Int]
flatten = foldl(\acc x -> acc ++ x) [] 

myLength::String->Int
myLength xs = foldl(\acc x -> acc + 1) 0 xs 

myReverse::[Int]->[Int]
myReverse xs = foldl(\acc x -> x : acc) [] xs

quantesVegades::[Int]->Int->Int
quantesVegades [] a = 0
quantesVegades (x:xs) n 
	| x == n = quantesVegades xs n + 1
	| otherwise = quantesVegades xs n + 0


countIn::[[Int]]->Int->[Int]
countIn xs n = map(\x -> quantesVegades x n) xs 

firstWord::String -> String
firstWord = head . words
