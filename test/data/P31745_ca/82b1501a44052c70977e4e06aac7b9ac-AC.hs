flatten :: [[Int]] -> [Int]
flatten x = foldl (++) [] x

myLength :: String -> Int
myLength x = foldl (\acc _ -> acc + 1) 0 x

myReverse :: [Int] -> [Int]
myReverse x = foldl (\acc e -> e:acc) [] x

countIn :: [[Int]] -> Int -> [Int]
countIn x e = foldr (\l acc -> (length.filter (==e) $ l):acc) [] x

firstWord :: String -> String
firstWord x = takeWhile (\e -> not (e == ' ')) (dropWhile (==' ') x)

