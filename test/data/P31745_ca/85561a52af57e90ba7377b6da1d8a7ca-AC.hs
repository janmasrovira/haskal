flatten :: [[Int]] -> [Int]
flatten l = (foldr (++) [] l)

myLength :: String -> Int
myLength s = (foldr (\e acc -> acc+1) 0 s)

myReverse :: [Int] -> [Int]
myReverse l = (foldl (\acc e -> e:acc) [] l)
 
countIn :: [[Int]] -> Int -> [Int]
countIn l n = (map (\le -> (length (filter (==n) le))) l) 

firstWord :: String -> String
firstWord s = (takeWhile (\e -> not(e==' ')) (dropWhile (==' ') s))