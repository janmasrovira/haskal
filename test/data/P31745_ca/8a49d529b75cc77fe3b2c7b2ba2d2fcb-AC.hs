--1

flatten :: [[Int]] -> [Int]
flatten xs = concat xs

--2

myLength :: String -> Int
myLength xs = length xs

--3

myReverse :: [Int] -> [Int]
myReverse xs = reverse xs

--4 acabar

countIn :: [[Int]] -> Int -> [Int]
countIn xs k = map (\x -> length (filter (\a -> a == k) x)) xs

--5

firstWord :: String -> String
firstWord s = head (words s)

