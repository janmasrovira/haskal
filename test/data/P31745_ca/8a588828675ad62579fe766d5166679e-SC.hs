-- flatten [[1,2,3],[4,5],[6],[],[3,3]] = [1,2,3,4,5,6,3,3]
flatten :: [[Int]] -> [Int]
flatten l = concat l

-- myLength "Albert"                    = 6
myLength :: [Char] -> Int
myLength l = length l

-- myReverse [1..10]                    = [10,9,8,7,6,5,4,3,2,1]
myReverse :: [Int] -> [Int]
myReverse [] = []
myReverse (x:l) = (myReverse l) ++ [x]

-- countIn [[3,2,3],[3],[], [2,2]] 3    = [2,1,0,0]
countIn :: [[Int]] -> Int -> [Int]
countIn l x = map (count) l
	where
		count :: [Int] -> Int
		count [] = 0
		count (y:l)
			| y == x = 1 + count l
			| otherwise = count l

-- firstWord "  Volem pa amb oli  "     = "Volem"