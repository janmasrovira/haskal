
flatten::[[Int]]->[Int]
flatten xs = foldr (\x y-> x++y) [] xs


myLength::String->Int
myLength xs = foldr(\y x-> x+1) 0 xs


myReverse::[Int]->[Int]
myReverse xs = foldl (\x y -> y:x) [] xs

filtra::[Int]->Int->Int
filtra x y= length (filter (\p-> p==y') x)
    where y' = y

countIn::[[Int]]->Int->[Int]
countIn xs y= foldr (\x y-> (filtra x y') :y) [] xs
    where y' = y

