flatten :: [[Int]] -> [Int]
flatten ll = foldl (++) [] ll

myLength :: String -> Int
myLength s = length s

myReverse :: [Int] -> [Int]
myReverse l = reverse l

countIn :: [[Int]] -> Int -> [Int]
countIn ll n = map (\l -> length (filter (== n) l)) ll

firstWord :: String -> String
firstWord s = takeWhile (/= ' ') (dropWhile (== ' ') s)
