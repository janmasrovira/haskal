flatten :: [[Int]] -> [Int]

flatten = foldl (++) []

myLength :: String -> Int

myLength = length

myReverse :: [Int] -> [Int]

myReverse xs = foldl (flip (:)) [] xs

{-countIn :: [[Int]] -> Int -> [Int]

countIn -}

firstWord :: String -> String

firstWord xs = takeWhile (/= ' ') (dropWhile (== ' ') xs)