flatten :: [[Int]] -> [Int]
flatten lst = foldr (++) [] lst

myLength :: String -> Int
myLength s = foldr (\s n -> n+1) 0  s   

myReverse :: [Int] -> [Int]
myReverse ls = foldr (\a l -> l++[a]) [] ls

countIn :: [[Int]] -> Int -> [Int]
countIn ls to_find = foldl (\acum lst -> acum++[countInSingle lst to_find]) [] ls
			  where countInSingle ls to_find = (foldl(\count y -> if y==to_find then count+1 else count) 0 ls)

firstWord :: String -> String
firstWord s = takeWhile (\ch -> ch /= ' ') s
