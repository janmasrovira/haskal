flatten :: [[Int]] -> [Int]
flatten = foldr (++) []

myLength :: String -> Int
myLength cs = foldr (+) 0 ( map one cs )
	where
		one :: Char -> Int
		one c = 1

myReverse :: [Int] -> [Int]
myReverse = foldl (flip (:)) []

countIn :: [[Int]] -> Int -> [Int]
countIn xs n = map ( foldr (countn) 0 ) xs
	where
		countn :: Int -> Int -> Int
		countn a b
			| a == n = b+1
			| otherwise = b

firstWord :: String -> String
firstWord s = takeWhile (not.func) (dropWhile func s)
	where
		func:: Char -> Bool
		func x
			| x == ' ' = True
			| otherwise = False
