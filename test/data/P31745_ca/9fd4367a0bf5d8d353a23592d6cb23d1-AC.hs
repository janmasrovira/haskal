flatten::[[Int]] -> [Int]
flatten = foldl (++) []

myLength::String -> Int
myLength s = foldr (+) 0 (map (const 1) s)
  
myReverse::[Int] -> [Int]
myReverse x = foldl (flip (:)) [] x

countIn::[[Int]] -> Int -> [Int]
countIn x y = map (length) (map (filter (==y)) x)

firstWord:: String -> String
firstWord str = takeWhile (/=' ') (dropWhile(==' ') str)