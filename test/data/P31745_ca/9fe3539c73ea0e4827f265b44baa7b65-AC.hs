flatten :: [[Int]] -> [Int]
flatten x = foldl (++) [] x

myLength :: String -> Int
myLength x = length x

myFunc :: [Int] -> Int -> [Int]
myFunc xs x = [x]++xs

myReverse :: [Int] -> [Int]
myReverse x = foldl myFunc [] x

{-
myReverse :: [Int] -> [Int]
myReverse x = foldl myFunc [] x

myReverse :: [Int] -> [Int]
myReverse x = reverse x
-}

count :: [Int] -> Int -> Int
count [] y = 0
count (x:xs) y 
  |y == x = 1+(count xs y)
  |otherwise = count xs y

countIn :: [[Int]] -> Int -> [Int]
countIn (x:xs) y 
  |xs == [] = [(count x y)]
  |otherwise = [(count x y)] ++ (countIn xs y) 

firstWord :: String -> String
firstWord [] = []
firstWord s = x
  where (x:xs) = words s 