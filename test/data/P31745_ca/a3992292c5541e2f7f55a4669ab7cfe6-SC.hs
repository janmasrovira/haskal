flatten :: [[Int]] -> [Int]
flatten l = foldr (++) [] l

myLength :: String -> Int
myLength xs = foldr (+) 0 [1 | x<-xs]

