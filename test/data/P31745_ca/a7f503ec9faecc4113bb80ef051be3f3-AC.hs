flatten :: [[Int]] -> [Int]

flatten a = foldr (++) [] a

myLength :: String -> Int

myLength a = foldr (\x b -> b+1) 0 a

myReverse :: [Int] -> [Int]

myReverse a = foldl (\x y -> (y:x)) [] a

countIn :: [[Int]] -> Int -> [Int]

countIn a x = foldl (\y z -> y++[length (filter (== x) z)]) [] a

firstWord :: String -> String

firstWord a = takeWhile (/= ' ') (dropWhile (== ' ') a)