{-
:t map
map :: (a -> b) -> [a] -> [b]
:t all
all :: (a -> Bool) -> [a] -> Bool
:t any
any :: (a -> Bool) -> [a] -> Bool
:t filter
filter :: (a -> Bool) -> [a] -> [a]
:t dropWhile
dropWhile :: (a -> Bool) -> [a] -> [a]
:t takeWhile
takeWhile :: (a -> Bool) -> [a] -> [a]
:t foldl
foldl :: (b -> a -> b) -> b -> [a] -> b
:t iterate
iterate :: (a -> a) -> a -> [a]
:t foldr
foldr :: (a -> b -> b) -> b -> [a] -> b
:t zipWith
zipWith :: (a -> b -> c) -> [a] -> [b] -> [c]
[1..5]
[1,2,3,4,5]
[1,3..15]
[1,3,5,7,9,11,13,15]
[i| i<-[1..5]]
[1,2,3,4,5]
[2*i| i<-[1..5]]
[2,4,6,8,10]
[2*i| i<-[1..15]]
[2,4,6,8,10,12,14,16,18,20,22,24,26,28,30]
[2*i| i<-[1..5], odd i]
[2,6,10]
[2*i| i<-[1..55], odd i]
[2,6,10,14,18,22,26,30,34,38,42,46,50,54,58,62,66,70,74,78,82,86,90,94,98,102,106,110]
[2*i| i<-[1..15], odd i]
[2,6,10,14,18,22,26,30]
[2*i| i<-[1..15], odd i, i `mod` 5 == 0]
[10,30]
[(i,j)| i<-[1..5], j<-[1..3]]
[(1,1),(1,2),(1,3),(2,1),(2,2),(2,3),(3,1),(3,2),(3,3),(4,1),(4,2),(4,3),(5,1),(5,2),(5,3)]
-}

-- part 2

eql :: [Int] -> [Int] -> Bool 
-- indica si dues llistes d’enters són iguals.
eql l1 l2 = all id (zipWith (==) l1 l2) && length l1 == length l2

prod :: [Int] -> Int 
-- calcula el producte dels elements d’una llista d’enters.
prod xs = foldl (*) 1 xs

prodOfEvens :: [Int] -> Int 
-- multiplica tots el nombres parells d’una llista d’enters.
prodOfEvens l = prod (filter even l)
-- also prodOfEvens = product . (filter even)

powersOf2 :: [Int] 
-- genera la llista de totes les potències de 2.
powersOf2 = iterate (*2) 1

scalarProduct :: [Float] -> [Float] -> Float
-- calcula el producte escalar de dues llistes de reals de la mateixa mida.
scalarProduct l1 l2 = foldl (+) 0 (zipWith (*) l1 l2)

fact :: Int -> Int
-- calcula el factorial
fact n = foldl (*) 1 [1..n]

-- part 2
flatten :: [[Int]] -> [Int] 
-- aplana una llista de llistes d’enters en una llista d’enters.
flatten l = foldl (++) [] l

myLength :: [a] -> Int 
-- retorna la llargada d’una cadena de caràcters.
myLength l = foldl (\n _-> 1+n) 0 l
-- myLength l = foldl f 0 l
--		where f n x = 1+n 
-- myLength l = sum [1 | x<-xs]

myReverse :: [Int] -> [Int] 
-- inverteix els elements d’una llista d’enters.
myReverse l = foldl (flip (:)) [] l

countAux :: Int -> [Int] -> Int
-- donada una llista d'elements i un element x retorna quants cops apareix x en la llista.
countAux x l = myLength (filter (== x) l)

countIn :: [[Int]] -> Int -> [Int] 
-- donada una llista de llistes d’elements ℓ i un element x ens torna la llista 
-- que indica quants cops apareix x en cada llista de ℓ.
countIn l x = map (countAux x) l

firstWord :: String -> String 
-- donat un string amb blancs i caràcacters alfabètics), en retorna la primera paraula.
firstWord a = takeWhile (/= ' ') (dropWhile (== ' ') a)



