flatten :: [[Int]] -> [Int]
flatten x = concat x

myLength :: String -> Int
myLength xs = sum(foldr (\x acc -> 1:acc) [] xs)

myReverse :: [Int]->[Int]
myReverse xs = foldl (flip(:)) [] xs

countIn :: [[Int]]->Int->[Int]
countIn l x = map length (map (filter (== x)) l)
  
firstWord :: String->String
firstWord xs = takeWhile (/= ' ') (dropWhile (== ' ') xs)