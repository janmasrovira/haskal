


flatten::[[Int]]->[Int]
flatten xs = foldr (\x y-> x++y) [] xs


myLength::String->Int
myLength xs = foldr(\y x-> x+1) 0 xs


myReverse::[Int]->[Int]
myReverse xs = foldl (\x y -> y:x) [] xs