flatten :: [[Int]]->[Int]
flatten = foldl (++) []

myLength :: String -> Int
myLength = foldr (\_ -> (+1)) 0

myReverse :: [Int] -> [Int]
myReverse = foldl (\xs x->x:xs) []

--countIn :: [[Int]->Int->[Int]

firstWord :: String->String
firstWord s = takeWhile (/= ' ') (dropWhile (==' ') s)
