flatten::[[Int]] -> [Int]
flatten ([]:xs) = (foldl (++) [] xs)
flatten l@((x:xs):ys) = foldl (++) [] l

myLength::String->Int 
myLength x = foldl (\x y -> x+1) 0 x


myReverse::[Int]->[Int]
myReverse [] = []
myReverse (x:xs) = foldl (\x y -> y:x) [] (x:xs)

countIn::[[Int]]->Int->[Int]
countIn ([]:xs) z = []
countIn l@((x:xs):ys) z = tail (scanl (func) z l) 
  where func y [] = 0
	func y (x:xs) = length $ filter (==z) (x:xs) 


firstWord::String->String
firstWord l@(x:xs) 
  | x == ' ' = takeWhile (\x -> x /= ' ') (dropWhile (\x -> x == ' ') l)
  | otherwise = takeWhile (\x -> x /= ' ') l
