flatten :: [[Int]] -> [Int]
flatten xs = foldl (++) [] xs

myLength :: String -> Int
myLength x = foldl (+) (0) (map (f) (map (/= ' ') x))
	where
		f True = 1
		f False = 0

myReverse :: [Int] -> [Int]
myReverse xs = foldl (flip (:)) [] xs

--countIn :: [[Int]] -> Int -> [Int]
--countIn [x] y = icountIn x y
--countIn (x:xs) y = (icountIn x y)++(countIn xs y)

--icountIn :: [Int] -> Int -> Int -> [Int]
--icountIn z w = foldl (+) (0) (map (+2) lista)
--		where
--			lista = filter (==w) z

firstWord :: String -> String
firstWord xs = takeWhile (/= ' ') (dropWhile (==' ')  xs)