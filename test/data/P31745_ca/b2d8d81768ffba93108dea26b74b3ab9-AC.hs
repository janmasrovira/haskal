flatten :: [[Int]] -> [Int]
flatten ls = foldl (++) [] ls

myF :: Int -> Char -> Int
myF a c = a+1

myLength :: String -> Int
myLength s = foldl (myF) 0 s

myReverse :: [Int] -> [Int]
myReverse as = reverse as

count :: Int -> [Int] -> Int
count x as = length (filter (== x) as)

countIn :: [[Int]] -> Int -> [Int]
countIn ls x = map (count x) ls

firstWord :: String -> String
firstWord s = head (words s)