
flatten::[[Int]]->[Int]
flatten = foldl (++) []

myLength::String->Int
myLength = foldl (\acc _ -> acc + 1) 0

myReverse::[Int]->[Int]
myReverse = foldl (\acc x -> x : acc) []

countIn::[[Int]]->Int->[Int]
countIn xss n = map (\xs -> count xs n) xss where
  count xs n = foldl (\acc x -> acc + fromEnum(x==n)) 0 xs

firstWord::String->String
firstWord = takeWhile (/= ' ') . dropWhile (== ' ')

