flatten :: [[Int]] -> [Int]
flatten xs = foldl (++) [] xs

--[1..5]
--[1,3..15]
--[i | i<-[1..5]]
--[2*i | i<-[1..5]]
--[2*i | i<-[1..5], odd i]
--[(i,j) | i<-[1..5], j<-[1..3]]

myLength :: String -> Int
myLength xs = sum [ 1 | x<-xs]

myLength2 :: String -> Int
myLength2 xs = foldl f 0 xs
	where f n x = 1+n

myLength3 :: String -> Int
myLength3 xs = foldl (\n _ -> 1+n) 0 xs

myReverse :: [Int] -> [Int]
myReverse xs = foldr (\n ys -> ys++[n]) [] xs

count :: [Int] -> Int -> Int
count xs n = foldl (\n _ -> 1+n ) 0 (filter (n==) xs)

countIn :: [[Int]] -> Int -> [Int]
countIn xs n = foldl f [] xs
	where f as bs = as++[count bs n]

--firstWord :: String -> String
--firstWord n = takeWhile ()

