flatten::[[Int]]->[Int]
flatten [] = []
flatten (x:xs) = concat (x:xs)

myLength::String->Int
myLength s = length s

myReverse::[Int]->[Int]
myReverse [] = []
myReverse (x:xs) = reverse (x:xs)

countIn::[[Int]]->Int->[Int]
countIn [] n = [0]
countIn ((x:xs):ys)  n = map (contar n) ((x:xs):ys)

contar::Int->[Int]->Int
contar n [] = 0
contar n (x:xs)
	|n == x = 1+contar n xs
	|otherwise = contar n xs

-- firstWord::String->String
