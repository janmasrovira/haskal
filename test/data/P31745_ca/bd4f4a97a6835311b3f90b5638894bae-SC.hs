-- exemple ús if then else
flatten a = foldr (++) [] a
myLength = foldr (\a b -> b + 1) 0
myReverse = foldr (\a b -> b++[a]) []
countIn l x = map length $ filter (==x) l
firstWord s = takeWhile (/= ' ')  (dropWhile (== ' ') s)