
-- #1
-- aplana una llista de llistes en una sola llista
flatten :: [[Int]] -> [Int]
flatten xs = foldl (++) [] xs



-- #2
-- retorna la llargada d'un string
myLength :: String -> Int
myLength xs = sum [1 | x<-xs]

myLength2 :: String -> Int
myLength2 xs = foldl f 0 xs
    where f acc val = 1+acc
          


-- #3
-- inverteix els elements d'una llista
myReverse :: [Int] -> [Int]
myReverse xs = foldl f [] xs
    where f acc val = flip (:) acc val



-- #4
-- retorna una llista amb les ocurrencies d'x en cada llista de ls
countIn :: [[Int]] -> Int -> [Int]
countIn ls x = foldl f [] ls
    where f n l = n ++ [length (filter (==x) l)]



-- #5
-- donada un string amb una frase, retorna la primera paraula
firstWord :: String -> String
firstWord xs = takeWhile (/=' ') (dropWhile (==' ') xs)
