-- flatten [[1,2,3],[4,5],[6],[],[3,3]]
-- [1,2,3,4,5,6,3,3]

flatten::[[a]] -> [a]
flatten = concat

-- myLength "Albert"
-- 6
myLength::[a]->Int
myLength [] = 0
myLength (x:xs) = 1 + myLength xs


-- myReverse [1..10]
-- [10,9,8,7,6,5,4,3,2,1]

myReverse::[a] -> [a]
myReverse [] = []
myReverse xs = last xs : myReverse (init xs)

-- countIn [[3,2,3],[3],[], [2,2]] 3
-- [2,1,0,0]

-- countIn::[[Int]]->Int->[Int]
-- countIn [xs:ys] x = length [y | y <- xs, y == x]

-- firstWord "  Volem pa amb oli  "
-- "Volem"

firstWord::[Char]->[Char]
firstWord xs = if (head xs == ' ') then firstWord (dropWhile (==' ') xs)
	else (takeWhile (/=' ') xs)
