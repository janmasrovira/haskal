flatten :: [[Int]] -> [Int]
flatten = foldl (++) []

myLength :: String -> Int
myLength s = foldl (\n _ -> 1 + n) 0 s

myReverse :: [Int] -> [Int]
myReverse = foldl (flip (:)) [] 

countIn :: [[Int]] -> Int -> [Int]
countIn ls x = map (length . (filter (== x))) ls

firstWord :: String -> String
firstWord = (takeWhile (/= ' ')) . (dropWhile (== ' '))
