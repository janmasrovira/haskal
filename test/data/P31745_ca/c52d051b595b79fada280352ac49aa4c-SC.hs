flatten::[[Int]]->[Int]

flatten = foldl (++) []

mas1::Int->Char->Int

mas1 x c = x+1

myLength::String->Int

myLength s = foldl mas1 0 s

myReverse::[Int]->[Int]

myReverse xs = foldl (flip(:)) [] xs

sumasi::Int->Int->Int->Int

sumasi x y z
	| x == z = y+1
	| otherwise = y

count::Int->[Int]->Int

count x xs = foldl (sumasi x) 0 xs

countIn::[[Int]]->Int->[Int]

countIn xs x = map (count x) xs
