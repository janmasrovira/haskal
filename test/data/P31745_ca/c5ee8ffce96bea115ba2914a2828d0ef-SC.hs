flatten::[[Int]] -> [Int]
flatten l = foldr (++) [] l 

myLength:: String -> Int
myLength l = foldr (sum1) 0 l

sum1:: Char -> Int -> Int
sum1 c n = n + 1

myReverse::[Int] -> [Int]
myReverse l = foldr concat2 [] l

concat2:: Int -> [Int] -> [Int]
concat2 l n = n++[l]

countIn::[[Int]] -> Int -> [Int]
countIn l n = map (contarN n) l

contarN:: Int -> [Int] -> Int
contarN n l = foldr (equal n) 0 l

equal:: Int -> Int -> Int -> Int
equal n m l 
	| n == m = l+1
	| otherwise = l
