
flatten :: [[Int]] -> [Int]
flatten = foldl (++) []


myLength :: String -> Int
myLength = sum . map (const 1)


myReverse :: [Int] -> [Int]
myReverse = foldl (flip (:)) []


countIn :: [[Int]] -> Int -> [Int]
countIn xss x = map countx xss
    where countx xs = sum $ map (\y -> if x==y then 1 else 0) xs


firstWord :: String -> String
firstWord cs = takeWhile (/= ' ') $ dropWhile (== ' ') cs

