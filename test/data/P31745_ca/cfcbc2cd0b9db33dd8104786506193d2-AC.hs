--Problema 1
flatten :: [[Int]] -> [Int]
flatten xs = foldr (++) [] xs

--Problema 2
myLength :: String -> Int
myLength xs = foldl (\n c -> n+1) 0 xs 

--Problema 3
myReverse :: [Int] -> [Int]
myReverse xs = foldl (flip (:)) [] xs


--Problema 4
countIn :: [[Int]] -> Int -> [Int]
countIn xss y = map count xss
	where count xs = length $ filter (== y) xs  

--Problema 5
firstWord :: String -> String
firstWord s = takeWhile (/= ' ') $ dropWhile (== ' ') s