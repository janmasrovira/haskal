-- Aplana una llista de llistes d’enters en una llista d’enters.
flatten :: [[Int]] -> [Int]
flatten li = foldr (\x y -> x++y) [] li

-- Retorna la llargada d’una cadena de caràcters.
myLength :: String -> Int
myLength li = foldr (\x y -> y+1) 0 li

-- Inverteix els elements d’una llista d’enters.
myReverse :: [Int] -> [Int]
myReverse li = foldr (\x y -> y++(x:[])) [] li

-- Donada una llista de llistes d’elements ℓ i un element x ens 
-- torna la llista que indica quants cops apareix x en cada llista de ℓ.
countIn :: [[Int]] -> Int -> [Int]
countIn li e = map (foldr (countf e) 0) li 

countf :: Int -> Int -> Int -> Int
countf e x y
    | x == e    = y + 1
    | otherwise = y

-- Donat un string amb blancs i caràcacters alfabètics, en retorna 
-- la primera paraula.
firstWord :: String -> String 
firstWord s = takeWhile (/= ' ') $ dropWhile (== ' ') s