
--flatten
flatten :: [[Int]] -> [Int]
flatten xs = foldr (++) [] xs


--myLength
myLength :: String -> Int
myLength s = sum (map func s)
    where
        func:: Char -> Int
        func x = 1


--myReverse
myReverse :: [Int] -> [Int]
myReverse xs = foldl (flip (:)) [] xs


--countIn
countIn :: [[Int]] -> Int -> [Int]
countIn xs x = map func xs
    where
        func:: [Int] -> Int
        func ys = length (filter (== x) ys)



--firstWord
firstWord :: String -> String
firstWord s = takeWhile (not.func) (dropWhile func  s)
    where
        func:: Char -> Bool
        func x
            |x == ' '   = True
            |otherwise  = False