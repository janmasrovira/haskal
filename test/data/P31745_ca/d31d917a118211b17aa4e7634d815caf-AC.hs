flatten :: [[Int]] -> [Int]
flatten = foldr (++) []

myLength :: String -> Int
myLength = foldl (\acc x -> succ acc) 0

myReverse :: [Int] -> [Int]
myReverse = foldl (\acc x -> x : acc) []

countIn :: [[Int]] -> Int -> [Int]
countIn l i = map count l
	where count = foldl (\acc elem -> if elem == i then succ acc else acc) 0

firstWord :: String -> String
firstWord s = takeWhile (/=' ') $ drop (length $ takeWhile (== ' ') s) s