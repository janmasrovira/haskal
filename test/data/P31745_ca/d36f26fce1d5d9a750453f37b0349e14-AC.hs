--FLATTEN


flatten :: [[Int]] -> [Int]


flatten a = foldr (\k acc -> accoda acc k) [] a
	where
		accoda :: [Int] -> [Int] -> [Int] 
		accoda acc a = foldr (\t acc -> t:acc) acc a




--MY LENGTH


myLength :: String -> Int

myLength a = foldr (\_ acc -> acc + 1) 0 a





--MY REVERSE


myReverse :: [Int] -> [Int]

myReverse = foldl (\acc x -> (x:acc)) []





--COUNT IN


countIn :: [[Int]] -> Int -> [Int]

countIn a x = map (conta x) a
	where
		conta :: Int -> [Int] -> Int
		conta x k = length $ filter (== x) k





--FIRST WORD


firstWord :: String -> String

firstWord a
 | (length $ filter (/= ' ') a) == 0	= error "No hay palabras en esta stringa."
 | otherwise				= takeWhile (/= ' ') $ dropWhile (== ' ') a
	
			

