

flatten::[[Int]]->[Int]
flatten l = foldl (++) [] l


myLength::String->Int
myLength l = foldl f 0 l
  where 
    f acum val = (+1) acum
    

myReverse::[Int]->[Int]
myReverse l = foldl f [] l
  where f acc val = flip (:) acc val
	
	
	
countIn::[[Int]]->Int->[Int]
countIn ls n = foldl f [] ls
  where f acc lista = (++) acc [length (filter (== n) lista)]
	
firstWord::String->String
firstWord ls = takeWhile (/= ' ') (dropWhile (== ' ') ls)