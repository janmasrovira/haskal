flatten :: [[Int]] -> [Int]
flatten = foldr (++) []

myLength :: String -> Int
myLength = sum.map (\s -> 1)

myReverse :: [Int] -> [Int]
myReverse = foldl (\xs x -> x:xs) []

countIn :: [[Int]] -> Int -> [Int]
countIn l n = foldr (\x xs -> (length $ filter (== n) x):xs) [] l

firstWord :: String -> String
firstWord s = takeWhile lletra $ dropWhile (not.lletra) s
lletra c = if ((elem c ['a'..'z']) || (elem c ['A'..'Z'])) then True else False