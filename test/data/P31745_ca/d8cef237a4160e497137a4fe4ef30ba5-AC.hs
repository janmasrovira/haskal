-- flatten
flatten :: [[Int]] -> [Int]
flatten x = foldl (++) [] x

-- myLength
myLength :: String -> Int
myLength s = foldl (\n _ -> 1+n) 0 s

-- myReverse
myReverse :: [Int] -> [Int]
myReverse xs = foldl (\a b -> [b]++a) [] xs

-- countIn
countIn :: [[Int]] -> Int -> [Int]
countIn xs i = foldl (\a b -> a++[(count b i)]) [] xs

-- count    
count :: [Int] -> Int -> Int
count x i = foldl (\a b -> if b == i then a+1 else a) 0 x

-- firstWord
firstWord :: String -> String
firstWord w = takeWhile (/= ' ') (dropWhile (== ' ') w)
