flatten :: [[Int]] -> [Int]
flatten ll = foldl (++) [] ll

myLength :: String -> Int
myLength = foldl (\n _ -> 1+n) 0

myReverse :: [Int] -> [Int]
myReverse = foldl (\xs x -> [x]++xs) []

countIn :: [[Int]] -> Int -> [Int]
countIn xss i = foldl (\rs xs -> rs++[countOcc xs i]) [] xss

countOcc :: [Int] -> Int -> Int
countOcc xs i = foldl (\r x -> if x == i then r+1 else r) 0 xs

firstWord :: String -> String
firstWord = takeWhile (/=' ')

