import Data.List

flatten :: [[Int]] -> [Int]
flatten xs = concat xs

myLength :: String -> Int
myLength [] = 0
myLength l = length l

myReverse :: [Int] -> [Int]
myReverse l = reverse l

countIn :: [[Int]] -> Int -> [Int]
countIn [] _ = []
countIn (x:xs) n = [nump] ++ countIn xs n
    where
        nump = length (filter (==n) x)

firstWord :: String -> String
firstWord f = g
    where g = head (words f)
