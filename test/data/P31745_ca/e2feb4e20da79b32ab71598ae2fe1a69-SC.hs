flatten [] = [] 
flatten (x:xs) = x ++ flatten xs

myLength l = foldr (+) 0 [1 | x <- l]

myReverse :: [Int] -> [Int]
myReverse [] = []
myReverse (x:xs) = (myReverse xs) ++ [x]

countIn l n = map (\g x -> myLength (filter (==n) g)) l 

firstWord l = takeWhile (\x -> x /= ' ') (dropWhile (\x -> x == ' ') l)