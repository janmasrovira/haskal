flatten :: [[Int]] -> [Int]
flatten = foldl (++) []

myLength :: String -> Int
myLength l = foldr (\currElem acumValue -> (acumValue + 1)) 0 l

myReverse :: [Int] -> [Int]
myReverse = foldl (\acumList currElem -> (currElem:acumList)) []

countIn :: [[Int]] -> Int -> [Int]
countInOne x l = length $ filter (==x) l
countIn ll x = map (countInOne x) ll

firstWord :: String -> String
firstWord s= takeWhile (/= (' ')) (dropWhile (== (' ')) s) 
