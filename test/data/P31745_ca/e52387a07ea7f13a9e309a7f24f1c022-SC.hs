
-- Aplana una llista de llistes d'enters en una llista d'enters
flatten :: [[Int]] -> [Int]
flatten xss = foldl (++) [] xss

-- Retorna la llargada d'un String
myLength :: String -> Int
myLength s = sum [1 | x <- s]

-- Inverteix l'ordre d'una llista d'enters
myReverse :: [Int] -> [Int]
myReverse xs = foldl (flip (:)) [] xs
