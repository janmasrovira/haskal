--1
flatten :: [[Int]] -> [Int]
flatten = foldl (++) []

--2
myLength :: String -> Int
myLength xs
  | xs == " "  = 0
  | otherwise = sum [1 | x <- xs]
{--  
myLength1 :: String -> Int
myLength1 xs = foldl f 0 xs
  where f n x = 1+n
	
myLength2 :: String -> Int
myLength2 xs = foldl (\ n _ -> 1+n) 0 xs 
--}

--3
myReverse :: [Int] -> [Int]
myReverse xs = foldl (flip (:)) [] xs

--4
countIn :: [[Int]] -> Int -> [Int]
countIn xs n = foldl f [] xs
  where f acc val = acc ++ [length (filter (== n) val)]

--5
firstWord :: String -> String
firstWord xs = takeWhile (/= ' ')(dropWhile (== ' ') xs)














