flatten :: [[Int]] -> [Int]
flatten = foldl (++) []

myLength :: String -> Int
myLength = foldl (\n _ -> n+1) 0

myReverse :: [Int] -> [Int]
myReverse = foldl gira []
  where gira s c = c:s

countIn :: [[Int]] -> Int -> [Int]
countIn xs n = map compara xs
  where compara ys = length $ filter (==n) ys

firstWord :: String -> String
firstWord s = takeWhile (/=' ') (elimina s)
  where elimina s = dropWhile (== ' ') s