flatten :: [[Int]] -> [Int]
flatten x = foldl (++) [] x

myLength :: String -> Int
myLength x = length x
  
myReverse :: [Int] -> [Int]
myReverse x = foldl (flip (:)) [] x

count :: [Int] -> Int -> Int
count ar x = length (filter (==x) ar)  

countIn :: [[Int]] -> Int -> [Int]
countIn ar x = map (flip count x) ar

firstWord :: String -> String
firstWord s = head (words s) 