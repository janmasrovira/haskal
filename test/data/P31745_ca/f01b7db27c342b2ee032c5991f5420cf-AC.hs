flatten :: [[Int]] -> [Int]
flatten = foldr (++) []

myLength :: String -> Int
myLength xs	 = foldr (\x acc -> (+) 1 acc) 0 xs

myReverse :: [Int] -> [Int]
myReverse = foldl (flip (:)) [] 

cont :: [Int] -> Int -> Int
cont xs n = length (filter (== n) xs)

countIn :: [[Int]] -> Int -> [Int]
countIn	xs n = map length (map (filter (== n)) xs)

firstWord :: String -> String
firstWord xs = takeWhile ((/=) ' ') (dropWhile ((==) ' ') xs)