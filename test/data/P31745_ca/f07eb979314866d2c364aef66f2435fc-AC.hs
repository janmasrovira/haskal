flatten:: [[Int]] -> [Int]
flatten l = concat l

myLength s = length s

myReverse xs = reverse xs

countIn l cmp = map length aux
    where
        aux = map (filter (==cmp)) l

firstWord s = head (words s) 
