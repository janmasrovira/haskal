
flatten :: [[Int]] -> [Int]
flatten = foldl (++) []

myLength :: String -> Int
myLength = foldl (\n _ -> n+1) 0

myReverse :: [Int] -> [Int]
myReverse = foldl posaDavant []
  where posaDavant s c = c:s

countIn :: [[Int]] -> Int -> [Int]
countIn l n = map count l
  where count xs = length $ filter (==n) xs

firstWord :: String -> String
firstWord s = takeWhile (/=' ') (dropWhile (==' ') s)