--flatten
flatten::[[Int]]->[Int]
flatten xs = concat xs

--myLength
myLength::String->Int
myLength x = length x

--myReverse
myReverse::[Int]->[Int]
myReverse xs = reverse xs

--countIn
countIn::[[Int]]-> Int -> [Int]
countIn xs n = map (length) (map(filter(==n)) xs)

--firstWord
firstWord::String->String
firstWord n = head (words n)


