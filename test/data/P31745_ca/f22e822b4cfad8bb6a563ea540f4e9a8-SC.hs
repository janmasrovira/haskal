flatten :: [[Int]] -> [Int]
flatten [] = []
flatten (x:xs) = foldr (++) [] (x:xs)

myLength :: String -> Int
myLength x = foldr (\_ n -> 1 + n) 0 x

myReverse :: [Int] -> [Int]
myReverse l = foldl (flip (:)) [] l

-- countIn :: [[Int]] -> Int -> [Int]
-- countIn (x:xs) y = scanr (\n -> (count y) ++ []) []
-- 
-- 
-- count x = length . filter (\x' -> x' == x)

