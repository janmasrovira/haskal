
-- Aplana una llista de llistes d'enters en una llista d'enters
flatten :: [[Int]] -> [Int]
flatten xss = foldl (++) [] xss

-- Retorna la llargada d'un String
myLength :: String -> Int
myLength s = sum [1 | x <- s]

-- Inverteix l'ordre d'una llista d'enters
myReverse :: [Int] -> [Int]
myReverse xs = foldl (flip (:)) [] xs

-- Donada una llista de llistes d'enters l, i un element x, indica quantes vegades apareix x en la llista l
countIn :: [[Int]] -> Int -> [Int]
countIn l x = [length (filter (== x) a) | a <- l]

-- Donat un String amb blancs i caracters alfabetics, en retorna la primera paraula
firstWord :: String -> String
firstWord s = takeWhile (/= ' ') (dropWhile (== ' ') s)
