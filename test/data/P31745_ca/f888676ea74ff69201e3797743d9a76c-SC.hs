flatten::[[Int]] -> [Int]
flatten ([]:xs) = (foldl (++) [] xs)
flatten l@((x:xs):ys) = foldl (++) [] l

myLength::String->Int 
myLength x = foldl (\x y -> x+1) 0 x


myReverse::[Int]->[Int]
myReverse [] = []
myReverse (x:xs) = foldl (\x y -> y:x) [] (x:xs)




