
flatten :: [[Int]] -> [Int]
flatten x = foldl (++) [] x


myLength :: String -> Int
myLength s = foldl (\acc x -> acc + 1) 0 s


myReverse :: [Int] -> [Int]
myReverse = foldl (\acc x -> x : acc) []


countIn0 :: [Int] -> Int -> Int
countIn0 l x = length (filter (x==) l)

countIn1 :: Int -> [Int] -> Int
countIn1 x l = countIn0 l x
-- countIn1 = flip countIn0

countIn :: [[Int]] -> Int -> [Int]
countIn l x = map (countIn1 x) l
-- countIn l x = map ((flip countIn0) x) l

firstWord :: String -> String
firstWord x = takeWhile (' ' <) (dropWhile (' ' ==) x)





