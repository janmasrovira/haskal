flatten::[[Int]] -> [Int]
flatten n = foldl (++) [] n

myLength::String -> Int
myLength [] = 0
myLength (h:st) = 1 + (myLength st)

myReverse::[Int] -> [Int]
myReverse [] = []
myReverse (x:xs) = (myReverse xs) ++ [x]


conta _ [] = 0
conta n (x:xs)
    | n == x = 1 + conta n xs
    | otherwise = conta n xs

countIn::[[Int]] -> Int -> [Int]
countIn [] x = []
countIn (l:ls) x = map (conta x) (l:ls)

firstWord::String -> String 
firstWord str = takeWhile (\x -> x /= ' ') (dropWhile (\x -> x == ' ' ) str)