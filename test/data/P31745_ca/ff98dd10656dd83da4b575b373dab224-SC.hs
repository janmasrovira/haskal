flatten::[[Int]]->[Int]
flatten [] = []
flatten (x:xs) = foldr (++) [] (x:xs)