eql::[Int]->[Int]->Bool
eql [] [] = True
eql [] (l:ls) = False
eql (l:ls) [] = False
eql (l:l1) (ll:l2) = l == ll && eql l1 l2

prod::[Int]->Int
prod = foldl (*) 1

prodOfEvens::[Int]->Int
prodOfEvens (x:xs) = foldl (*) 1 q
	where q = filter even (x:xs)

powerOf2::[Int]
powerOf2 = [2^x | x<-[0..]]

scalarProduct::[Float]->[Float]->Float
scalarProduct xx yy = sum (zipWith (*) xx yy)