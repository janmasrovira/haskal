eql :: [Int] -> [Int] -> Bool
eql as bs = (length as == length bs) && and (zipWith (==) as bs)

prod :: [Int] -> Int
prod = foldl (*) 1

prodOfEvens :: [Int] -> Int
prodOfEvens = prod . filter (not . odd)

powersOf2 :: [Int]
powersOf2 = map (2^) [(0::Int) ..]


scalarProduct :: [Float] -> [Float] -> Float
scalarProduct as bs = sum $ zipWith (*) as bs
