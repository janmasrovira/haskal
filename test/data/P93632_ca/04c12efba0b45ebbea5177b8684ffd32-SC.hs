import Debug.Trace


eqI :: [Int] -> [Int] -> Bool
eqI as bs = bo && null rest
  where
    (bo, rest) = foldl f (True, as) bs
    f (_, []) _ = (False, [])
    f (t, (a:as')) b = (t && a == b, as')
{-
eqI as bs = bo && null rest
  where
    (bo, rest) = foldr f (True, as) bs
    f _ (_, []) = (False, []) `debug` "End"
    f b (t, (a:as')) = (t && (a == b )), as')
-}

{-
eqI [] [] = True
eqI [] _ = False
eqI _ [] = False
eqI (a:as) (b:bs) = a == b && eqI as bs
-}

prod :: [Int] -> Int
prod = foldl (*) 1

prodOfEvens :: [Int] -> Int
prodOfEvens = prod . filter (not . odd)

powersOf2 :: [Int]
powersOf2 = map (2^) [(0::Int) ..]

scalarProduct :: [Float] -> [Float] -> [Float]
scalarProduct as bs = map (uncurry (*)) $ zip as bs --zipWith (*)
