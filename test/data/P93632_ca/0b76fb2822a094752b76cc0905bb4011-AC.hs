-- eql::[Int] -> [Int] -> Bool

eql [] [] = True
eql [] (y:ys) = False
eql (x:xs) [] = False
eql (x:xs) (y:ys)
	|all (==x) [y] = eql xs ys
	|otherwise = False 

-- prod::[Int] -> Int

prod xs = foldr (*) 1 xs

-- prodOfEvens::[Int] -> Int

prodOfEvens xs = foldr (*) 1 (filter (even) xs)

-- powersOf2::[Int]

powersOf2 = iterate (*2) 1

--scalarProduct::[Float] -> [Float] -> Float

scalarProduct xs ys = sum (zipWith (*) xs ys)



