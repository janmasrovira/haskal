eql :: [Int] -> [Int] -> Bool
prod :: [Int] -> Int
prodOfEvens :: [Int] -> Int
powersOf2 :: [Int]
scalarProduct :: [Float] -> [Float] -> Float

eql ln lm | length ln /= length lm = False
	  | otherwise = and $ zipWith (==) ln lm

prod = foldl (*) 1

prodOfEvens ln = product (filter even ln)
		   
powersOf2 = iterate (*2) 1

f a (x,y) = a + x * y
scalarProduct ln lm = foldl f 0 (zip ln lm)