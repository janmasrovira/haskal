eql::[Int]->[Int]->Bool
eql [] [] = True
eql [] (l:ls) = False
eql (l:ls) [] = False
eql (l:l1) (ll:l2) = l == ll && eql l1 l2

prod::[Int]->Int
prod = foldl (*) 1

prodOfEvens::[Int]->Int
prodOfEvens [] = 0
prodOfEvens (x:xs)
	| length q > 0 = foldl (*) 1 q
	| otherwise = 0
	where q = filter even (x:xs)

powerOf2::[Int]
powerOf2 = [pow 2 x | x<-[0..]]

pow x 0 = 1
pow x y = x*(pow x (y-1))

scalarProduct::[Float]->[Float]->Float
scalarProduct xx yy = sum (zipWith (*) xx yy)