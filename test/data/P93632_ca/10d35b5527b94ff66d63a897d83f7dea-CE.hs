eql:: [Int] -> [Int] -> Bool
eql x y =
	all (==true) (zipWith (==) x y) 


prod:: [Int] -> Int
prod x =
	foldr (*) 1 x
