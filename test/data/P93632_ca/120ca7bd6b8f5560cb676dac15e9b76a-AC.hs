eql::[Int] -> [Int] -> Bool
eql as bs = (all id (zipWith (==) as bs)) && length as == length bs

prod::[Int] -> Int
prod = foldl (*) 1

prodOfEvens::[Int] -> Int
prodOfEvens = prod . filter even

powersOf2::[Int]
powersOf2 = iterate (*2) 1

scalarProduct::[Float] -> [Float] -> Float
scalarProduct as bs = sum (zipWith (*) as bs)

