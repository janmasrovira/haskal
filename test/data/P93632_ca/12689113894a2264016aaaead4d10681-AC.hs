eql :: [Int] -> [Int] -> Bool 
eql xs ys = all (==0) (zipWith (-) xs ys) && (llarg xs == llarg ys) -- o all id (map ...) id = identitat
		where
			llarg [] = 0
			llarg (_:xs) = 1 + llarg xs

prod :: [Int] -> Int
prod xs = foldl (*) 1 xs

prodOfEvens :: [Int] -> Int
prodOfEvens xs = foldl (*) 1 (filter even xs)

powersOf2 :: [Int]
powersOf2 = iterate (*2) 1

scalarProduct :: [Float] -> [Float] -> Float 
scalarProduct xs ys = sum (zipWith (*) xs ys)