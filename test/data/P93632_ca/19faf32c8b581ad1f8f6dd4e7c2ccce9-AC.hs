--diu si dues llistes son iguals

eqBool :: [Bool] -> Bool
eqBool l = foldl (&&) True l

-- zipWith aplica compt a les 2 llistes
eql :: [Int] -> [Int] -> Bool
eql l1 l2 = eqBool l3 && (length l1 == length l2)
  where l3 = zipWith (==) l1 l2


prod :: [Int] -> Int
prod l = foldl (*) 1 l


func::Int -> Bool
func x
	|mod x 2 == 0    = True
	|otherwise      = False

prodOfEvens :: [Int] -> Int
prodOfEvens l = foldl (*) 1(filter func l)


powersOf2 :: [Int]
powersOf2 = iterate (* 2)  1

scalarProduct :: [Float] -> [Float] -> Float
scalarProduct [] [] = 0
scalarProduct l1 l2 = foldl1 (+) (zipWith (*) l1 l2)
