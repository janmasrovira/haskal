eql [] [] = True
eql (a:l) [] = False
eql [] (a:l) = False
eql (a:l1) (b:l2)
    | a /= b    = False
	| otherwise = eql l1 l2

prod [] = 1
prod (a:l) = a * (prod l)

prodOfEvens l = prod [x | x <- l, even x]

-- powersOf2 = [2**x | x <- [0..] ]
powersOf2 = iterate (*2) 1

scalarProduct p q = suma (multiplica p q)

suma [] = 0
suma (a:l) = a + (suma l)

multiplica [a] [b] = [a*b]
multiplica (a:p) (b:q) = [a*b]++(multiplica p q)