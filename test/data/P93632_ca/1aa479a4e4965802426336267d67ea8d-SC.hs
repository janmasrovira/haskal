
eql :: [Int] -> [Int] -> Bool
eql x y = any (==False) (zipWith (==) x y)


prod :: [Int] -> Int
prod [] = 0
prod (x:xs) = foldl (*) x xs


prodOfEvens :: [Int] -> Int
prodOfEvens (x:xs) = foldl (*) x (filter even xs)


powersOf2 :: [Int]
powersOf2 = iterate (2*) 1
-- [2^x | x <- [0..]]


scalarProduct :: [Float] -> [Float] -> Float
scalarProduct x y = foldl (+) 0 (zipWith (*) x y)














