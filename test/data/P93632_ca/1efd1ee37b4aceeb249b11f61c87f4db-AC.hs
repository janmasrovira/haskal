



eql::[Int]->[Int]->Bool
eql xs ys = all (== 0) (zipWith (-) xs ys) && tamany xs - tamany ys == 0
  where
    tamany [] = 0
    tamany (_:cua) = 1 + tamany cua

prod::[Int]->Int
prod xs = foldl (*) 1 xs 

prodOfEvens::[Int]-> Int
prodOfEvens xs = foldl (*) 1 (filter even xs)

powersOf2::[Int]
powersOf2 = iterate (*2) 1

scalarProduct::[Float]->[Float]->Float
scalarProduct xs ys = foldl (+) 0 (zipWith (*) xs ys)