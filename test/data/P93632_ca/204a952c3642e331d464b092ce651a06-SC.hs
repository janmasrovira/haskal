eql l g =
	let m = zip l g in
	all (\m -> (fst m) == (snd m)) m 