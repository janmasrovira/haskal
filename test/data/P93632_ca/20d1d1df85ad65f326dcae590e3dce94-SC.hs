eql :: [Int] -> [Int] -> Bool
eql [] [] = True
eql [x] [] = False
eql [] [x] = False
eql (x:xs) (y:ys) = x == y && eql xs ys

prod :: [Int] -> Int
prod xs =  (foldl (*) 1 xs)

prodOfEvens :: [Int] -> Int
prodOfEvens xs = (foldl (*) 1 (filter even xs))

powers :: Int -> [Int]
powers x = [y] ++ powers y
    where
        y = x*2

powersOf2 :: [Int]
powersOf2 = [1] ++ powers 1

scalarProduct :: [Float] -> [Float] -> Float
scalarProduct a b = sum (zipWith (*) a b)
