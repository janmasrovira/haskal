--1

eql :: [Int] -> [Int] -> Bool
eql xs ys
	|length xs == length ys = all (\(x,y)-> x==y) (zip xs ys)
	|otherwise = False
	
--2

prod :: [Int] -> Int
prod xs = foldl (*) 1 xs

--3

prodOfEvens :: [Int] -> Int
prodOfEvens xs = prod (filter even xs)

--4

powersOf2 :: [Int]
powersOf2 = iterate (2*) 1

--5

scalarProduct :: [Float] -> [Float] -> Float
scalarProduct xs ys = foldl (+) 0 (zipWith (*) xs ys)


