eql :: [Int] -> [Int] -> Bool
eql [] [] = True
eql [] w@(y:ys) = False
eql l@(x:xs) [] = False
eql l@(x:xs) w@(y:ys) 
  | x == y 	= eql xs ys
  | otherwise	= False
  
prod :: [Int] -> Int
prod l@(x:xs) = foldl (*) 1 l

prodOfEvens :: [Int] -> Int
prodOfEvens l@(x:xs) = foldl (*) 1 (filter even l)

powersOf2 :: [Int]
powersOf2 = [2^x | x <- [0..]]

scalarProduct :: [Float] -> [Float] -> Float
scalarProduct l@(x:xs) w@(y:ys) = foldl (+) 0 (mult l w)
  where mult :: [Float] -> [Float] -> [Float]
	mult [] [] = []
	mult l@(x:xs) w@(y:ys) = [x*y] ++ mult xs ys
