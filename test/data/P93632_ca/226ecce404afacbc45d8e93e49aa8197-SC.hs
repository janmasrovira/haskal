
eql :: [Int] -> [Int] -> Bool
eql [] [] = True
eql [] x = False
eql x [] = False
eql x y = (length x == length y) && all (==True) (zipWith (==) x y)


prod :: [Int] -> Int
prod [] = 0
prod (x:xs) = foldl (*) x xs


prodOfEvens :: [Int] -> Int
prodOfEvens [] = 0
prodOfEvens (x:xs) = foldl (*) 1 (filter even (x:xs))


powersOf2 :: [Int]
powersOf2 = iterate (2*) 1
-- [2^x | x <- [0..]]


scalarProduct :: [Float] -> [Float] -> Float
scalarProduct x y = foldl (+) 0 (zipWith (*) x y)














