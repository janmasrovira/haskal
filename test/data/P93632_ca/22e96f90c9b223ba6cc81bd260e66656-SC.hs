
--foldr f x [x1,....,xn] = -- (f x1 (f x2 ( .....(f xn (foldr f x []) )...))) 
--foldr f x [] = x
--foldr (+) 0 [3,2,(-1)]

eql :: [Int] -> [Int] -> Bool
eql (x:xs) (y:ys) = (and (zipWith (==) (x:xs) (y:ys))) && ((myLength (x:xs)) == (myLength (y:ys)))
    where 
	myLength list = foldr (\_ n -> 1 + n) 0 list


prod :: [Int] -> Int
prod [] = 1
prod (x:xs) = foldr (*) 1 (x:xs)

prodOfEvens :: [Int] -> Int
prodOfEvens xs = prod (filter (even) xs)

powersOf2 :: [Int]
powersOf2 = iterate (*2) 1

scalarProduct:: [Float] -> [Float] -> Float
scalarProduct (x:xs) (y:ys) = foldr (+) 0 (zipWith (*) (x:xs) (y: ys))