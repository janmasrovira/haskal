
eqI :: [Int] -> [Int] -> Bool
eqI as bs = (length as == length bs)
            && (and $ zipWith (==) as bs)

prod :: [Int] -> Int
prod = foldl (*) 1

prodOfEvens :: [Int] -> Int
prodOfEvens = prod . filter (not . odd)

powersOf2 :: [Int]
powersOf2 = map (^(2 :: Int)) [0..]

scalarProduct :: [Float] -> [Float] -> [Float]
scalarProduct = zipWith (*)
