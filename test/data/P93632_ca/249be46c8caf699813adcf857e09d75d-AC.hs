eql :: [Int] -> [Int] -> Bool
eql l1 l2 = all id (zipWith (==) l1 l2) && length l1 == length l2

prod :: [Int] -> Int
prod l = foldl (*) 1 l

prodOfEvens :: [Int] -> Int
prodOfEvens l = prod (filter even l)
{-prodOfEvens = prod . (filter even)-}

powersOf2 :: [Int]
powersOf2 = iterate (*2) 1

scalarProduct :: [Float] -> [Float] -> Float
scalarProduct l1 l2 = sum (map myProd (zip l1 l2))

myProd :: (Float, Float) -> Float
myProd x = (fst x) * (snd x)

