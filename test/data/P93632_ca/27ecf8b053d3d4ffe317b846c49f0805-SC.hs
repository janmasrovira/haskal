
-- Indica si dos llistes d'enters son iguals
eql :: [Int] -> [Int] -> Bool
eql as bs = (and (zipWith (==) as bs)) && (length as == length bs)

-- Retorna el producte d'una llista d'enters
prod :: [Int] -> Int
prod xs = foldl (*) 1 xs

-- Retorna el producte dels nombres parells d'una llista d'enters
prodOfEvens :: [Int] -> Int
prodOfEvens xs = foldl (*) 1 (filter even xs)
