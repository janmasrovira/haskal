
eqI :: [Int] -> [Int] -> Bool
eqI [] [] = True
eqI [] _ = False
eqI _ [] = False
eqI (a:as) (b:bs) = a == b && eqI as bs

prod :: [Int] -> Int
prod = foldl (*) 1

prodOfEvens :: [Int] -> Int
prodOfEvens = prod . filter (not . odd)

powersOf2 :: [Int]
powersOf2 = map (2^) [(0::Int) ..]

scalarProduct :: [Float] -> [Float] -> [Float]
scalarProduct = zipWith (*)
