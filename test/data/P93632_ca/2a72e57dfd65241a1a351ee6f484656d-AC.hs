eql :: [Int] -> [Int] -> Bool
eql a b = a == b

prod :: [Int] -> Int
prod xs =  (foldl (*) 1 xs)

prodOfEvens :: [Int] -> Int
prodOfEvens xs = (foldl (*) 1 (filter even xs))

powers :: Int -> [Int]
powers x = [y] ++ powers y
    where
        y = x*2

powersOf2 :: [Int]
powersOf2 = [1] ++ powers 1

scalarProduct :: [Float] -> [Float] -> Float
scalarProduct a b = sum (zipWith (*) a b)
