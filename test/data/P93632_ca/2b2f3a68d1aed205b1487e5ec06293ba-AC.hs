eql :: [Int] -> [Int] -> Bool
eql [] [] = True
eql [] xs = False
eql xs [] = False
eql xs ys = if head ls == 0 then eql (tail xs) (tail ys)
	else False
	where
		ls = zipWith (-) xs ys

prod :: [Int] -> Int
prod xs = foldr (*) 1 xs

prodOfEvens :: [Int] -> Int
prodOfEvens xs = foldr (*) 1 (filter (even) xs)

powersOf2 :: [Int]
powersOf2 = iterate (*2) 1

scalarProduct :: [Float] -> [Float] -> Float
scalarProduct xs ys = foldr (+) 0 (zipWith (*) xs ys)

igual :: [Int] -> [Int] -> Bool
igual [] [] = True
igual [] xs = False
igual xs [] = False
igual (x:xs) (y:ys) = if x == y then igual xs ys
	else False