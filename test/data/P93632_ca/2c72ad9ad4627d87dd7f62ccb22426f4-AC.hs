eql :: [Int] -> [Int] -> Bool
eql a b = and ((length a == (length b)):(zipWith (==) a b))

prod :: [Int] -> Int
prod = foldl (*) 1

prodOfEvens :: [Int] -> Int
prodOfEvens a = prod [x | x<-a, even x]

powersOf2 :: [Int]
powersOf2 = 1 : [2 * x | x<-powersOf2]

scalarProduct :: [Float] -> [Float] -> Float
scalarProduct a b = sum $ zipWith (*) a b
