eql::[Int]->[Int]->Bool
eql l1 l2 = let lzip = zip l1 l2
						in foldl (\x (u,v) -> (&&) x $ u == v) True lzip

prod::[Int]->Int
prod l = foldl (*) 1 l

prodOfEvens::[Int]->Int
prodOfEvens l = 
	foldl (\x y -> if (mod y 2) == 0 then x*y else x) 1 l

powersOf2::[Int]
powersOf2 = map (2^) [1,2..]

scalarProduct::[Float]->[Float]->Float
scalarProduct l1 l2 = let lzip = zip l1 l2
											in foldl (\x (u,v) -> x + u*v) 0 lzip
