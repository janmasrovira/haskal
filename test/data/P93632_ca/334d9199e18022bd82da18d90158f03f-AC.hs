eql :: [Int] -> [Int] -> Bool
eql a b 
	| length a == length b = and $ zipWith (==) a b
	| otherwise = False

prod :: [Int] -> Int
prod = foldl (*) 1

prodOfEvens :: [Int] -> Int
prodOfEvens = foldl (*) 1 . filter (even)

powersOf2 :: [Int]
powersOf2 = iterate (*2) 1

scalarProduct :: [Float] -> [Float] -> Float
scalarProduct a b = sum $ zipWith (*) a b