-- eql
eql :: [Int] -> [Int] -> Bool
eql x y = and (zipWith (==) x y) && length x == length y

-- prod
prod :: [Int] -> Int
prod x = foldl (*) 1 x

-- prodOfEvens
prodOfEvens :: [Int] -> Int
prodOfEvens x = prod (filter even x)

-- powersOf2
powersOf2 :: [Int]
powersOf2 = iterate (*2) 1

-- scalarProduct
scalarProduct :: [Float] -> [Float] -> Float
scalarProduct = 0.0

