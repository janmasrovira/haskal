eql :: [Int] -> [Int] -> Bool
eql a b = all id (zipWith (==) a b) && length a == length b
-- alternativa
  --eql as bs = foldl (&&) True (zipWith (==) as bs)
-- alternativa
  --lengtth filter id (zipWith (==) as bs)) == length bs && length as == length bs

prod :: [Int] -> Int
prod = foldl (*) 1

prodOfEvens :: [Int] -> Int
prodOfEvens l = foldl (*) 1 ((filter even) l)

powersOf2 :: [Int]
powersOf2 = iterate (*2) 1

scalarProduct :: [Float] -> [Float] -> Float
scalarProduct as bs = foldl (+) 0.0 (zipWith (*) as bs)


