eql::[Int]->[Int]->Bool
eql as bs = all id (zipWith (==) as bs) --all the bs and as are the same number (the == function returns true)
    && length as == length bs --id funció identitat
    

eql2::[Int]->[Int]->Bool
eql2 as bs = foldl (&&) True (zipWith (==) as bs)
    && length as == length bs
    
eql3::[Int]->[Int]->Bool
eql3 as bs = length (filter id (zipWith (==) as bs)) == length bs
    && length as == length bs
    
prod::[Int]->Int
prod l = foldl (*) 1 l
    
prodOfEvens::[Int]->Int
prodOfEvens = prod . (filter even) --first we filter the even and then we do the product

powersOf2::[Int]
powersOf2 = map (2^) [0,1..] 

powersOf2Second::[Int]
powersOf2Second = iterate (*2) 1

scalarProduct::[Float]->[Float]->Float
scalarProduct [] [] = 0 
scalarProduct as bs = foldl (+) 0 (zipWith (*) as bs)