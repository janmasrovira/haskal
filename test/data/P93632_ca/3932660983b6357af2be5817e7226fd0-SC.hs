

eql::[Int]->[Int]->Bool
eql [] [] = True
eql (x:xs) [] = False
eql [] (x:xs) = False
eql (x:xs) (y:ys) = (x:xs) == (y:ys)


prod::[Int]->Int
prod [] = 1
prod (x:xs) = x*prod xs

prodOfEvens::[Int]->Int
prodOfEvens [] = 1
prodOfEvens (x:xs)
	|even x == True = x*prodOfEvens xs
	|otherwise = prodOfEvens xs				

powersOf2::[Int] 
powersOf2 = [x*x|x<-[1..], even x]


scalarProduct::[Float]->[Float]->Float
scalarProduct [] [] = 1
scalarProduct (x:xs) (y:ys) = x*y + scalarProduct xs ys

 
