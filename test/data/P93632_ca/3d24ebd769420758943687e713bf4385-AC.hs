eql :: [Int] -> [Int] -> Bool
eql l1 l2 = ((length l1) == (length l2)) && (all id (zipWith (==) l1 l2))

prod :: [Int] -> Int
prod ls = (foldl (*) 1 ls)

prodOfEvens :: [Int] -> Int
prodOfEvens ls = prod (filter even ls)

powersOf2 :: [Int]
powersOf2 = 
    let
        powersGt x = (x:(powersGt (2*x)))
    in  
        powersGt 1

scalarProduct :: [Float] -> [Float] -> Float
scalarProduct l1 l2 = foldl (+) 0 (zipWith (*) l1 l2)

