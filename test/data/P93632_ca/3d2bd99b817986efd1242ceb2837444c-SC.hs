eql :: [Int] -> [Int] -> Bool
eql xs ys
		| length xs /= length ys = False
		| otherwise 			 = and (zipWith (==) xs ys)

prod :: [Int] -> Int
prod xs = product xs

prodOfEvents :: [Int] -> Int
prodOfEvents xs = product (filter p xs)
		where p x = x `mod` 2 == 0

powersOf2 :: [Int]
powersOf2 = map (2^) [1,2..]

scalarProduct :: [Float] -> [Float] -> Float
scalarProduct xs ys
		| length xs == length ys = sum (zipWith (*) xs ys)