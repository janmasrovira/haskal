eql:: [Int] -> [Int] -> Bool
eql a b  = a==b

prod:: [Int] -> Int
prod a = product a


prodOfEvens:: [Int] -> Int
prodOfEvens a = product ( filter (\x->x `mod` 2 == 0) a ) 

powersOf2::[Int]
powersOf2 = map (2^) [0..]

scalarProduct:: [Float] -> [Float] -> Float
scalarProduct a b = sum( zipWith(*) a b )

