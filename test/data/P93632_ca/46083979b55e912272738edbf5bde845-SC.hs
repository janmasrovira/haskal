eql l g
	| length l /= length g = False 
	| otherwise = 
		let m = zip l g in
		all (\m -> (fst m) == (snd m)) m

prod l = foldr (*) 1 l