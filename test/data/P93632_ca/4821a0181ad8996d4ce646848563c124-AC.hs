eql :: [Int] -> [Int] -> Bool
eql as bs = (and (zipWith (==) as bs)) && length as == length bs

prod :: [Int] -> Int
prod as = foldl (*) 1 as

prodOfEvens :: [Int] -> Int
prodOfEvens = prod . (filter even)

powersOf2 :: [Int]
powersOf2 = iterate (*2) 1

scalarProduct :: [Float] -> [Float] -> Float
scalarProduct as bs = foldl (+) 0 (zipWith (*) as bs)
