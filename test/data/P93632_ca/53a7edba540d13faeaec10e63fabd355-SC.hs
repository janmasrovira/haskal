eql::[Int]->[Int]-> Bool
eql [] [] = True
eql l1@(x:xs) l2@(y:ys) = ((and (zipWith (==) l1 l2)) == True) && (length l1 == length l2)


prod::[Int] -> Int
--prod [] = 0
prod (x:xs) = foldl (*) 1 (x:xs)


prodOfEvens::[Int]->Int

prodOfEvens (x:xs) = foldl (*) 1 (filter (even) (x:xs))


powersOf2::[Int]

powersOf2 = [ 2^x | x <- [0..] ]


scalarProduct::[Float] -> [Float] -> Float

scalarProduct l1@(a:as) l2@(b:bs) = sum (zipWith (*) l1 l2)
