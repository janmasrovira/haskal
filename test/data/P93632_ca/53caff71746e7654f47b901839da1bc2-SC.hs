eql :: [Int] -> [Int] -> Bool
eql = (==)

prod :: [Int] -> Int
prod = foldr1 (*)

prodOfEvens :: [Int] -> Int
prodOfEvens = prod . filter even

powersOf2 :: [Int]
powersOf2 = [2^i | i <- [0..]]

scalarProduct :: [Float] -> [Float] -> Float
scalarProduct l = sum . zipWith (*) l
