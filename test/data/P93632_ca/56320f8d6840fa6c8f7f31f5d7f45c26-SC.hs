eql::[Int]->[Int]->Bool
eql (a:as) (b:bs) = all (\x->x) (zipWith (==) (a:as) (b:bs))