
eql :: [Int] -> [Int] -> Bool
eql (x:xs) (y:ys) = all (== True) (zipWith (==) (x:xs) (y:ys))
