-- Indica si dues llistes d’enters són iguals.
eql :: [Int] -> [Int] -> Bool
eql x y = apply2 (==) (&&) (&& False) True x y

-- Donades dues llistes, aplica f a cada parell d'elements
-- de les llistes, i els resultats són combinats utilitzant
-- la funció g. En cas de que les llistes no tinguin la 
-- mateixa llargada, aplica h. El quart paràmetre és el resultat
-- per defecte. 
apply2 :: (a -> a -> b) -> (b -> b -> b) -> (b -> b) -> b -> [a] -> [a] -> b
apply2 _ _ _ r [] [] = r
apply2 _ _ h r [] _ = h r
apply2 _ _ h r _ [] = h r
apply2 f g h r (x:xs) (y:ys) = apply2 f g h (g (f x y) r) xs ys

-- Calcula el producte dels elements d’una llista d’enters.
prod :: [Int] -> Int
prod [] = 0
prod x = foldr (*) 1 x

-- Multiplica tots el nombres parells d’una llista d’enters.
prodOfEvens :: [Int] -> Int 
prodOfEvens x = prod $ filter (\y -> mod y 2 == 0) x

-- Genera la llista de totes les potències de 2.
powersOf2 :: [Int]
powersOf2 = iterate (*2) 1

-- Calcula el producte escalar de dues llistes de reals de 
-- la mateixa mida.
scalarProduct :: [Float] -> [Float] -> Float
scalarProduct x y = apply2 (*) (+) (+ 0) 0 x y