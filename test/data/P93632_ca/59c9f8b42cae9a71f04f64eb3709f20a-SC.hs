eql :: [Int]->[Int]->Bool

eql [] [] = True
eql [x] [] = False
eql [] [y] = False
eql xs [] = False
eql [] ys = False
eql [x][y]
  | x==y = True
  |otherwise = False
  
eql (x:xs) (y:ys) = eql [x] [y] && eql xs ys
  
  
prod :: [Int]->Int

prod [] = 0
--prod [x] = x
prod xs = foldr (*) 1 xs


prodOfEvens :: [Int]->Int

prodOfEvens [] = 0
prodOfEvens xs = prod (filter even xs)

powersOf2 :: [Int] 

powersOf2  = iterate (2*) 1


lfib m n = m : (lfib n (m+n))
 
fib n = (lfib 0 1) !! n

scalarProduct :: [Float]->[Float]->Float

scalarProduct [] [] = 0
scalarProduct (x:xs) (y:ys) = x*y+scalarProduct xs ys

