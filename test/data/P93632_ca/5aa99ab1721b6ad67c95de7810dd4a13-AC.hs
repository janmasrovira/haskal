eql :: [Int] -> [Int] -> Bool
eql x y = foldr (&&) True (zipWith (==) x y) && length x == length y

prod :: [Int] -> Int
prod x = product x

prodOfEvens :: [Int] -> Int
prodOfEvens x = product (llistaEvens x)

llistaEvens :: [Int] -> [Int]
llistaEvens [] = []
llistaEvens (x:xs) 
  |even x = [x]++llistaEvens xs
  |otherwise = llistaEvens xs

powersOf2::[Int]
powersOf2 = [2^x| x <- [0..]]  

{-
powersOf2::[Int]
powersOf2 = [2^x| x <- [0..]]  

powersOf2 :: [Int]
powersOf2 = (power 0)
-}

power :: Int -> [Int]
power x = [2^x]++(power(x+1))

scalarProduct :: [Float] -> [Float] -> Float
scalarProduct x y = foldl (+) 0 (zipWith (*) x y)