-- eql::[Int] -> [Int] -> Bool

eql [] [] = True
eql [] (y:ys) = False
eql (x:xs) [] = False
eql (x:xs) (y:ys)
	|all (==x) [y] = eql xs ys
	|otherwise = False 

-- prod::[Int] -> Int

prod (x:xs) = foldr (*) x xs

-- prodOfEvens::[Int] -> Int

prodOfEvens (x:xs) = foldr (*) 1 (filter (even) (x:xs))

-- powersOf2::[Int]

powersOf2 = iterate (*2) 1

--scalarProduct::[Float] -> [Float] -> Float

scalarProduct (x:xs) (y:ys) = sum (zipWith (*) (x:xs) (y:ys))



