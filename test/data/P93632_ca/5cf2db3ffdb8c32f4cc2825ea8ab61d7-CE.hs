eql:: [Int] -> [Int] -> Bool
eql x y =
	all (eq true) (zipWith eq x y) 