-- eql

eql :: [Int] -> [Int] -> Bool
eql x y = ((length x) == (length y)) && (all (True==) (zipWith (==) x y))

-- prod

prod :: [Int] -> Int
prod [] = 1
prod (x:xs) = (foldr (*) x xs)

-- prodOfEvens

prodOfEvens :: [Int] -> Int
prodOfEvens [] = 1
prodOfEvens x = (foldr (*) 1 (filter even x))

-- powersOf2

powersOf2 :: [Int]
powersOf2 = (iterate (2*) 1)

-- scalarProduct

scalarProduct :: [Float] -> [Float] -> Float
scalarProduct x y = (foldr (+) 0 (zipWith (*) x y))
