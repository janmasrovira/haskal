

eql :: [Int] -> [Int] -> Bool 
eql [] [] = True
eql [] (y:ys) = False
eql (x:xs) [] = False
eql (x:xs) (y:ys)
    | x == y	= eql xs ys
    | otherwise	= False
    
prod :: [Int] -> Int
prod [x] = x
prod (x:xs) = x * (prod xs)

prodOfEvens :: [Int] -> Int
prodOfEvens [x]
    | mod x 2 == 0	= x
    | otherwise		= 1
prodOfEvens (x:xs)
    | mod x 2 == 0	= x * (prodOfEvens xs)
    | otherwise		= prodOfEvens xs
    
powersOf2 :: [Int]
powersOf2 = pow2 2
    where
	pow2 n = n : map (* n) (pow2 n)
	
scalarProduct :: [Float] -> [Float] -> Float
scalarProduct [x] [y] = x * y
scalarProduct (x:xs) (y:ys) = x * y + (scalarProduct xs ys)