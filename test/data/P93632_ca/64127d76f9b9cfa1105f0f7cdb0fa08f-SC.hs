eql::[Int]->[Int]->Bool
eql x y = foldr (&&) True (zipWith (==) x y) && length x == length y

prod::[Int]->Int
prod x = product x

prodOfEvens::[Int]->Int
pordOfEvens [] = 1;
prodOfEvens (x:xs) = foldr (\z y-> if (mod z 2) == 0 then z*y else y) x xs