
-- #1
-- indica si dues lliste son inguals
eql :: [Int] -> [Int] -> Bool
eql as bs = all id (zipWith (==) as bs)
            && length as == length bs

eql2 :: [Int] -> [Int] -> Bool
eql2 as bs = foldl (&&) True (zipWith (==) as bs)
             && length as == length bs

eql3 :: [Int] -> [Int] -> Bool
eql3 as bs = length (filter id (zipWith (==) as bs)) == length bs
             && length as == length bs


-- #2
-- calcula el producte dels elements d'una llista
prod :: [Int] -> Int
prod xs = foldl (*) 1 xs



-- #3
-- calcula el producte dels elements parells d'una llista
prodOfEvens :: [Int] -> Int
prodOfEvens xs = prod (filter even xs)

-- let prodOfEvens = product . (filter even)



-- #4
-- genera la llista de totes les potencies de 2
powersOf2 :: [Int]
powersOf2 = iterate (*2) 1



-- #5
-- calcula el producte escalar de dues llista de mida igual
scalarProduct :: [Float] -> [Float] -> Float
scalarProduct xs ys = foldl (+) 0 (zipWith (*) xs ys)