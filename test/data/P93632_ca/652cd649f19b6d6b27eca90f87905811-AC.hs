
-- Indica si dos llistes d'enters son iguals
eql :: [Int] -> [Int] -> Bool
eql as bs = (and (zipWith (==) as bs)) && (length as == length bs)

-- Retorna el producte d'una llista d'enters
prod :: [Int] -> Int
prod xs = foldl (*) 1 xs

-- Retorna el producte dels nombres parells d'una llista d'enters
prodOfEvens :: [Int] -> Int
prodOfEvens xs = foldl (*) 1 (filter even xs)

-- Retorna totes les potencies de 2
powersOf2 :: [Int]
powersOf2 = iterate (*2) 1

-- Retorna el producte escalar de dues llistes amb mateixa mida
scalarProduct :: [Float] -> [Float] -> Float
scalarProduct as bs = sum (zipWith (*) as bs)
