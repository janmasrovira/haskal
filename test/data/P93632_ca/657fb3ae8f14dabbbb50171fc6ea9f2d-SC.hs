--eql [1,2,3] [1,2,3]
--eql [1,2,3] [3,2,1]
--eql [1,2,3] [1,2,3,4]


eql::[Int]->[Int]->Bool
eql [] [] = True
eql [s] [] = False
eql [] [s] = False
eql (x:xs) (y:ys)
	| length(x:xs) == length(y:ys) && x == y = eql xs ys
	| not( length(x:xs) == length(y:ys) && x /= y) = False

--prod [2,10,5]

prod::[Int]->Int
prod [] = 1
prod (x:xs) = x * prod xs


--prodOfEvens [2,10,5]

prodOfEvens::[Int]->Int
prodOfEvens [] = 1
prodOfEvens (x:xs) = if (x `mod` 2 == 0) then x * prodOfEvens xs
	else 1 * prodOfEvens xs

--take 5 powersOf2

powersOf2::[Int]
powersOf2 = (map (2^) [0..])

--scalarProduct [2.0,1.0,5.0] [3.0,2.0,2.0]

scalarProduct::[Float]->[Float]->Float
scalarProduct v1 v2 = sum $ zipWith (*) v1 v2
