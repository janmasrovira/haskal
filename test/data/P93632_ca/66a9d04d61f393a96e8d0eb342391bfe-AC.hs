eql :: [Int] -> [Int] -> Bool
eql xs ys
	| (length xs) == (length ys) = and (zipWith (==) xs ys)
	| otherwise = False

prod :: [Int] -> Int
prod xs = foldl (*) 1 xs

prodOfEvens = prod . (filter even)

powersOf2 = iterate (*2) 1

scalarProduct as bs = sum (zipWith (*) as bs)
