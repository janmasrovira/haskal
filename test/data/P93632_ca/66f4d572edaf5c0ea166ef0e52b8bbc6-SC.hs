eql::[Int]->[Int]->Bool
eql xs ys = foldl f True (zip xs ys)
	where 
		f b (x,y) = b && x==y

prod::[Int] -> Int
prod xs = foldl (*) 1 xs


prodOfEvens::[Int] -> Int
prodOfEvens xs = foldl (*) 1 (filter even xs) 

--powersOf2::[Int]
--powersOf2 = 


scalarProduct::[Float] -> [Float]->Float
scalarProduct xs ys = sum (zipWith (*) xs ys)


