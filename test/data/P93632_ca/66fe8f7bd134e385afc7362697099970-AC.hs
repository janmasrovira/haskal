eql :: [Int] -> [Int] -> Bool
eql l1 l2
  | not ((length l1) == (length l2)) = False
  | otherwise = foldl (&&) True (zipWith (==) l1 l2)
  
prod :: [Int] -> Int
prod x = foldl (*) 1 x

prodOfEvens :: [Int] -> Int
prodOfEvens x = foldl (*) 1 (filter even x)

powersOf2 :: [Int]
powersOf2 = (iterate (*2) 1)

scalarProduct :: [Float] -> [Float] -> Float
scalarProduct x y = foldl (+) 0 (zipWith (*) x y)