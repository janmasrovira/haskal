eql::[Int]->[Int]->Bool
eql [][] = True
eql a b
	| (length a) /= (length b) = False
	| otherwise = and (zipWith (==)  a b) 

prod::[Int]->Int
prod a = foldr (*) 1 a

prodOfEvens::[Int]->Int
prodOfEvens a = prod (filter even a)

powersOf2::[Int]
powersOf2 = iterate (* 2) 1

scalarProduct::[Float]->[Float]->Float
scalarProduct x y = sum (zipWith (*) x y) 
