eql:: [Int] -> [Int] -> Bool
eql a b = (a == b)

prod:: [Int] -> Int
prod l = (foldl (*) 1 l)

prodOfEvens:: [Int]->Int
prodOfEvens l = (foldl (*) 1 [x | x<-l, even x])

powersOf2:: [Int]
powersOf2 = (map (2^) [0..])

scalarProduct:: [Float] -> [Float] -> Float
scalarProduct a b = (sum  (zipWith (*) a b) )