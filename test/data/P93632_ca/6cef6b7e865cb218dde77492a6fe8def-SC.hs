eql [] [] = True
eql l1 l2
    | (length l1) /= (length l2) = False
    | otherwise = and (zipWith (==) l1 l2)

prod [] = 0
prod l = foldr (*) 1 l

prodOfEvens [] = 0
prodOfEvens l = prod (filter even l)

powersOf2 = iterate (2*) 1

scalarProduct l1 l2 = sum (zipWith (*) l1 l2)