eql::[Int]->[Int]->Bool
eql x y = foldr (&&) True (zipWith (==) x y) && length x == length y

prod::[Int]->Int
prod x = product x