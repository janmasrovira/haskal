eql :: [Int] -> [Int] -> Bool
eql x y
  | length x /= length y = False
  | otherwise = foldl (&&) True (zipWith (==) x y)

prod :: [Int] -> Int
prod x = foldl (*) 1 x

prodOfEvens :: [Int] -> Int
prodOfEvens x = foldl (prodEven) 1 x

prodEven :: Int -> Int -> Int
prodEven x y
  | mod y 2 == 0 = x * y
  | otherwise = x

powersOf2 :: [Int]
powersOf2 = iterate (*2) 1

scalarProduct :: [Float] -> [Float] -> Float
scalarProduct x y = foldl (+) 0 (zipWith (*) x y)