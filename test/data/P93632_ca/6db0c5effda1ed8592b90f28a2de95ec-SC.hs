eql :: [Int] -> [Int] -> Bool
eql lx ly = eqla (-) lx ly

eqla :: (Int -> Int -> Int) -> [Int] -> [Int] -> Bool
eqla f [] _ = False
eqla f _ [] = False
eqla f (x:lx) (y:ly)
  | f x y == 0 = True && eqla f lx ly
  | otherwise = False