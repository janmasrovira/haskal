samesize::[Int]->[Int]->Bool

samesize [] [] = True
samesize [] x = False
samesize x [] = False
samesize (x:xs) (y:ys) = (samesize xs ys)

eql::[Int]->[Int]->Bool

eql xs ys = all id (zipWith (==) xs ys) && (samesize xs ys)

