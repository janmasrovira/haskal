allTrue :: [Bool] -> Bool
allTrue [] = True
allTrue (x:xs)
	| x 			= allTrue xs
	| otherwise		= False

eql :: [Int] -> [Int] -> Bool
eql l1 l2 =
	allTrue (zipWith (==) l1 l2) && (length l1 == length l2)

prod :: [Int] -> Int
prod l = 
	foldr (*) 1 l

prodOfEvens :: [Int] -> Int
prodOfEvens l =
	prod (filter even l)

powersOf2 :: [Int]
powersOf2 = [2^n | n<-[0..]]

scalarProduct :: [Float] -> [Float] -> Float
scalarProduct l1 l2 =
	sum (zipWith (*) l1 l2)