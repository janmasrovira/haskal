eql::[Int]->[Int]->Bool
eql x y 
	| length x == length y		= all (==True) (zipWith (==) x y)
	| otherwise					= False

prod::[Int]->Int
prod [] = 0
prod l = foldl (*) 1 l

prodOfEvens::[Int]->Int
prodOfEvens [] = 0
prodOfEvens l = foldl (*) 1 (filter even l)

powersOf2::[Int]
powersOf2 = map (2^) [0..]

scalarProduct::[Float]->[Float]->Float
scalarProduct x y = foldl (+) 0 (zipWith (*)  x y)