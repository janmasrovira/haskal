
 --1
eql :: [Int] -> [Int] -> Bool
eql xs ys = all id (zipWith (==) xs ys) 
    && length xs == length ys
    
----------------------------------------------------------------------------------------------------
{--otra version
eql1 :: [Int] -> [Int] -> Bool
eql1 xs ys = foldl (&&) True (zipWith (==) xs ys) && length xs == length ys

eql2 :: [Int] -> [Int] -> Bool
eql2 xs ys = length (filter id (zipWith (==) xs ys)) == length xs && length xs == length ys 
--}
----------------------------------------------------------------------------------------------------

--2
prod :: [Int] -> Int
prod xs = foldl (*) 1 xs
----------------------------------------------------------------------------------------------------

--3  
prodOfEvens :: [Int] -> Int
prodOfEvens xs = prod (filter even xs)

{--otra version
prodOfEvens :: [Int] -> Int
prodOfEvens  = product . (filter even)
prodOfEvens :: [Int] -> Int
prodOfEvens xs 
  | xs == [] = 0
  | otherwise = foldl (*) 1 (filter even xs)
--}

----------------------------------------------------------------------------------------------------
--4
powersOf2 :: [Int]
powersOf2  = [2^i | i <- [0..]]

----------------------------------------------------------------------------------------------------
--5
scalarProduct :: [Float] -> [Float] -> Float
scalarProduct xs ys = foldl (+) 0 (zipWith (*) xs ys)
