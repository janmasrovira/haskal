

eql::[Int]->[Int]->Bool
eql xs xy = all(\x -> x== 0) p && (length xs == length xy)
    where p =zipWith(\x y -> x-y) xs xy
    
          
suma::Integer->Integer->Integer
suma= \x y -> x*y;


prod::[Int]->Int
prod xs= foldr (\x y -> x*y) 1 xs

prod2::[Int]->Int
prod2 xs= foldr prod' 1 xs
  where prod' a b = a*b


prodOfEvens::[Int]->Int
prodOfEvens xs= foldr (\x y -> x*y ) 1 (filter even xs)

    
    
scalarProduct:: [Float]->[Float]-> Float
scalarProduct xs xy = foldr (\x y -> x+y) 0 p
    where p =zipWith (\x y -> x*y) xs xy

    
     

powersOf2::[Int]
powersOf2 = map (\x -> 2^x) [0..]