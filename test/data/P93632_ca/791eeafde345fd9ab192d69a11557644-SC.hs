
eql::[Int] -> [Int] -> Bool
eql [] [] = True
eql [] (y:ys) = False
eql (x:xs) [] = False
eql (x:xs) (y:ys)
	| x == y = eql xs ys
	| otherwise = False

prod::[Int] -> Int
prod[] = 0
prod(x:[]) = x
prod(x:xs) = product (x:xs)

prodOfEvens::[Int] -> Int
prodOfEvens[] = 0
prodOfEvens(x:xs) = prod (filter(even) (x:xs))

powersOf2::[Int]
powersOf2 = [ 2^x | x<- [0..] ]

scalarProduct::[Float]-> [Float] -> Float
scalarProduct[][] = 0
scalarProduct [] (y:ys) = 0
scalarProduct (x:xs) [] = 0
scalarProduct (x:xs) (y:ys) = x*y + scalarProduct xs ys

