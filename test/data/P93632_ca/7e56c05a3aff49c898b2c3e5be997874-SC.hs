eql ::[Int] -> [Int] -> Bool
eql (a) (b) = (length(a) == length(b)) && length(dropWhile (==True) (zipWith  (==) (a) (b))) == 0

prod :: [Int]-> Int
prod [] = 0
prod (a) = foldr (*) 1 (a)

prodOfEvens :: [Int]-> Int
prodOfEvens [] = 0
prodOfEvens (a) = foldr (*) 1 (takeWhile  (even) (a))

powersOf2::[Int]
powersOf2 =  zipWith (^) (iterate(+0) 2) (iterate(+1) 1)

scalarProduct::[Float]->[Float]->Float
scalarProduct (a) (b) = foldr (+) 0 (zipWith (*) (a) (b))
