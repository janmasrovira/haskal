
-- Indica si dos llistes d'enters son iguals
eql :: [Int] -> [Int] -> Bool
eql as bs = (and (zipWith (==) as bs)) && (length as == length bs)
