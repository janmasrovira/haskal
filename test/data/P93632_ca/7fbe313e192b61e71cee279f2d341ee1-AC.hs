eql :: [Int] -> [Int] -> Bool
eql l1 l2
  | (length l1) == (length l2) = and (zipWith (\x y-> x == y) l1 l2) 
  | otherwise = False
  
prod :: [Int] -> Int
prod l = foldl (*) 1 l

prodOfEvens :: [Int] -> Int
prodOfEvens l = foldl (*) 1 (filter (\x -> mod x 2 == 0) l)

powersOf2 :: [Int]
powersOf2 = iterate (*2) 1

scalarProduct :: [Float] -> [Float] -> Float
scalarProduct l1 l2 = sum (zipWith (\x y-> x * y) l1 l2)
