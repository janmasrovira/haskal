--leql :: [Int] -> [Int] -> Bool

--leql [] [] = True
--leql [] _ = False
--leql _ [] = False
--leql (x:xs) (y:ys) = if x == y then leql xs ys else False


eql :: [Int] -> [Int] -> Bool

eql a b
	| length a /= length b						= False
	| length a == 0							= True		
	| length[k | k <- (zipWith (==) a b), k == False] > 0		= False
	| otherwise							= True



prod :: [Int] -> Int

--prod = foldl1 (*)
prod [] = 1
prod (x:xs) = x * prod xs



prodOfEvens :: [Int] -> Int

prodOfEvens a = foldr1 (*) $ filter even a



powersOf2 :: [Int]

powersOf2 = map twoeln [0, 1..]
	
twoeln :: Int -> Int
twoeln 0 = 1
twoeln n = 2 * twoeln(n-1) 


scalarProduct :: [Float] -> [Float] -> Float

scalarProduct a b 
 | length a /= length b		= error "You cannot multiply vectors of different lengths"
 | otherwise			= sum $ zipWith (*) a b

