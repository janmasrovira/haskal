eql::[Int] -> [Int] -> Bool
eql l t =  all eql2 (zip l t)

eql2::(Int,Int) -> Bool
eql2 (x,y) = x == y

prod::[Int] -> Int
prod l = foldr (*) 1 l 

prodOfEvens::[Int] -> Int
prodOfEvens l = prod (filter md l)

md:: Int -> Bool
md n = (mod n 2) == 0 

powersOf2::[Int]
powersOf2 = iterate (* 2) 1

scalarProduct::[Float] -> [Float] -> Float
scalarProduct l t = foldr (+) 0 (zipWith (*) l t)
