
identity :: [Int] -> [Int]
identity x = x

evenFilter :: [Int] -> [Int]
evenFilter x
	| null x = []
	| even h = (h:t)
	| otherwise = ([1]++t)
	where
		h = (head x)
		t = evenFilter (tail x)
		
comparer :: ([Int] -> [Int] -> Bool) -> [Int] -> [Int] -> Bool
comparer f a b = f a b

solver :: ([Int] -> Int) -> ([Int] -> [Int]) -> [Int] -> Int
solver f g a = f (g a)

eql :: [Int] -> [Int] -> Bool
eql x y = comparer (==) x y

prod :: [Int] -> Int
prod x = solver (product) (identity) x

prodOfEvens :: [Int] -> Int
prodOfEvens x = solver (product) (evenFilter) x

doPower :: (Int -> Int -> Int) -> Int -> Int -> Int
doPower f b e = (f b e)
	
powersOf2 :: [Int]
powersOf2 = [doPower (^)(2)(e) | e <- [0 ..]]

operate :: (Float -> Float -> Float) -> Float -> Float -> Float
operate f a b = f a b

scalarProduct :: [Float] -> [Float] -> Float
scalarProduct [] []	= 0
scalarProduct (x:xs) (y:ys) = operate (*)(x)(y) + scalarProduct xs ys
  