eql :: [Int] -> [Int] -> Bool
eql l1 l2 
    |(length l1)  /= (length l2) = False
    |otherwise = (and ( zipWith (==) l1 l2))

prod :: [Int] -> Int
prod l = foldr (*) 1 l 


prodOfEvens :: [Int] -> Int
prodOfEvens x = prod (filter even x)

powersOf2 :: [Int] 
powersOf2 = iterate (*2) 1

scalarProduct :: [Float] -> [Float] -> Float 
scalarProduct x y = sumav (zipWith (*) x y)
	where 
		sumav :: [Float] -> Float
		sumav l = foldr (+) 0 l