eql :: [Int] -> [Int] -> Bool
eql l1 l2 = l1 == l2

prod :: [Int] -> Int
prod l = foldl (*) 1 l

prodOfEvens :: [Int] -> Int
prodOfEvens l = foldl (*) 1 $ filter even l

powersOf2 :: [Int]
powersOf2 = map (\x -> 2^x) [0..]

scalarProduct :: [Float] -> [Float] -> Float
scalarProduct l1 l2 = foldl (+) 0 $ zipWith (\a b -> a*b) l1 l2