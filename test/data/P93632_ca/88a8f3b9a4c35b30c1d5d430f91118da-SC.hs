eql xs ys = (length xs) == (length ys) && (and $ zipWith (==) xs ys)

prod xs 
  | length xs == 0 = 0
  | otherwise = foldl (*) 1 xs

prodOfEvens xs 
  | length (filter even xs) == 0 = 0
  | otherwise = foldl (*) 1 (filter even xs)
  
powersOf2 = iterate (2*) 1

scalarProduct xs ys = sum (zipWith (*) xs ys)