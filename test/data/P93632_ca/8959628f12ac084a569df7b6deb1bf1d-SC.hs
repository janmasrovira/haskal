eql :: [Int] -> [Int] -> Bool
eql list1 list2 
	| (length list1) /= (length list2)= False
	|otherwise = and (zipWith (==) list1 list2)  

prod :: [Int] -> Int
prod list =  foldl (*) 1 list

prodOfEvens :: [Int] -> Int
prodOfEvens list = foldl (*) 1 (takeWhile (even) list)

powersOf2 :: [Int]
powersOf2= iterate (*2) 1

scalarProduct :: [Float] -> [Float] -> Float
scalarProduct list1 list2 = sum (zipWith (+) list1 list2)