eql :: [Int] -> [Int] -> Bool 
eql l s = (length l == length s) && foldr (\ a b -> and [(fst a) == (snd a), b] ) True (zip l s)

prod :: [Int] -> Int
prod l = foldr (*) 1 l

prodOfEvens :: [Int] -> Int 
prodOfEvens l = prod (filter even l)

powersOf2 :: [Int]
powersOf2 = map (\x -> 2^x) [0..]

scalarProduct :: [Float] -> [Float] -> Float
scalarProduct v1 v2 = foldr (+) 0 (zipWith (*) v1 v2)

