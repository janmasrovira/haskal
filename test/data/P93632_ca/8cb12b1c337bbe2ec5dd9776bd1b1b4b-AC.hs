eql [] [] = True
eql (x:xs) [] = False
eql [] (y:ys) = False
eql (x:xs) (y:ys) | x == y		= eql xs ys
				  | otherwise	= False
				  
prod [] = 1
prod (x:xs) = x * prod xs

prodOfEvens [] = 1
prodOfEvens (x:xs) | even x = x*prodOfEvens xs
				   | otherwise = prodOfEvens xs
				   
powersOf2 = iterate (\x->2*x) 1

scalarProduct :: [Float] -> [Float] -> Float
scalarProduct = (\x y -> (foldr (+) 0.0 (zipWith (*) x y)))
