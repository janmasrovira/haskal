eql :: [Int] -> [Int] -> Bool
eql xs ys
		| length xs /= length ys = False
		| otherwise 			 = and (zipWith (==) xs ys)

prod :: [Int] -> Int
prod xs = product xs

prodOfEvens :: [Int] -> Int
prodOfEvens xs = product (filter p xs)
		where p x = x `mod` 2 == 0

powersOf2 :: [Int]
powersOf2 = map (2^) [0,1..]

scalarProduct :: [Float] -> [Float] -> Float
scalarProduct xs ys
		| length xs == length ys = sum (zipWith (*) xs ys)