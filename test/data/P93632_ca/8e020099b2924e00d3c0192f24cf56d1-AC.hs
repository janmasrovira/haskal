--exemple
--suma l = foldr f 0 l
--suma l = foldr (+) 0 l
--f e acc = e + acc --> foldr
--f acc e = acc + e -->foldl

eql :: [Int] -> [Int] -> Bool
eql x y
  | (length x) == (length y) = foldr (&&) True (zipWith (==) x y )
  | otherwise = False
  
prod :: [Int]->Int
prod x = foldr (*) 1 x

prodOfEvens :: [Int] -> Int
prodOfEvens x = foldr (*) 1 (filter (even) x)

powersOf2 :: [Int]
powersOf2 = iterate (*2) 1

scalarProduct :: [Float] -> [Float] -> Float
scalarProduct x y = foldr (+) 0 (zipWith (*) x y)