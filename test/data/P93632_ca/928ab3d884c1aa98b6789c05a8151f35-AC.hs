eql l1 l2 = l1 == l2

prod l = foldl (*) 1 l

prodOfEvens l = prod (filter even l)

powersOf2 = iterate (*2) 1

scalarProduct l1 l2 = sum (zipWith (*) l1 l2)

