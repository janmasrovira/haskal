prod::[Int]->Int
prod [] = 1
prod (x:xs) = foldr (*) x xs

eql::[Int]->[Int]->Bool
eql x y = foldr (&&) True (zipWith (==) x y) && length x == length y

prodOfEvens::[Int]->Int
prodOfEvens [] = 1
prodOfEvens (x:xs) = foldr (\z y-> if (mod z 2) == 0 then z*y else y) x xs

powersOf2::[Int]
powersOf2 = [2^x| x <- [0..]]

scalarProduct::[Float]->[Float]->Float
scalarProduct x y = foldl (+) 0 (zipWith (*) x y)
