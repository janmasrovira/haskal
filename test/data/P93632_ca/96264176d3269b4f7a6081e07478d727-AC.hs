eql :: [Int]->[Int]->Bool
eql l1 l2 = (length l1 == length l2) && all (== True) (zipWith (==) l1 l2)
--eql [] [] = True
--eql [] _ = False
--eql _ [] = False
--eql (l1:l11) (l2:l22)
--  |l1 /= l2 = False
--  |otherwise = eql l11 l22

prod :: [Int] -> Int
prod [] = 1
prod (x:xs) = x*(prod xs)

prodOfEvens :: [Int] -> Int
prodOfEvens l = prod (filter (even) l)

powersOf2 :: [Int]
powersOf2 = (iterate (*2) 1)

scalarProduct :: [Float] -> [Float] -> Float
scalarProduct l1 l2 = foldr (+) 0 (zipWith (*) l1 l2)
