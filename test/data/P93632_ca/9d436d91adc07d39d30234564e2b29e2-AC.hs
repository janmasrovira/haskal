eql :: [Int] -> [Int] -> Bool 
eql x y
  |x==[] && y == [] = True
  |x==[] || y==[] = False
  |head x == head y = eql (tail x) (tail y)
  |otherwise = False
  
prod :: [Int] -> Int
prod x  
  |x == [] = 1
  |otherwise = (head x) * prod (tail x)

prodOfEvens :: [Int] -> Int
prodOfEvens x
  |x == [] = 1
  |even (head x) = (head x) * prodOfEvens (tail x)
  |otherwise = prodOfEvens (tail x)


powersOf2 :: [Int] 
powersOf2 = map (2^) [0..]
  
scalarProduct :: [Float] -> [Float] -> Float 
scalarProduct x y
  | x ==[] = 0.0
  | otherwise = (head x) * (head y) + scalarProduct (tail x) (tail y)
 
