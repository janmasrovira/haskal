prod::[Int] -> Int
prod l = foldr (*) 1 l 

prodOfEvens::[Int] -> Int
prodOfEvens l = prod (filter md l)

md:: Int -> Bool
md n = (mod n 2) == 0 

powersOf2::[Int]
powersOf2 = iterate (* 2) 1
