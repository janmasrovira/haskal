eql :: [Int] -> [Int] -> Bool
eql l m 
	|length l /= length m	= False
	|otherwise	= all (==0) (zipWith (-) l m)

prod :: [Int] -> Int
prod [] = 1
prod (x:xs) = foldl (*) x xs

prodOfEvens :: [Int] -> Int
prodOfEvens [] = 1
prodOfEvens l 
	|length onlyEven == 0 	= 1
	|otherwise	= foldl (*) (head onlyEven) (tail onlyEven)
		where
			onlyEven = (filter (even) l)
