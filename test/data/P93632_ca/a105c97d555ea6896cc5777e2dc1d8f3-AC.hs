myLength [] = 0
myLength (x:xs) = 1 + myLength xs

eql :: [Int] -> [Int] -> Bool
eql x y
        | not (myLength x == myLength y) = False
        | otherwise = all (==True) (zipWith (==) x y)

prod :: [Int] -> Int
prod [] = 1
prod x = foldr (*) 1 x

prodOfEvens :: [Int] -> Int
prodOfEvens x = prod (filter (even) x)

powersOf2 :: [Int]
powersOf2 = iterate (*2) 1

scalarProduct :: [Float] -> [Float] -> Float
scalarProduct x y= foldr (+) 0 (zipWith (*) x y)