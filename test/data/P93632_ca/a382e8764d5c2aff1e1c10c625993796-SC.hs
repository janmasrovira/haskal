myLength [] = 0
myLength (x:xs) = 1 + myLength xs

eql :: [Int] -> [Int] -> Bool
eql x y
        | not (myLength x == myLength y) = False
        | otherwise = all (==True) (zipWith (==) x y)

prod :: [Int] -> Int
prod [] = 0
prod x = foldr (*) 1 x

prodOfEvens :: [Int] -> Int
prodOfEvens [] = 0
prodOfEvens x = foldr (*) 1 (filter (even) x)

powersOf2 :: [Int]
powersOf2 = iterate (*2) 1

scalarProduct :: [Float] -> [Float] -> Float
scalarProduct x y= foldr (+) 0 (zipWith (*) x y)