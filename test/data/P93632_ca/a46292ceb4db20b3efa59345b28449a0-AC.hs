eql::[Int]->[Int]->Bool
eql as bs
  | length as /= length bs = False
  | otherwise = and (zipWith (==) as bs)

prod::[Int]->Int
prod [] = 1
prod xs = foldr (*) 1 xs

prodOfEvens::[Int]->Int
prodOfEvens [] = 1
prodOfEvens xs = foldr (*) 1 (filter (even) xs)

powersOf2::[Int]
powersOf2 = iterate (*2) 1

scalarProduct::[Float]->[Float]->Float
scalarProduct [] [] = 1
scalarProduct as bs = foldr (+) 0 (zipWith (*) as bs)