-- exemple ús if then else

eql [] [] = True
eql x y
	| (length x) /= (length y) = False
	| otherwise = and (zipWith (==) x y)

prod a = foldr (*) 1 a
prodOfEvens b = prod (filter even b)

powersOf2 = iterate (* 2) 1

scalarProduct x y = sum (zipWith (*) x y) 