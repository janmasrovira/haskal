
--eql :: [Int] -> [Int] -> Bool

eql [] [] = True
eql (x:xs) [] = False
eql [] (y:ys) = False
eql (x:xs) (y:ys)
    |(((==)$x)y) = eql xs ys
    |otherwise = False

--prod :: [Int] -> Int
prod [] = 1
prod (x:xs) = (((*)$x)(prod xs))

--prodOfEvens :: [Int] -> Int

prodOfEvens [] = 1
prodOfEvens (x:xs) 
    |(even$x) = (((*)$x)(prodOfEvens xs))
    |otherwise = prodOfEvens(xs)

--powersOf2 :: [Int]


--scalarProduct :: [Float] -> [Float] -> Float

scalarProduct [] [] = 0
scalarProduct (x:xs) (y:ys) = ((x*y)+(scalarProduct xs ys))

