{-
  Feu una funció eql :: [Int] -> [Int] -> Bool que indiqui si dues llistes d’enters són iguals.
  Feu una funció prod :: [Int] -> Int que calculi el producte dels elements d’una llista d’enters.
  Feu una funció prodOfEvens :: [Int] -> Int que multiplica tots el nombres parells d’una llista d’enters.
  Feu una funció powersOf2 :: [Int] que generi la llista de totes les potències de 2.
  Feu una funció scalarProduct :: [Float] -> [Float] -> Float que calculi el producte escalar de dues llistes de reals de la mateixa mida.
-}

eql :: [Int] -> [Int] -> Bool
eql l1 l2 
  | not (length l1 == length l2) = False
  | otherwise = all (== True) (zipWith (==) l1 l2)


prod :: [Int] -> Int
prod l = foldr (*) 1 l

prodOfEvensAux :: Int -> Int -> Int
prodOfEvensAux e acc
  | (mod e 2) == 0	= e*acc
  | otherwise	= acc

prodOfEvens :: [Int] -> Int
prodOfEvens l = foldr (prodOfEvensAux) 1 l

powersOf2 :: [Int]
powersOf2 = iterate (2 *) 1

scalarProduct :: [Float] -> [Float] -> Float
scalarProduct l1 l2 = foldr (+) 0 (zipWith (*) l1 l2)