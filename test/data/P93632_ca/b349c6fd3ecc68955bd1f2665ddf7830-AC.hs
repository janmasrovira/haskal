-- 1 funcio k diu si dos llistes son iguals
eql :: [Int] -> [Int] -> Bool
eql xs ys
	| (length xs) == (length ys) = and (zipWith (==) xs ys)
	| otherwise = False

--2 funcio k calculi el producte duna llista
prod :: [Int] -> Int
prod xs = foldl (*) 1 xs

--3 multiplica els nombres parells duna llista 
prodOfEvens :: [Int] -> Int
prodOfEvens xs = foldl (*) 1 (filter even xs)

--4 genera les potencies de 2
powersOf2 :: [Int]
powersOf2 = iterate (*2) 1

--5 calcula el producte escalar de dos llistes
scalarProduct :: [Float] -> [Float] -> Float
scalarProduct xs ys = foldl (+) 0 (zipWith (*) xs ys)
