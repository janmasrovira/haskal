eql::[Int]->[Int]->Bool

eql l g
	| length l /= length g = False
	| otherwise = (foldr(+) 0 (zipWith (-) l g)) == 0

prod::[Int]->Int

prod l = foldr (*) 1 l

prodOfEvens::[Int]->Int

prodOfEvens l = prod (filter (even) l)

powersOf2::[Int]

powersOf2 = iterate (*2) 1

scalarProduct::[Float]->[Float]->Float

scalarProduct l g = foldr (+) 0.0 (zipWith (*) l g) 
