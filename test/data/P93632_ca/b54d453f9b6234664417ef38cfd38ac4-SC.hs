eql :: [Int] -> [Int] -> Bool
eql [] [] = True
eql _ [] = False
eql [] _ = False
eql (x:xs) (y:ys) = (x == y) && eql xs ys

prod :: [Int] -> Int
prod [] = 0
prod l = foldr (*) 1 l