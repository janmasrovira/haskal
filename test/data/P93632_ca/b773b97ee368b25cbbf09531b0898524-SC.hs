eql :: [Int] -> [Int] -> Bool
prod :: [Int] -> Int
prodOfEvens :: [Int] -> Int
powersOf2 :: [Int]
auxPower :: Int -> [Int]
scalarProduct :: [Float] -> [Float] -> Float

eql [] [] = True
eql (x:xs) (y:ys) | x == y = eql xs ys
		  | otherwise = False

prod [] = 1
prod (x:xs) = x * prod xs

prodOfEvens [] = 1
prodOfEvens (x:xs) | x `mod` 2 == 0 = x * (prodOfEvens xs)
		   | otherwise = prodOfEvens xs
		   
powersOf2 = (auxPower 1)

auxPower n = (n:(auxPower (n*2)))

scalarProduct [] [] = 0
scalarProduct (x:xs) (y:ys) = x*y + scalarProduct xs ys