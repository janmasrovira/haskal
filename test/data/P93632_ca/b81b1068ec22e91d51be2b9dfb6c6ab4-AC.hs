
eql::[Int]->[Int]->Bool
eql xs ys = (length xs == length ys) && (and $ zipWith (==) xs ys)

prod::[Int]->Int
prod [] = 1
prod xs = foldl1 (*) xs

prodOfEvens::[Int]->Int
prodOfEvens xs = prod (filter even xs)

powersOf2::[Int]
powersOf2 = iterate (*2) 1

scalarProduct::[Float]->[Float]->Float
scalarProduct xs ys = sum $ zipWith (*) xs ys

