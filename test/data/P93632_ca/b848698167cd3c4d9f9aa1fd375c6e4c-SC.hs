l::[Int]
l2::[Int]
l = [1,2,3,4,5]
l2 = [1,2,3,4,5]

eql::[Int]->[Int]->Bool
eql x y = and (zipWith (==) x y) && (length x == length y)

prod::[Int] -> Int
prod x = foldl (*) 1 x

prodOfEvens::[Int] -> Int
prodOfEvens l = foldl (*) 1 (filter even l)

powersOf2::[Int]
powersOf2 = map (^2) [n | n<- [1..], even n]

scalarProduct:: [Float] -> [Float] -> Float
scalarProduct x y = foldl (+) 1 (zipWith (*) x y)