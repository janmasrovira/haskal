
-- Indica si dos llistes d'enters son iguals
eql :: [Int] -> [Int] -> Bool
eql [] []   = True
eql [] [a]  = False
eql [a] []  = False
eql [a] [b] = [a] == [b]
