--funcio que compara dos llistes:
eql :: [Int] -> [Int] -> Bool
--eql as bs = length as == length bs && all (== True) (zipWith (==) as bs) 
--eql as bs = length as == length bs && all id (zipWith (==) as bs) 
--eql as bs = length as == length bs && and (zipWith (==) as bs)
eql as bs = length as == length bs && foldl (&&) True (zipWith (==) as bs)

--funcio producte dels elements d'una llista:
prod :: [Int] -> Int
prod xs = foldl (*) 1 xs

--funcio producte dels nombres parells d'una llista:
prodOfEvens :: [Int] -> Int
prodOfEvens xs = prod (filter even xs)

--funcio que genera la llista de totes les potencies de 2
powersOf2 :: [Int]
powersOf2 = iterate (*2) 1
 
--funcio del producte escalar de dos llistes:
scalarProduct :: [Float] -> [Float] -> Float
scalarProduct as bs = sum (zipWith (*) as bs)