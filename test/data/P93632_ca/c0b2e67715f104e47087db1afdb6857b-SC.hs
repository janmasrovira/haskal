eql :: [Int] -> [Int] -> Bool
eql [] [] = True
eql x [] = False
eql [] x = False
eql (x:xs) (y:ys)
	| x == y = eql xs ys
	| otherwise = False

prod :: [Int] -> Int
prod = foldr (*) 1

prodOfEvens :: [Int] -> Int
prodOfEvens x = prod ( map seven x )
	where
		seven y
			| even y = y
			| otherwise = 1

powersOf2 :: [Int]
powersOf2 = iterate (* 2) 2

scalarProduct :: [Float] -> [Float] -> Float
scalarProduct xs ys = foldr (+) 0 (map (work) (zip xs ys))
	where
		work ( a, b ) = a * b
