eql :: [Int] -> [Int] -> Bool
eql x y = (length x == length y) && all (==True) (zipWith (==) x y)
  
prod :: [Int] -> Int
prod x = foldr (*) 1 x 

prodOfEvens :: [Int] -> Int
prodOfEvens x = prod (filter even x)


powersOf2 :: [Int] 
powersOf2 = map (2^) [0..]
  
scalarProduct :: [Float] -> [Float] -> Float 
scalarProduct x y = sum (zipWith (*) x y)