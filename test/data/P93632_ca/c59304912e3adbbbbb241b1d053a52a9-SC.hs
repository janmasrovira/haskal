-- equals

eql:: [Int] -> [Int] -> Bool

eql xs ys  
  |length xs /= length ys = False
  |otherwise = and (zipWith (==) xs ys)
  
  
--prod

prod::[Int] -> Int

prod [] = 1
prod (x:xs) = foldr (*) x xs

-- prodOfEvens

prodOfEvens::[Int]->Int

prodOfEvens n = product (takeWhile (even) n)
--powersOf2

powersOf2::[Int]

powersOf2 = (iterate (2*) 1)

--scalarProduct

scalarProduct::[Float]->[Float]->Float

scalarProduct x y = sum (zipWith (*) x y)