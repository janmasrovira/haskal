eql::[Int]->[Int]-> Bool
eql [] [] = True
eql (x:xs) [] = False
eql [] (x:xs) = False
eql l1@(x:xs) l2@(y:ys) = ((and (zipWith (==) l1 l2)) == True) && (length l1 == length l2)


prod::[Int] -> Int
prod (x:xs) = foldr (*) 1 (x:xs)


prodOfEvens::[Int]->Int
prodOfEvens (x:xs) = foldr (*) 1 (filter (even) (x:xs))


powersOf2::[Int]

powersOf2 = [ 2^x | x <- [0..] ]


scalarProduct::[Float] -> [Float] -> Float

scalarProduct l1@(a:as) l2@(b:bs) = sum (zipWith (*) l1 l2)
