eql::[Int]->[Int]->Bool
eql x y
  |length x /= length y = False
  |otherwise = all (==True) (zipWith (==) x y)

prod::[Int]->Int
prod l = foldl (*) 1 l

prodOfEvens::[Int]->Int
prodOfEvens l = prod (filter (even) l)

powersOf2::[Int]
powersOf2 = [2^x | x<-[0..]]

scalarProduct::[Float]->[Float]->Float
scalarProduct x y = sum (zipWith (*) x y)