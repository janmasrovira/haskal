eql::[Int]->[Int]->Bool
eql (a:as) (b:bs)
  | length (a:as) /= length (b:bs) = False
  | otherwise = all (\x->x) (zipWith (==) (a:as) (b:bs))