eql :: [Int] -> [Int] -> Bool
eql [] [] = True
eql _ [] = False
eql [] _ = False
eql (x:xs) (y:ys) = (x == y) && eql xs ys

prod :: [Int] -> Int
prod l = foldl (*) 1 l

prodOfEvens :: [Int] -> Int
prodOfEvens l = prod (filter (\x -> (x `mod` 2) == 0) l)