eql:: [Int] -> [Int] -> Bool
eql x y =
	all (== True) (zipWith (==) x y)


prod:: [Int] -> Int
prod x =
	foldr (*) 1 x

prodOfEvens:: [Int] -> Int
prodOfEvens x =
	foldr (*) 1 (takewhile (even) x)