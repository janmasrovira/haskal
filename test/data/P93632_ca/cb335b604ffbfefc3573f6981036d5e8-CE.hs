eql :: [Int] -> [Int] -> Bool
eql lx ly = eqla (-) lx ly

eqla :: (Int -> Int -> Int) -> [Int] -> [Int] -> Bool
eqla _ [] [] = True
eqla _ [] _ = False
eqla _ _ [] = False
eqla f (x:lx) (y:ly)
  | f x y == 0 = True && eqla f lx ly
  | otherwise = False
  
prod :: [Int] -> Int
prod lx = proda (*) lx

proda :: (Int -> Int) -> [Int] -> Int
proda _ [x] = x
proda f (x:lx) = f x lx

