eql :: [Int] -> [Int] -> Bool
eql [] [] = True
eql _ [] = False
eql [] _ = False
eql (x:xs) (y:ys) = (x == y) && eql xs ys

prod :: [Int] -> Int
prod l = foldl (*) 1 l

prodOfEvens :: [Int] -> Int
prodOfEvens l = prod (filter (\x -> (x `mod` 2) == 0) l)

powersOf2 :: [Int]
powersOf2 = iterate (*2) 1

scalarProduct :: [Float] -> [Float] -> Float
scalarProduct l m = foldl (+) 0 (zipWith (*) l m)