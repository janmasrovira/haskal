eql :: [Int] -> [Int] -> Bool
eql xs ys	
    | length xs /= length ys		= False
    | otherwise				= all (==0) (zipWith (-) xs ys)
    
prod :: [Int] -> Int
prod xs		= foldl (*) 1 xs

prodOfEvens :: [Int] -> Int
prodOfEvens (x:xs)	= foldl (*) 1 (filter (even) (x:xs))

powersOf2 :: [Int]
powersOf2 = iterate (2*) 1

scalarProduct :: [Float] -> [Float] -> Float
scalarProduct (x:xs) (y:ys) 	= foldr (+) 0 (zipWith (*) (x:xs) (y:ys))