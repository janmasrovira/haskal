eql [] [] = True
eql _ [] = False
eql [] _ = False
eql (x:xs) (y:ys) 
    | x == y = eql xs ys
    | otherwise = False

prod [] = 1
prod (x:xs) = x*(prod xs)

prodOfEvens [] = 1
prodOfEvens (x:xs) 
    | even x = x*(prodOfEvens xs)
    | otherwise = prodOfEvens xs

powerOf2 = iterate (*2) 1

scalarProduct [] [] = 0
scalarProduct (x:xs) (y:ys) = (x*y)+(scalarProduct xs ys)
