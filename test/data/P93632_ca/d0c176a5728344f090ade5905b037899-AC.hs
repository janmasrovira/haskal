eql :: [Int] -> [Int] -> Bool
eql l1 l2 
	| not (length l1 == length l2) = False 
	| otherwise                    = 0 == (length (filter (==False) ((zipWith (==) l1 l2))))

prod :: [Int] -> Int
prod l  = foldr (*) 1 l

prodOfEvens :: [Int] -> Int
prodOfEvens l = (product (filter (even) l))

powersOf2 :: [Int]
powersOf2 = iterate (fpowersOf2) 1 

fpowersOf2 :: Int -> Int
fpowersOf2 e = e*2

scalarProduct :: [Float] -> [Float] -> Float
scalarProduct l1 l2 = (foldr (+) 0 (zipWith (*) l1 l2))
