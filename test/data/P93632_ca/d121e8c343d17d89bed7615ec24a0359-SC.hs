eql :: [Int]->[Int]->Bool
--eql = map (==) [l1] [l2]
eql [] [] = True
eql [] [_] = False
eql [_] [] = False
eql (l1:l11) (l2:l22)
  |l1 /= l2 = False
  |otherwise = eql l11 l22
