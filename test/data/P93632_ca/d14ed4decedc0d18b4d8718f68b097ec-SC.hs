--funcio eql::[Int] -> [Int] -> Bool
--indica si les llistes son iguals
eql::[Int] -> [Int] -> Bool
eql [][] = True
eql _ [] = False
eql [] _ = False
eql (x:xs) (y:ys) 
    | x /= y = False
    | otherwise = eql xs ys 
    
-- producte dels elements de la llista passada
prod::[Int] -> Int
prod l = foldl (*) 1 l  

--prodofevents multiplica els nombres parells de una llista
prodOfEvents::[Int] -> Int
prodOfEvents l = foldl (*) 1 list
    where list = filter even l
          
--powersOf2 genera la llista de les potències de dos
powersOf2::[Integer]
powersOf2 = iterate (\x -> x*2) 2 

--scalar product que calcula el prod escalar de dues llistes 
--de reals de la mateixa mida
scalarProduct::[Float] -> [Float] -> Float
scalarProduct l l2 = sum(zipWith (*) l l2)
