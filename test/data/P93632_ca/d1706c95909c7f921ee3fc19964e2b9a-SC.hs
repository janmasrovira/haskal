eql::[Int]->[Int]->Bool
eql [] [] = True
eql [] [b] = False
eql [a] [] = False
eql (a:as) (b:bs)
  | length (a:as) /= length (b:bs) = False
  | otherwise = all (\x->x) (zipWith (==) (a:as) (b:bs))

prod::[Int]->Int
prod [] = 1
prod (x:xs) = foldr (*) 1 (x:xs)

prodOfEvens::[Int]->Int
prodOfEvens [] = 1
prodOfEvens (x:xs) = prod (filter (even) (x:xs))

