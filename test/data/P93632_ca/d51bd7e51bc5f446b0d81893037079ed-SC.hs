-- Feu una funció eql :: [Int] -> [Int] -> Bool que indiqui si dues llistes d’enters són iguals.
eql :: [Int] -> [Int] -> Bool
eql [] [] = True
eql [] l = False
eql l [] = False
eql (cap1:cua1) (cap2:cua2)
  | cap1 == cap2 = eql cua1 cua2
  | otherwise = False
-- Feu una funció prod :: [Int] -> Int que calculi el producte dels elements d’una llista d’enters.
prod :: [Int] -> Int
prod [] = 0
prod [x] = x
prod (cap:cua) = cap * prod cua
-- Feu una funció prodOfEvens :: [Int] -> Int que multiplica tots el nombres parells d’una llista d’enters.
prodOfEvens :: [Int] -> Int
prodOfEvens [x]
  | x `mod` 2 == 0 = x
  | otherwise = 1
prodOfEvens (cap:cua)
  | cap `mod` 2 == 0 = cap * prodOfEvens cua
  | otherwise = prodOfEvens cua

-- Feu una funció powersOf2 :: [Int] que generi la llista de totes les potències de 2.
powersOf2 :: [Int]
powersOf2 = [2^n | n <- [0..]]

-- Feu una funció scalarProduct :: [Float] -> [Float] -> Float que calculi el producte escalar de dues llistes de reals de la mateixa mida
scalarProduct :: [Float] -> [Float] -> Float
scalarProduct [] [] = 0
scalarProduct [x1] [x2] = x1*x2
scalarProduct (cap1:cua1) (cap2:cua2) = (cap1*cap2) + scalarProduct cua1 cua2