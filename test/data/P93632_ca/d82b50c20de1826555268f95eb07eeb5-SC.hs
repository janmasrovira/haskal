
--eql
eql :: [Int] -> [Int] -> Bool
eql xs ys = foldl f True (zip xs ys)
  where
      f b (x,y) = b&&(x==y)


--prod
prod :: [Int] -> Int
prod xs = foldl (*) 1 xs


--prodOfEvens
prodOfEvens :: [Int] -> Int
prodOfEvens xs = prod (filter even xs)


--powersOf2
powersOf2 :: [Int]
powersOf2 = iterate (*2) 1



--scalarProduct
--scalarProduct :: [Float] -> [Float] -> Float 
