eql :: [Int] -> [Int] -> Bool
eql l1 l2
	| length l1 == length l2 = all (\x -> x == 0) (zipWith (-) l1 l2)
	| otherwise = False

prod :: [Int] -> Int
prod l = foldr (*) 1 l

prodOfEvens :: [Int] -> Int
prodOfEvens l = foldr condMult 1 l
	where
		condMult x y
			| odd x = y
			| otherwise = x*y

powersOf2 :: [Int]
powersOf2 = iterate (\x -> 2*x) 1

scalarProduct :: [Float] -> [Float] -> Float
scalarProduct l1 l2 = foldr (+) 0 (zipWith (*) l1 l2)