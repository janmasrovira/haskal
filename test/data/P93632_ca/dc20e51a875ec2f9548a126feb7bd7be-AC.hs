eql l1 l2 
    |(length l1) /= (length l2) = False
    |otherwise = (and ( zipWith (==) l1 l2))

prod l = foldl (*) 1 l

aux acc x
    |mod x 2 == 0 = acc*x
    |otherwise = acc
    
prodOfEvens l = foldl aux 1 l

powersOf2 = iterate (*2) 1

scalarProduct:: [Float] ->[Float] -> Float
scalarProduct l1 l2=  sum  (zipWith (*) l1 l2)
