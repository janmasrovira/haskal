eql xs ys 
    | (length xs) /= (length ys) = False
    | otherwise = and $ zipWith (==) xs ys  
prod = foldl (*) 1 
prodOfEvens = prod.(filter even)
powersOf2 = iterate (2*) 1
scalarProduct xs ys = foldl (+) 0 $ zipWith (*) xs ys