prod :: [Int] -> Int
prod xs =  (foldl (*) 1 xs)

prodOfEvens :: [Int] -> Int
prodOfEvens xs = (foldl (*) 1 (filter even xs))
