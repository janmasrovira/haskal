{-
:t all
all :: (a -> Bool) -> [a] -> Bool
:t any
any :: (a -> Bool) -> [a] -> Bool
:t filter
filter :: (a -> Bool) -> [a] -> [a]
:t dropWhile
dropWhile :: (a -> Bool) -> [a] -> [a]
:t takeWhile
takeWhile :: (a -> Bool) -> [a] -> [a]
:t foldl
foldl :: (b -> a -> b) -> b -> [a] -> b
:t iterate
iterate :: (a -> a) -> a -> [a]
:t foldr
foldr :: (a -> b -> b) -> b -> [a] -> b
:t zipWith
zipWith :: (a -> b -> c) -> [a] -> [b] -> [c]
-}

eql :: [Int] -> [Int] -> Bool 
-- indica si dues llistes d’enters són iguals.
eql l1 l2 
	| length l1 == length l2 = all (==True) (zipWith (==) l1 l2)
	| True = False

prod :: [Int] -> Int 
-- calcula el producte dels elements d’una llista d’enters.
prod (h:t) = foldl (*) h t

fact :: Int -> Int
-- calcula el factorial
fact n = foldl (*) 1 [1..n]

prodOfEvens :: [Int] -> Int 
-- multiplica tots el nombres parells d’una llista d’enters.
prodOfEvens l = prod (filter even [2,10,5])

powersOf2 :: [Int] 
-- genera la llista de totes les potències de 2.
powersOf2 = iterate (*2) 1

scalarProduct :: [Float] -> [Float] -> Float
-- calcula el producte escalar de dues llistes de reals de la mateixa mida.
scalarProduct l1 l2 = foldl (+) 0 (zipWith (*) l1 l2)