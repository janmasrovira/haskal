
eql :: [Int] -> [Int] -> Bool
eql (x:xs) (y:ys) = all (== True) (zipWith (==) (x:xs) (y:ys))

prod :: [Int] -> Int
prod [] = 1
prod (x:xs) = foldr (*) x xs

prodOfEvens :: [Int] -> Int
prodOfEvens (x:xs) = prod (filter (even) xs)

powersOf2 :: [Int]
powersOf2 = iterate (*2) 1

scalarProduct:: [Float] -> [Float] -> Float
scalarProduct (x:xs) (y:ys) = foldr (+) 0 (zipWith (*) (x:xs) (y: ys))