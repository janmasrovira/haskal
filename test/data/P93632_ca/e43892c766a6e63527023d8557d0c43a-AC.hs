eql :: [Int] -> [Int] -> Bool
eql xs ys = and $ (length xs == length ys) : zipWith (==) xs ys

prod :: [Int] -> Int
prod = foldl (*) 1

prodOfEvens :: [Int] -> Int
prodOfEvens = prod.(filter even)

powersOf2 :: [Int]
powersOf2 = iterate (*2) 1

scalarProduct :: [Float] -> [Float] -> Float
scalarProduct l = sum.zipWith (*) l