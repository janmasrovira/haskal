-- eql::[Int] -> [Int] -> Bool

eql [] [] = True
eql [] (y:ys) = False
eql (x:xs) [] = False
eql (x:xs) (y:ys)
	|all (==x) [y] = eql xs ys
	|otherwise = False 

-- prod::[Int] -> Int

prod [] = 0
prod (x:xs) = foldr (*) x xs

-- prodOfEvens::[Int] -> Int

prodOfEvens [] = 0;
prodOfEvens (x:xs) = foldr (*) 1 (filter (even) (x:xs))

-- powersOf2::[Int]

powersOf2 = iterate (*2) 1

--scalarProduct::[Float] -> [Float] -> Float

scalarProduct [] [] = 0.0
scalarProduct (x:xs) (y:ys) = sum (zipWith (*) (x:xs) (y:ys))



