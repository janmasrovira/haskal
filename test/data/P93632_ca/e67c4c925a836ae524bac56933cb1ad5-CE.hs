eql xs ys = (length xs) == (length ys) && (and $ zipWith (==) xs ys)

prod::[Int]->Int
prod xs 
  | length xs == 0 = 0
  | otherwise = foldl (*) 1 xs
prodOfEvens::::[Int]->Int
prodOfEvens xs 
  | length (filter even xs) == 0 = 0
  | otherwise = foldl (*) 1 (filter even xs)
  
powersOf2 = iterate (2*) 1

scalarProduct xs ys 
  | length xs /= length ys = 0
  | otherwise = sum (zipWith (*) xs ys)