--Problema 1
eql :: [Int] -> [Int] -> Bool
eql xs ys = equalPairs && sameLength
	where
		equalPairs = and $ zipWith (==) xs ys
		sameLength =  length xs == length ys


--Problema 2
prod :: [Int] -> Int
prod xs = foldr (*) 1 xs 

--Problema 3
prodOfEvens :: [Int] -> Int
prodOfEvens xs = prod $ filter even xs

--Problema 4
powersOf2 :: [Int]
powersOf2 = iterate (* 2) 1

--Problema 5
scalarProduct :: [Float] -> [Float] -> Float
scalarProduct xs ys = sum multiplications
	where multiplications = zipWith (*) xs ys