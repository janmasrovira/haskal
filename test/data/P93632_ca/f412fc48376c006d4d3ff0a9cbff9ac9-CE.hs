eql [] [] = True
eql [x:xs] [] = False
eql [] [y:ys] = False
eql [x:xs] [y:ys] = x == y and eql [xs] [ys]
