eql::[Int]->[Int]->Bool
eql as bs
  | length as /= length bs = False
  | otherwise = and (zipWith (==) as bs)

prod::[Int]->Int
prod [] = 1
prod (x:xs) = foldr (*) 1 (x:xs)

prodOfEvens::[Int]->Int
prodOfEvens [] = 1
prodOfEvens (x:xs) = product (filter (even) (x:xs))

powersOf2::[Int]
powersOf2 = iterate (*2) 1

scalarProduct::[Float]->[Float]->Float
scalarProduct [] [] = 1
scalarProduct (a:as) (b:bs) = sum (zipWith (*) (a:as) (b:bs))