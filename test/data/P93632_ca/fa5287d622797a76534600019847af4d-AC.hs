eql :: [Int]->[Int]->Bool
eql = (==)

prod::[Int]->Int
prod = foldl(*) 1


prodOfEvens::[Int]->Int
prodOfEvens xs = foldl(\acc x -> if mod x 2 == 0 then acc * x else acc) 1 xs

powersOf2 :: [Int]
powersOf2 = [2^i | i <- [0..]]

scalarProduct::[Float]->[Float]->Float
scalarProduct xs ys = sum[x*y | (x,y) <- zip xs ys]
