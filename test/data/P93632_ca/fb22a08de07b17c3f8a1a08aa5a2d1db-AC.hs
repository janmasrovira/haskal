samesize::[Int]->[Int]->Bool

samesize [] [] = True
samesize [] x = False
samesize x [] = False
samesize (x:xs) (y:ys) = (samesize xs ys)

eql::[Int]->[Int]->Bool

eql xs ys = all id (zipWith (==) xs ys) && (samesize xs ys)

prod::[Int]->Int

prod = foldl (*) 1

prodOfEvens::[Int]->Int

prodOfEvens xs = prod (filter even xs) 

powersOf2::[Int]

powersOf2 = iterate (*2) 1

scalarProduct::[Float]->[Float]->Float

scalarProduct xs ys = sum(zipWith (*) xs ys)
