--indiqui si dues llistes denters son iguals
eql :: [Int]->[Int]->Bool
eql xs ys
	| length xs /= length ys = False
	| otherwise = and.zipWith(==) xs $ ys
	
-- calculi el producte dels elements duna llista
-- denters
prod :: [Int]->Int
prod xs = foldr (*) 1 xs

-- multiplica tots els nombres parells d'una llista d'enters
--prodOfEvens :: [Int]->Int
prodOfEvens xs = prod(filter even xs)

-- genera la llista de totes les potencies de 2
powersOf2 :: [Int]
powersOf2 = map (2^) [0..]

-- calcula el producte escalar de dues llistes reals de la mateixa mida
scalarProduct :: [Float]->[Float]->Float
scalarProduct xs ys = sum.zipWith(*) xs $ys
