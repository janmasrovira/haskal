eql :: [Int] -> [Int] -> Bool
eql l m 
	|length l /= length m	= False
	|otherwise	= all (==0) (zipWith (-) l m)

prod :: [Int] -> Int
prod [] = 0
prod (x:xs) = foldr (*) x xs

prodOfEvens :: [Int] -> Int
prodOfEvens [] = 0
prodOfEvens l = foldr (*) (head onlyEven) (tail onlyEven)
	where
		onlyEven = (filter (even) l)