--Feu una funció eql :: [Int] -> [Int] -> Bool que indiqui si dues llistes d’enters són iguals.

eql :: [Int] -> [Int] -> Bool
eql l l2 
	| not ((length l) == (length l2))	= False
	| otherwise 						= all (== True) (zipWith (==) l l2) 

--Feu una funció prod :: [Int] -> Int que calculi el producte dels elements d’una llista d’enters.

prod :: [Int] -> Int
prod l = foldr (*) 1 l

--Feu una funció prodOfEvens :: [Int] -> Int que multiplica tots el nombres parells d’una llista d’enters.

prodE :: Int -> Int -> Int
prodE e acc 
	| (mod e 2) == 0	= acc * e
	| otherwise			= acc 

prodOfEvens :: [Int] -> Int
prodOfEvens l = foldr prodE 1 l


--Feu una funció powersOf2 :: [Int] que generi la llista de totes les potències de 2.

powersOf2 :: [Int]
powersOf2 = iterate (2*) 1

 
--Feu una funció scalarProduct :: [Float] -> [Float] -> Float que calculi el producte escalar de dues llistes de reals de la mateixa mida.

scalarProduct :: [Float] -> [Float] -> Float
scalarProduct l l2 = foldr (+) 0 (zipWith (*) l l2)

