eql :: [Int] -> [Int] -> Bool
eql [] [] = True
eql (x:xs) (y:ys) = x == y && eql xs ys
eql _ _ = False

prod :: [Int] -> Int
prod [] = 1
prod (x:xs) = x*prod xs

prodOfEvens :: [Int] -> Int
prodOfEvens = prod . filter even

powersOf2 :: [Int] 
powersOf2 = iterate (*2) 1

scalarProduct :: [Float] -> [Float] -> Float
scalarProduct a = (sum . zipWith (*) a)