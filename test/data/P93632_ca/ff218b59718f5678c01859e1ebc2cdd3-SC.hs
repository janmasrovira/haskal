eql :: [Int] -> [Int] -> Bool
eql [] [] = False
eql [x] [y] = if x == y then True
	else False
eql (x) [] = False
eql [] (x) = False
eql (x:xs) (y:ys) = if x == y then eql (xs) (ys) 
	else False

prod :: [Int] -> Int
prod [] = 1
prod [x] = x
prod (x:xs) = x * (prod xs)

prodOfEvens :: [Int] -> Int
prodOfEvens [] = 0
prodOfEvens [0] = 0
prodOfEvens [x] = if mod x 2 == 0 then x
	else 1
prodOfEvens (x:xs) = if mod x 2 == 0 then x * prodOfEvens xs
	else prodOfEvens xs

powersOf2 :: [Int]
powersOf2 = iterate (*2) 1

scalarProduct :: [Float] -> [Float] -> Float
scalarProduct [] [] = 1
scalarProduct [x] [y] = x*y
scalarProduct (x:xs) (y:ys) = x*y + (scalarProduct xs ys)