module Tests.Data.Parsing where

import qualified Data.List (sort)
import Data.Maybe
import Control.Monad

type Number = Double
newtype Pos = Pos (Number, Number)
data Something = TheseInts [Int] | ThatNumber Number deriving (Show, Eq)

-- guards and ifs
guards x y
  | x == y = 0 | x < y = 1
  | let xy = x*y, otherwise = if even x then 2
                              else xy


layout =
  let a = 0; b = 1 -- single line with multiple declarations
      c = 2
         in let { a' = a -- explicit serparators and delimiters
 ; b = 2; r = 3} in a
  where x = 1    -- layout using indent
        y = x

-- type signature
typesig :: (Ord a) => a -> a -> Maybe (a,a)
typesig a b = Just (a,b)
  where x = ([1,2,3] :: [Int]) ++ [1]

myLength :: [Int] -> Int
-- myLength [] = 0
-- myLength (x:xs) = 1 + (myLength xs) 
myLength ls = 
    case ls of
        [] -> 0
        (hd:tl) -> 1 + (myLength tl)


-- pattern matching
patm (a:b:_:r@((_,x):_)) = -- pattern matchin via arguments
  case x of -- pattern matching via case
  ("string", [123], False) -> a
  (_, l, _) -- -> b  -- guards in cases
    | null l -> b    | otherwise -> a
  _ -> a
patm (x:xs) = patm xs

-- multiline expressions, operators and sections
ops = (((((((1+))))))) . (+1.00001) . (1.32 -) $ 1 +
      2*3 - 4 + (((((((12))))))) `max` 45

-- list comprehension
listc = [ let x = 2*b in b | a <- [1..100], let y = a + 1,
          even a, b <- [a, 2*a..10*a]]

-- do notation
main = do
  f <- readFile "file1"
  when (not $ null f) $ putStrLn "not null"
  let f2 = readFile   -- let statement
           "file2"
  let x = "res"  -- traditional let..in also allowed
    in print x
  putStr f
  fmap lines f2 >>= print
