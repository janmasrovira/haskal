module Tests.Data.Queries where

selfRecur 0 = 1
selfRecur 1 = 1
selfRecur x = 1 + f
  where f = if True then 1 else selfRecur (x - 1)

nonDirectSelfRecur 0 = 1
nonDirectSelfRecur 2 = f 1
  where f x
          | x == 1 = 3
          | otherwise = nonDirectSelfRecur 0
nonDirectSelfRecur x = x

recur 0 = 1
recur x
  | x == 1 = 12
  | otherwise = selfRecur x

nonRecur x = 3*(f 1)
  where f = x
        infinite = infinite


