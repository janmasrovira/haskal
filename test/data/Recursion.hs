module Tests.Data.Recursion where

import Data.List
import qualified Data.Function as F

fac :: Int -> Int
fac 0 = g
  where
    g :: Int
    g = 1
fac n = let r = fac (n - 1)
        in n * r
