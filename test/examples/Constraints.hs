module Tests.Data.Constraints where


import Data.List

myLength = 1
  where r = length + r

listComp x = 1
  where b = [1 | y <- []]
