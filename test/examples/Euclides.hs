module Tests.Data.Euclides where

--import Prelude hiding (gcd)

f a b = if a <= 0 then undefined
        else if  b <= 0 then undefined
             else if a == b then a
        else if a < b then f a (b-a)
             else f (a-b) b


g a b
  | a <= 0 = error "a must be positive"
  | b <= 0 = error "b must be positive"
  | a == b = a
  | a > b = g (a-b) b
  | otherwise = g a (b-a)
