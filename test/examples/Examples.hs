module Tests.Data.Examples where

fac n
  | n <= 2 = n
  | otherwise = n * fac (n-1)

lengthk [] = 5
lengthk (_:xs) = 3 + lengthk xs 

mymaximum [x] = x
mymaximum (x:xs) = max x (mymaximum xs)

listZero [] = 0
listZero (x:xs) = x * listZero xs 


producte [] = 1
producte (x:xs) = x * producte xs
