module Tests.Data.FunctionCalls where

fib 0 = 0
fib 1 = 1
fib n = if n > 0 then fib (n-1) else undefined
