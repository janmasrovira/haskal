module Tests.Data.Halting where

fib n =
  if n < 2 then n
  else fib (n-1) + fib (n - 2)



sumN n
  | n <= 0 = 0
  | 0<=0 = n + sumN (n-1)
