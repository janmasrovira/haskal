module Tests.Data.Inlining where

x, l :: Int


l = 12

x = x + y
  where
    y,k,q ::Int
    q = 122
      where sdf x = 1 + x
    y = k
    k = z
          where z  = 12

fib :: Int -> Int
fib 0 = 0
fib 1 = 1
fib n = fib a + fib b
  where a = n - 1
        b = a - 1
