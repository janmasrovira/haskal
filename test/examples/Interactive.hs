module Tests.Data.Interactive where

f1 n
  | n < 2 = n
  | otherwise = n * f1 (n-1)

f2 [] = 5
f2 (_:xs) = 3 + f2 xs 

f3 [] = 0
f3 (x:xs) = x * f3 xs 


mylength [] = 0
mylength (_:xs) = 1 + mylength xs

mylengthK [] = 5
mylengthK (_:xs) = 3 + mylengthK xs

flatten [] = []
flatten (x:xs) = x ++ flatten xs

fib n
  | n < 2 = n
  | otherwise = fib (n-1) + fib (n-2) 
