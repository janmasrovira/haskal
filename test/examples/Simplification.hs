module Data.Tests.Simplification where

flatten a = reverse (flatten' [] a)
  where flatten' acc [] = [] ++ acc
        flatten' acc (a : as) = flatten' (reverse a ++ acc) as

