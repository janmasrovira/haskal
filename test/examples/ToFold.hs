-- Examples for the fold transformation

module Tests.Data.ToFold where


-- (+a). (*k) . length
f1 [] = a
  where a = 3
f1 (_:b) = k + f1 b
  where k = 51

-- sum
f2 [] = 3
f2 (a:b) =  (3*a) `max` (f2 b)
