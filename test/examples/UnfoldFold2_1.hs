module Tests.Data.UnfoldFold2_1 where


f _ [] = []
f g (a:b) =
  if g a
  then [a] ++ f g b
  else f g b


f2 _ [] = []
f2 g (a:b)
  | g a = [a] ++ f2 g b
  | otherwise = f2 g b

f3 _ [] = []
f3 g (a:b) = let
  aux
    | g a = [a]
    | otherwise = []
  in aux ++ f3 g b

