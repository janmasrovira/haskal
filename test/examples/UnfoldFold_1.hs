module Tests.Data.UnfoldFold_1 where

fac :: Integer -> Integer
fac 0 = 1
fac n = n * fac (n - 1)

rev :: [a] -> [a]
rev [] = []
rev l = rev (tail l) ++ [head l] 

rev2 :: [a] -> [a]
rev2 [] = []
rev2 l = rev2 (drop 2 l) ++ take 2 l


z [] = 0
z (x:xs) = 1 `max` z xs

z acc [] = 0 `max` acc
z acc (x : xs) = z (1 `max` acc) xs

b = [0,-1]


idl :: [Integer] -> [Integer]
idl [] = b
idl l = y ++ idl l'
  where y = take 3 l
        l' = drop 2 l

primeDivisors n
  | primes == [] = [n]
  | otherwise = primes ++ primeDivisors (reduce n (head primes))
  where primes
          = take 1
              [y | y <- [2 .. floor (sqrt (fromIntegral n))], n `mod` y == 0]



sumUpToN :: Integer -> Integer
sumUpToN 0 = 0
sumUpToN n = n + sumUpToN (n - 1)



fac' :: Integer -> Integer
fac' = fac'' 1
  where fac'' y 0 = y * 1
        fac'' y n = fac'' (y * n) (n - 1)

maxx :: [Integer] -> Integer
maxx [x] = x
maxx l = maximum (take 1 l) `max` maxx (drop 1 l)

maxx' :: [Integer] -> Integer
maxx' = maxx'' 0
  where maxx'' y [x] = y `max` x
        maxx'' y l = maxx'' (y `max` maximum (take 1 l)) (drop 1 l)

idl' :: [a] -> [a]
idl' = idl'' []
  where idl'' y [] = y ++ []
        idl'' y l = idl'' (y ++ [head l]) (tail l)

hl l = (head l, last l)
        
l = [1..5*10^(3::Integer)] :: [Integer]
--x = let r = idl l in (head r, last r)
--y = let r = idl' l in (head r, last r)



f [] = []
f (a:b) = (let c = b in f c) ++ x:[a]
  where x = 1
        
