module Tests.Data.UnFoldFold_2 where

suml [x] = x
suml (a:b) = a + suml b


rev [] = []
rev (a:b) = rev b  ++ [a]


useless = undefined
  where
    idl [] = []
    idl (a:b) = [a] ++ idl b

sumEvens [] = 0
sumEvens (a:b) = if even a
                 then 1 + a + sumEvens b + a
                 else sumEvens b



flatten_ l
   | l == [] = []
   | otherwise = x ++ flatten_ xs
  where x : xs = l

flatten (x : xs) = x ++ (flatten xs)
flatten _ = []
