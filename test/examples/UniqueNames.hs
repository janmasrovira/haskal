module Tests.Data.UniqueNames where

f x
  | null x = let x = y
                   where y = []
                 y = (\x -> let y = x in y) x
                 z = [(\x->x), (\x->x)]
             in x
  | otherwise = case x of
    (x:y:z) -> [y]
    (x:y) -> [x]
      where x = []
  where x = []
f x = []


l = (\x -> x)
  $  (\x y->
    (\x -> x + y))
